<?php

include_once "sessionCheck.php";

$sPageTopTitle = "Combi Billing Master";
$iID = $oSessionManager->iUserID;
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
 	<?php include_once 'medixcelHeaderB3.php'; ?>
 	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="js/moment.js"></script>
    <script src='js/pnotify.custom.js'></script>
</head>
<body class="stickyMedixcelFooter flat-blue">
<?php include_once 'medixcelNavbarB3.php'; ?>

	<div class="container-fluid classContainerBody">
        <div class="row mr-3">
            <div class="col-lg-12">
                <a href="CombiBillingMasterDetail.php" class="btn btn-dark pull-right text-white" name="idAddCombiBilling" id="idAddCombiBilling">Combi Billing Master</a>
            </div>
        </div>
        <br>
	</div>
</body>
</html>
