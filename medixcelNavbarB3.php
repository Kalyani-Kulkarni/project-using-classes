<style type="text/css">
	.navbar-brand{
	    margin-right: 0rem;
	}
	.navbar{
		padding: 0rem;
		border-radius: 0;
	}
</style>

<?php
$sPage = isset($sPageTopTitle) ? $sPageTopTitle : ORG_NAME;
?>

<div class="app-container">
 	<div class="row content-container">
		<nav class="navbar navbar-custom navbar-fixed-top">
			<div class="container-fluid">
				<div class="navbar-header classBaseTopHeader" style="display: inline-block; width: 95%">
				  	<a href="#" class="navbar-brand">
				  		<span class="title"><?php echo(CONFIG_BRAND_NAME) ?></span>
				  	</a>
					<a href="logout.php" class="navbar-brand pull-right ml-2" title="Logout"><i class="fa fa-1x5x fa-home"></i> Logout</a>
					<a class="navbar-brand pull-right mr-5">
						<?php echo ($oSessionManager->sName) ?>
					</a>
				</div>
			</div>
		</nav>

		<!-- For slider -->
		<div class="side-menu">
		    <nav class="navbar navbar-default" role="navigation" style="background-color: #363c46;">
		        <div class="side-menu-container">
		        	<div class="navbar-header classSliderHeader">
                        <a class="navbar-brand" href="#" >
                            <div class="icon fa fa fa-bars icon classTextColor"></div>
                            <div class="title classTextColor"> <?php echo(CONFIG_BRAND_NAME) ?></div>
                        </a>
                    </div>
		        </div>
		    </nav>

		    <ul class="nav navbar-nav classCollapse bg-gradient-primary sidebar sidebar-dark accordian" style="overflow: auto;max-height: 95vh;" id="accordian">
	            <li class="nav-item">
	            	<a class="nav-link classDropdownTextColor" href="#" name="idUserManagement" id="idUserManagement" data-toggle="collapse" data-target="#idCollapseUtilities" aria-expanded="true">
	            		<span class="icon fa fa-user"></span><span class="title">User Management<span class="caret ml-4"></span></span>
	            	</a>

	            	<div id="idCollapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordian">
	            		<div class="bg-white py-2 collapse-inner rounded">
	            			<?php if($sPage == 'Customer Dashboard'){ ?>
		            			<a href="#idAddModal" name="idRegisterBtn" id="idRegisterBtn" class="register_data" data-toggle="modal">
			            			<span class="icon fa fa-pencil classDropdownTextColor"></span>
			            			<span class="title">Register New User</span>
			            		</a>
		            		<?php } ?>
	            		</div>

	            		<div class="bg-white py-2 collapse-inner rounded">
	            			<?php if($sPage != 'Customer Dashboard'){ ?>
		            			<a href="CustomerDashboard.php" name="idDashboardBtn" id="idDashboardBtn">
			            			<span class="icon fa fa-tachometer classDropdownTextColor"></span>
			            			<span class="title">Customer Dashboard</span>
			            		</a>
		            		<?php } ?>
	            		</div>
	            	</div>
	            </li>

	            <li>
	            	<a href="customer.php" class="classDropdownTextColor">
	            		<span class="icon fa fa-plus-square"></span><span class="title">Add Customer List Price</span>
	            	</a>
	            </li>

	            <li>
	            	<a href="QuickAppointment.php" class="classDropdownTextColor">
	            		<span class="icon fa fa-plus-square"></span><span class="title">Add Quick Appointment</span>
	            	</a>
	            </li>

	            <li>
	            	<a href="CombiBillingMaster.php" class="classDropdownTextColor">
	            		<span class="icon fa fa-plus-square"></span><span class="title">Combi Billing Master</span>
	            	</a>
	            </li>

	            <li>
	            	<a href="logout.php" class="classDropdownTextColor">
	            		<span class="icon fa fa-1x5x fa-home"></span><span class="title">Logout</span>
	            	</a>
	            </li>
            </ul>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$(".nav-link").click(function(){
  			$(".in").toggle();
		});
  	});
</script>
