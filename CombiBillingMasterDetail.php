<?php

include_once "sessionCheck.php";

$sPageTopTitle = "Combi Billing Master Detail";
$iUserID = $oSessionManager->iUserID;
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
 	<?php include_once 'medixcelHeaderB3.php'; ?>
 	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="js/moment.js"></script>
    <script type="text/javascript" src="assets/select2/dist/js/select2.full.js"></script>
    <script type="text/javascript" src="assets/select2/dist/js/select2.full.min.js"></script>
    <style type="text/css">
        .classTextAlign{
            text-align: center;
        }
        a {
            color: #FFFFFF;
        }
        .classTDHead{
            padding: 3px 3px 7px !important;
        }
        .classSampleTypeColumn{
            width: 100px;
        }
        .classPackageNameColumn, .classTestNameColumn{
            width: 200px;
        }
        .classTCSColumn{
            width: 90px;
    }

    </style>
</head>
<body class="stickyMedixcelFooter flat-blue" style="overflow-x: hidden;">
<?php include_once 'medixcelNavbarB3.php'; ?>

	<div class="container-fluid classManageCulturePanel ml-5">
        <div class="row-fluid">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-2 col-md-2 col-sm-2">
                    <div class="form-group ml-1">
                        <label>Sample Type</label>
                        <select class="form-control" id="idSampleTypeSearch" name="idSampleTypeSearch"></select>
                    </div>
                </div>

                <div class="col-lg-2 col-md-2 col-sm-2">
                    <div class="form-group">
                        <label>Package Name</label>
                        <select class="form-control" id="idPackageTypeSearch" name="idPackageTypeSearch"></select>
                    </div>
                </div>

                <div class="col-lg-2 col-md-2 col-sm-2">
                    <div class="form-group">
                        <label>Test Name</label>
                        <select class="form-control" id="idTestTypeSearch" name="idTestTypeSearch"></select>
                    </div>
                </div>

                <div class="col-lg-2 col-md-2 col-sm-2 pull-right">
                    <a class="btn btn-sm btn-primary classClearFilter pull-right ml-3 mr-2" title="Clear Filter" style="border-radius: 0px; font-size: 10px;margin-top: 25px;background-color: #333;border-color: #333;"><i class="fa fa-trash"></i> &nbsp;&nbsp; Clear</a>

                    <a class="btn btn-primary btn-sm classBtnSearch pull-right" id="idButtonSearch" style="border-radius: 0px; font-size: 10px;margin-top: 25px;background-color: #333;border-color: #333;"><i class="fa fa-search" ></i>&nbsp;&nbsp; Search</a>
                </div>
            </div>
            
        </div>
        <form id="idFormCombiBillingMaster">
            <div class="row-fluid">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <input type="hidden" name="idUserID" value="<?php echo $iUserID; ?>">
                    <table class="table table-hover table-bordered " id="idTableCombiBillingMaster" style="color: #000; width: 97%" align="center">
                        <thead style="color: #fff;background: #238bc4;font-size: 12px;" >
                            <tr>
                                <th class="classTextAlign classSampleTypeColumn" rowspan="2">Sample Type</th>
                                <th class="classTextAlign classPackageNameColumn" rowspan="2">Package Name</th>
                                <th class="classTextAlign classTestNameColumn" rowspan="2">Test Name</th>
                                <th class="classTextAlign classTDHead classTCSColumn" colspan="2">Test Completed Successfully</th>
                                <th class="classTextAlign classTDHead" colspan="2">Test Rejected</th>
                                <th class="classTextAlign classTDHead" colspan="2">Test Cancelled</th>
                            </tr>
                            <tr>
                                <th class="classTextAlign classTDHead classPercentColumn">%</th>
                                <th class="classTextAlign classTDHead classAmountColumn">Amount</th>
                                <th class="classTextAlign classTDHead classPercentColumn">%</th>
                                <th class="classTextAlign classTDHead classAmountColumn">Amount</th>
                                <th class="classTextAlign classTDHead classPercentColumn">%</th>
                                <th class="classTextAlign classTDHead classAmountColumn">Amount</th>
                            </tr>
                        </thead>
                        <tbody id="idTableManageCombiBilling" style="font-size: 12px;">
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="row-fluid">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <button class="btn btn-md btn-info">Save</button>
                </div>
            </div>
        </form>
	</div>

    <script type="text/javascript">

    $(document).ready(function() {
        window.oTableUIDataTable.fnDraw();
   });   

    //to get all sample details
    window.oTableUIDataTable = $('#idTableCombiBillingMaster').dataTable( {
        "processing": true,
        "serverSide": true,
        "searching": false,
        "pageLength": 20,
        "ajax": {
            "url": "ajaxFile.php?sFlag=GetCombiBillingList",
            "data": function(d) {
                d.iSampleTypeID = $('#idSampleTypeSearch').val();
                d.iPackageID = $('#idPackageTypeSearch').val();
                d.iServiceID = $('#idTestTypeSearch').val();
            }
        },
        "lengthChange": false
    });

    $(function() {
        $('#idFormCombiBillingMaster').submit(function(e) {
            e.preventDefault();

            data = new FormData($('#idFormCombiBillingMaster')[0]);
            $.ajax({
                type: 'POST',
                url: 'ajaxFile.php?sFlag=AddCombiBilling',
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                success: function(data) {
                    if(data != false) {
                        window.oTableUIDataTable.fnDraw();
                        alert("Data Entered Successfully");
                    }
                    else {
                        alert("Some Error Occured");
                    }
                },
            });
        });
    });

    $(document).on('change','.classTestCompletePercent',function(){
        var iControlID = $(this).attr('id').split("_").pop();
        var sControlID = $(this).attr('id');
        if ($('#idTestCompleteAmount_'+iControlID).val()!="") {
            alert("Cant put percent as you have already set amount for test completed");
            $('#'+sControlID).val('');
        }                    
    });

    $(document).on('change','.classTestCompleteAmount',function(){
        var iControlID = $(this).attr('id').split("_").pop();
        var sControlID = $(this).attr('id');
        if ($('#idTestCompletePercent_'+iControlID).val()!="") {
            alert("Cant put amount as you have already set percentage for test completed");
            $('#'+sControlID).val('');
        }                    
    });

    $(document).on('change','.classTestRejectPercent',function(){
        var iControlID = $(this).attr('id').split("_").pop();
        var sControlID = $(this).attr('id');
        if ($('#idTestRejectAmount_'+iControlID).val()!="") {
            alert("Cant put percent as you have already set amount for test rejected");
            $('#'+sControlID).val('');
        }                    
    });

    $(document).on('change','.classTestRejectAmount',function(){
        var iControlID = $(this).attr('id').split("_").pop();
        var sControlID = $(this).attr('id');
        if ($('#idTestRejectPercent_'+iControlID).val()!="") {
            alert("Cant put amount as you have already set percentage for test rejected");
            $('#'+sControlID).val('');
        }                    
    });

    $(document).on('change','.classTestCancelPercent',function(){
        var iControlID = $(this).attr('id').split("_").pop();
        var sControlID = $(this).attr('id');
        if ($('#idTestCancelAmount_'+iControlID).val()!="" ) {
            alert("Cant put percent as you have already set amount for test cancelled");
            $('#'+sControlID).val('');
        }                    
    });

    $(document).on('change','.classTestCancelAmount',function(){
        var iControlID = $(this).attr('id').split("_").pop();
        var sControlID = $(this).attr('id');
        if ($('#idTestCancelPercent_'+iControlID).val()!="") {
            alert("Cant put amount as you have already set percentage for test cancelled");
            $('#'+sControlID).val('');
        }                    
    });


    $("#idSampleTypeSearch").select2({               
        width: '100%',
        allowClear: true,
        placeholder: "Search Sample",
        minimumInputLength: 3,
        width: 'resolve',
        ajax: {
            url: "ajaxFile.php?sFlag=SearchSample",
            data: function (params) {
                return {
                    sSampleName: params.term,
                };
            },
            processResults: function (data, params) {
                var results = [];

                $.each(data, function(index, Sample) {
                    results.push({
                        id: Sample.sample_id,
                        text: Sample.sample_label
                    });
                });

                return { results: results };
            }
        }
    });

    $("#idPackageTypeSearch").select2({               
        width: '100%',
        allowClear: true,
        placeholder: "Search Package",
        minimumInputLength: 3,
        width: 'resolve',
        ajax: {
            url: "ajaxFile.php?sFlag=SearchPackage",
            data: function (params) {
                return {
                    sPackageName: params.term,
                };
            },
            processResults: function (data, params) {
                var results = [];

                $.each(data, function(index, Package) {
                    results.push({
                        id: Package.hcp_id,
                        text: Package.name
                    });
                });

                return { results: results };
            }
        }
    });

    $("#idTestTypeSearch").select2({               
        width: '100%',
        allowClear: true,
        placeholder: "Search Test",
        minimumInputLength: 3,
        width: 'resolve',
        ajax: {
            url: "ajaxFile.php?sFlag=SearchTest",
            data: function (params) {
                return {
                    sTestName: params.term,
                };
            },
            processResults: function (data, params) {
                var results = [];

                $.each(data, function(index, Test) {
                    results.push({
                        id: Test.service_id,
                        text: Test.service_name+" ( "+Test.sample_label+" )"
                    });
                });

                return { results: results };
            }
        }
    });

    $(document).on('click', '#idButtonSearch', function(){
        window.oTableUIDataTable.fnDraw();
    });

     $(document).on('click', '.classClearFilter', function(){
        $('#idSampleTypeSearch').val('').text('');
        $('#idPackageTypeSearch').val('').text('');
        $('#idTestTypeSearch').val('').text('');
        window.oTableUIDataTable.fnDraw();
    });
    </script>
</body>
</html>
