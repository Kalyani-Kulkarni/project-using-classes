<?php

/*	This file contains all alert message constants. 
*	Constants starting with S prefix are the Success Messages
*	Constants starting with E prefix are the Error Messages
*/

//! Success Messages
define('S1','You\'ve been logged out successfully.');
define('S2','Clinic added successfully');
define('S3','Clinic updated successfully');
define('S4','Special Service offer rates updated successfully');
define('S5','OPD Bill updated successfully');
define('S6','Plan added successfully');
define('S7','Plan updated successfully');
define('S8','Corporate added successfully');
define('S9','Corporate updated successfully');
define('S10','Patient added successfully');
define('S11','Patient updated successfully');
define('S12','Staff member added successfully');
define('S13','Staff member updated successfully');
define('S14','Doctor added successfully');
define('S15','Doctor updated successfully');
define('S17','Expense added successfully');
define('S18','Expense updated successfully');
define('S19','Campaign added successfully');
define('S20','Campaign updated successfully');
define('S21','Campaign sent successfully');
define('S22','Email Template added successfully');
define('S23','Email Template updated successfully');
define('S24','Vital entry updated successfully');
define('S25','Vital entry added successfully');
define('S26','Alert added successfully');
define('S27','Alert updated successfully');
define('S28','External reported added successfully');
define('S29','External reported deleted successfully');
define('S30','Lab values uploaded successfully');
define('S31','Referral Added successfully');
define('S32','Referral Updated successfully');
define('S33','Non Member Visit Added successfully');
define('S34','Non Member Visit Updated successfully');
define('S35','Leave added successfully!');
define('S36','Package has been activated successfully!');
define('S37','IPD Bill updated successfully');
define('S38','Lead Widget Script Form Added successfully');
define('S39','Mortuary Bill updated successfully');
define('S40','Package approval request created successfully');
define('S41','SMS Template added successfully');
define('S42','SMS Template updated successfully');
define('S43','Auto Ticket Message Template added successfully');
define('S44','Auto Ticket Message updated successfully');

//! Error Messages
define('E1','You are forcefully logged out for logging-in somewhere else.');
define('E2','Access Denied.');
define('E3','Incorrect Username and Password.');
define('E4','Schedule not added because some error was occur.Please try again');
define('E5','Clinic not added because some error was occur.Please try again');
define('E6','Clinic not updated because some error was occur.Please try again');
define('E7','Special Service Discount Offer rates not set. Please try again');
define('E8','OPD Bill not updated. Please try again');
define('E9','Plan not added because some error was occur. Please try again');
define('E10','Plan not updated because some error was occur. Please try again');
define('E11','Plan details added but service details is not added because some error was occur. Please try again');
define('E12','Corporate not added because some error was occur. Please try again');
define('E13','Corporate not updated because some error was occur. Please try again');
define('E14','Corporate details added but service details is not added because some error was occur. Please try again');
define('E15','Patient not added because some error was occur. Please try again');
define('E16','Patient not update because some error was occur. Please try again');
define('E17','Staff member not added because some error was occur. Please try again');
define('E18','Staff member not update because some error was occur. Please try again');
define('E19','Doctor not added because some error was occur. Please try again');
define('E20','Doctor not update because some error was occur. Please try again');
define('E21','Expense details not added because some error was occur. Please try again');
define('E22','Expense details not updated because some error was occur. Please try again');
define('E23','Campaign not added because some error was occur. Please try again');
define('E24','Campaign not update because some error was occur. Please try again');
define('E25','Campaign recipient list not added because some error was occur. Please try again');
define('E26','Campaign recipient list added but not sent because some error was occur. Please try again');
define('E27','Email Template not added because some error was occur. Please try again');
define('E28','There was some problem deleting external report. Please try again!');
define('E29','Alert not added because some error was occur. Please try again');
define('E30','Alert not update because some error was occur. Please try again');
define('E31','Please upload file.');
define('E32','Invalid file uploaded.');
define('E33','There was error uploading lab values.');
define('E34','Some error was occur while adding External reports . Please try again');
define('E35','Some error was occur while updating Email Template . Please try again');
define('E36','Some error was occur while Adding Referral . Please try again');
define('E37','Some error was occur while Updating Referral . Please try again');
define('E38','Some error was occur while adding non member visit. Please try again');
define('E39','Some error was occur while updating non member visit. Please try again');
define('E40','There was some problem adding leave. Please try again');
define('E41','You available is below zero. Please recharge to login!');
define('E42','Package cannot be activated successfully!');
define('E43','IPD Bill not updated. Please try again');
define('E44','Some error was occur while adding widget form detail. Please try again');
define('E45','Mortuary Bill not updated. Please try again');
define('E46','Vital entry not updated. Please try again');
define('E47','SMS Template not added because some error was occur. Please try again');
define('E48','SMS Template not updated because some error was occur. Please try againn');
define('E51','Auto Ticket Message not added because some error was occur. Please try again');
define('E52','Auto Ticket Message not updated because some error was occur. Please try againn');


//! Warning Message
define("W3", "Your licence key has expired. Please contact support.");
?>