<?php
//! Include Required Audit Class
/*include_once dirname(__FILE__).DIRECTORY_SEPARATOR."class.AuditTrail.php";
include_once dirname(__FILE__).DIRECTORY_SEPARATOR."class.PermissionHandler.php";*/
/*!
 * @class SessionManager
 * This class manages the user session.
 */
include_once "class.DBConnManager.php";
include_once "config/config.database.php";
include_once "config/config.medixcel.php";
include_once "config/config.messageConstants.php";
include_once "functions.php";

class SessionManager {
    public $isLoggedIn;
    public $iUserID;
    public $sUsername;
    public $sName;
    public $sError;
    public $iUserTypeID;
    public $sUserType;
    public $sLoginDate;
    public $sLoginTime;
    public $sIP;
    public $sBrowser;
    public $sDeviceType;
    public $lastActivity;
    public $iSessionDuration = SESSION_TIME; // In Min
    public $sUserSubtype;


    //constructor
    function __construct()
    {
        if(!isset($_SESSION))
        {
            session_start();
        }

        if(isset($_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_lastActivity']))
        {

            if($_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_lastActivity'] < (time() - $this->iSessionDuration*60))
            {
                $this->fLogOut();
                return;
            }
        }

        /*  This functionality gets the logout the previous insatnces of a user. */
        $bLogoutCheck = $this->fGetLogoutStatusForInstance();

        // If logout time status is true then it destroys the session and redirect the user out of application
        if($bLogoutCheck){
            if(isset($_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_isLoggedIn'])){
                $_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_isLoggedIn'] = FALSE;
            }
            // Unset session data.
            session_unset();
            // Calling function to Unset the cookie of browser.
            $this->fUnsetAllCookies();
            // In value B is for forcefully logout message and redirecting to login page.
            $sMsg = array();
            $sMsg[] = "E1";
            //Config Added by Ritu (2018-06-18) for new login page
            if(MEDIXCEL_NEW_LOGIN_PAGE == TRUE){
                //! Redirect User with appropriate alert message
                fRedirectWithAlert("newLogin.php", $sMsg);
                exit;
            }else{
                //! Redirect User with appropriate alert message
                fRedirectWithAlert("login.php", $sMsg);
                exit;
            }
        } 

        if(isset($_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_isLoggedIn'])){
            if($_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_isLoggedIn'] === TRUE){
                $this->isLoggedIn = TRUE;
                $this->iUserID=$_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_iUserID'];
                $this->sUsername=$_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_sUsername'];
                $this->sName=$_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_sName'];
                $this->iStaffID=$_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_iStaffID'];
                $this->sLoginDate=$_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_sLoginDate'];
                $this->sLoginTime=$_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_sLoginTime'];
                $this->iUserTypeID=$_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_iUserTypeID'];
                $this->sUserType=$_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_sUserType'];
                $this->sUserSubtype=$_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_sUserSubtype']; 
                $this->sIP = $_SERVER['REMOTE_ADDR'];
                $this->sBrowser = $_SERVER['HTTP_USER_AGENT'];
                $this->sDeviceType = "undefined";
                $_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_lastActivity'] = time();
                $this->lastActivity = time();
                $this->sError = NULL; 
                //! Permission matrix
                $this->aPermMatrix = unserialize(urldecode($_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_permMatrix']));
            }
            else {
                $this->isLoggedIn = FALSE;
            }
        }
    }
    
    //! @return boolean Boolean value stating whether current user is logged in or not
    function isLoggedIn()
    {
        return $this->isLoggedIn;

    }
    /*!
    * @brief Redirects User to login page if not logged in 
    * This function should be called on the top of the pages which require user to be authenticated.
    * If user is not authenticated, it will be redirected to login page. On successful login, it will be
    * redirected to url specificed in argument
    */
    function fRequireAuth($sURL = FALSE)
    {
        if(!$this->isLoggedIn())
        {
            $sMsg = array();
            $sMsg[] = "E2";
            fRedirectWithAlert("login.php", $sMsg);
        }
    }

    //!@ Brief - This function returns boolean value TRUE if user is logged in.
    function FUrlAuth(){ // newly added
        if($this->isLoggedIn()){
            return TRUE;
        }
    }

    function fAuthenticateUser($sUsername, $sPassword)
    {
        $sUsername = trim($sUsername);
        $sPassword = trim($sPassword);

        if ($sUsername == '' || $sPassword == '')
        {
            $this->sError = INVALID_DATA;
            return FALSE;
        }
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        $sUsername = $conn->real_escape_string($sUsername);
        $sPassword = $conn->real_escape_string($sPassword);
        $sLoginTable = DATABASE_TABLE_PREFIX.'_user_logins';
        $sQuery = "SELECT user_id,password FROM {$sLoginTable} WHERE username='{$sUsername}'";

		$result = $conn->query($sQuery);
		if(!$result){
            $this->sError = DATABASE_ERROR;
            return FALSE;
        }
        $aTemp = $result->fetch_array();

         if($aTemp[0]=='')
        {
                $_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_sLoginDate'] = date('Y-m-d');
                $_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_sLoginTime'] = date('H:i:s',time());
                $_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_sLoginDateTime']=date('d/m/Y  H:i',mktime(date('H'),date('i')));

                $this->iUserID='NULL';
                $this->sUserName=$sUsername;
                $this->sUserPassword=$sPassword;
                $this->sLoginDate=$_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_sLoginDate'];
                $this->sLoginTime=$_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_sLoginTime'];
                $this->sIP = $_SERVER['REMOTE_ADDR'];
                $this->sBrowser = $_SERVER['HTTP_USER_AGENT'];
                $this->sDeviceType = "undefined";
                $this->fSaveFailedLoginInstance(); //! call the save failed login instance fn if username does not exist in our database
        }
        $sRealPass = $aTemp[1];
        $iUID = $aTemp[0];
		
        $verified = $this->fVerifyPassword($sPassword,$sRealPass);
		
        if($verified){

            $this->isLoggedIn = TRUE;
            $_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_isLoggedIn']=TRUE;

            $this->iUserID=$iUID;

            $oUser = new User($this->iUserID);
            $this->sUsername=$oUser->sUsername;
            $_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_sUsername'] = $this->sUsername;
            $this->iUserTypeID = $oUser->iUserTypeID;
            $_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_iUserTypeID'] = $oUser->iUserTypeID;
            $this->sUserType = $oUser->sUserType;
            $_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_sUserType'] = $oUser->sUserType;
            $this->sName=$oUser->sName;
            $this->sUserSubtype=$oUser->sUserSubtype;
            $_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_sUserSubtype']=$oUser->sUserSubtype;
            $_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_sName']=$oUser->sName;
            $_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_iStaffID']=$oUser->iStaffID;
            $_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_sLoginDate'] = date('Y-m-d');
            $_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_iUserID'] = $this->iUserID;
            $_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_sLoginTime'] = date('H:i:s',time());
            $this->sLoginDate=$_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_sLoginDate'];
            $this->sLoginTime=$_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_sLoginTime'];
            $_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_lastActivity'] = time();
            $_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_sLoginDateTime']=date('d/m/Y  H:i',mktime(date('H'),date('i')));
            $this->sIP = $_SERVER['REMOTE_ADDR'];
            $_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_sIP'] = $this->sIP;
            $this->sBrowser = $_SERVER['HTTP_USER_AGENT'];
            $this->sDeviceType = "undefined";
           
           
            /*  This functionality generates a new session id 

            /*  and logs out the previous insatnces of a user */
            //session_regenerate_id(true);

            $this->sUserSessionID = session_id();
            $this->fLogoutPreviousInstance($this->iUserID);

            $_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_instanceId']=$this->fSaveLoginInstance();
            if(SESSION_CHECKER===true) {
                $this->fLogOut();
                //Config Added by Ritu (2018-06-18) for new CPH login page
                if(MEDIXCEL_NEW_LOGIN_PAGE == TRUE){
                    fRedirectWithAlert("newLogin.php", array("W3"));
                    exit;
                }else{
                    fRedirectWithAlert("login.php", array("W3"));
                    exit;
                }
            }

/*            $this->aPermMatrix = PermissionHandler::getPermissionModel($oUser);
            $_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_permMatrix'] = urlencode(serialize($this->aPermMatrix));
*/            return TRUE;
        }
        else {
            $this->sError = WRONG_CREDENTIAL;
            $this->iUserID=$iUID;
            $_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_sLoginDate'] = date('Y-m-d');
            $_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_iUserID'] = $this->iUserID;
            $_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_sLoginTime'] = date('H:i:s',time());
            $_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_sLoginDateTime']=date('d/m/Y  H:i',mktime(date('H'),date('i')));
            $this->sUserName=$sUsername;
            $this->sUserPassword=$sPassword;
            $this->sLoginDate=$_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_sLoginDate'];
            $this->sLoginTime=$_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_sLoginTime'];
            $this->sIP = $_SERVER['REMOTE_ADDR'];
            $_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_sIP'] = $this->sIP;
            $this->sBrowser = $_SERVER['HTTP_USER_AGENT'];
            $this->sDeviceType = "undefined";
            $this->fSaveFailedLoginInstance();
            return FALSE;
        }
    }

    static function fVerifyPassword($sPlainPassword, $sPasswordHash)
    {
        $salt = substr($sPasswordHash,0,32);
        $hash = substr($sPasswordHash,32);
        $sSHAHash = hash("sha256", $salt.$sPlainPassword);
        if($sPasswordHash == $salt.$sSHAHash){
            return TRUE;
        }
        else {
            return FALSE;
        }
    }

    function fLogOut(){

        /*  Additions Does - It unset the cookies of browser and removes the session data from server. */        
        // Unset cookies

        if($_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_iUserID']){

            $iAuditableUserId = $_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_iUserID'];
            $iInstanceId = $_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_instanceId'];
            $this->fUpdateLoginInstanceUsingInstanceId($iInstanceId);

            unset($_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_instanceId']);
            unset($_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_isLoggedIn']);
            unset($_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_sUsername']);
            unset($_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_iUserTypeID']);
            unset($_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_sUserType']);
            unset($_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_sName']);
            unset($_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_iStaffID']);
            unset($_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_sLoginDate']);
            unset($_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_iUserID']);
            unset($_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_sLoginTime']);
            unset($_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_sLoginDate']);
            unset($_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_sLoginTime']);
            unset($_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_lastActivity']);
            unset($_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_sLoginDateTime']);
            unset($_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_sIP']);
            session_unset();
            $this->fUnsetAllCookies();
            $this->fUnSetAllSessionData();
            if($iAuditableUserId){
                $auditMsg = "User [user]{$iAuditableUserId}[/user] was logged out.";
            }
        }    
    }

    function fSaveLoginInstance(){

        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        $sLoginInstanceTable = DATABASE_TABLE_PREFIX.'_login_instance';
        $sQuery = "INSERT INTO `{$sLoginInstanceTable}` (`instance_id`, `user_id`, `login_date`, `login_time`, `logout_time`,`browser`, `ip`, `device_type`, `user_session_id`) 
            VALUES (NULL, {$this->iUserID}, '{$this->sLoginDate}', '{$this->sLoginTime}', '','{$this->sBrowser}', '{$this->sIP}', '{$this->sDeviceType}', '{$this->sUserSessionID}')";
            //var_dump($sQuery);exit;
        $conn->query($sQuery);
        $iInsertId = $conn->insert_id;
        return $iInsertId;

    }

    //! @fn function fUpdateLoginInstanceUsingInstanceId()
    /* @brief Update the log out time using instance id 
    * @parameter instance id is passed
    */

    function fUpdateLoginInstanceUsingInstanceId($iInstanceId){

        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        $logoutTime = date('H:i:s',time());
        $sLoginInstanceTable = DATABASE_TABLE_PREFIX.'_login_instance';
        
        $sQuery = "UPDATE `{$sLoginInstanceTable}` SET `logout_time`='{$logoutTime}' WHERE  `instance_id`='{$iInstanceId}'";
        
        $conn->query($sQuery);
    }

    //! @fn function fSaveFailedLoginInstance()
    /* @save the failed login user instances 
    */

    function fSaveFailedLoginInstance()
    {
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        $sLoginInstanceTable = DATABASE_TABLE_PREFIX.'_failed_login_instance';
        $sQuery = "INSERT INTO `{$sLoginInstanceTable}` (`instance_id`, `user_id`, `username`,`password`,`login_date`, `login_time`,`browser`, `ip`, `device_type`) 
            VALUES (NULL, {$this->iUserID}, '{$this->sUserName}','{$this->sUserPassword}','{$this->sLoginDate}', '{$this->sLoginTime}', '{$this->sBrowser}', '{$this->sIP}', '{$this->sDeviceType}')";
            //var_dump($sQuery);exit;
        $conn->query($sQuery);
    }


    /*! @fn function fGetLoggedInUser()
    * @brief Give the logged in user
    * This function will return the object of User who is currently logged In.
    * @return object It will return the object of User class
    */
    function fGetLoggedInUser(){
        return new User($this->iUserID);
    }


    //!@ Brief - This function returns the status of logout time of a user for an instances.
    //!@ Return - True when logout time is filled otherwise returns false.
    function fGetLogoutStatusForInstance(){

        // Status implies that logout time is filled or not
        $sStatus = false;
        // Get instance id
        $iInstanceId = 0;
        if(isset($_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_instanceId'])){

            $iInstanceId = $_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_instanceId'];
        }

        // Creating object of database connection class.
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        $sLoginInstanceTbl = DATABASE_TABLE_PREFIX."_login_instance";

        $sSelectQuery = "SELECT `logout_time` FROM {$sLoginInstanceTbl} WHERE `instance_id` = '{$iInstanceId}'";
        $result = $conn->query($sSelectQuery);
        $row = $result->fetch_array();
        $tLogoutTime = $row['logout_time'];

        // Check the fetched value of logout time, if not empty set boolean values
        if($tLogoutTime != null){
            if($tLogoutTime != '00:00:00'){
                $sStatus = TRUE;
            }
        }
        // Return status
        return $sStatus;
    }   

    //!@ Brief - This function update the previous logout time of loggedin instances for a user.
    //!@ Parameter - User ID
    function fLogoutPreviousInstance($iUserID){

        // Creating object of database connection class.
        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();
        $sLoginInstanceTbl = DATABASE_TABLE_PREFIX."_login_instance";

        $tLogoutTime = date('H:i:s',time());
        $sQuery = "UPDATE {$sLoginInstanceTbl} SET `logout_time` = '{$tLogoutTime}' WHERE `logout_time` = '00:00:00' AND `user_id` = '{$iUserID}'";
        $result = $conn->query($sQuery);
    }

    //!@ Brief - This function unset all cookies from the browser.
    function fUnsetAllCookies(){

        // Unset cookies.
        if (isset($_COOKIE[session_name()])) {
            setcookie(session_name(),'', time()-3600, '/');
        }
        
        // Regenerating session id
        session_regenerate_id(true);
    }

    //! function to write the data in sesstion
    function fAddSessionData($sSessionKey , $aSessionValue){
        $sSessionValue = serialize($aSessionValue);    
        $_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_'.$sSessionKey] = $sSessionValue;
    }

    //! function to get the sesion data for the key passed in parameter
    function fGetSessionData($sSessionKey){
        if(isset($_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_'.$sSessionKey])){
            $sSessionData = $_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_'.$sSessionKey];
            $aSessionData = unserialize($sSessionData);
            return $aSessionData;
        }else{
            return array();
        }
    }

    function fUnSetAllSessionData(){
        session_unset();
    }


// get ehr user id for cph user
    function fGetEHRUserForCphUserID($iCphUserId){

        $DBMan = new DBConnManager(CPH_DATABASE);
        $conn =  $DBMan->getConnInstance();
        $bInsertQueryResult = FALSE;

        $sTable = "cph_ehr_user_relation";
        $sQuery = "SELECT ehr_user_id FROM `{$sTable}` WHERE `cph_user_id`={$iCphUserId} ";

        $iEhrUserId = $conn->query($sQuery)->fetch_object()->ehr_user_id;
        return $iEhrUserId;
    }

    // Validate token to allow direct login from cph
    function validateToken($sToken, $iCphUserId){

        $DBMan = new DBConnManager();
        $conn =  $DBMan->getConnInstance();

        $sTable = DATABASE_TABLE_PREFIX."_direct_ehr_login_token";

        $sQuery = "SELECT * FROM {$sTable} WHERE token LIKE '{$sToken}' AND user_id={$iCphUserId} AND is_used=0 limit 0,1";

        $sSelectQueryR = $conn->query($sQuery);
        if($sSelectQueryR != FALSE){
           $oResult = $conn->query($sQuery)->fetch_object();
        }
        // print_r($oResult);die;
        if(!empty((array)$oResult)){
            if(date("H:i:s",strtotime($oResult->added_on)) < (date("H:i:s",time()) - EHR_TOKEN_DURATION *60)){
                return FALSE;
            }
            $iEhrUserID = $this->fGetEHRUserForCphUserID($iCphUserId);
            
            $this->isLoggedIn = TRUE;
            $_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_isLoggedIn']=TRUE;
            $this->iUserID=$iEhrUserID;
            $oUser = new User($this->iUserID);
            $this->sUsername=$oUser->sUsername;
            $_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_sUsername'] = $this->sUsername;
            $this->iUserTypeID = $oUser->iUserTypeID;
            $_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_iUserTypeID'] = $oUser->iUserTypeID;
            $this->sUserType = $oUser->sUserType;
            $_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_sUserType'] = $oUser->sUserType;
            $this->sName=$oUser->sName;
            $this->sUserSubtype=$oUser->sUserSubtype;
            $_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_sUserSubtype']=$oUser->sUserSubtype;
            $_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_sName']=$oUser->sName;
            $_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_iStaffID']=$oUser->iStaffID;
            $_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_sLoginDate'] = date('Y-m-d');
            $_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_iUserID'] = $this->iUserID;
            $_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_sLoginTime'] = date('H:i:s',time());
            $this->sLoginDate=$_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_sLoginDate'];
            $this->sLoginTime=$_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_sLoginTime'];
            $_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_lastActivity'] = time();
            $_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_sLoginDateTime']=date('d/m/Y  H:i',mktime(date('H'),date('i')));
            $this->sIP = $_SERVER['REMOTE_ADDR'];
            $_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_sIP'] = $this->sIP;
            $this->sBrowser = $_SERVER['HTTP_USER_AGENT'];
            $this->sDeviceType = "undefined";
           
           
            /*  This functionality generates a new session id 
            /*  and logs out the previous insatnces of a user */
            
            //session_regenerate_id(true);
            $this->sUserSessionID = session_id();
            $this->fLogoutPreviousInstance($this->iUserID);

            $_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_instanceId']=$this->fSaveLoginInstance();
            if(SESSION_CHECKER===true) {
                $this->fLogOut();
                //Config Added by Ritu (2018-06-18) for new CPH login page
                if(MEDIXCEL_NEW_LOGIN_PAGE == TRUE){
                    fRedirectWithAlert("newLogin.php", array("W3"));
                exit;
            }else{
                    fRedirectWithAlert("login.php", array("W3"));
                    exit;
                }
            }

            $this->aPermMatrix = PermissionHandler::getPermissionModel($oUser);
            $_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_permMatrix'] = urlencode(serialize($this->aPermMatrix));

            $sUpdateQuery = "UPDATE `{$sTable}` SET `is_used`=1 WHERE  `token` LIKE '{$sToken}' AND `user_id`={$iCphUserId}";

            $conn->query($sUpdateQuery);
            return TRUE;

        }else{
            return FALSE;
        }
    }
}
?>