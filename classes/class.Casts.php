<?php

include_once "class.DBConnManager.class";

class Casts
{
	public $iID;
	public $sRealName;
	public $sIndustryName;
	public $sQualificaton;
	public $sGender;
	public $dDateOfBirth;
	public $sMobile;
	public $sEmail;
	public $sAddress;
	public $sCity;
	public $sState;
	public $sCountry;
	public $iNumberOfMovies;
	public $sLatestMovie;
	public $iNumberOfAwards;

	/*
		This function is instantiated when the object of class is created according to the passed $iId
	*/

	function __construct($iId=NULL)
	{
		if($iID!==NULL)
		{
			$DBMan = new DBConnManager();
			$conn = $DBMan->getConnInstance();
			$sProfileTable = DATABASE_TABLE_PREFIX.'_profile';
			$sCareerTable = DATABASE_TABLE_PREFIX.'_career';
			$sQuery = "Select * from `{$sProfileTable}` a, `{sCareerTable}` b where a.id={$iID} AND a.id=b.id";
			$sResult = $conn->query($sQuery);
			if($sResult!==FALSE){
				$aRow = $sResult->fetch_array();
				$this->sRealName = $aRow['real_name'];
				$this->sIndustryName = $aRow['industry_name'];
				$this->sQualificaton = $aRow['qualification'];
				$this->sGender = $aRow['gender'];
				$this->dDateOfBirth = $aRow['DOB'];
				$this->sMobile = $aRow['mobile'];
				$this->sEmail = $aRow['email'];
				$this->sAddress = $aRow['address'];
				$this->sCity = $aRow['city'];
				$this->sState = $aRow['state'];
				$this->sCountry = $aRow['country'];
				$this->iNumberOfMovies = $aRow['number_of_movies'];
				$this->sLatestMovie = $aRow['latest_movie'];
				$this->iNumberOfAwards = $aRow['number_of_awards'];
			}
			$DBMan=NULL;
		}
	}

	/*
		This function is used to check if cast already exist or not
	*/

	function fCheckDuplicate($sRealName){
		$DBMan = new DBConnManager();
		$conn = $DBMan->getConnInstance();
		$sProfileTable = DATABASE_TABLE_PREFIX.'_profile';
		$sQuery = "select COUNT(*) from `{sProfileTable}` where real_name='{$sRealName}' and deleted=0";
		$sResult = $conn->query($sQuery);
		if($sResult){
			$aCount = $sResult->fetch_array();
			if($aCount[0]>0){
				return FALSE;
			}
		}
		else{
			return FALSE;
		}
	}

	/*
		This function is used to add new user
	*/

	function fAddActor($sRealName)
	{
		$DBMan = new DBConnManager();
		$conn = $DBMan->getConnInstance();
		$sProfileTable = DATABASE_TABLE_PREFIX.'_profile';
		$sCareerTable = DATABASE_TABLE_PREFIX.'_career';
		$sFlag = $this->fCheckDuplicate($sRealName);
		if($sFlag!==FALSE)
		{
			$sQuery = "Insert into `{$sProfileTable}` (`id`, `real_name`, `qualification`, `gender`, `DOB`, `mobile`, `email`, `address`, `city`, `state`, `country`) values (NULL, '{$this->sRealName}', '{$this->sQualificaton}', '{$this->sGender}', '{$this->dDateOfBirth}', '{$this->sMobile}', '{$this->sEmail}', '{$this->sAddress}', '{$this->sCity}', '{$this->sState}', '{$this->sCountry}');";
			$sResult = $conn->query($sQuery);
			if(!$sResult){
				return FALSE;
			}
			$this->$iID=$conn->insert_id;

			$sQuery = "Insert into `{sCareerTable}` (`id`, `industry_name`, `number_of_movies`, `latest_movie`, `number_of_awards`) values ('{$this->iID}', '{$this->sIndustryName}', '{$this->iNumberOfMovies}', '{$this->sLatestMovie}', '{$this->iNumberOfAwards}');";

			$sResult = $conn->query($sQuery);
			if(!$sResult)
			{
				$sTemp = "Delete * from `{sProfileTable}` where id={$this->iID}";
				$conn->query($sTemp);
				return FALSE;
			}
		}
		return $this->iID;
	}

	/*
		This function is used to get list of all casts
	*/

	function fGetAllCasts()
	{
		$aCastList = array();
		$DBMan = new DBConnManager();
		$conn = $DBMan->getConnInstance();
		$sProfileTable = DATABASE_TABLE_PREFIX.'_profile';
		$sCareerTable = DATABASE_TABLE_PREFIX.'_career';
		$sQuery = "Select * from `{$sProfileTable}` a, `{$sCareerTable}` b where a.id=b.id and a.deleted=0 and b.deleted=0";
		$sResult = $conn->query($sQuery);
		if($sResult!==FALSE){
			while($aRow=$sResult->fetch_array()){
				$aCastList[] = $aRow;
			}
		}
		return $aCastList;
	}

	/*
		This function is used to get cast detail by name
	*/

	function fGetCastByName($sRealName)
	{
		$aCastList = array();
		$DBMan = new DBConnManager();
		$conn = $DBMan->getConnInstance();
		$sProfileTable = DATABASE_TABLE_PREFIX.'_profile';
		$sQuery = "Select * from `{$sProfileTable}` where `real_name` LIKE '{$sRealName}%' and deleted=0";
		$sResult = $conn->query($sQuery);
		if($sResult!==FALSE){
			while ($aRow=$sResult->fetch_array()) {
				$aCastList[] = $aRow;
			}
		}
		return $aCastList;
	}

	/*
		This function is used to get cast detail by id
	*/

	function fGetCastById($iID)
	{
		$aCastList = array();
		$DBMan = new DBConnManager();
		$conn = $DBMan->getConnInstance();
		$sProfileTable = DATABASE_TABLE_PREFIX.'_profile';
		$sCareerTable = DATABASE_TABLE_PREFIX.'_career';
		$sQuery = "Select * from `{$sProfileTable}` a, `{$sCareerTable}` b where a.id={$iID} and a.id=b.id and a.deleted=0 and b.deleted=0";
		$sResult = $conn->query($sQuery);
		if($sResult!==FALSE){
			while ($aRow=$sResult->fetch_array()) {
				$aCastList[] = $aRow;
			}
		}
		return $aCastList;
	}

	/*
		This function is used to update cast profile
	*/

	function fUpdateActorProfile(){
		$DBMan = new DBConnManager();
		$conn = $DBMan->getConnInstance();
		$sProfileTable = DATABASE_TABLE_PREFIX.'_profile';
		$sQuery = "update `{$sProfileTable}` set `real_name`='{$this->sRealName}', `qualification`='{$this->sQualificaton}', `gender`='{$this->sGender}', `DOB`='{$this->dDateOfBirth}', `mobile`='{$this->sMobile}', `email`='{$this->sEmail}', `address`='{$this->sAddress}', `city`='{$this->sCity}', `state`='{$this->sState}, `country`='$this->sCountry' where `id`='{$this->iID}'";
		$sResult = $conn->query($sQuery);
		if(!$sResult){
			return FALSE;
		}
		return TRUE;
	}

	/*
		This function is used to update cast career
	*/

	function fUpdateActorCareer(){
		$DBMan = new DBConnManager();
		$conn = $DBMan->getConnInstance();
		$sCareerTable = DATABASE_TABLE_PREFIX.'_career';
		$sQuery = "update `{$sCareerTable}` set `industry_name`='{$this->sIndustryName}', `number_of_movies`='{$this->iNumberOfMovies}', `latest_movie`='{$this->sLatestMovie}', `number_of_awards`='{$this->iNumberOfAwards}' where `id`='{$this->iID}'";
		$sResult = $conn->query($sQuery);
		if(!$sResult){
			return FALSE;
		}
		return TRUE;
	}

	/*
		This function is used to get total number of casts
	*/

	function fGetAllCastCount(){
		$DBMan = new DBConnManager();
		$conn = $DBMan->getConnInstance();
		$sProfileTable = DATABASE_TABLE_PREFIX.'_profile';
		$sQuery = "select COUNT(*) from `{sProfileTable}` and deleted=0";
		$sResult = $conn->query($sQuery);
		if($sResult){
			$aCount = $sResult->fetch_array();
			$iCount = $aCount[0];
		}
		return $iCount;
	}

	/*
		This function is used to get list of cast according to the number of awards achieved
	*/

	function fGetCastByNumberOfAwards($iNumber){
		$DBMan = new DBConnManager();
		$conn = $DBMan->getConnInstance();
		$sProfileTable = DATABASE_TABLE_PREFIX.'_profile';
		$sCareerTable = DATABASE_TABLE_PREFIX.'_career';
		$sQuery = "Select * from `{$sProfileTable}` a, `{$sCareerTable}` b  where b.number_of_awards>='{$iNumber}' and a.id=b.id and a.deleted=0 and b.deleted=0";
		$sResult = $conn->query($sQuery);
		if($sResult){
			while($aRow=$sResult->fetch_array()){
				$aCastList[] = $aRow;
			}
		}
		return $aCastList;
	}

	/*
		This function is used to delete the cast
	*/

	function fDeleteCast($iID){
		$DBMan = new DBConnManager();
		$conn = $DBMan->getConnInstance();
		$sProfileTable = DATABASE_TABLE_PREFIX.'_profile';
		$sCareerTable = DATABASE_TABLE_PREFIX.'_career';
		$sQuery = "Update `{$sProfileTable}` set deleted = 1 where `id`='{$iID}'";
		$sResult = $conn->query($sQuery);
		if($sResult){
			$sQuery = "Update `{$sCareerTable}` set deleted =1 where `id`='{$iID}'";
			$sResult1 = $conn->query($sQuery);
			if(!$sResult1){
				return FALSE;
			}
		}
		return TRUE;
	}
}
?>