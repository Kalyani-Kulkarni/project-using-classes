

CREATE TABLE `cph_pathology_test_sample_mapping` (

  `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'unique record id',

  `service_id` INT(11) DEFAULT NULL COMMENT 'diagnostic service master id',

  `service_type_id` INT(11) DEFAULT NULL COMMENT 'diagnostic service type id',

  `sample_id` INT(11) DEFAULT NULL COMMENT 'sample master id',

  `is_compulsory` INT(5) DEFAULT NULL COMMENT '1- sample collection is compusory, 0 - is not complulsory',

  `status` INT(5) DEFAULT NULL COMMENT '1- valid record, 0- invalid record',

  PRIMARY KEY (`id`),

  KEY `service_id` (`service_id`),

  KEY `service_type_id` (`service_type_id`),

  KEY `sample_id` (`sample_id`),

  KEY `status` (`status`)

) ENGINE=INNODB AUTO_INCREMENT=28760 DEFAULT CHARSET=latin1;



INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (1,1,6,1,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (2,8,6,2,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (3,206,6,1,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (4,207,6,2,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (5,208,6,1,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (6,209,6,1,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (7,210,6,1,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (8,212,6,1,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (9,9,17,1,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (10,10,17,1,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (11,13,17,1,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (12,2,7,2,0,0);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (13,14,7,2,0,0);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (14,15,7,2,0,0);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (15,16,7,2,0,0);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (16,17,7,2,0,0);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (17,3,1,3,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (18,100,1,3,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (19,101,1,4,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (20,102,1,4,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (21,103,1,5,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (22,104,1,6,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (23,105,1,7,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (24,106,1,8,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (25,107,1,9,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (26,108,1,10,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (27,214,1,3,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (28,215,1,3,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (29,216,1,11,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (30,135,2,7,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (31,4,2,7,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (32,219,2,7,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (33,116,2,7,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (34,140,2,7,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (35,128,2,7,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (36,147,2,7,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (37,153,2,7,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (38,123,2,7,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (39,136,2,8,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (40,110,2,8,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (41,221,2,8,0,0);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (42,221,2,8,0,0);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (43,221,2,8,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (44,118,2,8,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (45,142,2,8,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (46,130,2,8,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (47,148,2,8,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (48,154,2,8,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (49,124,2,8,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (50,139,2,5,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (51,113,2,5,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (52,158,2,5,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (53,121,2,5,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (54,145,2,5,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (55,133,2,5,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (56,151,2,5,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (57,157,14,5,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (58,125,2,5,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (59,137,2,4,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (60,109,2,4,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (61,261,2,4,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (62,117,2,4,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (63,141,2,4,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (64,129,2,4,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (65,149,2,4,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (66,155,2,4,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (67,127,2,4,0,0);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (68,138,2,9,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (69,112,2,9,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (70,263,2,9,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (71,120,2,9,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (72,144,2,9,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (73,132,2,9,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (74,150,2,9,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (75,156,2,9,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (76,126,2,9,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (77,134,2,3,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (78,111,2,3,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (79,264,2,3,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (80,119,2,3,0,0);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (81,143,2,3,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (82,131,2,3,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (83,146,2,3,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (84,152,2,3,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (85,122,2,3,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (86,159,19,7,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (87,217,19,7,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (88,218,19,7,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (89,220,19,7,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (90,222,19,7,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (91,223,19,7,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (92,166,19,7,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (93,162,19,7,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (94,224,19,7,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (95,225,19,8,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (96,226,19,8,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (97,227,19,8,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (98,228,19,8,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (99,229,19,8,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (100,5,19,8,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (101,167,19,8,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (102,163,19,8,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (103,233,19,8,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (104,234,19,5,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (105,175,19,5,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (106,235,19,5,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (107,236,19,5,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (108,237,19,5,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (109,238,19,5,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (110,239,19,5,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (111,240,19,5,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (112,241,19,5,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (113,183,19,4,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (114,244,19,4,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (115,245,19,4,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (116,247,19,4,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (117,249,19,4,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (118,250,19,4,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (119,165,19,4,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (120,161,19,4,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (121,252,19,4,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (122,253,19,9,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (123,254,19,9,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (124,255,19,9,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (125,256,19,9,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (126,257,19,9,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (127,258,19,9,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (128,259,19,9,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (129,260,19,9,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (130,262,19,9,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (131,196,19,3,0,1);

INSERT  INTO `cph_pathology_test_sample_mapping`(`id`,`service_id`,`service_type_id`,`sample_id`,`is_compulsory`,`status`) VALUES (132,265,19,3,0,1);