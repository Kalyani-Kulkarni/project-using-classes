

CREATE TABLE `cph_pathology_test_sample_master` (

  `sample_id` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'unique record id and unique sample id',

  `sample_name` VARCHAR(50) DEFAULT NULL COMMENT 'unique sample name auto generated',

  `sample_label` VARCHAR(250) DEFAULT NULL COMMENT 'sample label value which is display to UI',

  `extra` VARCHAR(150) DEFAULT NULL COMMENT 'for future use',

  `status` INT(1) DEFAULT '1' COMMENT '1 for valid record, 0 for invalid record',

  PRIMARY KEY (`sample_id`),

  KEY `status` (`status`)

) ENGINE=INNODB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;



/*Data for the table `cph_pathology_test_sample_master` */



INSERT  INTO `cph_pathology_test_sample_master`(`sample_id`,`sample_name`,`sample_label`,`extra`,`status`) VALUES (1,'S-00001','Serum','',1);

INSERT  INTO `cph_pathology_test_sample_master`(`sample_id`,`sample_name`,`sample_label`,`extra`,`status`) VALUES (2,'S-00002','Dried blood Spot','',1);

INSERT  INTO `cph_pathology_test_sample_master`(`sample_id`,`sample_name`,`sample_label`,`extra`,`status`) VALUES (3,'S-00003','Venous Blood','',1);

INSERT  INTO `cph_pathology_test_sample_master`(`sample_id`,`sample_name`,`sample_label`,`extra`,`status`) VALUES (4,'S-00004','Fetal Blood','',1);

INSERT  INTO `cph_pathology_test_sample_master`(`sample_id`,`sample_name`,`sample_label`,`extra`,`status`) VALUES (5,'S-00005','Cord Blood','',1);

INSERT  INTO `cph_pathology_test_sample_master`(`sample_id`,`sample_name`,`sample_label`,`extra`,`status`) VALUES (6,'S-00006','Bone Marrow','',1);

INSERT  INTO `cph_pathology_test_sample_master`(`sample_id`,`sample_name`,`sample_label`,`extra`,`status`) VALUES (7,'S-00007','Amniotic Fluid','',1);

INSERT  INTO `cph_pathology_test_sample_master`(`sample_id`,`sample_name`,`sample_label`,`extra`,`status`) VALUES (8,'S-00008','Chorionic Villi','',1);

INSERT  INTO `cph_pathology_test_sample_master`(`sample_id`,`sample_name`,`sample_label`,`extra`,`status`) VALUES (9,'S-00009','Product of Conception','',1);

INSERT  INTO `cph_pathology_test_sample_master`(`sample_id`,`sample_name`,`sample_label`,`extra`,`status`) VALUES (10,'S-00010','Cystic Fluid','',1);

INSERT  INTO `cph_pathology_test_sample_master`(`sample_id`,`sample_name`,`sample_label`,`extra`,`status`) VALUES (11,'S-00011','Skin','',1);

INSERT  INTO `cph_pathology_test_sample_master`(`sample_id`,`sample_name`,`sample_label`,`extra`,`status`) VALUES (12,'S-00012','Extracted DNA','',1);

INSERT  INTO `cph_pathology_test_sample_master`(`sample_id`,`sample_name`,`sample_label`,`extra`,`status`) VALUES (13,'S-00013','Extracted DNA','',1);

INSERT  INTO `cph_pathology_test_sample_master`(`sample_id`,`sample_name`,`sample_label`,`extra`,`status`) VALUES (14,'S-00014','Dried Blood spot','',1);

INSERT  INTO `cph_pathology_test_sample_master`(`sample_id`,`sample_name`,`sample_label`,`extra`,`status`) VALUES (15,'S-00015','Venous Blood','',1);

INSERT  INTO `cph_pathology_test_sample_master`(`sample_id`,`sample_name`,`sample_label`,`extra`,`status`) VALUES (16,'S-00016','Whole Blood','',1);

INSERT  INTO `cph_pathology_test_sample_master`(`sample_id`,`sample_name`,`sample_label`,`extra`,`status`) VALUES (17,'S-00017','Saliva Swab','',1);

INSERT  INTO `cph_pathology_test_sample_master`(`sample_id`,`sample_name`,`sample_label`,`extra`,`status`) VALUES (18,'S-00018','DNA-AF','',1);

INSERT  INTO `cph_pathology_test_sample_master`(`sample_id`,`sample_name`,`sample_label`,`extra`,`status`) VALUES (19,'S-00019','DNA-CV','',1);

INSERT  INTO `cph_pathology_test_sample_master`(`sample_id`,`sample_name`,`sample_label`,`extra`,`status`) VALUES (21,'S-00021','DNA','',1);

INSERT  INTO `cph_pathology_test_sample_master`(`sample_id`,`sample_name`,`sample_label`,`extra`,`status`) VALUES (23,'S-00023','Product Of Conception','',1);

