

CREATE TABLE `cph_hcp_services` (

  `id` INT(11) NOT NULL AUTO_INCREMENT,

  `hcp_id` INT(11) DEFAULT NULL,

  `service_id` INT(11) DEFAULT NULL,

  `status` INT(1) DEFAULT '1' COMMENT '1-valid, 0-invalid',

  PRIMARY KEY (`id`),

  KEY `hcp_id` (`hcp_id`),

  KEY `service_id` (`service_id`),

  KEY `status` (`status`)

) ENGINE=INNODB AUTO_INCREMENT=214 DEFAULT CHARSET=latin1;



/*Data for the table `cph_hcp_services` */



INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (1,1,5,0);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (2,1,158,0);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (3,1,159,0);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (4,1,160,0);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (5,1,161,0);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (6,1,162,0);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (7,1,163,0);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (8,1,164,0);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (9,1,165,0);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (10,1,166,0);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (11,1,167,0);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (12,1,168,0);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (13,1,169,0);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (14,1,170,0);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (15,1,171,0);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (16,1,172,0);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (17,1,173,0);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (18,1,174,0);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (19,1,175,0);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (20,1,176,0);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (21,1,177,0);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (22,1,178,0);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (23,1,179,0);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (24,1,180,0);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (25,1,181,0);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (26,1,182,0);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (27,1,183,0);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (28,1,184,0);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (29,1,185,0);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (30,1,186,0);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (31,1,187,0);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (32,1,188,0);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (33,1,189,0);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (34,1,190,0);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (35,1,191,0);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (36,1,192,0);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (37,1,193,0);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (38,1,194,0);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (39,1,195,0);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (40,1,196,0);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (41,1,197,0);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (42,1,198,0);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (43,1,199,0);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (44,1,200,0);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (45,1,5,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (46,2,105,0);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (47,2,114,0);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (48,3,105,0);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (49,3,128,0);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (50,4,105,0);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (51,4,135,0);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (52,6,3,0);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (53,6,152,0);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (54,7,101,0);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (55,7,155,0);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (56,8,105,0);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (57,8,153,0);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (58,9,106,0);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (59,9,154,0);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (60,10,3,0);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (61,10,146,0);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (62,11,101,0);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (63,11,149,0);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (64,12,105,0);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (65,12,147,0);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (66,13,106,0);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (67,13,148,0);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (68,14,105,0);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (69,14,116,0);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (72,16,105,0);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (73,16,140,0);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (76,18,100,0);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (77,18,146,0);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (78,19,100,0);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (79,19,134,0);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (80,4,105,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (81,4,135,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (82,2,105,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (83,2,4,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (84,17,105,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (85,17,219,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (86,14,105,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (87,14,116,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (88,16,105,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (89,16,140,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (90,3,105,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (91,3,128,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (92,12,105,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (93,12,147,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (94,8,105,0);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (95,8,153,0);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (96,8,105,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (97,8,153,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (98,20,105,0);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (99,20,123,0);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (100,21,106,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (101,21,136,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (102,22,106,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (103,22,110,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (104,23,106,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (105,23,221,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (106,24,106,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (107,24,118,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (108,25,106,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (109,25,142,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (110,26,106,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (111,26,130,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (112,13,106,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (113,13,148,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (114,9,106,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (115,9,154,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (116,27,106,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (117,27,124,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (118,28,103,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (119,28,139,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (120,30,103,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (121,30,113,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (122,31,103,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (123,31,158,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (124,32,103,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (125,32,121,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (126,33,103,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (127,33,145,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (128,34,103,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (129,34,133,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (130,35,103,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (131,35,151,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (132,36,103,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (133,36,271,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (134,37,103,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (135,37,125,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (136,38,101,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (137,38,137,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (138,39,101,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (139,39,109,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (140,40,101,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (141,40,261,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (142,41,101,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (143,41,117,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (144,42,101,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (145,42,141,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (146,43,101,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (147,43,129,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (148,11,101,0);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (149,11,149,0);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (150,7,101,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (151,7,155,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (152,44,101,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (153,44,127,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (154,45,107,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (155,45,138,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (156,46,107,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (157,46,112,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (158,47,107,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (159,47,263,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (160,48,107,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (161,48,120,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (162,49,107,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (163,49,144,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (166,51,107,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (167,51,150,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (168,52,107,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (169,52,156,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (170,53,107,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (171,53,126,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (172,54,3,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (173,54,134,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (174,55,3,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (175,55,111,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (176,56,3,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (177,56,264,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (178,57,3,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (179,57,119,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (182,50,107,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (183,50,132,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (184,58,3,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (185,58,143,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (188,59,3,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (189,59,131,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (190,10,3,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (191,10,146,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (192,6,3,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (193,6,152,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (194,11,101,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (195,11,149,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (196,60,105,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (197,60,19,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (198,61,106,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (199,61,6,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (200,63,107,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (201,63,23,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (202,18,100,0);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (203,18,146,0);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (204,19,100,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (205,19,134,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (206,20,105,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (207,20,123,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (208,64,3,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (209,64,122,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (210,15,100,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (211,15,152,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (212,18,100,1);

INSERT  INTO `cph_hcp_services`(`id`,`hcp_id`,`service_id`,`status`) VALUES (213,18,146,1);
