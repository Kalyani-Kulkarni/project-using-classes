CREATE TABLE `mxcel_combi_billing_master_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `added_on` datetime DEFAULT NULL,
  `added_by` int(11) DEFAULT NULL,
  `deleted` tinyint(2) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `added_on` (`added_on`),
  KEY `added_by` (`added_by`),
  KEY `deleted` (`deleted`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1