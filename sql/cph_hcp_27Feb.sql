CREATE TABLE `cph_hcp` (

  `hcp_id` INT(11) NOT NULL AUTO_INCREMENT,

  `name` VARCHAR(100) DEFAULT NULL,

  `is_patho_profile` TINYINT(1) NOT NULL DEFAULT '0',

  `hcp_code` VARCHAR(20) DEFAULT 'NULL' COMMENT 'hcp service code',

  `sample_id` INT(11) DEFAULT NULL COMMENT 'Sample id',

  `added_by` INT(11) DEFAULT NULL,

  `added_on` DATETIME DEFAULT NULL,

  `updated_by` INT(11) DEFAULT NULL,

  `updated_on` DATETIME DEFAULT NULL,

  `status` INT(1) DEFAULT '1' COMMENT '1-valid, 0-invalid, 2-frozen',

  `test_group` VARCHAR(20) DEFAULT NULL COMMENT 'Test group for tally',

  PRIMARY KEY (`hcp_id`),

  KEY `status` (`status`),

  KEY `is_patho_profile` (`is_patho_profile`)

) ENGINE=INNODB AUTO_INCREMENT=65 DEFAULT CHARSET=latin1;

INSERT  INTO `cph_hcp`(`hcp_id`,`name`,`is_patho_profile`,`hcp_code`,`sample_id`,`added_by`,`added_on`,`updated_by`,`updated_on`,`status`,`test_group`) VALUES (2,'Karyotyping & FISH for Aneuploidy Screen  (13, 18, 21 PROBES)',0,'FAF02C',7,29,'2018-03-31 06:56:32',29,'2018-03-31 06:56:32',1,'KARYOTYPING/ FISH');

INSERT  INTO `cph_hcp`(`hcp_id`,`name`,`is_patho_profile`,`hcp_code`,`sample_id`,`added_by`,`added_on`,`updated_by`,`updated_on`,`status`,`test_group`) VALUES (3,'Karyotyping & FISH for Chromosome 21 ',0,'FAF06C',7,29,'2018-03-31 06:57:31',29,'2018-03-31 06:57:31',1,'KARYOTYPING/ FISH');

INSERT  INTO `cph_hcp`(`hcp_id`,`name`,`is_patho_profile`,`hcp_code`,`sample_id`,`added_by`,`added_on`,`updated_by`,`updated_on`,`status`,`test_group`) VALUES (4,'Karyotyping & FISH for 22q Microdeletion ',0,'FAF01C',7,25,'2018-03-31 07:10:26',25,'2018-03-31 07:10:26',1,'KARYOTYPING/ FISH');

INSERT  INTO `cph_hcp`(`hcp_id`,`name`,`is_patho_profile`,`hcp_code`,`sample_id`,`added_by`,`added_on`,`updated_by`,`updated_on`,`status`,`test_group`) VALUES (6,'Karyotyping & FISH for Prader Willi / Angelman Syndrome ',0,'FAF08C',7,25,'2018-03-31 07:12:27',25,'2018-03-31 07:12:27',1,'KARYOTYPING/ FISH');

INSERT  INTO `cph_hcp`(`hcp_id`,`name`,`is_patho_profile`,`hcp_code`,`sample_id`,`added_by`,`added_on`,`updated_by`,`updated_on`,`status`,`test_group`) VALUES (7,'Karyotyping & FISH for Prader Willi / Angelman Syndrome ',0,'FCV08C',8,25,'2018-03-31 07:13:42',25,'2018-03-31 07:13:42',1,'KARYOTYPING/ FISH');

INSERT  INTO `cph_hcp`(`hcp_id`,`name`,`is_patho_profile`,`hcp_code`,`sample_id`,`added_by`,`added_on`,`updated_by`,`updated_on`,`status`,`test_group`) VALUES (8,'Karyotyping & FISH for Prader Willi / Angelman Syndrome',0,'FCB08C',5,25,'2018-03-31 07:16:38',25,'2018-03-31 07:16:38',1,'KARYOTYPING/ FISH');

INSERT  INTO `cph_hcp`(`hcp_id`,`name`,`is_patho_profile`,`hcp_code`,`sample_id`,`added_by`,`added_on`,`updated_by`,`updated_on`,`status`,`test_group`) VALUES (9,'Karyotyping & FISH for Prader Willi / Angelman Syndrome',0,'FFB08C',4,25,'2018-03-31 07:17:53',25,'2018-03-31 07:17:53',1,'KARYOTYPING/ FISH');

INSERT  INTO `cph_hcp`(`hcp_id`,`name`,`is_patho_profile`,`hcp_code`,`sample_id`,`added_by`,`added_on`,`updated_by`,`updated_on`,`status`,`test_group`) VALUES (10,'Karyotyping & FISH for Miller Dieker Syndrome ',0,'FAF07C',7,25,'2018-03-31 07:18:54',25,'2018-03-31 07:18:54',1,'KARYOTYPING/ FISH');

INSERT  INTO `cph_hcp`(`hcp_id`,`name`,`is_patho_profile`,`hcp_code`,`sample_id`,`added_by`,`added_on`,`updated_by`,`updated_on`,`status`,`test_group`) VALUES (11,'Karyotyping & FISH for Miller Dieker Syndrome ',0,'FCV07C',8,25,'2018-03-31 07:19:51',25,'2018-03-31 07:19:51',1,'KARYOTYPING/ FISH');

INSERT  INTO `cph_hcp`(`hcp_id`,`name`,`is_patho_profile`,`hcp_code`,`sample_id`,`added_by`,`added_on`,`updated_by`,`updated_on`,`status`,`test_group`) VALUES (12,'Karyotyping & FISH for Miller Dieker Syndrome',0,'FCB07C',5,25,'2018-03-31 07:20:36',25,'2018-03-31 07:20:36',1,'KARYOTYPING/ FISH');

INSERT  INTO `cph_hcp`(`hcp_id`,`name`,`is_patho_profile`,`hcp_code`,`sample_id`,`added_by`,`added_on`,`updated_by`,`updated_on`,`status`,`test_group`) VALUES (13,'Karyotyping & FISH for Miller Dieker Syndrome',0,'FFB07C',4,25,'2018-03-31 07:21:16',25,'2018-03-31 07:21:16',1,'KARYOTYPING/ FISH');

INSERT  INTO `cph_hcp`(`hcp_id`,`name`,`is_patho_profile`,`hcp_code`,`sample_id`,`added_by`,`added_on`,`updated_by`,`updated_on`,`status`,`test_group`) VALUES (14,'Karyotyping & FISH for Chromosome 13 ',0,'FAF04C',7,25,'2018-03-31 07:22:19',25,'2018-03-31 07:22:19',1,'KARYOTYPING/ FISH');

INSERT  INTO `cph_hcp`(`hcp_id`,`name`,`is_patho_profile`,`hcp_code`,`sample_id`,`added_by`,`added_on`,`updated_by`,`updated_on`,`status`,`test_group`) VALUES (15,'Karyotyping with High Resolution Banding & FISH for Prader-Will / Angelmann Syndrome',0,'FVB12C',3,25,'2018-03-31 07:23:45',25,'2018-03-31 07:23:45',1,'KARYOTYPING/ FISH');

INSERT  INTO `cph_hcp`(`hcp_id`,`name`,`is_patho_profile`,`hcp_code`,`sample_id`,`added_by`,`added_on`,`updated_by`,`updated_on`,`status`,`test_group`) VALUES (16,'Karyotyping & FISH for Chromosome 18',0,'FAF05C',7,25,'2018-03-31 07:24:31',25,'2018-03-31 07:24:31',1,'KARYOTYPING/ FISH');

INSERT  INTO `cph_hcp`(`hcp_id`,`name`,`is_patho_profile`,`hcp_code`,`sample_id`,`added_by`,`added_on`,`updated_by`,`updated_on`,`status`,`test_group`) VALUES (17,'Karyotyping & FISH for Aneuploidy Screen  (13,18,21, X, Y Probes)',0,'FAF03C',7,25,'2018-03-31 07:25:20',25,'2018-03-31 07:25:20',1,'KARYOTYPING/ FISH');

INSERT  INTO `cph_hcp`(`hcp_id`,`name`,`is_patho_profile`,`hcp_code`,`sample_id`,`added_by`,`added_on`,`updated_by`,`updated_on`,`status`,`test_group`) VALUES (18,'Karyotyping with High resolution banding & FISH for Miller Dieker ',0,'FVB10C',3,25,'2018-03-31 07:28:29',25,'2018-03-31 07:28:29',1,'KARYOTYPING/ FISH');

INSERT  INTO `cph_hcp`(`hcp_id`,`name`,`is_patho_profile`,`hcp_code`,`sample_id`,`added_by`,`added_on`,`updated_by`,`updated_on`,`status`,`test_group`) VALUES (19,'KARYOTYPING with High Resolution Banding & FISH for 22q Microdeletion',0,'FVB11C',3,25,'2018-03-31 07:29:31',25,'2018-03-31 07:29:31',1,'KARYOTYPING/ FISH');

INSERT  INTO `cph_hcp`(`hcp_id`,`name`,`is_patho_profile`,`hcp_code`,`sample_id`,`added_by`,`added_on`,`updated_by`,`updated_on`,`status`,`test_group`) VALUES (20,'Karyotyping & FISH for X, Y PROBES',0,'FVB09C',3,25,'2018-05-29 07:40:22',25,'2018-05-29 07:40:22',1,'KARYOTYPING/ FISH');

INSERT  INTO `cph_hcp`(`hcp_id`,`name`,`is_patho_profile`,`hcp_code`,`sample_id`,`added_by`,`added_on`,`updated_by`,`updated_on`,`status`,`test_group`) VALUES (21,'Karyotyping & FISH for 22q Microdeletion',0,'FCV01C',8,25,'2018-05-29 07:43:23',25,'2018-05-29 07:43:23',1,'KARYOTYPING/ FISH');

INSERT  INTO `cph_hcp`(`hcp_id`,`name`,`is_patho_profile`,`hcp_code`,`sample_id`,`added_by`,`added_on`,`updated_by`,`updated_on`,`status`,`test_group`) VALUES (22,'Karyotyping & FISH for Aneuploidy Screen  (13, 18, 21 PROBES)',0,'FCV02C',8,25,'2018-05-29 07:45:21',25,'2018-05-29 07:45:21',1,'KARYOTYPING/ FISH');

INSERT  INTO `cph_hcp`(`hcp_id`,`name`,`is_patho_profile`,`hcp_code`,`sample_id`,`added_by`,`added_on`,`updated_by`,`updated_on`,`status`,`test_group`) VALUES (23,'Karyotyping & FISH for Aneuploidy Screen  (13,18,21, X, Y Probes)',0,'FCV03C',8,25,'2018-05-29 07:48:25',25,'2018-05-29 07:48:25',1,'KARYOTYPING/ FISH');

INSERT  INTO `cph_hcp`(`hcp_id`,`name`,`is_patho_profile`,`hcp_code`,`sample_id`,`added_by`,`added_on`,`updated_by`,`updated_on`,`status`,`test_group`) VALUES (24,'Karyotyping & FISH for Chromosome 13 ',0,'FCV04C',8,25,'2018-05-29 07:51:09',25,'2018-05-29 07:51:09',1,'KARYOTYPING/ FISH');

INSERT  INTO `cph_hcp`(`hcp_id`,`name`,`is_patho_profile`,`hcp_code`,`sample_id`,`added_by`,`added_on`,`updated_by`,`updated_on`,`status`,`test_group`) VALUES (25,'Karyotyping & FISH for Chromosome 18',0,'FCV05C',8,25,'2018-05-29 07:52:33',25,'2018-05-29 07:52:33',1,'KARYOTYPING/ FISH');

INSERT  INTO `cph_hcp`(`hcp_id`,`name`,`is_patho_profile`,`hcp_code`,`sample_id`,`added_by`,`added_on`,`updated_by`,`updated_on`,`status`,`test_group`) VALUES (26,'Karyotyping & FISH for Chromosome 21',0,'FCV06C',8,25,'2018-05-29 07:55:34',25,'2018-05-29 07:55:34',1,'KARYOTYPING/ FISH');

INSERT  INTO `cph_hcp`(`hcp_id`,`name`,`is_patho_profile`,`hcp_code`,`sample_id`,`added_by`,`added_on`,`updated_by`,`updated_on`,`status`,`test_group`) VALUES (27,'Karyotyping & FISH for X, Y PROBES ',0,'FAF09C',7,25,'2018-05-29 08:50:44',25,'2018-05-29 08:50:44',1,'KARYOTYPING/ FISH');

INSERT  INTO `cph_hcp`(`hcp_id`,`name`,`is_patho_profile`,`hcp_code`,`sample_id`,`added_by`,`added_on`,`updated_by`,`updated_on`,`status`,`test_group`) VALUES (28,'Karyotyping & FISH for 22q Microdeletion',0,'FCB01C',5,25,'2018-05-29 09:00:48',25,'2018-05-29 09:00:48',1,'KARYOTYPING/ FISH');

INSERT  INTO `cph_hcp`(`hcp_id`,`name`,`is_patho_profile`,`hcp_code`,`sample_id`,`added_by`,`added_on`,`updated_by`,`updated_on`,`status`,`test_group`) VALUES (30,'Karyotyping & FISH for Aneuploidy Screen  (13, 18, 21 PROBES)',0,'FCB02C',5,25,'2018-05-29 09:06:43',25,'2018-05-29 09:06:43',1,'KARYOTYPING/ FISH');

INSERT  INTO `cph_hcp`(`hcp_id`,`name`,`is_patho_profile`,`hcp_code`,`sample_id`,`added_by`,`added_on`,`updated_by`,`updated_on`,`status`,`test_group`) VALUES (31,'Karyotyping & FISH for Aneuploidy Screen  (13,18,21, X, Y Probes)',0,'FCB03C',5,25,'2018-05-29 09:08:00',25,'2018-05-29 09:08:00',1,'KARYOTYPING/ FISH');

INSERT  INTO `cph_hcp`(`hcp_id`,`name`,`is_patho_profile`,`hcp_code`,`sample_id`,`added_by`,`added_on`,`updated_by`,`updated_on`,`status`,`test_group`) VALUES (32,'Karyotyping & FISH for Chromosome 13',0,'FCB04C',5,25,'2018-05-29 09:11:17',25,'2018-05-29 09:11:17',1,'KARYOTYPING/ FISH');

INSERT  INTO `cph_hcp`(`hcp_id`,`name`,`is_patho_profile`,`hcp_code`,`sample_id`,`added_by`,`added_on`,`updated_by`,`updated_on`,`status`,`test_group`) VALUES (33,'Karyotyping & FISH for Chromosome 18',0,'FCB05C',5,25,'2018-05-29 09:12:29',25,'2018-05-29 09:12:29',1,'KARYOTYPING/ FISH');

INSERT  INTO `cph_hcp`(`hcp_id`,`name`,`is_patho_profile`,`hcp_code`,`sample_id`,`added_by`,`added_on`,`updated_by`,`updated_on`,`status`,`test_group`) VALUES (34,'Karyotyping & FISH for Chromosome 21',0,'FCB06C',5,25,'2018-05-29 09:14:19',25,'2018-05-29 09:14:19',1,'KARYOTYPING/ FISH');

INSERT  INTO `cph_hcp`(`hcp_id`,`name`,`is_patho_profile`,`hcp_code`,`sample_id`,`added_by`,`added_on`,`updated_by`,`updated_on`,`status`,`test_group`) VALUES (35,'Karyotyping & FISH for Miller Dieker Syndrome',0,'FPC07C',9,25,'2018-05-29 09:16:40',25,'2018-05-29 09:16:40',1,'KARYOTYPING/ FISH');

INSERT  INTO `cph_hcp`(`hcp_id`,`name`,`is_patho_profile`,`hcp_code`,`sample_id`,`added_by`,`added_on`,`updated_by`,`updated_on`,`status`,`test_group`) VALUES (36,'Karyotyping & FISH for Prader Willi / Angelman Syndrome ',0,'FPC08C',9,25,'2018-05-29 09:26:56',25,'2018-05-29 09:26:56',1,'KARYOTYPING/ FISH');

INSERT  INTO `cph_hcp`(`hcp_id`,`name`,`is_patho_profile`,`hcp_code`,`sample_id`,`added_by`,`added_on`,`updated_by`,`updated_on`,`status`,`test_group`) VALUES (37,'Karyotyping & FISH for X, Y PROBES',0,'FCV09C',8,25,'2018-05-29 09:30:35',25,'2018-05-29 09:30:35',1,'KARYOTYPING/ FISH');

INSERT  INTO `cph_hcp`(`hcp_id`,`name`,`is_patho_profile`,`hcp_code`,`sample_id`,`added_by`,`added_on`,`updated_by`,`updated_on`,`status`,`test_group`) VALUES (38,'Karyotyping & FISH for 22q Microdeletion',0,'FFB01C',4,25,'2018-05-29 09:33:49',25,'2018-05-29 09:33:49',1,'KARYOTYPING/ FISH');

INSERT  INTO `cph_hcp`(`hcp_id`,`name`,`is_patho_profile`,`hcp_code`,`sample_id`,`added_by`,`added_on`,`updated_by`,`updated_on`,`status`,`test_group`) VALUES (39,'Karyotyping & FISH for Aneuploidy Screen  (13, 18, 21 PROBES)',0,'FFB02C',4,25,'2018-05-29 09:35:29',25,'2018-05-29 09:35:29',1,'KARYOTYPING/ FISH');

INSERT  INTO `cph_hcp`(`hcp_id`,`name`,`is_patho_profile`,`hcp_code`,`sample_id`,`added_by`,`added_on`,`updated_by`,`updated_on`,`status`,`test_group`) VALUES (40,'Karyotyping & FISH for Aneuploidy Screen  (13,18,21, X, Y Probes)',0,'FFB03C',4,25,'2018-05-29 09:36:58',25,'2018-05-29 09:36:58',1,'KARYOTYPING/ FISH');

INSERT  INTO `cph_hcp`(`hcp_id`,`name`,`is_patho_profile`,`hcp_code`,`sample_id`,`added_by`,`added_on`,`updated_by`,`updated_on`,`status`,`test_group`) VALUES (41,'Karyotyping & FISH for Chromosome 13 ',0,'FFB04C',4,25,'2018-05-29 09:37:52',25,'2018-05-29 09:37:52',1,'KARYOTYPING/ FISH');

INSERT  INTO `cph_hcp`(`hcp_id`,`name`,`is_patho_profile`,`hcp_code`,`sample_id`,`added_by`,`added_on`,`updated_by`,`updated_on`,`status`,`test_group`) VALUES (42,'Karyotyping & FISH for Chromosome 18 ',0,'FFB05C',4,25,'2018-05-29 09:39:33',25,'2018-05-29 09:39:33',1,'KARYOTYPING/ FISH');

INSERT  INTO `cph_hcp`(`hcp_id`,`name`,`is_patho_profile`,`hcp_code`,`sample_id`,`added_by`,`added_on`,`updated_by`,`updated_on`,`status`,`test_group`) VALUES (43,'Karyotyping & FISH for Chromosome 21',0,'FFB06C',4,25,'2018-05-29 09:41:07',25,'2018-05-29 09:41:07',1,'KARYOTYPING/ FISH');

INSERT  INTO `cph_hcp`(`hcp_id`,`name`,`is_patho_profile`,`hcp_code`,`sample_id`,`added_by`,`added_on`,`updated_by`,`updated_on`,`status`,`test_group`) VALUES (44,'Karyotyping & FISH for X, Y PROBES',0,'FCB09C',5,25,'2018-05-29 09:44:50',25,'2018-05-29 09:44:50',1,'KARYOTYPING/ FISH');

INSERT  INTO `cph_hcp`(`hcp_id`,`name`,`is_patho_profile`,`hcp_code`,`sample_id`,`added_by`,`added_on`,`updated_by`,`updated_on`,`status`,`test_group`) VALUES (45,'Karyotyping & FISH for 22q Microdeletion ',0,'FPC01C',9,25,'2018-05-29 09:46:14',25,'2018-05-29 09:46:14',1,'KARYOTYPING/ FISH');

INSERT  INTO `cph_hcp`(`hcp_id`,`name`,`is_patho_profile`,`hcp_code`,`sample_id`,`added_by`,`added_on`,`updated_by`,`updated_on`,`status`,`test_group`) VALUES (46,'Karyotyping & FISH for Aneuploidy Screen  (13, 18, 21 PROBES)',0,'FPC02C',9,25,'2018-05-29 09:47:06',25,'2018-05-29 09:47:06',1,'KARYOTYPING/ FISH');

INSERT  INTO `cph_hcp`(`hcp_id`,`name`,`is_patho_profile`,`hcp_code`,`sample_id`,`added_by`,`added_on`,`updated_by`,`updated_on`,`status`,`test_group`) VALUES (47,'Karyotyping & FISH for Aneuploidy Screen  (13,18,21, X, Y Probes)',0,'FPC03C',9,25,'2018-05-29 09:50:54',25,'2018-05-29 09:50:54',1,'KARYOTYPING/ FISH');

INSERT  INTO `cph_hcp`(`hcp_id`,`name`,`is_patho_profile`,`hcp_code`,`sample_id`,`added_by`,`added_on`,`updated_by`,`updated_on`,`status`,`test_group`) VALUES (48,'Karyotyping & FISH for Chromosome 13 ',0,'FPC04C',9,25,'2018-05-29 09:53:50',25,'2018-05-29 09:53:50',1,'KARYOTYPING/ FISH');

INSERT  INTO `cph_hcp`(`hcp_id`,`name`,`is_patho_profile`,`hcp_code`,`sample_id`,`added_by`,`added_on`,`updated_by`,`updated_on`,`status`,`test_group`) VALUES (49,'Karyotyping & FISH for Chromosome 18',0,'FPC05C',9,25,'2018-05-29 09:55:38',25,'2018-05-29 09:55:38',1,'KARYOTYPING/ FISH');

INSERT  INTO `cph_hcp`(`hcp_id`,`name`,`is_patho_profile`,`hcp_code`,`sample_id`,`added_by`,`added_on`,`updated_by`,`updated_on`,`status`,`test_group`) VALUES (50,'Karyotyping & FISH for Chromosome 21',0,'FPC06C',9,25,'2018-05-29 09:58:50',25,'2018-05-29 09:58:50',1,'KARYOTYPING/ FISH');

INSERT  INTO `cph_hcp`(`hcp_id`,`name`,`is_patho_profile`,`hcp_code`,`sample_id`,`added_by`,`added_on`,`updated_by`,`updated_on`,`status`,`test_group`) VALUES (51,'Karyotyping & FISH for Miller Dieker Syndrome ',0,'FVB07C',3,25,'2018-05-29 10:00:13',25,'2018-05-29 10:00:13',1,'KARYOTYPING/ FISH');

INSERT  INTO `cph_hcp`(`hcp_id`,`name`,`is_patho_profile`,`hcp_code`,`sample_id`,`added_by`,`added_on`,`updated_by`,`updated_on`,`status`,`test_group`) VALUES (52,'Karyotyping & FISH for Prader Willi / Angelman Syndrome ',0,'FVB08C',3,25,'2018-05-29 10:01:43',25,'2018-05-29 10:01:43',1,'KARYOTYPING/ FISH');

INSERT  INTO `cph_hcp`(`hcp_id`,`name`,`is_patho_profile`,`hcp_code`,`sample_id`,`added_by`,`added_on`,`updated_by`,`updated_on`,`status`,`test_group`) VALUES (53,'Karyotyping & FISH for X, Y PROBES ',0,'FFB09C',4,25,'2018-05-29 10:02:53',25,'2018-05-29 10:02:53',1,'KARYOTYPING/ FISH');

INSERT  INTO `cph_hcp`(`hcp_id`,`name`,`is_patho_profile`,`hcp_code`,`sample_id`,`added_by`,`added_on`,`updated_by`,`updated_on`,`status`,`test_group`) VALUES (54,'Karyotyping & FISH for 22q Microdeletion',0,'FVB01C',3,25,'2018-05-29 10:09:34',25,'2018-05-29 10:09:34',1,'KARYOTYPING/ FISH');

INSERT  INTO `cph_hcp`(`hcp_id`,`name`,`is_patho_profile`,`hcp_code`,`sample_id`,`added_by`,`added_on`,`updated_by`,`updated_on`,`status`,`test_group`) VALUES (55,'Karyotyping & FISH for Aneuploidy Screen  (13, 18, 21 PROBES)',0,'FVB02C',3,25,'2018-05-29 10:10:22',25,'2018-05-29 10:10:22',1,'KARYOTYPING/ FISH');

INSERT  INTO `cph_hcp`(`hcp_id`,`name`,`is_patho_profile`,`hcp_code`,`sample_id`,`added_by`,`added_on`,`updated_by`,`updated_on`,`status`,`test_group`) VALUES (56,'Karyotyping & FISH for Aneuploidy Screen  (13,18,21, X, Y Probes)',0,'FVB03C',3,25,'2018-05-29 10:10:49',25,'2018-05-29 10:10:49',1,'KARYOTYPING/ FISH');

INSERT  INTO `cph_hcp`(`hcp_id`,`name`,`is_patho_profile`,`hcp_code`,`sample_id`,`added_by`,`added_on`,`updated_by`,`updated_on`,`status`,`test_group`) VALUES (57,'Karyotyping & FISH for Chromosome 13 ',0,'FVB04C',3,25,'2018-05-29 10:11:49',25,'2018-05-29 10:11:49',1,'KARYOTYPING/ FISH');

INSERT  INTO `cph_hcp`(`hcp_id`,`name`,`is_patho_profile`,`hcp_code`,`sample_id`,`added_by`,`added_on`,`updated_by`,`updated_on`,`status`,`test_group`) VALUES (58,'Karyotyping & FISH for Chromosome 18',0,'FVB05C',3,25,'2018-05-29 10:20:20',25,'2018-05-29 10:20:20',1,'KARYOTYPING/ FISH');

INSERT  INTO `cph_hcp`(`hcp_id`,`name`,`is_patho_profile`,`hcp_code`,`sample_id`,`added_by`,`added_on`,`updated_by`,`updated_on`,`status`,`test_group`) VALUES (59,'Karyotyping & FISH for Chromosome 21',0,'FVB06C',3,25,'2018-05-29 10:21:51',25,'2018-05-29 10:21:51',1,'KARYOTYPING/ FISH');

INSERT  INTO `cph_hcp`(`hcp_id`,`name`,`is_patho_profile`,`hcp_code`,`sample_id`,`added_by`,`added_on`,`updated_by`,`updated_on`,`status`,`test_group`) VALUES (60,'Karyotyping & Prenatal BoBs ',0,'PBKT01',7,162,'2018-06-09 07:01:49',162,'2018-06-09 07:01:49',1,'BoBs / Karyotyping');

INSERT  INTO `cph_hcp`(`hcp_id`,`name`,`is_patho_profile`,`hcp_code`,`sample_id`,`added_by`,`added_on`,`updated_by`,`updated_on`,`status`,`test_group`) VALUES (61,'Karyotyping & Prenatal BoBs',0,'PBKT02',8,162,'2018-06-09 07:02:22',162,'2018-06-09 07:02:22',1,'BoBs / Karyotyping');

INSERT  INTO `cph_hcp`(`hcp_id`,`name`,`is_patho_profile`,`hcp_code`,`sample_id`,`added_by`,`added_on`,`updated_by`,`updated_on`,`status`,`test_group`) VALUES (63,'Karyotyping & Karoylite BoBs',0,'KLKT01',9,162,'2018-06-09 07:07:00',162,'2018-06-09 07:07:00',1,'BoBs / Karyotyping');

INSERT  INTO `cph_hcp`(`hcp_id`,`name`,`is_patho_profile`,`hcp_code`,`sample_id`,`added_by`,`added_on`,`updated_by`,`updated_on`,`status`,`test_group`) VALUES (64,'Karyotyping & FISH for X, Y PROBES ',0,'FPC09C',9,162,'2018-06-09 09:29:49',162,'2018-06-09 09:29:49',1,'KARYOTYPING/ FISH');