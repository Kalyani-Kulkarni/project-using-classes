<?php

include_once "classes/class.SessionManager.php";
include_once "classes/class.DBConnManager.php";
include_once "config/config.database.php";
include_once "config/config.medixcel.php";
include_once "classes/class.User.php";
include_once "functions.php";

$sUsername = $_POST['idUsername'];
$sPassword = $_POST['idPassword'];

$oSessionManager = new SessionManager();

if($oSessionManager->fAuthenticateUser($sUsername, $sPassword))
{
	header('location:CustomerDashboard.php');
}
else
{
	$sMsg = array();
	$sMsg[] = "E3";
	fRedirectWithAlert("login.php", $sMsg);
}

?>