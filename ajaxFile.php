<?php

include_once "classes/class.User.php";
include_once "classes/class.SessionManager.php";
include_once "functions.php";

header('Content-Type: application/json');

$sFlag=isset($_REQUEST['sFlag'])?$_REQUEST['sFlag']:0;

if($sFlag == 0)
{
	echo false;
}

//Adds New User
if($sFlag=='AddUser')
{
	$sName=$_REQUEST['sName'];
    $sEmail=$_REQUEST['sEmail'];
    $sUsername=$_REQUEST['sUsername'];
    $sPassword=$_REQUEST['sPassword'];
    $iType=3;
    $oUser = new User();
    $oUser->sName = $sName;
    $oUser->sUsername = $sUsername;
    $oUser->sEmail = $sEmail;
    $oUser->iUserTypeID = $iType;
	$iResult = $oUser->fAddUser($sPassword);
	echo(json_encode($iResult));
}

//Adds Customer List Price
if($sFlag=='AddCustomerListPrice')
{

	$iCustomerType = $_POST['idCustomerType'];
	$iYear = $_POST['idYear'];
	$iServiceType = $_POST['idServiceID'];
	$iPrice = $_POST['idPrice'];
	$iCurrencyID = $_POST['idCurrencyID'];
	$iUserID = $_POST['idUserId'];
	$sResult = addCustomerListPrice($iCustomerType, $iYear, $iServiceType, $iPrice, $iCurrencyID, $iUserID);
	echo(json_encode($sResult));
}

//Gets data for Table "Combi Billing Detail"
if($sFlag=='GetCombiBillingList')
{
	$iStart = isset($_REQUEST['start']) ? $_REQUEST['start']:0;
	$aFilters = array();
	$iLength =  isset($_REQUEST['length']) ? $_REQUEST['length']:0;
	
	// $aServiceList = getAllCombiBillingRecords('','');
	// $iTotalRecords = count($aServiceList);

	if(isset($_GET['iSampleTypeID']) && $_GET['iSampleTypeID']!='') {
		

		$aFilters['iSampleTypeID'] = $_GET['iSampleTypeID'];
	}

	if(isset($_GET['iPackageID']) && $_GET['iPackageID']!='') {
		

		$aFilters['iPackageID'] = $_GET['iPackageID'];
	}

	if(isset($_GET['iServiceID']) && $_GET['iServiceID']!='') {

		$aFilters['iServiceID'] = $_GET['iServiceID'];
	}

	$aServiceList = getAllCombiBillingRecords($iStart, $iLength, $aFilters, false);
	$iFilterCount = getAllCombiBillingRecords($iStart, 0, $aFilters, true);

	foreach ($aServiceList as $key => $value) {
		$iSampleId = $value['sample_id'];
		$sSampleType = $value['sample_label'];
		$iPackageId = $value['hcp_id'];
		$sPackageName = $value['name'];
		$iServiceId = $value['service_id'];
		$sServiceName = $value['service_name'];

		$aFilter['iServiceId'] = $iServiceId;
		$aCombiBillingMaster = getAllCombiBillingMaster($aFilter);

		if (count($aCombiBillingMaster)>0) {
			if($aCombiBillingMaster[0]['test_completed_successfully_percentage'] == 0)
			{
				$iCompletedSuccessfullyPercent = "";
			}
			else{
				$iCompletedSuccessfullyPercent = $aCombiBillingMaster[0]['test_completed_successfully_percentage'];
			}

			if($aCombiBillingMaster[0]['test_completed_successfully_amount'] == 0)
			{
				$iCompletedSuccessfullyAmount = "";
			}
			else{
				$iCompletedSuccessfullyAmount = $aCombiBillingMaster[0]['test_completed_successfully_amount'];
			}

            if ($aCombiBillingMaster[0]['test_rejected_percentage'] == 0) {
                $iTestRejectedPercent = "";
            }else{
                $iTestRejectedPercent = $aCombiBillingMaster[0]['test_rejected_percentage'];
            }

            if ($aCombiBillingMaster[0]['test_rejected_amount'] == 0) {
                $iTestRejectedAmount = "";
            }else{
                $iTestRejectedAmount = $aCombiBillingMaster[0]['test_rejected_amount'];
            }

            if ($aCombiBillingMaster[0]['test_cancelled_percentage'] == 0) {
                $iTestCancelledPercent = "";
            }else{
                $iTestCancelledPercent = $aCombiBillingMaster[0]['test_cancelled_percentage'];
            }

            if ($aCombiBillingMaster[0]['test_cancelled_amount'] == 0) {
                $iTestCancelledAmount = "";
            }else{
                $iTestCancelledAmount = $aCombiBillingMaster[0]['test_cancelled_amount'];
            }
        }else{
        	$iCompletedSuccessfullyPercent = "";
            $iCompletedSuccessfullyAmount = "";
            $iTestRejectedPercent = "";
            $iTestRejectedAmount = "";
            $iTestCancelledPercent = "";
            $iTestCancelledAmount = "";
        }

		$aTableData[] = array(
			$sSampleType ,
			$sPackageName,
			$sServiceName,
			'<input type="number" min="0" name="idTestCompletePercent[]" id="idTestCompletePercent_'.$iServiceId.''.$iPackageId.'" class="classTestCompletePercent" value="'.$iCompletedSuccessfullyPercent.'" style="width:60px;"">',
			'<input type="number" min="0" name="idTestCompleteAmount[]" id="idTestCompleteAmount_'.$iServiceId.''.$iPackageId.'" class="classTestCompleteAmount" value="'.$iCompletedSuccessfullyAmount.'" style="width:100px;">',
			'<input type="number" min="0" name="idTestRejectPercent[]" id="idTestRejectPercent_'.$iServiceId.''.$iPackageId.'" style="width:60px;" class="classTestRejectPercent" value="'.$iTestRejectedPercent.'">',
			'<input type="number" min="0" name="idTestRejectAmount[]" id="idTestRejectAmount_'.$iServiceId.''.$iPackageId.'" class="classTestRejectAmount" style="width:100px;" value="'.$iTestRejectedAmount.'">',
			'<input type="number" min="0" name="idTestCancelPercent[]" id="idTestCancelPercent_'.$iServiceId.''.$iPackageId.'" style="width:60px;" class="classTestCancelPercent" value="'.$iTestCancelledPercent.'">',
			'<input type="number" min="0" name="idTestCancelAmount[]" id="idTestCancelAmount_'.$iServiceId.''.$iPackageId.'" class="classTestCancelAmount" style="width:100px;" value="'.$iTestCancelledAmount.'">'.
			'<input type="hidden" value="'.$iSampleId.'" id="idHiddenSampleId" name="idHiddenSampleId[]">'.
			'<input type="hidden" value="'.$iPackageId.'" id="idHiddenPackageId" name="idHiddenPackageId[]"">'.
			'<input type="hidden" value="'.$iServiceId.'" id="idHiddenServiceId" name="idHiddenServiceId[]">'
		);
	}

	$response = array('data'=>array(), 'draw'=>$_GET['draw'], "recordsTotal"=>$iFilterCount, "recordsFiltered"=>$iFilterCount );

	foreach ($aTableData as $colData) {
			$response['data'][] = array_values($colData);
		}
	echo(json_encode($response));
}

//Adds Combi Billing List
if($sFlag=='AddCombiBilling')
{
	$iUserID = $_POST['idUserID'];
	$aSampleId = $_POST['idHiddenSampleId'];
	$aPackageId = $_POST['idHiddenPackageId'];
	$aServiceId = $_POST['idHiddenServiceId'];
	$aTcsPercent = $_POST['idTestCompletePercent'];
	$aTcsAmount = $_POST['idTestCompleteAmount'];
	$aTrPercent = $_POST['idTestRejectPercent'];
	$aTrAmount = $_POST['idTestRejectAmount'];
	$aTcPercent = $_POST['idTestCancelPercent'];
	$aTcAmount = $_POST['idTestCancelAmount'];


	for ($iii=0; $iii < count($aServiceId) ; $iii++) {

		invalidateCombiBill($aServiceId[$iii], $aPackageId[$iii]);

		$fTestCompleteAmount  =  (isset($aTcsAmount[$iii]) && $aTcsAmount[$iii] != "") ? $aTcsAmount[$iii] : 0;
		$fTestRejectAmount  = (isset($aTrAmount[$iii]) && $aTrAmount[$iii] != "") ? $aTrAmount[$iii] : 0;
		$fTestCancelAmount = (isset($aTcAmount[$iii]) && $aTcAmount[$iii] != "") ? $aTcAmount[$iii] : 0;
		$fTestCompletePercent = (isset($aTcsPercent[$iii]) && $aTcsPercent[$iii] != "" ) ? $aTcsPercent[$iii] : 0;
		$fTestRejectPercent  = (isset($aTrPercent[$iii]) && $aTrPercent[$iii] != "" ) ? $aTrPercent[$iii] : 0;
		$fTestCancelPercent = (isset($aTcPercent[$iii]) && $aTcPercent[$iii] != "") ? $aTcPercent[$iii] : 0;

		$sResult = addCombiBillingMaster($aSampleId[$iii], $aPackageId[$iii], $aServiceId[$iii], $fTestCompleteAmount, $fTestRejectAmount, $fTestCancelAmount, $fTestCompletePercent, $fTestRejectPercent, $fTestCancelPercent, $iUserID);
	}

	echo(json_encode($sResult));
}

if($sFlag == "SearchSample")
{
	$aFilters['sSampleName'] = $_GET['sSampleName'];
	$aFilters['iIsGroupBy'] = isset($_GET['iIsGroupBy']) ? $_GET['iIsGroupBy'] : 0;
	$aSamples = getAllServiceSamples($aFilters);
	echo(json_encode($aSamples));
}

if($sFlag == "SearchPackage")
{
	$aFilters['sPackageName'] = $_GET['sPackageName'];
	$aFilters['iIsGroupBy'] = isset($_GET['iIsGroupBy']) ? $_GET['iIsGroupBy'] : 0;
	$aPackages = getAllPackageNames($aFilters);
	echo(json_encode($aPackages));
}

if($sFlag == "SearchTest")
{
	$aFilters['sTestName'] = $_GET['sTestName'];
	$aFilters['iIsGroupBy'] = isset($_GET['iIsGroupBy']) ? $_GET['iIsGroupBy'] : 0;
	$aServices = getAllServiceNames($aFilters);
	echo(json_encode($aServices));
}
?>