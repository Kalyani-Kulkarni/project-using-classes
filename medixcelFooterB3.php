<style type="text/css">
	.classMedixcelFooter{
		width: 97%;
		position: fixed;
		bottom: 1rem;
		background-color: #3E4651;
	}
</style>

<script type="text/javascript" src="js/plugins/noty/packaged/jquery.noty.packaged.min.js"></script>
<script type="text/javascript" src="assets/noty/lib/noty.js"></script>
<script type="text/javascript" src="js/plugins/noty/themes/notyEhrTheme.js"></script>
</div>
</div>
<div class="classMedixcelFooter">
	<div class="container-fluid">
		<div class="row text-white">
			<div class="col-md-3">
				Support : 
				<a class="" href="http://www.twitter.com/plus91" target="_blank"><i class="fa fa-twitter-square btn-info" aria-hidden="true"></i></a>
				| Phone: +91 22 6230 1716/17
			</div>
			<div class="col-md-6 col-md-offset-3 text-right">
				Copyright &copy; <?php echo $iYear ?> <a href='http://www.plus91.in' target='_blank' style="color:white;"><b>Plus91 Technologies.</b></a> Powered By Medixcel-EHR.
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(function() {
        $(".navbar-expand-toggle").click(function() {
            $(".app-container").toggleClass("expanded");
            return $(".navbar-expand-toggle").toggleClass("fa-rotate-90");
        });
    });
</script>

<script type="text/javascript">
	function generateNotification(type, msg) {
		var n = noty({
            text        : msg,
            type        : type,
            dismissQueue: true,
            closeWith   : ['click'],
            layout      : 'topRight',
            theme       : 'notyEhrTheme',
            maxVisible  : 10
        });
	}
</script>

<?php 
if(ENABLE_DOCTOR_AVAILABILITY_MODAL){
	?>
	<style>
		.fc-time-grid .fc-slats td { min-height: 30px; height: 30px}
		.fc-unthemed .fc-today {background-color: white}
		.fc-bgevent{opacity: 0.7}
		.fc-event .fc-content{padding: 3px; font-size: 1.2em}
		.fc-event .fc-content .fc-time{display: inline-block;}
		.fc-event .fc-content .fc-title{display: inline-block;margin-left: 5px}
		.fc-highlight{border: 1px solid blue}
		.classDoctorEventScheduled{ cursor: pointer; }
		.classDoctorAvailInfoColor{
	    	display: inline-block;
	    	margin-bottom: 10px;
	    	color: black;
	    	line-height: 20px;
	    	margin-right: 20px;
		}
		.classDoctorAvailInfoColorbox{
			float: left;
			display: inline-block;
			background: black;
	    	width: 20px;
	    	height: 20px;
	    	margin-right: 5px;
	    	border: 1px solid black;
		}
		.classDoctorAvailModal .select2-results__option{
			color:black;
		}
	</style>
	<!--Doctor Availability Modal-->
	<div class="modal fade classDoctorAvailModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" id="idDoctorAvailModal">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel">Doctor Availability</h4>
				</div>
				<div class="modal-body">
					<div id="idDoctorAvailModalController">
						<div class="row">
							<div class="col-md-3">
								<select id="idDoctorAvailModalDoctorControl">
									<option value="">Select <?php echo(DOCTOR_LABEL) ?></option>
								</select>
							</div>
							<div class="col-md-3">
								<select id="idDoctorAvailModalClinicControl">
									<option value="">Select <?php echo(EHR_LABLE_FOR_CLINIC) ?></option>
								</select>
							</div>
							<div class="col-md-2 text-right">
							 	<div class="input-group">
									<input type="text" class="form-control" id="idDoctorAvailModalDate" value="<?php echo(date('d-m-Y')) ?>" readonly>
									<span class="input-group-addon" id="basic-addon2"><i class="fa fa-calendar"></i></span>
								</div>
							</div>
							<div class="col-md-2 text-left">
								<div class="btn-group" id="idDoctorAvailModalCalendarTab" data-toggle="buttons-radio">
									<button type="button" data-view="agendaWeek" class="btn btn-primary active">Week</button>
									<button type="button" data-view="agendaDay" class="btn btn-primary">Day</button>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-5 text-left">
								<p style="font-size: 14px; color: black;">Select available timeslot to add appointment for <?php echo(strtolower(PATIENT_OR_MEMBER)) ?>:</p>
							</div>
							<div class="col-md-7 text-right">
								<span class="classDoctorAvailInfoColor">
									<span class="classDoctorAvailInfoColorbox" style="background: white"></span>
									<strong>Not Available</strong>
								</span>
								<span class="classDoctorAvailInfoColor">
									<span class="classDoctorAvailInfoColorbox" style="background: lightgreen"></span>
									<strong>Available</strong>
								</span>
								<span class="classDoctorAvailInfoColor">
									<span class="classDoctorAvailInfoColorbox" style="background: blue"></span>
									<strong>Scheduled</strong>
								</span>
								<span class="classDoctorAvailInfoColor">
									<span class="classDoctorAvailInfoColorbox" style="background: red"></span>
									<strong>Leave</strong>
								</span>
							</div>
						</div>
					</div>
					<div id="idDoctorAvailCalendar"></div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
	<script>
		(function() {
			$('#idDoctorAvailModalCalendarTab').button();

			$('#idDoctorAvailModalCalendarTab button').on('click', function(event) {
				event.preventDefault();
				
				$('#idDoctorAvailCalendar').fullCalendar( 'changeView', $(this).data('view') );
			});

			$('#idDoctorAvailModalDate').datepicker({
	            changeMonth: true,
	            dateFormat: '<?php echo DEFINED_DATE_FORMAT_FOR_JS_DATEPICKER_3; ?>',
	            changeYear: true,
	            showOn: "button",
	            buttonImage: "images/Calendar-icon.png",
	            buttonImageOnly: true,
	            onSelect: function(date) {
					$('#idDoctorAvailCalendar').fullCalendar( 'gotoDate', moment(date,"<?php echo DEFINED_DATE_FORMAT_FOR_JS_DATEPICKER; ?>") );
	            }
	        });

			$("#idDoctorAvailModalDoctorControl").select2({
				width: '100%',
				placeholder: 'Select <?php echo(DOCTOR_LABEL) ?>',
				dropdownParent: $("#idDoctorAvailModal"),
				allowClear: true
			});

			$("#idDoctorAvailModalClinicControl").select2({
				width: '100%',
				placeholder: 'Select Clinic',
				dropdownParent: $("#idDoctorAvailModal"),
				allowClear: true
			});

			$("#idDoctorAvailModalDoctorControl").on('change', function() {
				
				// Get Clinic of the doctor
				var iStaffID = $(this).val();

				$("#idDoctorAvailModalClinicControl option[value!='']").remove();

				if(iStaffID){
					$.ajax({
						url: "apiv2/staffs/"+iStaffID+"/clinics"
					}).done(function(aClinics) {
						$.each(aClinics, function(index, aClinic){
							$("#idDoctorAvailModalClinicControl").append("<option value='"+aClinic.clinic_id+"'>"+aClinic.clinic_name+"</option>");
						});
					});
				}
			});

			$("#idDoctorAvailModalDoctorControl, #idDoctorAvailModalClinicControl").on('change', function() {
				
				$('#idDoctorAvailCalendar').fullCalendar( 'refetchEvents' );
			});
			
			$("#idDoctorAvailModal").on('shown.bs.modal', function () {
				rescale();

				$("#idDoctorAvailModalDoctorControl option[value!='']").remove();

				// Load Doctors
				$.ajax({
					url: "ajaxEhr.php?sFlag=SearchDoctorByFilters",
					data: {
						iIsWellnewssProvider: '<?php echo($iIsWellnewssProvider) ?>',
						iClinicID: '<?php echo($iStaffClinicID) ?>'
					}
				}).done(function(aDoctors){
					$.each(aDoctors, function(index, aDoctor){
						$("#idDoctorAvailModalDoctorControl").append("<option value='"+aDoctor.iStaffID+"'>"+aDoctor.sStaffName+"</option>");
					});

					$("#idDoctorAvailModalDoctorControl").trigger("change");
				});

				$('#idDoctorAvailCalendar').fullCalendar({
					defaultView: 'agendaWeek',
					allDaySlot: false,
					height: $('#idDoctorAvailCalendar').closest('.modal-body').height() - $("#idDoctorAvailModalController").height() - 30,
					slotDuration: '00:20:00',
					slotLabelInterval: '00:20:00',
					minTime: "07:00:00",
					maxTime: "22:22:00",
					selectable: true,
					selectConstraint: function(){
						return "background";
					},
					select: function( start, end, jsEvent, view){
						var displayDate = start.format("<?php echo DEFINED_DATE_FORMAT_FOR_JS_DATEPICKER; ?>"),
							displayTime = start.format("hh:mm A"),
							time = start.format("HH:mm"),
							$calendar = $('#idDoctorAvailCalendar'),
							allowedDuration = "<?php echo(DEFAULT_APPOINTMENT_TIMESLOT) ?>",
							iSelectedClinicID = $("#idDoctorAvailModalClinicControl").val();

						// Check if time slot is allowed timeslot
						if(confirm("Are you sure want to add schedule at "+displayDate+" "+displayTime+"?")){
							var $patientSearchModal = $('#idSerachPatientModal');

			            	$("#idDoctorAvailModal").modal("hide");
			            	$patientSearchModal.find("#idSchDate").val(displayDate);
			            	$patientSearchModal.find("#idTime").val(time);
			            	$patientSearchModal.find("#idTime option[value='"+time+"']").prop("selected", true);
			            	if(iSelectedClinicID){
			            		$patientSearchModal.find("#idApptClinic").val(iSelectedClinicID);
			            		$patientSearchModal.find("#idApptClinic option[value='"+iSelectedClinicID+"']").prop("selected", true);
			            	}
			            	$patientSearchModal.modal('show'); 
							$calendar.fullCalendar("unselect");
						} else {
							$calendar.fullCalendar("unselect");
						}
					},
    				selectOverlap: function(event) {
    					console.log(event);
				        return event.rendering === 'background';
				    },
					slotLabelFormat: {
					    agenda: 'hh:mm A'
					},
					columnFormat: {
			            week: 'D-M-YYYY ddd',
			            day: 'D-M-YYYY dddd'
			        },
					header: false,
					events: function(start, end, timezone, callback) {
						var iStaffID = $("#idDoctorAvailModalDoctorControl").val();
						var iClinicID = $("#idDoctorAvailModalClinicControl").val();

						if(iStaffID){
							$.ajax({
								url: 'ajaxEhr.php',
								data: {
									sFlag: "getDoctorAvailablityStatus",
									iStaffID: iStaffID,
									iClinicID: iClinicID,
									sStartDate: start.format("YYYY-MM-DD"),
									sEndDate: end.format("YYYY-MM-DD")
								},
								success: function(aStatus) {
					                var events = [];

									$.each(aStatus, function(sType, aTimeslots) {
										$.each(aTimeslots, function(index, aTimeslot) {
										 	events.push(createFullCalendarEventObject(sType, aTimeslot));
										});
									});

									callback(events);
								}
							});
						} else {
							callback([]);
						}
				    },
				    eventClick: function(event, jsEvent, view) {
				    	if(event.sType == "scheduled"){
				    		window.open("viewSchedule.php?iScheduleID="+event.aData.schedule_id, '_blank');
				    	}
				    },
				    eventRender: function(event, element){
					    if(event.sType == "available"){
					        element.append("<div class='text-center' style='opacity: 0.9'><strong>"+event.title+"</strong></div>");
					    }
					}
				});
			});

			$("#idDoctorAvailModal").on('hidden', function () {
				$('#idDoctorAvailCalendar').fullCalendar( 'destroy' );
			});

			function rescale(){
			    var size = {width: $(window).width() , height: $(window).height() };
			    var offset = 100;
			    var $modal = $('#idDoctorAvailModal');

			    $modal.css({
			    	'height': "90%",
			    	'top': "3%"
			    });

			    var iModalBodyHeight = $modal.outerHeight() - ($modal.find(".modal-header").outerHeight() + $modal.find(".modal-footer").outerHeight());

			    $modal.find('.modal-body').css({
			    	'maxHeight': iModalBodyHeight,
			    	'height': iModalBodyHeight
			    });
			}

			function createFullCalendarEventObject(sType, aData) {
				var allDay = false;
					start = "",
					end = "",
					backgroundColor = "",
					textColor = "",
					overlap = sType != "available",
					title = ""
					url = "";

				if(sType == "available"){
					start = moment(aData.sDate+" "+aData.from_time);
					end = moment(aData.sDate+" "+aData.to_time);
					backgroundColor = "lightgreen";
					className = "classDoctorEventAvailable";
					title = aData.clinic_name;
					textColor = "white";
				} else if(sType == "scheduled"){
					start = moment(aData.schedule_date+" "+aData.schedule_time);
					end = moment(aData.schedule_date+" "+aData.schedule_end_time);
					backgroundColor = "blue";
					textColor = "white";
					title = aData.patient_name;
					className = "classDoctorEventScheduled";
				} else {
					start = moment(aData.start_date+" 00:07:00");
					end = moment(aData.end_date+" 22:40:00");
					backgroundColor = "red";
					textColor = "white";
					title = "Leave";
					className = "classDoctorEventLeave";
				}


				var event = {
					title: title,
					className: className,
					allDay: allDay,
					start: start,
					end: end,
					overlap:overlap,
					backgroundColor: backgroundColor,
					textColor: textColor,
					
					sType: sType,
					aData: aData
				};

				if(sType == "available"){
					event['rendering'] = "background";
				}

				return event;
			}

			$(window).bind("resize", rescale);
		})()
	</script>
	<?php
}
?>
<?php
	// include add task functionality
	include_once ABS_PATH_TO_EHR."addTaskFunctionalityB3.php";
?>
<?php
	// include call functionality
	include_once ABS_PATH_TO_EHR."callWidgetHelper.php";
?>
<script type="text/javascript">
	//Get current date
	function getCurrentDate(sFormate, sOption=''){
		var dTodayDate = new Date();

		if(sOption == "last_month"){			
			var dLastMonthDate = new Date(dTodayDate.setMonth(dTodayDate.getMonth() - 1));
			dTodayDate = dLastMonthDate;
		}else if(sOption == "last_3rd_month"){			
			var dLast3rdMonthDate = new Date(dTodayDate.setMonth(dTodayDate.getMonth() - 3));
			dTodayDate = dLast3rdMonthDate;
		}else if(sOption == "year_after_current_date"){			
			var dYearAfterTemp = new Date(dTodayDate.setMonth(dTodayDate.getMonth() + 12 ));
			var dYearAfterCurrentDate = new Date(dYearAfterTemp.setDate((dYearAfterTemp.getDate() - 1)));
			dTodayDate = dYearAfterCurrentDate;
		}

		var dTodayDay = dTodayDate.getDate();
		if(dTodayDay < 10){
            dTodayDay = "0"+dTodayDay;
        }

		var dTodayMonth = dTodayDate.getMonth() + 1;
		if(dTodayMonth < 10){
            dTodayMonth = "0"+dTodayMonth;
        }

		var dTodayYear = dTodayDate.getFullYear();
		var dTodayHour = dTodayDate.getHours();
		var dTodayMinute = dTodayDate.getMinutes();
		var dTodaySeconds = dTodayDate.getSeconds();

		var dCurrentDate = '';		

		if(sFormate == "dd-mm-yy"){
			var dCurrentDate = dTodayDay +"-"+ dTodayMonth +"-"+ dTodayYear;
		}else if(sFormate == "yy-mm-dd"){
			var dCurrentDate = dTodayYear +"-"+ dTodayMonth +"-"+ dTodayDay;
		}else if(sFormate == "dd/mm/yy"){
			var dCurrentDate = dTodayDay +"/"+ dTodayMonth +"/"+ dTodayYear;
		}else if(sFormate == "yy/mm/dd"){
			var dCurrentDate = dTodayYear +"/"+ dTodayMonth +"/"+ dTodayDay;
		}else if(sFormate == "h:i:s"){
			var dCurrentDate = dTodayHour +":"+ dTodayMinute +":"+ dTodaySeconds;
		}else if(sFormate == "h:00"){
			var dCurrentDate = dTodayHour +":00";
		}		
		return dCurrentDate;
	}	
</script>

<style type="text/css">
	.special-modal-header{
		padding: 15px;
	    border-bottom: 1px solid #e5e5e5;
	    height: 50px;	    
	    text-align: center;	    
	}

	.special-modal-header h3{
		font-size: 15px;
		color: darkblue;
	}
	.classModalLarge{
		height: 520px;
	}

	#idSerachPatientModal input, select, textarea {
		border: 1px solid lightgray;
		border-radius: 4px; 
		padding: 5px;
		margin-bottom: 2px;
	}
	#mandatory{
		color: red;
	}
	.classMandatorySign{
		color: red;		
	}
	.classTableData{
		margin-left: 10px;
	}
	.classHidden{
		display: none;
	}
</style>

<!-- Modal -->
<div id="idSerachPatientModal" class="modal fade classSearchPatientModal" role="dialog" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="myModalForSerachPatientLabel" aria-hidden="true">
    <div class="modal-dialog" style="width: 100% !important;">				    
      <!-- Modal content-->
      <div class="modal-content" >
        <div class="modal-header special-modal-header">
          <h3 class="modal-title myModalForSerachPatientLabel">Medixcel - Search <?php echo PATIENT_OR_MEMBER;?></h3>
        </div>
        <div class="modal-body classSearchPatientModalBody classModalLarge">
          	<div class='classDashbaordModalDiv'>
		      	<form class='classFormFormat' method='post' id="idFormPatientVisit" >
		        <?php 
		          include_once ABS_PATH_TO_EHR.'searchPatientModal.php';
		        ?>
		        <div id='divButton' align='center' style="width:100%;float:left;margin-top:10px;margin-bottom:5px;">
		          <?php
		            if(!WELLNESS_PROVIDER) {
		              ?>
		                <input class='btn btn-warning classAddNewPatientButton' type='button' value='Add Visit For New <?php echo PATIENT_OR_MEMBER;?>' id='idAddNewPatient' name='addNewPatient' style='float:left;'/>
		              <?php
		            }

		          ?>
		          <div style="float:right;">
		            <?php
		                if($iUserTypeId != 12){
		                ?>
		              	<input class='btn btn-info classUpdatePatientButton' type='button' value='Add Visit By Updating <?php echo PATIENT_OR_MEMBER;?> Details' id='idUpdatePatient' name='updatePatient' disabled />
		            <?php
		              }
		            ?>
		            <input class='btn btn-primary' type='button' value='Add <?php echo PATIENT_OR_MEMBER;?> Visit' id='idSubmitPatientRegistration' name='patient_visit' onclick="fAddPatientVisit();"  />
		            <input class='btn btn-primary' type='button' value='Reset' onclick="fResetPatientData();" >
		          </div>
		        </div>
		      	</form>
		  	</div>
        </div>
        <br>
        <br>
        <div class="modal-footer">
        	<span style='float:left;margin-left:10px;margin-top:4px;'>
	      	<?php 
	     		$iCurrentYear = date('Y');
	      		$sModalFooter = "Copyright &copy $iCurrentYear <b><a href=http://www.plus91.in target=_blank >Plus91</a></b>. All Rights Reserved.";
	      		echo $sModalFooter;
	      	?>
	    	</span>
          <button type="button" class="btn btn-danger btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>				      
    </div>

    <?php
    if(ENABLE_USERVOICE===true) {
      ?>
      <script>
        // Include the UserVoice JavaScript SDK (only needed once on a page)
        UserVoice=window.UserVoice||[];(function(){var uv=document.createElement('script');uv.type='text/javascript';uv.async=true;uv.src='//widget.uservoice.com/H97Mv7k7QKKK98nUTo42iA.js';var s=document.getElementsByTagName('script')[0];s.parentNode.insertBefore(uv,s)})();

        //
        // UserVoice Javascript SDK developer documentation:
        // https://www.uservoice.com/o/javascript-sdk
        //

        // Set colors
        UserVoice.push(['set', {
          accent_color: '#448dd6',
          trigger_color: 'white',
          trigger_background_color: 'rgba(46, 49, 51, 0.6)',
          ticket_custom_fields: {"Client": 'MYCARE'}
        }]);

        // Identify the user and pass traits
        // To enable, replace sample data with actual user traits and uncomment the line
        UserVoice.push(['identify', {
          //email:      'john.doe@example.com', // User’s email address
          //name:       'John Doe', // User’s real name
          //created_at: 1364406966, // Unix timestamp for the date the user signed up
          //id:         123, // Optional: Unique id of the user (if set, this should not change)
          //type:       'Owner', // Optional: segment your users by type
          //account: {
          //  id:           123, // Optional: associate multiple users with a single account
          //  name:         'Acme, Co.', // Account name
          //  created_at:   1364406966, // Unix timestamp for the date the account was created
          //  monthly_rate: 9.99, // Decimal; monthly rate of the account
          //  ltv:          1495.00, // Decimal; lifetime value of the account
          //  plan:         'Enhanced' // Plan name for the account
          //}
        }]);

        // Add default trigger to the bottom-right corner of the window:
        UserVoice.push(['addTrigger', {mode: 'contact', trigger_position: 'bottom-right' }]);

        // Or, use your own custom trigger:
        //UserVoice.push(['addTrigger', '#id', { mode: 'contact' }]);

        // Autoprompt for Satisfaction and SmartVote (only displayed under certain conditions)
        UserVoice.push(['autoprompt', {}]);
      </script>
      <?php
    }
  ?>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$("#idSchDate").datepicker('destroy');
		$("#idSchDate").datepicker({
			format: '<?php echo DEFINED_DATE_FORMAT_FOR_JS_DATEPICKER_2; ?>',                   
            todayHighlight: true,
            autoclose: true,
            todayBtn: 'linked',
		});			
	});

	//Open Add appointment Modal
	function fOpenScheduleManageOnNewAppointmentCalendar(iPatientIdForAddAppointment, iAddAppointment){		
        $("#idSchDate").val(getCurrentDate('<?php echo DEFINED_DATE_FORMAT_FOR_JS_DATEPICKER_3; ?>'));        
        $('#idSerachPatientModal').modal('show');

		if(iAddAppointment == 1 && iPatientIdForAddAppointment > 0){
			setTimeout(function(){
				fSetPatientData(iPatientIdForAddAppointment);
			}, 300);
		} 
	}        	
</script>

<?php
	if(isset($_REQUEST['openScheduleManager'])) {
		?>
		<script type="text/javascript">
			fOpenScheduleManageOnNewAppointmentCalendar(<?php echo $_REQUEST['iPatientId'];?>, <?php echo $_REQUEST['iAddAppointment'];?>);
		</script>
		<?php
	}
?>

<!-- New Patient Details Pop-ups start-->
<?php
	if(ENABLE_PATIENT_DETAILS_POPUP_TAB_VIEW == true && $oSessionManager->isLoggedIn ){	
		include_once ABS_PATH_TO_EHR."patientRecordPopupViewB3.php";
	}
?>
<!-- New Patient Details Pop-ups ends-->

<?php
	// include add followup functionality
	include_once ABS_PATH_TO_EHR."medixcelFooterB3AddFollowupModal.php";
?>
<script type="text/javascript">
	function setJqueryCookie(key, value) {
        // var dExpires = new Date();
        // dExpires.setTime(dExpires.getTime() + (1 * 24 * 60 * 60 * 1000));
        // document.cookie = key + '=' + value + ';expires=' + dExpires.toUTCString();
        document.cookie = key + '=' + value + ';expires=';
    }

    function getJqueryCookie(key) {
        var keyValue = document.cookie.match('(^|;) ?' + key + '=([^;]*)(;|$)');
        return keyValue ? keyValue[2] : null;
    }

    $(window).load(function() {
        var sHrefPathname = window.location.pathname;
        var aHrefPageName = sHrefPathname.split('/');
        var aHrefPageNameLength = aHrefPageName.length;
        var sHrefPageName = aHrefPageName[aHrefPageNameLength - 1];
        if(sHrefPageName == 'appointmentCalendar.php'){
            setJqueryCookie('NAPPMNT_CLNDR_SCROLL_POSN_reload', 'Yes');
        }else{
            setJqueryCookie('NAPPMNT_CLNDR_SCROLL_POSN_reload', 'No');
       		setJqueryCookie('NAPPMNT_CLNDR_SCROLL_POSN', 0);
        }
    });
</script>
