<?php

include_once "sessionCheck.php";

$sPageTopTitle = "Quick Appointment";
$iID = $oSessionManager->iUserID;

$times = create_time_range('07:00', '24.00');

function create_time_range($start, $end, $interval = '30 mins', $format = '12') {
    $startTime = strtotime($start); 
    $endTime   = strtotime($end);
    $returnTimeFormat = ($format == '12')?'g:i A':'G:i';

    $current   = time(); 
    $addTime   = strtotime('+'.$interval, $current); 
    $diff      = $addTime - $current;

    $times = array(); 
    while ($startTime < $endTime) { 
        $times[] = date($returnTimeFormat, $startTime); 
        $startTime += $diff; 
    } 
    $times[] = date($returnTimeFormat, $startTime); 
    return $times; 
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
 	<?php include_once 'medixcelHeaderB3.php'; ?>
 	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="js/moment.js"></script>

    <style type="text/css">
        
        .form-control{
            height: 27px;
            font-size: 11px;
        }

        label{
            font-size: 13px;
        }

        .classHeader{
            background-color: #2a3542;
            color: white;
        }

        .classPatientNameBlock {
            color: white !important;
            text-align: left;
            margin-bottom: 0px;
            margin-left: 5%
        }

        .classHeaderDetailsBlockLeft {
            text-align: left;
            margin-top: 0px;
            width: 270px;
        }

        .classHeaderDetailsBlockRight {
            text-align: left;
            margin-top: 0px;
            width: 270px;
        }
    </style>
</head>
<body class="stickyMedixcelFooter flat-blue overflow-hidden">
<?php include_once 'medixcelNavbarB3.php'; ?>

	<div class="container-fluid classContainerBody">
        <div class="row mr-3">
            <div class="col-lg-12">
                <a href="#idModalAddQuickAppointment" class="btn btn-dark pull-right text-white" data-toggle="modal" name="idAddQuickAppointment" id="idAddQuickAppointment">Add Quick Appointment</a>
            </div>
        </div>
        <br>
	</div>

    <div class="modal fade" id="idModalAddQuickAppointment" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="classHeader">
                    <button type="button" class="close text-white" data-dismiss="modal">×</button>
                    <div class="row m-1 my-3">
                        <div class="col-lg-4 col-md-12 col-sm-12">
                            <div class="classPatientNameBlock">
                                <b><h4 id="idPatientName" name="idPatientName">John Smith</h4></b>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="classHeaderDetailsBlockLeft">
                                <p style="margin-bottom:0px;">
                                    <span id="idPatientId">Patient ID</span>:&nbsp;<span id="idPatientIdData">P001</span>
                                </p>
                                <p style="margin-bottom:0px;">
                                    <span id="idGender">Gender</span>:&nbsp;<span id="idPatientGenderData">Male</span>
                                </p>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="classHeaderDetailsBlockRight">
                                <p style="margin-bottom:0px;">
                                    <span id="idDob">DOB</span>:&nbsp;<span id="idPatientDobData">21-12-1995</span>
                                </p>
                                <p style="margin-bottom:0px;">
                                    <span id="idAge">Age</span>:&nbsp;<span id="idPatientAgeData">23 years</span>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <hr width="99%" style="border-width:3px;" class="m-1">
                <form class="classFormSubmitAppointment" id="idQuickAppointmentForm">
                    <div class="modal-body">
                        <div class="row-fluid">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4">
                                        <label for="idSelect">Select From :</label>
                                    </div>
                                    <div class="col-lg-8 col-md-8">
                                        <select name="idSelect" id="idSelect" class="form-control" onchange="changeOptions();">
                                            <option value="0">Select Any</option>
                                            <option value="1">Package</option>
                                            <option value="2">Service</option>
                                            <option value="3">Complementary</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4">
                                        <label for="idPackage">Package :</label>
                                    </div>
                                    <div class="col-lg-8 col-md-8">
                                        <select name="idPackage" id="idPackage" class="form-control">
                                            <option value="0">Select Any</option>
                                            <option value="1">Hair Reduction # Abdomen (Laser)</option>
                                            <option value="2">Laser Rejuvenation # Cheeks (Laser)</option>
                                            <option value="3">Scarlet # Calf Area (Laser)</option>
                                            <option value="4">U+Jetting # Full Face (Regular Skin Therapy)</option>
                                            <option value="5">D+Jetting # Face (Special Skin Therapy)</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4">
                                        <label for="idSearchService">Search Service :</label>
                                    </div>
                                    <div class="col-lg-8 col-md-8">
                                        <select name="idSearchService" id="idSearchService" class="form-control">
                                            <option value="0">Select Any</option>
                                            <option value="1">Eye Treatment # Eye</option>
                                            <option value="2">Hair Reduction # Eyebrows</option>
                                            <option value="3">Affirm # Face</option>
                                            <option value="4">Affirm # Eyes</option>
                                            <option value="5">Botox # Crows Feet</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4">
                                        <label for="idClinic">Clinic :</label>
                                    </div>
                                    <div class="col-lg-8 col-md-8">
                                        <select name="idClicnic" id="idClicnic" class="form-control">
                                            <option value="0">Select Any</option>
                                            <option value="1">PANCHSHEEL PARK CLINIC</option>
                                            <option value="2">GREATER KAILASH CLINIC</option>
                                            <option value="3">PUNJABI BAGH CLINIC</option>
                                            <option value="4">MEHARCHAND CLINIC</option>
                                            <option value="5">JCM CLINIC</option>
                                            <option value="6">SOUTHPOINT MALL CLINIC</option>
                                            <option value="7">CHANDIGARH CLINIC</option>
                                            <option value="8">QuickHeal Care</option>
                                            <option value="9">Anagha Clinic</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4">
                                        <label for="idStartTime">Start Time :</label>
                                    </div>
                                    <div class="col-lg-8 col-md-8">
                                        <select name="idStartTime" id="idStartTime" class="form-control">
                                            <option value="0">Select Time</option>
                                            <?php foreach($times as $key=>$val){ ?>
                                            <option value="<?php echo $val ?>"><?php echo $val ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4">
                                        <label for="idEndTime">End Time :</label>
                                    </div>
                                    <div class="col-lg-8 col-md-8">
                                       <select name="idEndTime" id="idEndTime" class="form-control">
                                            <option value="0">Select Time</option>
                                            <?php foreach($times as $key=>$val){ ?>
                                            <option value="<?php echo $val ?>"><?php echo $val ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4">
                                        <label for="idTherapist">Therapist :</label>
                                    </div>
                                    <div class="col-lg-8 col-md-8">
                                       <select name="idTherapist" id="idTherapist" class="form-control">
                                            <option value="0">Select Any</option>
                                            <option value="1">Barkha</option>
                                            <option value="2">Pooja Pradhan</option>
                                            <option value="3">Anupam Joshi</option>
                                            <option value="4">Pooja Kapoor</option>
                                            <option value="5">Pooja Arya</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4">
                                        <label for="idRoom">Room :</label>
                                    </div>
                                    <div class="col-lg-8 col-md-8">
                                       <select name="idRoom" id="idRoom" class="form-control">
                                            <option value="0">Select Any</option>
                                            <option value="1">Consultation Room 1</option>
                                            <option value="2">Therapy Room 1</option>
                                            <option value="3">Therapy Room 2</option>
                                            <option value="4">Therapy Room 3</option>
                                            <option value="5">Therapy Room 4</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4">
                                        <label for="idNotes">Notes :</label>
                                    </div>
                                    <div class="col-lg-8 col-md-8">
                                       <textarea class="form-control" id="idNotes" name="idNotes"></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4">
                                        <label for="idTag">Tags :</label>
                                    </div>
                                    <div class="col-lg-8 col-md-8">
                                       <input class="form-control" type="text" name="idTag" id="idTag">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="idUserId" id="idUserId" value="<?php echo $iID ?>">
                    </div>
                    <div class="modal-footer">
                       <input type='submit' name='idButtonAddAppointment' id='idButtonAddAppointment' value='Add Appointment' class='btn btn-info text-white'/>
                        <button type="button" class="btn btn-danger text-white btn-md" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script type="text/javascript">

        //Initially disables Package and Service dropdown list
        $(document).ready(function(){
            document.getElementById("idPackage").disabled=true;
            document.getElementById("idSearchService").disabled=true;
        });

        //Gives alert after addition of appointment
        $("#idButtonAddAppointment").on("click", function(event){
            alert("Appointment added successfully");
        });


        /* Enable or disable 'Package' and 'Service' dropdown list according to selection of 'Select From' dowpdown list */
        function changeOptions(){
            var iSelected = $('#idSelect').val();

            if(iSelected==0){
                $("#idPackage").val("0");
                $("#idSearchService").val("0");
                document.getElementById("idPackage").disabled=true;
                document.getElementById("idSearchService").disabled=true;
            }

            if(iSelected==1){
                $('#idSearchService option').show();
                $("#idPackage").val("0");
                $("#idSearchService").val("0");
                $('#idSearchService option[value="50"]').hide();
                document.getElementById("idPackage").disabled=false;
                document.getElementById("idSearchService").disabled=false;
            }

            if(iSelected==2){
                $('#idSearchService option').show();
                $("#idPackage").val("0");
                $("#idSearchService").val("0");
                $('#idSearchService option[value="50"]').hide();
                document.getElementById("idPackage").disabled=true;
                document.getElementById("idSearchService").disabled=false;
            }

            if(iSelected==3){
                $("#idPackage").val("0");
                $("#idSearchService").val("0");
                document.getElementById("idPackage").disabled=true;
                document.getElementById("idSearchService").disabled=false;
                $('#idSearchService option').hide();
                $('#idSearchService').append($("<option></option>").val("50").text("Complementary Consultation"));
            }
        }
    </script>
</body>
</html>