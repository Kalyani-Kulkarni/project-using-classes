//! Function to get samples..
function getAllSamplesForFishTest()
{
    var sSampleTableRow = '';
    var aCultureDoneIDs = [];
    $.ajax({
        url: "ajaxCytologyLabManagement.php?sFlag=getSamplesForFishTest",
        data: {iScheduleID:iScheduleID, iServiceID:iServiceID, iPatientID:iPatientID},
        async: true,
        type: 'POST',
        datatype: 'json',
        success: function(data)
        {   
            if($.trim(data) != false)
            {   
                var iTemp = 1;
                var sBarcode = '';
                $.each(data, function(key, value)
                {

                	if (value['sample_status'] == 1)
                	{
                		sBarcode = value['collection_sample_barcode'];
                	}else if (value['sample_status'] == 2)
                	{
                		sBarcode = value['accession_sample_barcode'];
                	}else if (value['sample_status'] == -1)
                	{
                		sBarcode = value['accession_sample_barcode'];
                	}else{
                		sBarcode = "N/A";	
                	}

                    sSampleTableRow += "<tr>";
                       sSampleTableRow += '<td>'+iTemp+'</td>';
                       sSampleTableRow += '<td>'+value["sample_label"]+'<input type="hidden" name="idSampleTypeForAutofill" id="idSampleTypeForAutofill_'+value['aSampleProcessMasterID']['cytology_sample_process_master_id']+'" class = "classSampleTypeForAutofill" value = "'+value['sample_label']+'"></td>';
                       sSampleTableRow += '<td>'+sBarcode+'</td>';
                       sSampleTableRow += '<td><span class="btn btn-primary btn-small classAddFishDayOneDetails" data-sample-id = '+value['sample_id']+' data-sample-request-id = '+value['sample_request_id']+' data-sample-process-master-id = '+value['aSampleProcessMasterID']['cytology_sample_process_master_id']+' id="idBtnAddCultureInfo" data-toggle="modal" data-target="#idModalAddFishDayOneInformation">Add</span></td>';
                    sSampleTableRow += "<tr>";
                });
                $(".classSamplesFISHTestDay1Day2").html(sSampleTableRow);
            }
        }
    })
}

//! Function to get samples..
function getAllSamplesForCytologyTestValidation()
{
    var sSampleTableRow = '';
    var aCultureDoneIDs = [];
    $.ajax({
        url: "ajaxCytologyLabManagement.php?sFlag=getSamplesForCytologyTest",
        data: {iScheduleID:iScheduleID, iServiceID:iServiceID, iPatientID:iPatientID},
        async: true,
        type: 'POST',
        datatype: 'json',
        success: function(data)
        {   
            if($.trim(data) != false)
            {   
                var iTemp = 1;
                var sBarcode = '';
                $.each(data, function(key, value)
                {

                    if (value['sample_status'] == 1)
                    {
                        sBarcode = value['collection_sample_barcode'];
                    }else if (value['sample_status'] == 2)
                    {
                        sBarcode = value['accession_sample_barcode'];
                    }else if (value['sample_status'] == -1)
                    {
                        sBarcode = value['accession_sample_barcode'];
                    }else{
                        sBarcode = "N/A";   
                    }

                    sSampleTableRow += "<tr>";
                       sSampleTableRow += '<td>'+iTemp+'</td>';
                       sSampleTableRow += '<td>'+value["sample_label"]+'<input type="hidden" name="idSpecimenForAutofill" id="idSpecimenForAutofill_'+value['aSampleProcessMasterID']['cytology_sample_process_master_id']+'" class = "classSpecimenForAutofill" value = "'+value['sample_label']+'"></td>';
                       sSampleTableRow += '<td>'+sBarcode+'</td>';
                       sSampleTableRow += '<td>'+value['tat_first']+'</td>';
                       sSampleTableRow += '<td>'+value['tat_final']+'</td>';

                       if (value['aSampleProcessMasterID']['bDay1Day2Done'] == true)
                       {
                           sSampleTableRow += '<td><span class="btn btn-primary btn-small classAddAneuploidy" data-sample-id = '+value['sample_id']+' data-sample-request-id = '+value['sample_request_id']+' data-sample-process-master-id = '+value['aSampleProcessMasterID']['cytology_sample_process_master_id']+' id="idBtnAddAneuploidy" data-toggle="modal" data-target="#idModalAddAneuploidy">Add</span></td>';
                           sSampleTableRow += '<td><span class="btn btn-primary btn-small classAddMicroDeletion" data-sample-id = '+value['sample_id']+' data-sample-request-id = '+value['sample_request_id']+' data-sample-process-master-id = '+value['aSampleProcessMasterID']['cytology_sample_process_master_id']+' id="idBtnAddHarvestInfo" data-toggle="modal" data-target="#idModalAddMicroDeletion">Add</span></td>';
                       }else{
                            sSampleTableRow += '<td><span class="btn btn-primary btn-small" disabled>Add</span></td>';
                            sSampleTableRow += '<td><span class="btn btn-primary btn-small" disabled>Add</span></td>';
                       }    

                    sSampleTableRow += "<tr>";

                    if (value['aSampleProcessMasterID']['is_culture_and_harvest_done'] == 1)
                    {
                        aCultureDoneIDs.push(value['aSampleProcessMasterID']['cytology_sample_process_master_id']);
                    }
                    
                    iTemp ++;
                });
                $(".classSamplesForCultureAndHarvest").html(sSampleTableRow);
            }
        }
    })
}

//! Function to get samples list for cytology..
function getSamplesForCytologyTestInternalControl(){
    var sSampleTableRow = '';
    $.ajax({
        url: "ajaxCytologyLabManagement.php?sFlag=getSamplesForCytologyTest",
        data: {iScheduleID:iScheduleID, iServiceID:iServiceID, iPatientID:iPatientID},
        async: true,
        type: 'POST',
        datatype: 'json',
        success: function(data)
        {
            if($.trim(data) != false)
            {
                var iTemp = 1;
                var sBarcode = '';
                $.each(data, function(key, value)
                {

                    if (value['sample_status'] == 1)
                    {
                        sBarcode = value['collection_sample_barcode'];
                    }else if (value['sample_status'] == 2)
                    {
                        sBarcode = value['accession_sample_barcode'];
                    }else if (value['sample_status'] == -1)
                    {
                        sBarcode = value['accession_sample_barcode'];
                    }else{
                        sBarcode = "N/A";
                    }

                    if (value['aSampleProcessMasterID']['bValidation'] == true)
                    {
                        var sAutosome = '<span class="btn btn-primary btn-small classBtnInternalControlAutosome" id="idBtnInternalControlAutosome_'+key+'" data-sample-id = '+value['sample_id']+' data-sample-request-id = '+value['sample_request_id']+' data-sample-process-master-id = '+value['aSampleProcessMasterID']['cytology_sample_process_master_id']+' data-toggle="modal" data-target="#idModalInternalControlAutosome">Add</span>';

                        var sChromosome = '<span class="btn btn-primary btn-small classBtnInternalControlChromosome" id="idBtnInternalControlChromosome_'+key+'" data-sample-id = '+value['sample_id']+' data-sample-request-id = '+value['sample_request_id']+' data-sample-process-master-id = '+value['aSampleProcessMasterID']['cytology_sample_process_master_id']+' data-toggle="modal" data-target="#idModalInternalControlChromosome">Add</span>';
                    }else{
                        var sAutosome = '<span class="btn btn-primary btn-small" disabled>Add</span>';

                        var sChromosome = '<span class="btn btn-primary btn-small" disabled>Add</span>';
                    }

                    sSampleTableRow += "<tr>";
                       sSampleTableRow += '<td>'+iTemp+'</td>';
                       sSampleTableRow += '<td>'+value["sample_label"]+'</td>';
                       sSampleTableRow += '<td>'+sBarcode+'</td>';
                       sSampleTableRow += '<td>'+value['tat_first']+'</td>';
                       sSampleTableRow += '<td>'+value['tat_final']+'</td>';
                       sSampleTableRow += '<td>'+sAutosome+'</td>';
                       sSampleTableRow += '<td>'+sChromosome+'</td>';
                    sSampleTableRow += "<tr>";

                    iTemp ++;
                });
                $(".classTableBodyFISHTestInternalQuality").html(sSampleTableRow);
            }
        }
    })
}

//! Function to get samples list for cytology..
function getSamplesForCytologyTestFinalization(){
    var sSampleTableRow = '';
    $.ajax({
        url: "ajaxCytologyLabManagement.php?sFlag=getSamplesForCytologyTest",
        data: {iScheduleID:iScheduleID, iServiceID:iServiceID, iPatientID:iPatientID},
        async: true,
        type: 'POST',
        datatype: 'json',
        success: function(data)
        {
            if($.trim(data) != false)
            {
                var iTemp = 1;
                var sBarcode = '';
                $.each(data, function(key, value)
                {

                    if (value['sample_status'] == 1)
                    {
                        sBarcode = value['collection_sample_barcode'];
                    }else if (value['sample_status'] == 2)
                    {
                        sBarcode = value['accession_sample_barcode'];
                    }else if (value['sample_status'] == -1)
                    {
                        sBarcode = value['accession_sample_barcode'];
                    }else{
                        sBarcode = "N/A";
                    }

                    if (value['aSampleProcessMasterID']['aneuploidy_finalized']==1)
                    {
                        var dAneuploidyFinalizationDate = value['aSampleProcessMasterID']['dAneuploidyFinalizationDate'];
                        var tAneuploidyFinalizationTime = value['aSampleProcessMasterID']['tAneuploidyFinalizationTime'];
                        var sAneuploidyFinalized = 'Finalized on '+dAneuploidyFinalizationDate+' '+tAneuploidyFinalizationTime+'<span class="btn btn-info btn-small classBtnFinalizeAneuploidyView" id="idBtnFinalizeAneuploidy_'+key+'" data-sample-id = '+value['sample_id']+' data-sample-request-id = '+value['sample_request_id']+' data-sample-process-master-id = '+value['aSampleProcessMasterID']['cytology_sample_process_master_id']+' data-toggle="modal" data-target="#idModalFinalizeAneuploidyView">View</span>';
                    }
                    else if(value['aSampleProcessMasterID']['bValidation'] == true)
                    {
                        var sAneuploidyFinalized = '<span class="btn btn-primary btn-small classBtnFinalizeAneuploidy" id="idBtnFinalizeAneuploidy_'+key+'" data-sample-id = '+value['sample_id']+' data-sample-request-id = '+value['sample_request_id']+' data-sample-process-master-id = '+value['aSampleProcessMasterID']['cytology_sample_process_master_id']+' data-toggle="modal" data-target="#idModalFinalizeAneuploidy">Finalize</span>';
                    }else{
                        var sAneuploidyFinalized = '<span class="btn btn-primary btn-small" disabled>Finalize</span>';
                    }

                    if (value['aSampleProcessMasterID']['micro_deletion_finalized']==1)
                    {
                        var dMicroDeletionFinalizationDate = value['aSampleProcessMasterID']['dMicroDeletionFinalizationDate'];
                        var tMicroDeletionFinalizationTime = value['aSampleProcessMasterID']['tMicroDeletionFinalizationTime'];
                        var sMicroDeletionFinalized = 'Finalized on '+dMicroDeletionFinalizationDate+' '+tMicroDeletionFinalizationTime+'<span class="btn btn-info btn-small classBtnFinalizeMicroDeletionView" id="idBtnFinalizeMicroDeletion_'+key+'" data-sample-id = '+value['sample_id']+' data-sample-request-id = '+value['sample_request_id']+' data-sample-process-master-id = '+value['aSampleProcessMasterID']['cytology_sample_process_master_id']+' data-toggle="modal" data-target="#idModalFinalizeMicroDeletionView" style="margin-left:5px;">View</span>';
                    }
                    else if (value['aSampleProcessMasterID']['bValidation'] == true)
                    {
                        var sMicroDeletionFinalized = '<span class="btn btn-primary btn-small classBtnFinalizeMicroDeletion" id="idBtnFinalizeMicroDeletion_'+key+'" data-sample-id = '+value['sample_id']+' data-sample-request-id = '+value['sample_request_id']+' data-sample-process-master-id = '+value['aSampleProcessMasterID']['cytology_sample_process_master_id']+' data-toggle="modal" data-target="#idModalFinalizeMicroDeletion">Finalize</span>';
                    }else{
                        var sMicroDeletionFinalized = '<span class="btn btn-primary btn-small" disabled>Finalize</span>';
                    }   

                    sSampleTableRow += "<tr>";
                       sSampleTableRow += '<td>'+iTemp+'</td>';
                       sSampleTableRow += '<td>'+value["sample_label"]+'</td>';
                       sSampleTableRow += '<td>'+sBarcode+'</td>';
                       sSampleTableRow += '<td>'+value['tat_first']+'</td>';
                       sSampleTableRow += '<td>'+value['tat_final']+'</td>';
                       sSampleTableRow += '<td>'+sAneuploidyFinalized+'</td>';
                       sSampleTableRow += '<td>'+sMicroDeletionFinalized+'</td>';
                    sSampleTableRow += "<tr>";

                    iTemp ++;
                });
                $(".classTableBodyFISHTestFinalization").html(sSampleTableRow);
            }
        }
    })
}