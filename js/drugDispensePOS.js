/* 
Global Configs
*/
var _iDecimalPrecision = 2;

function getAgeFromDateString(dateString) {
    var parts = dateString.split("-");
    var birthDate = new Date(parts[2], parts[1] - 1, parts[0]);
    
    var today = new Date();
    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age--;
    }
    return age;
}
function POS(iBillingMode,iBillID,iAllowDispenseIfNoStock,dBillDate) {
	var self 			   = this;
	this.oBill 			   = new BillViewMaster(iBillID,dBillDate,iAllowDispenseIfNoStock);
	this.oPatient 		   = new Patient(0, self);
	
	// 0 - Add
	// 1 - Edit
	// 2 - View Mode
	this.iBillingMode 	   = ko.observable(iBillingMode);
	this.bPatientViewCollapsed = ko.observable(false);

	//! No permission to change drug rates manually
	this.iHasPermissionToChangeItemRatesManually = ko.observable(0);

	this.saveBill = function () {
		var oPatient = self.oPatient.getPatientData();

		// Validate Patient data
		if(oPatient.iPatientID == "" || oPatient.iPatientID == 0){
			alert("Please select patient.");
			return false;
		}

		// Check if item has been added
		if(!self.oBill.aStockItemParticulars().length){
			alert("Please select items to dispense.");
			return false;
		}

		// Check if paid amount is not greater then bill amount
		if(parseFloat(self.oBill.iPaidAmount()) > parseFloat(self.oBill.iGrandTotal())){
			alert("Please paid amout should not be more than total amount.");
			return false;
		}

		var aItems = [];
		var aPayments = [];
		var iIsPrescription = self.oPatient.oSelectedAppointmentForPrescription().iPrescriptionID() >0 ? 1 : 0;

		for (var i = 0; i < self.oBill.aStockItemParticulars().length; i++) {
			var aItemParticular = self.oBill.aStockItemParticulars()[i];
			aItems.push({
				'item_id': aItemParticular.oItem.iItemID,
				'item_name': aItemParticular.oItem.sItemName,
				'item_qty': aItemParticular.iQty,
				'item_rate': aItemParticular.oItem.iItemRate,
				'item_applied_rate': aItemParticular.oItem.iAppliedItemRate,
				'item_tax_rate': aItemParticular.iItemTax,
				'tax_rate': aItemParticular.iTaxAmount,
				'item_discount': aItemParticular.iItemDiscount,
				'discount_amount': aItemParticular.iDiscountAmount,
				'partial_dispense_status': aItemParticular.oItem.iSelectedPartialDispenseStatus(),
				'partial_prescription_item_id': aItemParticular.oItem.iPrescriptionItemID(),
				'total': aItemParticular.iTotal,
			});
		};

		for (var i = 0; i < self.oBill.aPaymentPerticular().length; i++) {
			var aPaymentParticular = self.oBill.aPaymentPerticular()[i];

			aPayments.push({
				'payment_id': aPaymentParticular.iPaymentID(),
				'payment_amount': aPaymentParticular.iPaidAmount(),
				'payment_mode': aPaymentParticular.iPaymentMode(),
				'payment_date': aPaymentParticular.dPaymentDate(),
			});
		};

		var iGrandTotal = self.oBill.iGrandTotal();

		var iDiscountAmount = self.oBill.iDiscountAmount();
		var iTaxAmount = self.oBill.iTaxAmount();
		var iNetAmount = self.oBill.iNetAmount();
		var iIsPaid = self.oBill.iIsPaid();
		var iIsIPDBill = self.oBill.bIsIPDBill()?1:0;
		var iAdmissionID = self.oPatient.iAdmissionID();

		if(self.oBill.aQueuedPayment.iPaidAmount() > 0){
			if(!confirm("One of your payment is still not added. Are your sure want to continue?")){
				return false;
			}
		}
		var sDrugDispenseNotes = $('#idDispenseNotes').val();
		$.post('apiv2/dispense', {
			iPatientID: oPatient.iPatientID,
			iPrescriptionID: self.oPatient.oSelectedAppointmentForPrescription().iPrescriptionID(),
			iIsPrescription: iIsPrescription,
			aItems: aItems,
			aPayments: aPayments,
			sDrugDispenseNotes: sDrugDispenseNotes,
			bMarkDispensed: self.oPatient.bPrescriptionDispensed()?1:0,
			iGrandTotal : iGrandTotal,
			iDiscountAmount : iDiscountAmount,
			iTaxAmount : iTaxAmount,
			iNetAmount : iNetAmount,
			iIsPaid : iIsPaid,
			iIsIPDBill : iIsIPDBill,
			iAdmissionID : iAdmissionID,
			iPharmacyDepartment:$('#idPharmacyDepartment').val(),
			sFlag: ""
		}, function(data, textStatus, xhr) {
			if(data == 0){
				alert("There was error saving billing information");
			} else {
				self.oBill.iBillID(data);
				window.location.href="viewDrugDispense.php?iDispenseID="+data;
			}
		});
	}

	this.editBill = function () {
		self.iBillingMode(1);
	}

	this.getPatientForBill = function (iBillID) {
		return 0;
		if(!iBillID){
		}

		$.get('api/bill.php', {
	    	sFlag: 'getBillingDetail',
	    	iBillID: iBillID
	    }, function(data) {
			self.oPatient.setPatient(data.patient_id);
	    });
	}

	// Initialize Patient
	this.getPatientForBill(iBillID);

	this.loadBill = function (iDispenseID) {
		$.get('apiv2/dispense/'+iDispenseID, {}, function(data, textStatus, xhr) {
			if(data == 0){
				alert("There was error loading billing information");
			} else {
				self.oBill.iBillID(data.dispense_id);
				self.oPatient.setPatient(data.patient_id);
				self.oPatient.oSelectedAppointmentForPrescription(new ScheduleAppointment ("", "","","", "", data.prescription_id, 1, 0,"",""));

				for(var iii=0; iii < data.dispense_drug_data.length; iii++) {
					var oItem = new Item(data.dispense_drug_data[iii].drug_id,data.dispense_drug_data[iii].drug_id,data.dispense_drug_data[iii].drug_name,data.dispense_drug_data[iii].drug_rate_actual, self,"",data.dispense_drug_data[iii].strength,data.dispense_drug_data[iii].formulation,0);

					oItem.iAppliedItemRate(data.dispense_drug_data[iii].drug_rate_applied);
					self.oBill.addStockItem(oItem, data.dispense_drug_data[iii].quantity, 0);
				}

				for(var iii=0; iii < data.dispense_return_data.length; iii++) {
					var oItem = new Item(data.dispense_return_data[iii].drug_id,data.dispense_return_data[iii].drug_id,data.dispense_return_data[iii].drug_name,data.dispense_return_data[iii].drug_rate_actual, self,"",data.dispense_return_data[iii].strength,data.dispense_return_data[iii].formulation,0);
					oItem.iAppliedItemRate(data.dispense_return_data[iii].product_price);
					self.oBill.addReturnedItem(data.dispense_return_data[iii].dispense_drug_return_id, oItem, data.dispense_return_data[iii].qty_returned, data.dispense_return_data[iii].refund_amount, data.dispense_return_data[iii].return_date, data.dispense_return_data[iii].return_note);
				}

				for(var iii=0; iii < data.dispensed_drug_Payment.length; iii++) {
					self.oBill.aPaymentPerticular.push(
						new Payment(
							0,data.dispensed_drug_Payment[iii].pay_amount,data.dispensed_drug_Payment[iii].payment_mode_id,data.dispensed_drug_Payment[iii].payment_date
						)
					);
				}

				self.oBill.bBillDispensed(data.isDispensed);

				self.oBill.bIsIPDBill(data.isIPDBill==1?true:false);
				self.oBill.sIPDBillNo(data.sIPDBillNo);

				$('#idDispenseNotes').text(data.dispense_note==""?"NA":data.dispense_note);
			}
		});

	}
	
	if(iBillID>0) {
		self.loadBill(iBillID);
	}
}


function Patient(iPatientID, oVM) {
	if(!iPatientID){
		iPatientID = 0;
	}
	var self = this;
	var oVM = oVM;
	this.iPatientID = ko.observable(iPatientID);
    this.sPatientNo = ko.observable();
    this.sPatientName = ko.observable();
    this.sMobile = ko.observable();
    this.sEmail = ko.observable();
    this.dDOB = ko.observable();
    this.iAge = ko.observable();
    this.sGender = ko.observable("Male");
    this.sBloodGroup = ko.observable();
    this.sDefaultProfilePlaceholderUrl = "images/patient_placeholder.png";
    this.sProfileImage = ko.observable(self.sDefaultProfilePlaceholderUrl);
    this.iMedixcelPatientID = ko.observable(0);
    this.sMembershipType = ko.observable("");
    this.sPlanName = ko.observable("");
    this.bNewPatientMode = ko.observable(false);
    this.bEnablePatientForm = ko.observable(false);
    this.bShowDispensedPrescriptions = ko.observable(false);
    this.bPrescriptionDispensed = ko.observable(false);
	this.aScheduleAppointments = ko.observableArray([]);
	this.oSelectedAppointmentForPrescription = ko.observable(new ScheduleAppointment());
	this.iMembershipTypeID = ko.observable(0);
	this.iMembershipID = ko.observable(0);
    this.iWeight = ko.observable();
    this.dWeightDate = ko.observable();

    this.bIsIPDAdmitted = ko.observable();
	this.iAdmissionID = ko.observable();
	this.iIPDBillNo = ko.observable();
	this.sIPDBillNo = ko.observable();

	// Pagination for Schedule Appointment
	this.iCurrentAppointmentPage  = ko.observable(0);
	this.iAppointmentDisplayLimit = 5;
	
	this.aFilteredScheduleAppointments = ko.pureComputed(function(){
		if(self.bShowDispensedPrescriptions()) {
            return self.aScheduleAppointments(); 
        } else {
			return ko.utils.arrayFilter(self.aScheduleAppointments(), function(schAppt) {
	            return schAppt.bDispensed() == 0;
	        });
		}
	},this);
	this.aPaginatedScheduleAppointments = ko.pureComputed(function(){
		var first = self.iCurrentAppointmentPage() * self.iAppointmentDisplayLimit;
        return self.aFilteredScheduleAppointments().slice(first, first + self.iAppointmentDisplayLimit);
	},this);

	this.totalScheduleAppointmentsPages = ko.computed(function() {
		var div = Math.ceil(self.aFilteredScheduleAppointments().length / self.iAppointmentDisplayLimit);
		//div += self.aFilteredScheduleAppointments().length % self.iAppointmentDisplayLimit > 0 ? 1 : 0;
		return div ;
	});

	this.setPatient = function(iPatientID){
		if(iPatientID > 0){
	    	$.get("apiv2/patient/"+iPatientID, {} , function(data) {
				self.iPatientID(data.iPatientID);
			    self.sPatientNo(data.sPatientNo);
			    self.sPatientName(data.sPatientName);
			    self.sMobile(data.sMobile);
			    self.sEmail(data.sEmail);
			    self.dDOB(data.dDOB);
			    self.iAge(data.iAge);
			    self.sGender(data.sGender);
			    self.sBloodGroup(data.sBloodGroup);
			    self.sMembershipType(data.sMembershipType);
			    self.sPlanName(data.sPlanName);
			    self.iWeight(data.iWeight);
			    self.dWeightDate(data.dWeightDate);
			    if(data.sProfileImage){
			    	self.sProfileImage(data.sProfileImage);
			    } else {
    				self.sProfileImage = ko.observable(self.sDefaultProfilePlaceholderUrl);
			    }

			    self.iMedixcelPatientID(data.iMedixcelPatientID);

				self.iMembershipTypeID = ko.observable(data.iMembershipTypeID);
				self.iMembershipID = ko.observable(data.iPlanID);

				self.bIsIPDAdmitted(data.bIsIPDAdmitted);
				self.iAdmissionID(data.iAdmissionID);
			 	//
	    	});
		} else {
			self.iPatientID(0);
		    self.sPatientNo(0);
		    self.sPatientName("");
		    self.sMobile("");
		    self.sEmail("");
		    self.dDOB("");
		    self.iAge("");
		    self.sGender("Male");
		    self.sBloodGroup("");
			self.sProfileImage = ko.observable(self.sDefaultProfilePlaceholderUrl);
		    self.iMedixcelPatientID(0);
		    self.iMembershipTypeID = ko.observable(0);
			self.iMembershipID = ko.observable(0);
		    self.iWeight("");
		    self.dWeightDate("");
    	}

    	// If patient is set then get his appointments
		self.aScheduleAppointments([]);

		if(iPatientID>0) {
			$.get("apiv2/schedules-with-prescription/"+iPatientID, {}, function(data) {
	    		if(data.length>0 && oVM.iBillingMode() != 2) {
    				oVM.bPatientViewCollapsed(true);
    			}
	    		$.each(data, function(index, aSchedule) {
					self.aScheduleAppointments.push(
						new ScheduleAppointment(aSchedule.schedule_id,aSchedule.service_id,aSchedule.staff_name,aSchedule.schedule_date, aSchedule.service_name, aSchedule.prescription_id, aSchedule.bDispensed, aSchedule.iPrescriptionCount, aSchedule.signature_by,aSchedule.set_as_emergency)
					);
	    		});
	    	});
		}

    	/*if(iMedixcelPatientID > 0){
		}*/
	}

	this.changeNewPatientMode = function (bPatientMode) {
	    self.bNewPatientMode(bPatientMode);
		self.bEnablePatientForm(bPatientMode);
	}

	this.bNewPatientMode.subscribe(function (bNewPatientMode) {
	    // Resetting form
	    self.setPatient(0,0);
	});

	this.dDOB.subscribe(function (dDOB) {
	    if(dDOB){
		    self.iAge(getAgeFromDateString(dDOB));
	    } else {
		    self.iAge("");
	    }
	});

	this.getPatientData = function () {
		return {
			iPatientID: self.iPatientID() ? self.iPatientID() : 0,
		    sPatientNo: self.sPatientNo() ? self.sPatientNo() : '',
		    sPatientName: self.sPatientName() ? self.sPatientName() : '',
		    sMobile: self.sMobile() ? self.sMobile() : '',
		    sEmail: self.sEmail() ? self.sEmail() : '',
		    dDOB: self.dDOB() ? self.dDOB() : '',
		    iAge: self.iAge() ? self.iAge() : 0,
		    sGender: self.sGender() ? self.sGender() : "Male",
		    sBloodGroup: self.sBloodGroup() ? self.sBloodGroup() : "",
		    sProfileImage: self.sProfileImage() ? self.sProfileImage() : "",
		    iMedixcelPatientID: self.iMedixcelPatientID() ? self.iMedixcelPatientID() : 0,
		    bNewPatientMode: self.bNewPatientMode() ? self.bNewPatientMode() : 0,
		    bEnablePatientForm: self.bEnablePatientForm() ? self.bEnablePatientForm() : 0,
		    iWeight: self.iWeight() ? self.iWeight() : '',
		    dWeightDate: self.dWeightDate() ? self.dWeightDate() : '',
		}
	}

	this.displaySchedulePrescriptions = function(oSelectedAppt) {
		self.oSelectedAppointmentForPrescription(oSelectedAppt);
		self.oSelectedAppointmentForPrescription().loadPrescriptionItems();
	};

	this.displayAllScheduleAppointments = function () {
		self.oSelectedAppointmentForPrescription(new ScheduleAppointment());
	}

	this.showCurrentMedication = function (oOP) {
		if(oOP.oPatient.iPatientID() > 0){
			//! Fetching current medication for patient..
			$.ajax({
				url : "ajaxPharmacy.php?sFlag=fGetPatientCurrentMedication",
				type : "POST",
				data: {iPatientID:oOP.oPatient.iPatientID()},
				async: false,
				success : function(data) {
					if($.trim(data) != false){
						if(data.length > 0){
							var sTemplate = '';
							$.each(data, function(index, value){
								sTemplate += '<tr>';
									sTemplate += '<td>'+value.drug_name+'</td>';
									sTemplate += '<td>'+value.formulation+'</td>';
									sTemplate += '<td>'+value.strength+'</td>';
									sTemplate += '<td>'+value.dosage+'</td>';
									sTemplate += '<td>'+value.start_date+'</td>';
									sTemplate += '<td>'+value.end_date+'</td>';
								sTemplate += '</tr>';
							});
							$("#idModalPatientCurrentMedication").find("#idBodyPatientCurrentMedication").html(sTemplate);
						}else{
							var sTemplate = '<tr><td colspan="6" style="text-align:center">No Current Medication Found</td></tr>';
							$("#idModalPatientCurrentMedication").find("#idBodyPatientCurrentMedication").html(sTemplate);
						}
					}else{
						var sTemplate = '<tr><td colspan="6" style="text-align:center">No Current Medication Found</td></tr>';
						$("#idModalPatientCurrentMedication").find("#idBodyPatientCurrentMedication").html(sTemplate);
					}
				}
			});
			$("#idModalPatientCurrentMedication").modal('show');
		}else{
			alert('Please select proper patient.');
			return false;
		}
	}
}

// function / contructor for Master Bill
function BillViewMaster(iBillID,dBillDate,iAllowDispenseIfNoStock){
	if(!dBillDate){
		dBillDate = moment().format("DD-MM-YYYY");
	}

	var self = this;
	
	this.iBillID  = ko.observable(iBillID);
	this.dBillDate= ko.observable(dBillDate);
	this.aStockItemParticulars = ko.observableArray([]);
	this.aPaymentPerticular = ko.observableArray([]);
	this.iPaidAmount = ko.observable(0);
	this.iSuggestionItem = ko.observable(0);
	this.iSuggestionQty = ko.observable(1);
	this.bBillDispensed = ko.observable(0);
	this.iSelectedPartialDispenseStatus = ko.observable();

	this.aDefaultPaymentModes = (new PaymentMode()).aDefaultModes;
	this.aPharmacyDepartments = (new pharmacyDepartments()).aPharmacyDepartment;

	this.aQueuedPayment = new Payment(0,0,1,moment().format("DD-MM-YYYY"));

	this.aReturnedItems = ko.observableArray([]);

	this.bIsIPDBill = ko.observable(false);
	this.sIPDBillNo = ko.observable();

	this.iGrandTotal = ko.pureComputed(function(){
	    var iTotal = 0;
		for (var iLoop in this.aStockItemParticulars()) {
			var oItem = this.aStockItemParticulars()[iLoop];
			var	iSum = Number.parseFloat(oItem.iTotal());
			iTotal = iTotal + iSum;
		}
		return iTotal.toFixed(_iDecimalPrecision);
	},this);

	this.iTotalRefund = ko.pureComputed(function(){
	    var iTotal = 0;
		for (var iLoop in this.aReturnedItems()) {
			var oItem = this.aReturnedItems()[iLoop];
			var	iSum = Number.parseFloat(oItem.iRefundAmount());
			iTotal = iTotal + iSum;
		 }
		return iTotal.toFixed(_iDecimalPrecision);
	},this);

	this.iTaxAmount = ko.pureComputed(function(){
		var iTotalTaxAmount = 0;
		for (var iLoop in this.aStockItemParticulars()) {
			var oItem = this.aStockItemParticulars()[iLoop];
			var	iSumTax = oItem.iTaxAmount();
			iTotalTaxAmount = iTotalTaxAmount + iSumTax;
		 }
		return iTotalTaxAmount.toFixed(_iDecimalPrecision);
	},this);

	this.iDiscountAmount = ko.pureComputed(function(){
		var iTotalDisAmount = 0;
		for (var iLoop in this.aStockItemParticulars()) {
			var oItem = this.aStockItemParticulars()[iLoop];
			var	iSumDisc = oItem.iDiscountAmount();
			iTotalDisAmount = iTotalDisAmount + iSumDisc;
		 }
		return iTotalDisAmount.toFixed(_iDecimalPrecision);
	},this);

	this.iNetAmount = ko.pureComputed(function(){
		var iNetAmount = 0;
		for (var iLoop in this.aStockItemParticulars()) {
			var oItem = this.aStockItemParticulars()[iLoop];
			var	iSumDisc = oItem.iDiscountAmount();
			iNetAmount = iNetAmount + iSumDisc;
		 }
		return (this.iGrandTotal() - this.iDiscountAmount()).toFixed(_iDecimalPrecision);
	},this);

	this.iPendingAmount = ko.pureComputed(function(){
		var iPendingAmount = this.iGrandTotal();
		iPendingAmount = Number.parseFloat(iPendingAmount);
		for (var iLoop in this.aPaymentPerticular()) {
			var oItem = this.aPaymentPerticular()[iLoop];
			iPendingAmount -= oItem.iPaidAmount();
		}

		if(iPendingAmount < 0){
		 	return 0;
		}
		
		return iPendingAmount.toFixed(_iDecimalPrecision);
	},this);

	this.iPaidAmount = ko.pureComputed(function(){
		var iPaidAmount = 0;
		for (var iLoop in this.aPaymentPerticular()) {
			var oItem = this.aPaymentPerticular()[iLoop];
			iPaidAmount += (1* oItem.iPaidAmount());
		}
		return iPaidAmount.toFixed(_iDecimalPrecision);
	},this);

	this.iIsPaid = ko.pureComputed(function(){
		if(self.iPendingAmount()==0) {
			return 1;
		}
		else if(self.iPaidAmount()>0) {
			return 2;
		}
		else {
			return 0;
		}
	},this);

	this.addStockItem = function(oItem, iQty, iHideShowPartialDispenseOnOperation) {
		if(! iQty){
			iQty = 1;
		}
		if(iHideShowPartialDispenseOnOperation == 1){
			oItem.iHideShowPartialDispenseOnOperation(true);
		}else{
			oItem.iHideShowPartialDispenseOnOperation(false);
		}
		self.aStockItemParticulars.push( new StockItemParticulars(oItem,iQty,0,0,iAllowDispenseIfNoStock) );

		//! Checking user has drug rate manually changing action permission or not..
		$.ajax({
			url : "ajaxPermissionChecker.php?sFlag=checkActionPermission",
			type : "POST",
			data: {iActionID:209},
			async: false,
			success : function(data) {
				if(data != false){
					oItem.iHasPermissionToChangeStockItemRatesManually(1);
				}
			}
		});
	}

	this.addReturnedItem = function(iReturnID, oItem, iReturnedQty, iRefundAmount, dRefundDate, sReturnNote) {
		self.aReturnedItems.push(new ReturnedItem(iReturnID, oItem, iReturnedQty, iRefundAmount, dRefundDate, sReturnNote));

		for(var iii= 0; iii< self.aStockItemParticulars().length; iii++) {
			if(self.aStockItemParticulars()[iii].oItem.iItemID == oItem.iItemID) {
				self.aStockItemParticulars()[iii].iReturnedQty(self.aStockItemParticulars()[iii].iReturnedQty() + iReturnedQty);
				break;
			}
		}
	}

	this.addDrugITem = function(iDrugID, iQty, iPrescriptionItemID,iIsPrescriptionStopped, oVM) {
		// Add Current Stock of all items
		$.get("apiv2/drug/"+iDrugID, {}, function(data) {
			var iDeptID = $('#idPharmacyDepartment').val();
			//! Hide / Show Partial Drug Dispense Drowdown..
			var iHideShowPartialDispenseOnOperation = 0;
			if(iPrescriptionItemID != "" && iPrescriptionItemID > 0){
				iHideShowPartialDispenseOnOperation = 1;
			}

			//! Stopped Drug Prescription..
			if(iIsPrescriptionStopped == 1){
				alert("Prescription for this medicine has been stopped. Hence, you cannot add this medicine");
				return false;
			}

			//! If medicine is deleted..
			if(data.status == undefined || data.status == null){
				alert("This medicine has been deleted/freezed from the system. Hence, you cannot add this medicine");
				return false;
			}
			var oItem = new Item(data.drug_id,data.drug_code,data.drug_name,data.product_price, oVM,iDeptID,data.strength,data.formulation,iPrescriptionItemID);
			self.addStockItem(oItem, iQty, iHideShowPartialDispenseOnOperation);
    	});
		
    	//! Checking user has drug rate manually changing action permission or not..
		$.ajax({
			url : "ajaxPermissionChecker.php?sFlag=checkActionPermission",
			type : "POST",
			data: {iActionID:209},
			async: false,
			success : function(data) {
				if(data != false){
					oVM.iHasPermissionToChangeItemRatesManually(1);
				}
			}
		});
	}

	this.addPayment = function(){
		var oPayment = self.aQueuedPayment;

		if(oPayment.iPaidAmount()){
			self.aPaymentPerticular.push(
				new Payment(
					0,oPayment.iPaidAmount(),oPayment.iPaymentMode(),oPayment.dPaymentDate()
				)
			);
			
			self.aQueuedPayment.iPaidAmount(0);
			self.aQueuedPayment.iPaymentMode(1);
			self.aQueuedPayment.dPaymentDate(moment().format("DD-MM-YYYY"));
		}
	}

	this.removePayment = function (iIndex) {
		if(iIndex == 0){
			this.aPaymentPerticular.shift(); 
		} else {
			this.aPaymentPerticular.splice(iIndex,1); 
		}
	}

	this.removeItem = function(iIndex){
		if(iIndex == 0){
			this.aStockItemParticulars.shift(); 
		} else {
			this.aStockItemParticulars.splice(iIndex,1); 
		}
	}

	// Load bill when bill id changes
	this.initializeBillingDetail = function (iBillID) {
		$.get('api/bill.php', {
	    	sFlag: 'getBillingDetail',
	    	iBillID: iBillID
	    }, function(data) {
	    	self.iBillID(data.bill_id);
			self.dBillDate(data.bill_date);
			self.aStockItemParticulars([]);
			self.aPaymentPerticular([]);


			$.each(data.aItems, function(index, aItem) {
				var iBatchID = aItem.batch_id;
				
				// Add Current Stock of all items
				$.get("ajaxHandler.php", {iBatchID: iBatchID, func: 'getBatchDetailByBatchID'}, function(data) {
					var iBatchAvailQty = parseInt(aItem.item_qty) + parseInt(data.available_qty);

					var iDeptID = $('#idPharmacyDepartment').val();

					var oBatch = new Batch(aItem.batch_id,aItem.batch_no,data.expiry_date,iBatchAvailQty),
						oItem = new Item(aItem.item_id,aItem.item_code,aItem.item_name,aItem.item_rate,"",iDeptID,aItem.strength,aItem.formulation,0);
					self.aStockItemParticulars.push(
						new StockItemParticulars(oBatch,oItem,aItem.item_qty,aItem.item_tax_rate,aItem.item_discount_amount)
					);
		    	});

			});

			$.each(data.aPayments, function(index, aPayment) {
				self.aPaymentPerticular.push(
					new Payment(
						aPayment.bill_payment_id,aPayment.payment_amount,aPayment.payment_mode_id,aPayment.payment_date
					)
				);
			});

			self.iSuggestionBatchID(0);
	    });
	}

	if(iBillID > 0){
	    //self.initializeBillingDetail(iBillID);
	}
}

function StockItemParticulars(oItem,iQty,iItemTax,iItemDiscount,iAllowDispenseIfNoStock) {
	var self = this;
	this.oItem = oItem;
	this.iQty = ko.observable(iQty);
	this.iItemTax= ko.observable(iItemTax);
	this.iItemDiscount=ko.observable(iItemDiscount);
	this.iPrescriptionItemID=ko.observable(oItem.iPrescriptionItemID());

	this.iReturnedQty = ko.observable(0);
	this.iReturedItemRate = ko.observable(this.oItem.iAppliedItemRate());

	this.iReturnableQty = ko.computed(function() {
        var value = this.iQty()-this.iReturnedQty();
        return value >= 0 ? value : 0 ;
    }, this);

	this.iItemTotalAmount = ko.computed(function() {
		var value = self.iQty() * self.oItem.iAppliedItemRate()
        return value >= 0 ? value : 0 ;
    }, this);

	this.iTaxAmount = ko.computed(function() {
        var value = ( self.iItemTotalAmount() * self.iItemTax() )/100;
        return value >= 0 ? value : 0 ;
    }, this);

    this.iDiscountAmount = ko.computed(function() {
        return self.iQty() * self.iItemDiscount();
    }, this);

    this.iTotal = ko.computed(function() {
        return (self.iItemTotalAmount() + self.iTaxAmount() - self.iDiscountAmount()).toFixed(_iDecimalPrecision);
    }, this);

	if(iAllowDispenseIfNoStock==false){
	    this.iQty.subscribe(function (iQty) {
	    	if((self.oItem.iItemQtyAvailable()!="?") && parseInt(iQty) > parseInt(self.oItem.iItemQtyAvailable())){
	    		self.iQty(self.oItem.iItemQtyAvailable());
	    		alert("You can not enter more qty than available qty.");
	    	}
		});
	}
}

// creating Item Object
function Item(iItemID,sItemCode,sItemName,iItemRate, oVM,iDeptID,sItemStrength,sItemFormulation,iPrescriptionItemID = 0){
	this.iItemID   = iItemID;
	this.sItemCode = sItemCode;
	this.sItemName = sItemName+"["+sItemStrength+" | "+sItemFormulation+"]";
	this.sItemStrength = sItemStrength;
	this.sItemFormulation = sItemFormulation;
	if(typeof iItemRate == 'undefined' || iItemRate == '') {
		iItemRate = 0;
	}

	var iAvailableProductStock = (new getAvailableProductDeptStock(iDeptID,iItemID)).iProductStock;
	this.aDefaultIndividualDrugDispenseModes = (new IndividualDrugDispenseModes()).aDefaultDispenseModes;

	this.iItemRate = ko.observable(iItemRate);
	this.iAppliedItemRate = ko.observable(iItemRate);
	this.iItemQtyAvailable = ko.observable(iAvailableProductStock);
	this.iPrescriptionItemID = ko.observable(iPrescriptionItemID);

	this.iProductID = ko.observable(0);
	this.iAppliedItemRateSet = ko.observable(null);
	this.iSelectedPartialDispenseStatus = ko.observable();
	this.iHideShowPartialDispenseOnOperation = ko.observable(true);
	this.iHasPermissionToChangeStockItemRatesManually = ko.observable(0);
	
	iMembershipTypeID = oVM.oPatient.iMembershipTypeID();
	iMembershipID = oVM.oPatient.iMembershipID();
	if (iMembershipID > 0 && iMembershipTypeID > 0) {
		this.iAppliedItemRate = ko.computed({
				read: function() {
							if(this.iAppliedItemRateSet() !== null) {
								return this.iAppliedItemRateSet();
							}

							var drugRate;
					    	$.ajax({
								url : "apiv2/drug-rate-for-membership/"+iItemID,
								type : "get",
								data: {iMembershipTypeID: iMembershipTypeID, iMembershipPlanID: iMembershipID},
								async: false,
								success : function(data) {
									drugRate = data;
								}
							});
					    	if(drugRate===false) {
					    		return this.iItemRate();
					    	}
					    	else {
					    		return drugRate;
					    	}
				},
				write: function(value) {
					this.iAppliedItemRateSet(value)
				}
		}, this);
	}
}

function Payment(iPaymentID,iPaidAmount,iPaymentMode,dPaymentDate){
	var self = this;
	this.iPaymentID = ko.observable(iPaymentID);
	this.iPaidAmount = ko.observable(iPaidAmount);
	this.iPaymentMode = ko.observable(iPaymentMode);
	this.dPaymentDate = ko.observable(dPaymentDate);
}

function PaymentMode () {
	this.aDefaultModes = [
		{value: 1,text: 'Cash'},
		{value: 2,text: 'Debit Card'},
		{value: 3,text: 'Credit Card'},
		{value: 4,text: 'Insurance'},
		{value: 5,text: 'Cheque'},
		{value: 6,text: 'Contract'},
		{value: 7,text: 'Case Study'},
		{value: 8,text: 'Coupon'}
	];
}

function ScheduleAppointment (iScheduleID,iServiceID,sDoctorName,dScheduleDate, sServiceName, iPrescriptionID, bDispensed, iPrescriptionCount, aSignedBy,iSetAsEmergency="") {
	var self = this;
	if(!iScheduleID){
		iScheduleID = 0;
	}

	this.iScheduleID = ko.observable(iScheduleID);
	this.iServiceID = ko.observable(iServiceID);
	this.sDoctorName = ko.observable(sDoctorName);
	this.sServiceName = ko.observable(sServiceName);
	this.dScheduleDate = ko.observable(dScheduleDate);
	this.iPrescriptionID = ko.observable(iPrescriptionID);
	this.bDispensed = ko.observable(bDispensed);
	this.iPrescriptionCount = ko.observable(iPrescriptionCount);
	this.oPrescription = ko.observable(new Prescription());
	this.loadPrescriptionItems = function () {
		self.oPrescription(new Prescription(iPrescriptionID));
		self.oPrescription().loadPrescriptionItems();
	}
	this.aSignedBy = ko.observableArray(aSignedBy);
	this.iSetAsEmergency = ko.observable(iSetAsEmergency);
}

function Prescription (iPrescriptionID) {
	var self = this;
	this.iPrescriptionID = iPrescriptionID;
	this.aPrescriptionItems = ko.observableArray([]);

	this.loadPrescriptionItems = function() {
		$.get("apiv2/prescription/"+iPrescriptionID, {}, function(data) {
			self.aPrescriptionItems([]);
			$.each(data, function(index, aPrescription) {
				self.aPrescriptionItems.push(new PrescriptionItem(aPrescription.drug_id,aPrescription.drug_name,aPrescription.generic_name,aPrescription.formulation,aPrescription.strength,aPrescription.dosage,aPrescription.days,aPrescription.no_of_tablets, aPrescription.instructions,aPrescription.prescription_dose,aPrescription.prescription_item_id,aPrescription.isDispensed,aPrescription.isStopped,aPrescription.manully_selection_of_formulation));
			});
    	});
	};
}

function PrescriptionItem(iItemID,sBrandName,sGenericName,sFormulation,sStrength,sDosage,iDays,iNoOfTablets, sInstructions,sPrescriptionDose,iPrescriptionItemID,iIsPrescribedDrugDispensed,iIsPrescribedDrugStopped,iIsManualSelectionOfDrugFormulation) {
	this.iItemID = iItemID;
	this.sBrandName = sBrandName;
	this.sGenericName = sGenericName;
	this.sFormulation = sFormulation;
	this.sStrength = sStrength;
	this.sDosage = sDosage;
	this.iDays = iDays;
	this.iNoOfTablets = iNoOfTablets;
	this.sInstructions = sInstructions;
	this.sPrescriptionDose = sPrescriptionDose;
	this.iPrescriptionItemID = iPrescriptionItemID;
	this.iIsPrescribedDrugDispensed = iIsPrescribedDrugDispensed;
	this.iIsPrescriptionStopped = iIsPrescribedDrugStopped;	
	this.iIsManualSelectionOfDrugFormulation = iIsManualSelectionOfDrugFormulation;	
	this.sPrescriptionActivity = "Add Medicine";		
	this.sPrescriptionActivityClass = "btn-primary";		
	if(iIsPrescribedDrugDispensed == 1){
		this.sPrescriptionActivity = "Fully Dipensed Prescription";		
	}
	if(iIsPrescribedDrugStopped == 1){
		this.sPrescriptionActivity = "Stopped Prescription";		
		this.sPrescriptionActivityClass = "btn-danger";		
	}
}

ko.bindingHandlers.patientTypeahead = {
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
	    var allBindings = allBindingsAccessor();
    	var enableTypeahead = allBindings.enableTypeahead;

        if(enableTypeahead){
        	initPatientTypeahead(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext);
        } else {
        	$(element).typeahead('destroy');
        }
    }
};

var initPatientTypeahead = function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
    var $element = $(element);
    var allBindings = allBindingsAccessor();
    var typeaheadArr = ko.utils.unwrapObservable(valueAccessor());
    var sExternalPatientLabel = allBindings.externalPatientLabel;
    var enableTypeahead = allBindings.enableTypeahead;

    var timeout;
    var currentAjaxRequest = null;

    $element
    	.attr("autocomplete", "off")
        .typeahead({
            hint: true,
            highlight: true,
            minLength: 3,
        }, {
            source: function (query, process, async) {

            	if (timeout) {
	                clearTimeout(timeout);
	            }

	            timeout = setTimeout(function() {
	            	currentAjaxRequest = $.ajax({
	            		url: "apiv2/patients",
				    	async: true,
				    	data:  {'sKeyword': query},
				    	beforeSend: function() {
				    		if(currentAjaxRequest != null) {
					            currentAjaxRequest.abort();
					        }
				    	},
						success: function(data) {
							var patients = [];

		                    $.map(data, function(patient){
		                        var group;
		                        group = {
		                            iPatientID: patient.iPatientID,
		                            name: patient.sPatientName,
		                            sMobile: patient.sMobile,
		                            sPatientNo: patient.sPatientNo,
		                            toString: function () {
		                                return JSON.stringify(this);
		                            },
		                            toLowerCase: function () {
		                                return this.name.toLowerCase();
		                            },
		                            indexOf: function (string) {
		                                return String.prototype.indexOf.apply(this.name, arguments);
		                            },
		                            replace: function (string) {
		                                return String.prototype.replace.apply(this.name, arguments);
		                            }
		                        };

		                        patients.push(group);
		                    });

		                    return async(patients);
						},
						complete: function(){
							currentAjaxRequest = null;
						}
	            	});

	            }, 800);
            },
            display: 'name',
            templates: {
                suggestion: function (data) {
                    return '<div><strong>' + data.name + '</strong> <small class="text-muted">('+data.sPatientNo+')</small><br/><div class="classMobile text-muted">'+data.sMobile+'</div></div>';
                }
            }
        })
		.on('typeahead:selected', function (obj, datum) {
			var value = valueAccessor();
            value.setPatient(datum.iPatientID);
        }).on('typeahead:autocompleted', function (obj, datum) {
        	var value = valueAccessor();
            value.setPatient(datum.iPatientID);
        });
}

ko.bindingHandlers.itemTypeahead = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var $element = $(element);
        var allBindings = allBindingsAccessor();
        var typeaheadArr = ko.utils.unwrapObservable(valueAccessor());

        var timeout;
    	var currentAjaxRequest = null;

        $element
        	.attr("autocomplete", "off")
            .typeahead({
	            hint: true,
	            highlight: true,
	            minLength: 3,
	        }, {
	            source: function (query, process, async) {

	            	if (timeout) {
		                clearTimeout(timeout);
		            }

		            timeout = setTimeout(function() {
		            	currentAjaxRequest = $.ajax({
		            		url: "apiv2/drugs",
					    	async: true,
					    	data:  {'drugTxt': query},
					    	beforeSend: function() {
					    		if(currentAjaxRequest != null) {
						            currentAjaxRequest.abort();
						        }
					    	},
							success: function(data) {
								var items = [];

			                    $.map(data, function(item){
			                        var group;
			                        group = {
			                            id: item.drug_id,
			                            name: item.drug_name,
			                            generic_name: item.generic_name,
			                            strength: item.strength,
			                            formulation: item.formulation,
			                            available_qty: item.available_qty,
			                            expiry_date: item.expiry_date,
			                            product_price: item.product_price,
			                            toString: function () {
			                                return JSON.stringify(this);
			                            },
			                            toLowerCase: function () {
			                                return this.name.toLowerCase();
			                            },
			                            indexOf: function (string) {
			                                return String.prototype.indexOf.apply(this.name, arguments);
			                            },
			                            replace: function (string) {
			                                return String.prototype.replace.apply(this.name, arguments);
			                            }
			                        };

			                        items.push(group);
			                    });

			                    return async(items);
							},
							complete: function(){
								currentAjaxRequest = null;
							}
		            	});
		            }, 800);
	            },
	            display: 'name',
	            templates: {
		            suggestion: function (data) {
		            	sExpiryDate = data.expiry_date ? data.expiry_date : "NA";

		            	return [
		            		"<div>",
			            		'<div class="row classNoBottomSpacing">',
			            			'<div class="col-md-8">',
			            				'<p><strong>'+data.name+'</strong> ('+data.generic_name+')</p>',
			            				'<p>'+data.strength+' | '+data.formulation+'</p>',
			            			'</div>',
					            '</div>',
				            "</div>",
		            	].join("");
	                }
	           	}
	        })
    		.on('typeahead:selected', function (obj, datum) {
            	var value = valueAccessor();
            	var iDeptID = $('#idPharmacyDepartment').val();
            	value.iSuggestionItem(new Item(datum.id,datum.drug_code,datum.name,datum.product_price, viewModel,iDeptID,datum.strength,datum.formulation,0));
            }).on('typeahead:autocompleted', function (obj, datum) {
            	var value = valueAccessor();
            	var iDeptID = $('#idPharmacyDepartment').val();
            	value.iSuggestionItem(new Item(datum.id,datum.drug_code,datum.name,datum.product_price, viewModel,iDeptID,datum.strength,datum.formulation,0));
            });
    }
};

ko.bindingHandlers.patientDOBPicker = {
	update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var $element = $(element);
    	var allBindings = allBindingsAccessor();
        $element.datepicker({
            format: 'dd-mm-yyyy',
            endDate: new Date(),
            zIndexOffset: '1001',
            autoClose: true
        });
    }
};


function ReturnedItem(iReturnID, oItem, iReturnedQty, iRefundAmount, dRefundDate, sReturnNote) {
	if (dRefundDate=="1970-01-01") {
		dRefundDate ="-";
	}
	this.iReturnID = ko.observable(iReturnID>0?iReturnID:0);
	this.oItem = oItem;
	
	this.iReturnedQty = ko.observable(iReturnedQty>0?iReturnedQty:0);
	this.iRefundAmount = ko.observable(iRefundAmount>0?iRefundAmount:0);
	this.dRefundDate = ko.observable(dRefundDate!=''?dRefundDate:'');
	this.sReturnNote = ko.observable(sReturnNote!=''?sReturnNote:'');
	this.iReturedItemRate = ko.observable(oItem.iAppliedItemRate()>0?oItem.iAppliedItemRate():0);
}


function pharmacyDepartments () {
	var oTempdata="";
	var iLoginUserID = $('#idUserID').val();
	var iUserTypeID = $('#idUserTypeID').val();

	$.ajax({
		url: "ajaxEhrInventory.php?sFlag=GetAllDepartmentForEhr&iDepartmentTypeID=2&iUserID="+iLoginUserID+"&iUserTypeID="+iUserTypeID,
    	async: false,
		success: function(data) {
			if($.trim(data) != false) {
				oTempdata = $.parseJSON(data);
			}
		}
	});
	this.aPharmacyDepartment = oTempdata;
}

function getAvailableProductDeptStock(iDeptID,iProductID){
	var iStock=0;
	$.ajax({
		url: "ajaxInventory.php?sFlag=GetProductStoctByDeprtment",
		data:{iDeptID:iDeptID,iProductID:iProductID},
    	async: false,
		success: function(data) {
			if($.trim(data) != false) {
				iStock = $.parseJSON(data);
			}
		}
	});
	this.iProductStock = iStock;
}

function IndividualDrugDispenseModes () {
	this.aDefaultDispenseModes = [
		{value: 0,text: 'No Dispense'},
		{value: 2,text: 'Partial Dispense'},
		{value: 1,text: 'Fully Dispense'}
	];
}