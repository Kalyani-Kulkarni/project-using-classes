//! To Get nano drop samples..
function getNanoDropSamples()
{
    var sNanoDropSample = '';
    $.ajax({
        type: "POST",
        url: "ajaxBatchProcessingManagement.php?sFlag=getNanoDropSamples",
        datatype: "JSON",
        data:{iPatientID:iPatientID,iServiceID:iServiceID,iScheduleID:iScheduleID},
        success: function(data) { 
            if(data != false)
            {
                var iSrNo = 1;
                $.each(data, function(key, value)
                {
                    if (value['sample_status'] == -1 || value['sample_status'] == -2)
                    {
                        sNanoDropSample += '<tr style="background:#ff7c7c;">';
                    }else{
                        sNanoDropSample += '<tr>';
                    }
                        sNanoDropSample += '<td>'+iSrNo+'</td>';

                        if (value['collection_sample_barcode'] == null)
                        {
                            var sBarcode = "NA";
                        }else{
                            var sBarcode = value['collection_sample_barcode'];
                        }

                        sNanoDropSample += '<td>'+sBarcode+'</td>';
                        sNanoDropSample += '<td>'+value['sample_label']+'</td>';

                        if (typeof value['aLuminex200Samples']['batch_no'] != "undefined")
                        {
                            var sBatchNo = value['aLuminex200Samples']['batch_no'];
                            var sTubeNo = value['aLuminex200Samples']['tube_no'];
                        }else{
                            var sBatchNo = "NA";
                            var sTubeNo = "NA";
                        }

                        if (!$.isEmptyObject(value['aNanoDropSamples']))
                        {
                            var sViewButton = '<a data-toggle="modal" data-target="#idModalNanoDropSampleDetails" class="btn btn-info btn-small classNanoDropSampleDetails" data-sample-id = "'+value['aNanoDropSamples'][0]['sample_id']+'" data-batch-master-id = "'+value['aNanoDropSamples'][0]['batch_master_id']+'">View</a>';
                        }else{
                            var sViewButton = "NA";
                        }

                        sNanoDropSample += '<td>'+sBatchNo+'</td>';
                        sNanoDropSample += '<td>'+sTubeNo+'</td>';
                        sNanoDropSample += '<td>'+sViewButton+'</td>';
                    sNanoDropSample += '</tr>';

                    iSrNo ++;
                });
                $("#idNanoDropSamples").html(sNanoDropSample);
            }else{
                $("#idNanoDropSamples").html("<span style='color:red;'>No samples found in nano drop.</span>");
            }
        }            
    });
}

//! To get Dilution samples..
function getDilutionSamples()
{
    var sDilutionSample = '';
    $.ajax({
        type: "POST",
        url: "ajaxBatchProcessingManagement.php?sFlag=getDilutionSamples",
        datatype: "JSON",
        data:{iPatientID:iPatientID,iServiceID:iServiceID,iScheduleID:iScheduleID},
        success: function(data) {
            if(data != false)
            {
                var iSrNo = 1;
                $.each(data, function(key, value)
                {
                    if (value['collection_sample_barcode'] == null)
                    {
                        var sBarcode = "NA";
                    }else{
                        var sBarcode = value['collection_sample_barcode'];
                    }

                    if (typeof value['aLuminex200Samples']['batch_no'] != "undefined")
                    {
                        var sBatchNo = value['aLuminex200Samples']['batch_no'];
                        var sTubeNo = value['aLuminex200Samples']['tube_no'];
                    }else{
                        var sBatchNo = "NA";
                        var sTubeNo = "NA";
                    }

                    if (!value['aDilutionSamples'] == "")
                    {
                        var sDilutionFactor = value['aDilutionSamples']['dilution_factor'];
                        var sDilutionValue = value['aDilutionSamples']['dilution_value'];
                    }else{
                        var sDilutionFactor = "NA";
                        var sDilutionValue = "NA";
                    }

                    if (value['sample_status'] == -1 || value['sample_status'] == -2)
                    {
                        sDilutionSample += '<tr style="background:#ff7c7c;">';
                    }else{
                        sDilutionSample += '<tr>';
                    }
                        sDilutionSample += '<td>'+iSrNo+'</td>';
                        sDilutionSample += '<td>'+sBarcode+'</td>';
                        sDilutionSample += '<td>'+value['sample_label']+'</td>';
                        sDilutionSample += '<td>'+sBatchNo+'</td>';
                        sDilutionSample += '<td>'+sTubeNo+'</td>';
                        sDilutionSample += '<td>'+sDilutionFactor+'</td>';
                        sDilutionSample += '<td>'+sDilutionValue+'</td>';
                    sDilutionSample += '</tr>';

                    iSrNo ++;
                });
                $("#idDilutionSamples").html(sDilutionSample);
            }else{
                $("#idDilutionSamples").html("<span style='color:red;'>No samples found in dilution.</span>");
            }
        }            
    });
}

//! To get purification samples..
function getPurificationSamples()
{
    var sPurificationSample = '';
    $.ajax({
        type: "POST",
        url: "ajaxBatchProcessingManagement.php?sFlag=getPurificationSamples",
        datatype: "JSON",
        data:{iPatientID:iPatientID,iServiceID:iServiceID,iScheduleID:iScheduleID},
        success: function(data) {
            if(data != false)
            {
                var iSrNo = 1;
                $.each(data, function(key, value)
                {
                    if (value['collection_sample_barcode'] == null)
                    {
                        var sBarcode = "NA";
                    }else{
                        var sBarcode = value['collection_sample_barcode'];
                    }

                    if (typeof value['aLuminex200Samples']['batch_no'] != "undefined")
                    {
                        var sBatchNo = value['aLuminex200Samples']['batch_no'];
                        var sTubeNo = value['aLuminex200Samples']['tube_no'];
                    }else{
                        var sBatchNo = "NA";
                        var sTubeNo = "NA";
                    }

                    if (!value['aPurificationSamples'] == "")
                    {
                        var sCellNo = value['aPurificationSamples']['cell_no'];
                    }else{
                        var sCellNo = "NA";
                    }

                    if (value['sample_status'] == -1 || value['sample_status'] == -2)
                    {
                        sPurificationSample += '<tr style="background:#ff7c7c;">';
                    }else{
                        sPurificationSample += '<tr>';
                    }
                        sPurificationSample += '<td>'+iSrNo+'</td>';
                        sPurificationSample += '<td>'+sBarcode+'</td>';
                        sPurificationSample += '<td>'+value['sample_label']+'</td>';
                        sPurificationSample += '<td>'+sBatchNo+'</td>';
                        sPurificationSample += '<td>'+sTubeNo+'</td>';
                        sPurificationSample += '<td>'+sCellNo+'</td>';
                    sPurificationSample += '</tr>';

                    iSrNo ++;
                });
                $("#idPurificationSamples").html(sPurificationSample);
            }else{
                $("#idPurificationSamples").html("<span style='color:red;'>No samples found in purification.</span>");
            }
        }            
    });
} 

//! To get hybridization samples..
function getHybridizationSamples()
{
    var sHybridizationSample = '';
    $.ajax({
        type: "POST",
        url: "ajaxBatchProcessingManagement.php?sFlag=getHybridizationSamples",
        datatype: "JSON",
        data:{iPatientID:iPatientID,iServiceID:iServiceID,iScheduleID:iScheduleID},
        success: function(data) {
            if(data != false)
            {
                var iSrNo = 1;
                $.each(data, function(key, value)
                {
                    if (value['collection_sample_barcode'] == null)
                    {
                        var sBarcode = "NA";
                    }else{
                        var sBarcode = value['collection_sample_barcode'];
                    }

                    if (typeof value['aLuminex200Samples']['batch_no'] != "undefined")
                    {
                        var sBatchNo = value['aLuminex200Samples']['batch_no'];
                        var sTubeNo = value['aLuminex200Samples']['tube_no'];
                    }else{
                        var sBatchNo = "NA";
                        var sTubeNo = "NA";
                    }

                    if (!value['aHybridizationSamples'] == "")
                    {
                        var sCellNo = value['aHybridizationSamples']['cell_no'];
                    }else{
                        var sCellNo = "NA";
                    }

                    if (value['sample_status'] == -1 || value['sample_status'] == -2)
                    {
                        sHybridizationSample += '<tr style="background:#ff7c7c;">';
                    }else{
                        sHybridizationSample += '<tr>';
                    }
                        sHybridizationSample += '<td>'+iSrNo+'</td>';
                        sHybridizationSample += '<td>'+sBarcode+'</td>';
                        sHybridizationSample += '<td>'+value['sample_label']+'</td>';
                        sHybridizationSample += '<td>'+sBatchNo+'</td>';
                        sHybridizationSample += '<td>'+sTubeNo+'</td>';
                        sHybridizationSample += '<td>'+sCellNo+'</td>';
                    sHybridizationSample += '</tr>';

                    iSrNo ++;
                });
                $("#idHybridizationSamples").html(sHybridizationSample);
            }else{
                $("#idHybridizationSamples").html("<span style='color:red;'>No samples found in hybridization.</span>");
            }
        }            
    });
}

//! To get luminex samples..
function getLuminex200Samples()
{
    var sLuminex200Sample = '';
    $.ajax({
        type: "POST",
        url: "ajaxBatchProcessingManagement.php?sFlag=getSampleListOfPatient",
        datatype: "JSON",
        data:{iPatientID:iPatientID,iServiceID:iServiceID,iScheduleID:iScheduleID},
        success: function(data) {
            if(data != false)
            {
                var iSrNo = 1;
                $.each(data, function(key, value)
                {
                    if (value['collection_sample_barcode'] == null)
                    {
                        var sBarcode = "NA";
                    }else{
                        var sBarcode = value['collection_sample_barcode'];
                    }

                    if (typeof value['aLuminex200Samples']['batch_no'] != "undefined")
                    {
                        var sBatchNo = value['aLuminex200Samples']['batch_no'];
                        var sTubeNo = value['aLuminex200Samples']['tube_no'];
                    }else{
                        var sBatchNo = "NA";
                        var sTubeNo = "NA";
                    }

                    if (value['sample_status'] == -1 || value['sample_status'] == -2)
                    {
                        sLuminex200Sample += '<tr style="background:#ff7c7c;">';
                    }else{
                        sLuminex200Sample += '<tr>';
                    }
                        sLuminex200Sample += '<td>'+iSrNo+'</td>';
                        sLuminex200Sample += '<td>'+sBarcode+'</td>';
                        sLuminex200Sample += '<td>'+value['sample_label']+'</td>';
                        sLuminex200Sample += '<td>'+sBatchNo+'</td>';
                        sLuminex200Sample += '<td>'+sTubeNo+'</td>';
                        if (sBatchNo !== "NA")
                        {
                            if (value['aLuminex200Samples']['is_lumnex200_completed'] == 1)
                            {
                                sLuminex200Sample += '<td>Completed on '+value['aLuminex200Samples']['dLuminex200CompletionDate']+'</td>';
                            }else{
                                sLuminex200Sample += '<td>Not Completed</td>'; 
                            }
                        }else{
                            sLuminex200Sample += '<td>NA</td>';
                        }    
                    sLuminex200Sample += '</tr>';

                    iSrNo ++;
                });
                $("#idLuminex200Samples").html(sLuminex200Sample);
            }else{
                $("#idLuminex200Samples").html("<span style='color:red;'>No samples found in luminex 200.</span>");
            }
        }            
    });
} 

//! To get BoB software samples..
function getBoBSoftwareSamples()
{
    var sBoBSoftwareSample = '';
    $.ajax({
        type: "POST",
        url: "ajaxBatchProcessingManagement.php?sFlag=getSampleListOfPatient",
        datatype: "JSON",
        data:{iPatientID:iPatientID,iServiceID:iServiceID,iScheduleID:iScheduleID},
        success: function(data) {
            if(data != false)
            {
                var iSrNo = 1;
                $.each(data, function(key, value)
                {
                    if (value['collection_sample_barcode'] == null)
                    {
                        var sBarcode = "NA";
                    }else{
                        var sBarcode = value['collection_sample_barcode'];
                    }

                    if (typeof value['aLuminex200Samples']['batch_no'] != "undefined")
                    {
                        var sBatchNo = value['aLuminex200Samples']['batch_no'];
                        var sTubeNo = value['aLuminex200Samples']['tube_no'];
                    }else{
                        var sBatchNo = "NA";
                        var sTubeNo = "NA";
                    }

                    if (value['sample_status'] == -1 || value['sample_status'] == -2)
                    {
                        sBoBSoftwareSample += '<tr style="background:#ff7c7c;">';
                    }else{
                        sBoBSoftwareSample += '<tr>';
                    }
                        sBoBSoftwareSample += '<td>'+iSrNo+'</td>';
                        sBoBSoftwareSample += '<td>'+sBarcode+'</td>';
                        sBoBSoftwareSample += '<td>'+value['sample_label']+'</td>';
                        sBoBSoftwareSample += '<td>'+sBatchNo+'</td>';
                        sBoBSoftwareSample += '<td>'+sTubeNo+'</td>';

                        if (sBatchNo !== "NA")
                        {
                            if (value['aLuminex200Samples']['is_bob_software_completed'] == 1)
                            {
                                sBoBSoftwareSample += '<td>Completed on '+value['aLuminex200Samples']['dBoBSoftwareCompletionDate']+'</td>';
                            }else{
                                sBoBSoftwareSample += '<td>Not Completed</td>'; 
                            }
                        }else{
                            sBoBSoftwareSample += '<td>NA</td>';
                        }
    
                    sBoBSoftwareSample += '</tr>';

                    iSrNo ++;
                });
                $("#idBoBSoftwareSamples").html(sBoBSoftwareSample);
            }else{
                $("#idBoBSoftwareSamples").html("<span style='color:red;'>No samples found in BoB software.</span>");
            }
        }            
    });
}

//! TO get the samples for sample rejection helper..
function molecularLabSampleRejection()
{
    var sSampleRow = '';
    $.ajax({
        type: "POST",
        url: "ajaxBatchProcessingManagement.php?sFlag=getSampleListOfPatient",
        datatype: "JSON",
        data:{iPatientID:iPatientID,iServiceID:iServiceID,iScheduleID:iScheduleID},
        success: function(data) { 
            if(data != false)
            {
                var iSrNo = 1;
                $.each(data, function(key, value)
                {
                    if (value['collection_sample_barcode'] == null)
                    {
                        var sBarcode = "NA";
                    }else{
                        var sBarcode = value['collection_sample_barcode'];
                    }

                    if (typeof value['aLuminex200Samples']['batch_no'] != "undefined")
                    {
                        var sBatchNo = value['aLuminex200Samples']['batch_no'];
                        var sTubeNo = value['aLuminex200Samples']['tube_no'];
                    }else{
                        var sBatchNo = "NA";
                        var sTubeNo = "NA";
                    }

                    if (value['sample_status'] == -1 || value['sample_status'] == -2)
                    {
                        sSampleRow += '<tr style="background:#ff7c7c;">';
                        var sReject = '<a class="btn btn-danger btn-small" disabled>Rejected</a>';
                    }else{
                        sSampleRow += '<tr>';
                        var sReject = '<a data-toggle="modal" data-target="#idModalSampleRejection" class="btn btn-danger btn-small classSampleRejection" data-sample-id = "'+value['sample_id']+'" data-batch-master-id = "'+value['aLuminex200Samples']['batch_master_id']+'" data-sample-request-id = "'+value['sample_request_id']+'" data-sample-collection-instance-id = "'+value['sample_collection_instance_id']+'">Reject</a>';
                    }

                        sSampleRow += '<td>'+iSrNo+'</td>';
                        sSampleRow += '<td>'+sBarcode+'</td>';
                        sSampleRow += '<td>'+value['sample_label']+'</td>';
                        sSampleRow += '<td>'+sBatchNo+'</td>';
                        sSampleRow += '<td>'+sTubeNo+'</td>';
                        sSampleRow += '<td>'+sReject+'</td>';
                    sSampleRow += '</tr>';

                    iSrNo ++;
                });
                $("#idSampleRejection").html(sSampleRow);
            }else{
                $("#idSampleRejection").html("<span style='color:red;'>No samples found in nano drop.</span>");
            }
        }            
    });
}