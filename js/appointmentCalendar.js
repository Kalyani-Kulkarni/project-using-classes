//Appointment Calendar
function ScheduleCalender(dCurrentDate, GlobalPatientOrMember, iClinicID, iScheduleStatusMasterID, iLoggedIdUserUserTypeID, iloggedInUserStaffID) {
	var self = this;
	self.dChooseDateFrom = ko.observable();	
	self.dChooseDateTo = ko.observable();	
	self.iLoggedIdUserUserTypeID = ko.observable();
	self.iLoggedIdUserUserTypeID(iLoggedIdUserUserTypeID);
	self.iLoggedInUserClinicID = ko.observable();
	self.iLoggedInUserClinicID(iClinicID);
	self.dChooseDateFrom(dCurrentDate);
	self.dChooseDateTo(dCurrentDate);
	self.iSelectedStates = ko.observable("All");
	self.iSelectedServiceTypeID = ko.observable(0);	
	self.iSelectedDoctor = ko.observable(0);	
	self.aSelectedClinic = ko.observableArray([]);
	self.aSelectedClinic([iClinicID]);
	self.aCentreList = ko.observableArray();
	self.aPatientScheduleList = ko.observableArray([]);
	self.iPatientScheduleCount = ko.observable(0);
	self.iODPatientScheduleCount = ko.observable(0);
	self.iPatientScheduleStudyCount = ko.observable(0);
	self.iODPatientScheduleStudyCount = ko.observable(0);
	self.iTotalScheduleAppointmentCount = ko.observable(0);
	self.iStatusScheduledCount = ko.observable(0);
	self.iStatusSuccessfulCount = ko.observable(0);
	self.iStatusCancelledCount = ko.observable(0);
	self.iStatusConfirmedCount = ko.observable(0);
	self.iStatusEmailReminderCount = ko.observable(0);
	self.iStatusSmsReminderCount = ko.observable(0);
	self.iStatusNotEmailReminderCount = ko.observable(0);
	self.iStatusNotSmsReminderCount = ko.observable(0);
	self.fPatientPendingAmount = ko.observableArray();
	self.sPatientOrMember = ko.observable(GlobalPatientOrMember);	
	self.sPatientScheduleStatus = ko.observableArray([]);	
	self.iScheduleStatusMasterID = ko.observable(iScheduleStatusMasterID); //Default Master ID for Schedule Statuses
	self.aStatusCounts = ko.observableArray();
	//for loading display
	self.bLoadingDisplay = ko.observable();
	self.bLoadingDisplay(false);

	if(!(self.iLoggedIdUserUserTypeID() == '1' || self.iLoggedIdUserUserTypeID() == '2') && !(self.iLoggedIdUserUserTypeID() == '3') ){
		//other than super admin and admin
		self.aSelectedClinic([iClinicID]);
	}else if(self.iLoggedIdUserUserTypeID() == '3'){
		//If doctor
		self.aSelectedClinic([iClinicID]);		
		// self.iSelectedDoctor(iloggedInUserStaffID);
	}

	self.addSchedule = function() {
		$.getJSON("ajaxEhr.php?sFlag=getAllScheduleAppointments&dDateFrom="+self.dChooseDateFrom()+"&dDateTo="+self.dChooseDateTo()+"&iSelectedStates="+self.iSelectedStates()+"&iSelectedServiceTypeID="+self.iSelectedServiceTypeID()+"&iSelectedDoctor="+self.iSelectedDoctor()+"&aSelectedClinic="+self.aSelectedClinic(), function(aDataSchedules) {

			//Total Schedule Appointments
			self.iTotalScheduleAppointmentCount(aDataSchedules.iStatusCount.iTotalScheduleAppointmentCount);
			//Set Status counts
        	self.aStatusCounts([]);
        	self.aStatusCounts(aDataSchedules.iStatusCount.aStatusCountArray);
			self.iStatusEmailReminderCount(aDataSchedules.iStatusCount.iStatusEmailReminderCount);
	        self.iStatusSmsReminderCount(aDataSchedules.iStatusCount.iStatusSmsReminderCount);	      
	        self.iStatusNotEmailReminderCount(aDataSchedules.iStatusCount.iStatusNotEmailReminderCount);
	        self.iStatusNotSmsReminderCount(aDataSchedules.iStatusCount.iStatusNotSmsReminderCount);

	        //Set OD, not-OD patient and service counts
	        self.iPatientScheduleCount(aDataSchedules.iStatusCount.iNotODPatientScheduleCount);
			self.iPatientScheduleStudyCount(aDataSchedules.iStatusCount.iNotODPatientScheduleStudyCount);
			self.iODPatientScheduleCount(aDataSchedules.iStatusCount.iODPatientScheduleCount);
			self.iODPatientScheduleStudyCount(aDataSchedules.iStatusCount.iODPatientScheduleStudyCount);	  

			// //Set Centres
			$.getJSON("ajaxEhr.php?sFlag=GetAllClinicList", function(aDataClinic) {  			
				self.aCentreList([]); //remove  previous values			
	            var aMappedCentres = $.map(aDataClinic, function(item) { 
	            	//Check if Loggedin User is Admin or super admin
	            	if(self.iLoggedIdUserUserTypeID() == '1' || self.iLoggedIdUserUserTypeID() == '2'){
		                self.aCentreList.push(new Clinic(item)); 
		                $.each(aDataSchedules.iStatusCount.aClinics, function(indexT, aItemT) {
		                	if(aItemT == item['clinic_id']){                		
		                		self.aSelectedClinic.push(new Clinic(item).iClinicID());
		                	}
		            	});
	            	}else{
	            		if(self.iLoggedInUserClinicID() == item['clinic_id']){
	            			self.aCentreList.push(new Clinic(item)); 
			                $.each(aDataSchedules.iStatusCount.aClinics, function(indexT, aItemT) {
			                	if(aItemT == item['clinic_id']){                		
			                		self.aSelectedClinic.push(new Clinic(item).iClinicID());
			                	}
			            	});
	            		}
	            	}
	            });                        
	        }); 

			//Create object of fetched data
	   		var aMappedSchedule = $.map(aDataSchedules.aPatientDetailsBolck, function(aDataItem) { 
				self.aPatientScheduleList.push(new PatientSchedule(aDataItem));
	        });	 	

	        //Hide Loader
			self.bLoadingDisplay(false);           
	    });		        
	};	
    
    self.getAllCentres = function() {
        $.getJSON("ajaxEhr.php?sFlag=GetAllClinicList", function(aDataClinic) {  			
			self.aCentreList([]); //remove  previous values			
	        var aMappedCentres = $.map(aDataClinic, function(item) { 
	            self.aCentreList.push(new Clinic(item));
	        });                        
	    }); 
    };  

    self.aStatusDropdownList = ko.observableArray([]);
    self.getAllStatusDropdownList = function() {
        $.getJSON("ajaxNewAppointmentCalendar.php?sFlag=GetAllScheduleAppointmentStatus&iScheduleStatusMasterID="+self.iScheduleStatusMasterID(), function(aDataStatus) { 
        	self.aStatusDropdownList.push({'status_name': "All Statuses", 'status_value': "All"});

        	$.each(aDataStatus, function(index, aItem) {
		    	self.aStatusDropdownList.push(aItem);
		    });
		}); 
    };
    self.getAllStatusDropdownList();   

	this.init = function(){
		self.bLoadingDisplay(true);
		self.addSchedule(); //initiate to autoload	
	}
	this.init();

	
	self.exportToken = function(iPatientScheduleID) {
		return "exportTokenForPatientScheduleAppointment.php?iScheduleID="+iPatientScheduleID;
	};

	self.openPatientScheduleHref = function(iPatientScheduleID) {
		return "viewSchedule.php?iScheduleID="+iPatientScheduleID;
	};

	self.updateOPDBillPatientScheduleHref = function(iBillID) {
		return "updateOPDBill.php?iBillID="+iBillID;
	};

	self.scheduleServiceLinkHref = function(iScheduleID, iGetPatientID,dScheduleDate,tScheduleTime,iScheduleClinicID, data) {

		if( data.iIsServiceCompleted() == '0'){
			//Add Page
			return data.sAddPage()+"?iServiceID="+data.iServiceID()+"&iScheduleID="+iScheduleID+"&tScheduleTime="+tScheduleTime+"&dScheduleDate="+dScheduleDate+"&iSchStaffID="+data.iStaffID()+"&iPatientID="+iGetPatientID+"&iClinicID="+iScheduleClinicID+"&iDoctorOrTeamFlag="+data.iAssignedDoctorTeamID();

		}else if(data.iIsServiceCompleted() == '1'){
			//View Page
			return data.sViewPage()+"?iServiceID="+data.iServiceID()+"&iScheduleID="+iScheduleID+"&tScheduleTime="+tScheduleTime+"&dScheduleDate="+dScheduleDate+"&iSchStaffID="+data.iStaffID()+"&iPatientID="+iGetPatientID+"&iClinicID="+iScheduleClinicID+"&iDoctorOrTeamFlag="+data.iAssignedDoctorTeamID()+"&iConsultationID=";
		}else if(data.iIsServiceCompleted() == '2'){
			//Draft Page
			return data.sDraftPage()+"?iServiceID="+data.iServiceID()+"&iScheduleID="+iScheduleID+"&tScheduleTime="+tScheduleTime+"&dScheduleDate="+dScheduleDate+"&iSchStaffID="+data.iStaffID()+"&iPatientID="+iGetPatientID+"&iClinicID="+iScheduleClinicID+"&iDoctorOrTeamFlag="+data.iAssignedDoctorTeamID()+"&iConsultationID=";
		}		
	};	

	self.getByLookup = function(dDateFrom, dDateTo){
		self.bLoadingDisplay(true);
		self.dChooseDateFrom(dDateFrom);
		self.dChooseDateTo(dDateTo);
		self.iPatientScheduleCount(0);
		self.iODPatientScheduleCount(0);
		self.iPatientScheduleStudyCount(0);
		self.iODPatientScheduleStudyCount(0);
		self.iStatusScheduledCount(0);
		self.iStatusSuccessfulCount(0);
		self.iStatusCancelledCount(0);
		self.iStatusConfirmedCount(0);
		self.iStatusEmailReminderCount(0);
		self.iStatusSmsReminderCount(0);
		self.iStatusNotEmailReminderCount(0);
		self.iStatusNotSmsReminderCount(0);		
		self.aPatientScheduleList([]);	

		if(!(self.iLoggedIdUserUserTypeID() == '1' || self.iLoggedIdUserUserTypeID() == '2')  && !(self.iLoggedIdUserUserTypeID() == '3')){
			//other than super admin and admin
			self.aSelectedClinic([iClinicID]);
		}else if(self.iLoggedIdUserUserTypeID() == '3'){
			//If doctor
			self.aSelectedClinic([iClinicID]);		
			// self.iSelectedDoctor(iloggedInUserStaffID);
		}
		//initiate to autoload		
		self.addSchedule();		
	};	

	//call model to update patient schedule ststus
	self.updataPatientScheduleStatus = function(aData){	
		var iScheduleID = aData.iScheduleID();
		var sPatientName = aData.aPatientList().sPatientName();
		var iVisitNo = aData.iScheduleNo();
		var tScheduleTime = aData.tScheduleTime();
		self.showScheduleStatus(iScheduleID, sPatientName, iVisitNo, tScheduleTime);
	}	
    
	//! Schedule status module
    self.showScheduleStatus = function(iScheduleID, sPatientName, iVisitNo, tScheduleTime) {
        $('#idSchStatusScheduleID').val(iScheduleID);
        
        $.ajax({
            url: "ajaxNewAppointmentCalendar.php?sFlag=GetScheduleStatusData&iScheduleStatusMasterID="+self.iScheduleStatusMasterID()+"&iScheduleID="+iScheduleID,
            asyn: false,
            success: function (data){
                $('.classScheduleStatusHistory').empty();
                if($.trim(data) != false){
                    // data = JSON.parse(data);
                    var bNotCompleted = false;
                    var sNotCompleteRemark = "";
                        
                    $('#idModalScheduleStatus .classPatientName').text(sPatientName);
                    $('#idModalScheduleStatus .classVisitNo').text(iVisitNo);

                    var str = [];
                    str.push("<ul>");
                    var aScheduleHistory = data.aStatuses;
                    for(var iii=0; iii<aScheduleHistory.length; iii++) {
                    	if(aScheduleHistory[iii].dtStatusTime){                    		
	                        var time = aScheduleHistory[iii].dtStatusTime.split(":");
	                        var time = time[0]+":"+time[1];
	                    }else{
	                    	var time = "00:00" ;
	                    }
                        str.push("<li>"+time+" <strong>"+aScheduleHistory[iii].sNewStatus+"</strong></li>");

                        if(aScheduleHistory[iii].bNewStatusIsTeminal) {
                            bNotCompleted = true;
                            sNotCompleteRemark = aScheduleHistory[iii].sRemarks;
                        }
                    }                                              
                    str.push("</ul>");              

                    if(bNotCompleted){
                        str.push("<p><strong>&nbsp;&nbsp;&nbsp;Note: </strong>"+sNotCompleteRemark+"</p>");
                    }
                    
                    $(".classScheduleStatusHistory").html(str.join(" "));                    

                    if(data.aCurrentStatus[0].bNewStatusIsTeminal) {                   
                        $('.classUpdateScheduleStatus').hide();
                        $('#idModalScheduleStatus').modal("show");
                        return;
                    }else{
                        $('.classUpdateScheduleStatus').show();                    	
                    }

                    var oCurrentTime = new Date();
                    var sCurrentTime = oCurrentTime.getHours()+":"+oCurrentTime.getMinutes()
                    var strNext = [];
                    $('.classNextStatus').empty();                    
                    var aScheduleUpcomming = data.aUpcommingStatus;                                    
                    if(aScheduleUpcomming){
                    	if(aScheduleUpcomming.length > 1){
		                    for(var iii=0; iii<aScheduleUpcomming.length; iii++) {

		                    	if(!(aScheduleUpcomming[iii].bNewStatusIsTeminal) && aScheduleUpcomming[iii].bNewStatusIsChecked){
		                    		if(aScheduleUpcomming[iii].bNewStatusIsReasoned){
		                    			strNext.push("<label class='card-lable' for='iNextStatus'><span id='idNextStatusLabel'>"+aScheduleUpcomming[iii].status_name+"&nbsp;</span></label><input type='radio' class='classRequiredReason' name='iNextStatus' value="+aScheduleUpcomming[iii].new_status+" id='idNextStatus' checked >&nbsp;&nbsp;&nbsp;");
		                    		}else{
		                    			strNext.push("<label class='card-lable' for='iNextStatus'><span id='idNextStatusLabel'>"+aScheduleUpcomming[iii].status_name+"&nbsp;</span></label><input type='radio' name='iNextStatus' value="+aScheduleUpcomming[iii].new_status+" id='idNextStatus' checked >&nbsp;&nbsp;&nbsp;");
		                    		}

		                    	}else{
		                    		if(aScheduleUpcomming[iii].bNewStatusIsReasoned){
		                    			strNext.push("<label class='card-lable' for='iNextStatus'><span id='idNextStatusLabel'>"+aScheduleUpcomming[iii].status_name+"&nbsp;</span></label><input type='radio' class='classRequiredReason' name='iNextStatus' value="+aScheduleUpcomming[iii].new_status+" id='idNextStatus' >&nbsp;&nbsp;&nbsp;");
		                    		}else{
		                    			strNext.push("<label class='card-lable' for='iNextStatus'><span id='idNextStatusLabel'>"+aScheduleUpcomming[iii].status_name+"&nbsp;</span></label><input type='radio' name='iNextStatus' value="+aScheduleUpcomming[iii].new_status+" id='idNextStatus' >&nbsp;&nbsp;&nbsp;");
		                    		}

		                    	}		                    	
		                    }
		                }else{
		                	for(var iii=0; iii<aScheduleUpcomming.length; iii++) {
		                    	if(aScheduleUpcomming[iii].bNewStatusIsReasoned){
		                    		strNext.push("<label class='card-lable' for='iNextStatus'><span id='idNextStatusLabel'>"+aScheduleUpcomming[iii].status_name+"&nbsp;</span></label><input type='radio' class='classRequiredReason' name='iNextStatus' value="+aScheduleUpcomming[iii].new_status+" id='idNextStatus' checked >&nbsp;&nbsp;&nbsp;");	                    		
		                    		$iShowReason = $iShowReason + 1;
		                    	}else{
		                    		strNext.push("<label class='card-lable' for='iNextStatus'><span id='idNextStatusLabel'>"+aScheduleUpcomming[iii].status_name+"&nbsp;</span></label><input type='radio' name='iNextStatus' value="+aScheduleUpcomming[iii].new_status+" id='idNextStatus' checked >&nbsp;&nbsp;&nbsp;");
		                    	}
		                    }
		                }
	                }
	                $(".classNextStatus").html(strNext.join(" "));	                 
                    $('#idNextStatusTime').timepicker('setTime', new Date());
                    $('#idModalScheduleStatus').modal("show");                    
                }else{
                    alert("Some error was occure while cancel the appointment. Please try again.");
                }
            }
        });
    }    
}

function PatientSchedule(aUniquePatient){	
	var aUniquePatient = aUniquePatient;	
	var self = this;
	self.iPatientID = ko.observable(aUniquePatient.iPatientID);
	self.sPatientNo = ko.observable(aUniquePatient.sPatientNo);
	self.sPatientName = ko.observable(aUniquePatient.sPatientName);
	self.iMobile = ko.observable(aUniquePatient.iMobile);
	self.iPhone = ko.observable(aUniquePatient.iPhone);
	self.sEmail = ko.observable(aUniquePatient.sEmail);
	self.fPendingAmount = ko.observable(aUniquePatient.fPendingAmount);
	self.aScheduleList = ko.observableArray([]);

	if(typeof aUniquePatient['schedule'] !== 'undefined' && aUniquePatient['schedule'] !== "" && aUniquePatient['schedule'] !== " " && aUniquePatient['schedule'] !== null){
		$.each(aUniquePatient['schedule'], function(index, aItem) {
	    	self.aScheduleList.push(new Schedule(aItem));
	    });
	}
}

function Schedule(aAllScheduleData) {
	var aAllScheduleData = aAllScheduleData;	
	var self = this;
	self.iGetPatientID = ko.observable(aAllScheduleData.patient_id);
	self.iScheduleID = ko.observable(aAllScheduleData.schedule_id);
	self.iScheduleNo = ko.observable(aAllScheduleData.schedule_no);
	self.dScheduleDate = ko.observable(aAllScheduleData.schedule_date);
	self.tScheduleTime = ko.observable(aAllScheduleData.schedule_time);
	self.tScheduleEndTime = ko.observable(aAllScheduleData.schedule_end_time);
	self.tScheduleDuration = ko.observable(aAllScheduleData.schedule_duration);
	self.sScheduleNote = ko.observable(aAllScheduleData.schedule_notes);
	self.iScheduleChannelID  = ko.observable(aAllScheduleData.channel_id);
	self.iScheduleClinicID  = ko.observable(aAllScheduleData.clinic_id);
	self.iScheduleReferralID = ko.observable(aAllScheduleData.referral_id);
	self.iScheduleOutsourceDignosticID = ko.observable(aAllScheduleData.outsource_diagnostic_id);
	self.iScheduleIsCompleted = ko.observable(aAllScheduleData.iIsSchCompleted);
	self.iScheduleAddedBy = ko.observable(aAllScheduleData.added_by);
	self.iScheduleMethodID;
	self.iScheduleOPID;
	self.iScheduleIsConfirmed = ko.observable(aAllScheduleData.iIsSchConfirmed);
	self.iScheduleStatus;
	self.iSchedueIsCancelledAppointment = ko.observable(aAllScheduleData.iIsSchCancelled);
	self.iSchedueIsNotificationSent = ko.observable(aAllScheduleData.iIsSchNotificationSent);
	self.iIsHCPSchedule = ko.observable(aAllScheduleData.IsHCPSchedule);
	self.iSchedueIsIpd;
	self.iSchedueType;
	self.iCustomerMasterID; 
	self.iCustomerBranchID; 
	self.iCustomerDoctorID; 
	self.iCustomerSchedulePriority; 
	self.iCustomerScheduleStatus;
	self.iScheduleActiveProjectID;
	var oClinic;
	var oPatient;
	var oScheduleAppointment;
	var oCustomer;
	var oReferralDoctor1;
	var oReferralDoctor2;
	var oBookedBy;
	var oBill;
	var sScheduleStatus;
	self.aPatientList = ko.observableArray();
	self.aPatientList(new Patient(aAllScheduleData.aPatientDetails));
	self.PatientScheduleStatusDetails = ko.observableArray();
	self.PatientScheduleStatusDetails(aAllScheduleData.aPatientScheduleStatusDetails);
	self.aScheduleAppointmentList = ko.observableArray();
	self.aClinicList = ko.observableArray();
	self.aClinicList(new Clinic(aAllScheduleData.aClinicDetails));
	self.aReferralDoctor1List = ko.observableArray();
	self.aReferralDoctor2List = ko.observableArray();
	self.aBookedByList = ko.observableArray();
	self.aBookedByList(new BookedBy(aAllScheduleData.aBookedByDetails));
	self.aBillList = ko.observableArray();	
	self.aBillList(new Bill(aAllScheduleData.aScheduleBill));

	if(typeof aAllScheduleData['appointments'] !== 'undefined' && aAllScheduleData['appointments'] !== "" && aAllScheduleData['appointments'] !== " " && aAllScheduleData['appointments'] !== null){
		$.each(aAllScheduleData['appointments'], function(index, aItem) {
	    	self.aScheduleAppointmentList.push(new ScheduleAppointment(aItem));
	    });
	}

	if(typeof aAllScheduleData['aScheduleRefferalDoc1'] !== 'undefined' && aAllScheduleData['aScheduleRefferalDoc1'] !== "" && aAllScheduleData['aScheduleRefferalDoc1'] !== " " && aAllScheduleData['aScheduleRefferalDoc1'] !== null){
	    $.each(aAllScheduleData['aScheduleRefferalDoc1'], function(index, aD1Item) {
	    	self.aReferralDoctor1List.push(new ReferralDoctor1(aD1Item));
	    });
	}

	if(typeof aAllScheduleData['aScheduleRefferalDoc2'] !== 'undefined' && aAllScheduleData['aScheduleRefferalDoc2'] !== "" && aAllScheduleData['aScheduleRefferalDoc2'] !== " " && aAllScheduleData['aScheduleRefferalDoc2'] !== null){
	    $.each(aAllScheduleData['aScheduleRefferalDoc2'], function(index, aD2Item) {
	    	self.aReferralDoctor2List.push(new ReferralDoctor2(aD2Item));
	    });
    } 
}

function Patient(aPatientData) {	
	var self = this;	
	self.iPatientID= ko.observable(aPatientData.iPatientID);
    self.sPatientNo= ko.observable(aPatientData.sPatientNo);
    self.sPatientTitle= ko.observable(aPatientData.sPatinetTitle);
    self.sPatientName= ko.observable(aPatientData.sPatientName);
    self.sPatientFirstName= ko.observable(aPatientData.sPatientfName); 
	self.sPatientMiddleName= ko.observable(aPatientData.sPatientmName); 
	self.sPatientLastName= ko.observable(aPatientData.sPatientlName); 
	self.sPatientAddress= ko.observable(aPatientData.sAddress);
	self.sPatientCity= ko.observable(aPatientData.sCity);
	self.sPatientState= ko.observable(aPatientData.sState);
	self.sPatientCountry= ko.observable(aPatientData.sCountry);
	self.iPatientPincode= ko.observable(aPatientData.iPincode); 
	self.iPatientPhone= ko.observable(aPatientData.iPhone); 
    self.iPatientMobile= ko.observable(aPatientData.iMobile);
    self.sPatientEmail= ko.observable(aPatientData.sEmail);
    self.sPatientOfficeAddress= ko.observable(aPatientData.sOfficeAddress); 
	self.sPatientOfficeCity= ko.observable(aPatientData.sOfficeCity); 
	self.sPatientOfficeState= ko.observable(aPatientData.sOfficeState); 
	self.sPatientOfficeCountry= ko.observable(aPatientData.sOfficeCountry);
	self.iPatientOfficePincode= ko.observable(aPatientData.iOfficePincode); 
	self.iPatientOfficePhone= ko.observable(aPatientData.iOfficePhone); 
	self.iPatientOfficeMobile= ko.observable(aPatientData.iOfficeMobile); 
	self.sPatientOfficeEmail= ko.observable(aPatientData.sOfficeEmail);
    self.sPatientDateOfBirth= ko.observable(aPatientData.dDOB);
    self.iPatientAge= ko.observable(aPatientData.iAge);
    self.sPatientGender= ko.observable(aPatientData.sGender);
    self.sPatientsBloodGroup= ko.observable(aPatientData.sBloodGroup);
    self.sPatientEmergencyContactName= ko.observable(aPatientData.sEmergencyContactName);
    self.iPatientEmergencyContactNo= ko.observable(aPatientData.iEmergencyContactNo);
	self.sPatientEmergencyContactRelation= ko.observable(aPatientData.sEmergencyContactRelation);
	self.iPatientUID= ko.observable(aPatientData.sUID);
	self.iPatientIsVIP= ko.observable(aPatientData.iIsVIP);
	self.iPatientMembershipTypeID= ko.observable(aPatientData.iMembershipTypeID);
	self.iPatientCorporatePlanID= ko.observable(aPatientData.iCorporatePlanID);
	self.sPatientFamilyCode= ko.observable(aPatientData.sFamilyCode);
	self.iPatientIsChild= ko.observable(aPatientData.iIsChild);
	self.iPatientIsFrozen= ko.observable(aPatientData.iIsFrozen);  
}

function ScheduleAppointment(aScheduleAppointmentData) {	
	var self = this;
	self.iStaffID = ko.observable(aScheduleAppointmentData.staff_id);
	self.sStaffName = ko.observable(aScheduleAppointmentData.staff_name);
	self.iRoomID = ko.observable(aScheduleAppointmentData.room_id);
	self.iServiceID = ko.observable(aScheduleAppointmentData.service_id);
	self.iServiceTypeID = ko.observable(aScheduleAppointmentData.service_type_id);
	self.sServiceName = ko.observable(aScheduleAppointmentData.service_name);
	self.sServiceTypeName = ko.observable(aScheduleAppointmentData.service_type);
	self.iAssignedDoctorTeamID = ko.observable(aScheduleAppointmentData.doctor_or_team_flag);
	self.iIsHcp = ko.observable(aScheduleAppointmentData.is_hcp);
	self.iHcpPlanID = ko.observable(aScheduleAppointmentData.hcp_plan_id);
	self.sHcpPlanName = ko.observable(aScheduleAppointmentData.hcp_plan_name);
	self.iIsPathoProfile = ko.observable(aScheduleAppointmentData.is_patho_profile);
	self.iIsServiceCompleted = ko.observable(aScheduleAppointmentData.iIsServiceCompleted);	
	self.sAddPage = ko.observable(aScheduleAppointmentData.add_page);
	self.sDraftPage = ko.observable(aScheduleAppointmentData.draft_page);
	self.sViewPage = ko.observable(aScheduleAppointmentData.view_page);
	self.sUpdatePage = ko.observable(aScheduleAppointmentData.update_page);	
	var oStaff;
	var oService;
	var oRoom;	
	self.aStaffList = ko.observableArray();
	self.aServiceList = ko.observableArray();
	self.aRoomList = ko.observableArray();
	self.bIsPackageService = ko.observable(aScheduleAppointmentData.is_package_service);
}
	

function Bill(aBillData) {
	var self = this;
	self.iBillID= ko.observable(aBillData.bill_id);
	self.iBillNo= ko.observable(aBillData.bill_no);
	self.dBillDate= ko.observable(aBillData.bill_date);
	self.iPatientID= ko.observable(aBillData.patient_id);
	self.iScheduleID= ko.observable(aBillData.schedule_id);
	self.fGrandAmount= ko.observable(aBillData.grand_amount);
	self.iDiscount= ko.observable(aBillData.discount); 
	self.iLoyaltyDiscount= ko.observable(aBillData.loyalty_discount);
	self.iLoyaltyDiscountRate= ko.observable(aBillData.loyalty_discount_rate); 
	self.iLoyaltyDiscountModified= ko.observable(aBillData.loyalty_discount_modified);
	self.fTax= ko.observable(aBillData.tax); 
	self.fNetAmount= ko.observable(0);
	self.iIsPackageServiceCount= ko.observable(aBillData.iIsPackageServiceCount);

	if( typeof aBillData.net_amount !== 'undefined' && aBillData.net_amount != "" && aBillData.net_amount != " " && aBillData.net_amount != null){ 
		self.fNetAmount= ko.observable(parseFloat(aBillData.net_amount).toFixed());
	}
	self.iIsPaid= ko.observable(aBillData.IsPaid);
	self.iIsCancelledBill= ko.observable(aBillData.is_cancelled_bill);
	var oBillItem;
	self.aBillItemList = ko.observableArray();
	self.aBillPayments = ko.observableArray();
	self.aBillPayments(aBillData.bill_payments);
	self.fPartialPaymentAmount= ko.observable(0);
	self.fPartialPendingPayAmount= ko.observable(0);
	
	if(typeof self.aBillPayments() !== 'undefined' && self.aBillPayments() != "" && self.aBillPayments() != " " && self.aBillPayments() != null){
		
		if(typeof self.aBillPayments().paid_amount_total !== 'undefined' && self.aBillPayments().paid_amount_total != "" && self.aBillPayments().paid_amount_total != " " && self.aBillPayments().paid_amount_total != null){
			self.fPartialPaymentAmount(parseFloat(self.aBillPayments().paid_amount_total).toFixed());	
			self.fPartialPendingPayAmount( parseFloat(self.fNetAmount() - parseFloat(self.aBillPayments().paid_amount_total)).toFixed() );	
		}	
	}	

	if(typeof aBillData['bill_service_items'] !== 'undefined' && aBillData['bill_service_items'] != " " && aBillData['bill_service_items'] != null){
		$.each(aBillData['bill_service_items'], function(index, aBillItems) {
	    	self.aBillItemList.push(new BillItem(aBillItems));
	    });
	}
  	
}

function BillItem(aBillItemData) {
	var self = this;
	self.iBillItemID= ko.observable(aBillItemData.item_id);
	self.iBillServiceTypeID= ko.observable(aBillItemData.service_type_id); 
	self.iBillItemTypeID= ko.observable(aBillItemData.bill_item_type_id);
	self.iItemName= ko.observable(aBillItemData.item_name); 
	self.iBillItemQty= ko.observable(aBillItemData.qty); 
	self.fOriginalRate= ko.observable(aBillItemData.original_rate); 
	self.fAppliedRate= ko.observable(aBillItemData.applied_rate); 
	self.sExtra= ko.observable(aBillItemData.extra); 
	self.sBillItemInformation= ko.observable(aBillItemData.bill_item_information); 
	self.dBillItemDate= ko.observable(aBillItemData.bill_item_date); 
	self.fItemTaxAmount= ko.observable(aBillItemData.item_tax_amount); 
	self.fItemTaxRate= ko.observable(aBillItemData.item_tax_rate); 
	self.fItemDiscountAmount= ko.observable(aBillItemData.item_discount_amount); 
	self.fItemDiscountRate= ko.observable(aBillItemData.item_discount_rate); 
	self.iBillItemIsHcp= ko.observable(aBillItemData.is_hcp);
}	

function Staff(aStaffData) {
	var self = this;
	self.iStaffID= ko.observable(aStaffData.iStaffID); 
	self.iStaffTypeID= ko.observable(aStaffData.iStaffTypeID); 
	self.sStaffType= ko.observable(aStaffData.sStaffType); 
	self.sStaffNo= ko.observable(aStaffData.sStaffNo); 
	self.sStaffName= ko.observable(aStaffData.sStaffName); 
	self.sStaffFname= ko.observable(aStaffData.sStaffFName); 
	self.sStaffMname= ko.observable(aStaffData.sStaffMName); 
	self.sStaffLname= ko.observable(aStaffData.sStaffLName); 
	self.sAddress= ko.observable(aStaffData.sStaffAddress); 
	self.sCity= ko.observable(aStaffData.sStaffCity); 
	self.sState= ko.observable(aStaffData.sStaffState); 
	self.sCountry= ko.observable(aStaffData.sStaffCountry); 
	self.iPincode= ko.observable(aStaffData.iStaffPincode); 
	self.iPhone= ko.observable(aStaffData.iStaffPhone); 
	self.iMobile= ko.observable(aStaffData.iStaffMobile); 
	self.iAltMobile= ko.observable(aStaffData.iAltStaffMobile); 
	self.sEmail= ko.observable(aStaffData.sStaffEmail); 
	self.sAltEmail= ko.observable(aStaffData.sAltStaffEmail); 
	self.sGender= ko.observable(aStaffData.sStaffGender); 
	self.dDob= ko.observable(aStaffData.dStaffDob); 
	self.dJoinDate= ko.observable(aStaffData.dJoinDate); 
	self.sQualification= ko.observable(aStaffData.sQualification); 
	self.sDesignation= ko.observable(aStaffData.sDesignation); 
	self.sStaffPhoto= ko.observable(aStaffData.sStaffPhoto); 
	self.sStaffPhotoDesc= ko.observable(aStaffData.sStaffPhotoDesc); 
	self.iStaffClinicID= ko.observable(aStaffData.iClinicID); 
	self.sSignature= ko.observable(aStaffData.sStaffSignature);
	self.iIsWellnessProviderStaff= ko.observable(aStaffData.iIsWellnewssProvider); 
	self.iStaffStatus= ko.observable(aStaffData.iStaffStatus);
}

function StaffDoctor(aStaffDoctorData) {
	var self = this;
	self.iStaffID= ko.observable(aStaffDoctorData.staff_id); 
	self.iStaffTypeID= ko.observable(aStaffDoctorData.staff_type_id); 
	self.sStaffNo= ko.observable(aStaffDoctorData.staff_no); 
	self.sStaffName= ko.observable(aStaffDoctorData.staff_name); 
	self.sStaffFname= ko.observable(aStaffDoctorData.staff_fname); 
	self.sStaffMname= ko.observable(aStaffDoctorData.staff_mname); 
	self.sStaffLname= ko.observable(aStaffDoctorData.staff_lname); 
	self.sAddress= ko.observable(aStaffDoctorData.address); 
	self.sArea= ko.observable(aStaffDoctorData.area); 
	self.sCity= ko.observable(aStaffDoctorData.city); 
	self.sState= ko.observable(aStaffDoctorData.state); 
	self.sCountry= ko.observable(aStaffDoctorData.country); 
	self.iPincode= ko.observable(aStaffDoctorData.pincode); 
	self.iPhone= ko.observable(aStaffDoctorData.phone); 
	self.iMobile= ko.observable(aStaffDoctorData.mobile); 
	self.iAltMobile= ko.observable(aStaffDoctorData.alt_mobile); 
	self.sEmail= ko.observable(aStaffDoctorData.email); 
	self.sAltEmail= ko.observable(aStaffDoctorData.alt_email); 
	self.sGender= ko.observable(aStaffDoctorData.gender); 
	self.dDob= ko.observable(aStaffDoctorData.dob); 
	self.dJoinDate= ko.observable(aStaffDoctorData.join_date); 
	self.sQualification= ko.observable(aStaffDoctorData.qualification); 
	self.sRegistrationNo= ko.observable(aStaffDoctorData.registration_no); 
	self.iDesignationID= ko.observable(aStaffDoctorData.designation_id); 
	self.sDesignation= ko.observable(aStaffDoctorData.sDesignation); 
	self.sStaffPhoto= ko.observable(aStaffDoctorData.staff_photo); 
	self.sStaffPhotoDesc= ko.observable(aStaffDoctorData.staff_photo_desc); 
	self.iStaffClinicID= ko.observable(aStaffDoctorData.clinic_id); 
	self.iStaffVerticalID= ko.observable(aStaffDoctorData.vertical_id); 
	self.iStaffSubverticalID= ko.observable(aStaffDoctorData.subvertical_id); 
	self.sSignature= ko.observable(aStaffDoctorData.signature);
	self.iIsWellnessProviderStaff= ko.observable(aStaffDoctorData.is_wellness_provider_staff); 
	self.iStaffStatus= ko.observable(aStaffDoctorData.status);
	self.iCreationUserID= ko.observable(aStaffDoctorData.creation_user_id);
	self.iUserID= ko.observable(aStaffDoctorData.user_id);
}

function Service(aServiceData){
	var self = this;
	self.iServiceID= ko.observable(aServiceData.service_id);
	self.iServiceTypeID= ko.observable(aServiceData.service_type_id);
	self.iServiceSubtypeID= ko.observable(aServiceData.service_subtype_id);
	self.sServiceName= ko.observable(aServiceData.service_name);
	self.sAddPage= ko.observable(aServiceData.add_page);
	self.sDraftPage= ko.observable(aServiceData.draft_page);
	self.sViewPage= ko.observable(aServiceData.view_page);
	self.sUpdatePage= ko.observable(aServiceData.update_page); 
	self.iTatFirst= ko.observable(aServiceData.tat_first);
	self.iServiceType  = ko.observable(aServiceData.service_type); 
	self.iServiceTypeCategory = ko.observable(aServiceData.service_type_category); 
	self.iSuperServiceTypeID = ko.observable(aServiceData.super_service_type_id);
}

function ServiceType(aServiceTypeData){
	var self = this;
	self.iServiceTypeID= ko.observable(aServiceTypeData.service_type_id);
	self.sServiceType= ko.observable(aServiceTypeData.service_type);
	self.iServiceTypeCategory= ko.observable(aServiceTypeData.service_type_category);
	self.iSuperServiceTypeID= ko.observable(aServiceTypeData.super_service_type_id);
}

function Clinic(aClinicData) {
	var self = this;
	self.iClinicID= ko.observable(aClinicData.clinic_id); 
	self.iClinicTypeID = ko.observable(aClinicData.clinic_type_id); 
	self.sClinicCode= ko.observable(aClinicData.clinic_code); 
	self.sClinicName= ko.observable(aClinicData.clinic_name);
	self.sRegistrationNo= ko.observable(aClinicData.registration_no);
	self.sClinicAddress= ko.observable(aClinicData.clinic_address); 
	self.sClinicCity= ko.observable(aClinicData.clinic_city);
	self.sClinicState= ko.observable(aClinicData.clinic_state) 
	self.sClinicCountry= ko.observable(aClinicData.clinic_country);
	self.iClinicPincode= ko.observable(aClinicData.clinic_pincode); 
	self.iClinicPhone= ko.observable(aClinicData.clinic_phone); 
	self.iClinicMobile= ko.observable(aClinicData.clinic_mobile);
	self.sClinicEmail= ko.observable(aClinicData.clinic_email); 
	self.sClinicWeb= ko.observable(aClinicData.clinic_web);
	self.iClinicFax= ko.observable(aClinicData.clinic_fax);
	self.iClinicLoyaltyCenterNo= ko.observable(aClinicData.clinic_loyalty_center_no); 
	self.sClinicLogo= ko.observable(aClinicData.clinic_logo);
	self.sClinicReportHeader= ko.observable(aClinicData.clinic_report_header);
	self.sClinicMapLink= ko.observable(aClinicData.clinic_map_link);
	self.iParentClinicID= ko.observable(aClinicData.parent_clinic_id);
	self.iGstInNo= ko.observable(aClinicData.gst_in_no); 	
	self.iClinicStatus= ko.observable(aClinicData.status);
}

function Room(aRoomData) {
	var self = this;
	self.iRoomID= ko.observable(aRoomData.iRoomID);
	self.sRoomNo= ko.observable(aRoomData.sRoomNo);
	self.sRoomName= ko.observable(aRoomData.sRoomName);
	self.iRoomClinicID= ko.observable(aRoomData.iClinicID);
}

function ReferralDoctor1(aRefDoc1Data) {
	var self = this;
	self.iScheduleReferralID= ko.observable(aRefDoc1Data.schedule_referral_id);
	self.iScheduleID= ko.observable(aRefDoc1Data.schedule_id);	
	self.iReferralID= ko.observable(aRefDoc1Data.referral_id);
	self.iPriority= ko.observable(aRefDoc1Data.priority);
	self.iReferralDoctor1ID= ko.observable(aRefDoc1Data.id);
	self.sReferralDoctor1Name= ko.observable(aRefDoc1Data.name);
	self.sReferralDoctor1Address= ko.observable(aRefDoc1Data.address);
	self.sReferralDoctor1Email= ko.observable(aRefDoc1Data.email);
	self.sReferralDoctor1Qualification= ko.observable(aRefDoc1Data.qualification);
	self.iReferralDoctor1Mobile= ko.observable(aRefDoc1Data.mobile);
	self.sReferralDoctor1Type= ko.observable(aRefDoc1Data.type);
	self.fReferralDoctor1revenue= ko.observable(aRefDoc1Data.revenue);
	self.iReferralDoctor1email_report= ko.observable(aRefDoc1Data.email_report);
	self.sReferralDoctor1report= ko.observable(aRefDoc1Data.report);
	self.iReferralDoctor1discountType= ko.observable(aRefDoc1Data.discount_type);
	self.iReferralDoctor1FeeType= ko.observable(aRefDoc1Data.fee_type);
	self.iReferralDoctor1DefaultDiscount= ko.observable(aRefDoc1Data.default_discount);
	self.iReferralDoctor1ClinicID= ko.observable(aRefDoc1Data.clinic_id);	
}

function ReferralDoctor2(aRefDoc2Data) {
	var self = this;
	self.iScheduleReferralID= ko.observable(aRefDoc2Data.schedule_referral_id);
	self.iScheduleID= ko.observable(aRefDoc2Data.schedule_id);	
	self.iReferralID= ko.observable(aRefDoc2Data.referral_id);
	self.iPriority= ko.observable(aRefDoc2Data.priority);
	self.iReferralDoctor2ID= ko.observable(aRefDoc2Data.id);
	self.sReferralDoctor2Name= ko.observable(aRefDoc2Data.name);
	self.sReferralDoctor2Address= ko.observable(aRefDoc2Data.address);
	self.sReferralDoctor2Email= ko.observable(aRefDoc2Data.email);
	self.sReferralDoctor2Qualification= ko.observable(aRefDoc2Data.qualification);
	self.iReferralDoctor2Mobile= ko.observable(aRefDoc2Data.mobile);
	self.sReferralDoctor2Type= ko.observable(aRefDoc2Data.type);
	self.fReferralDoctor2revenue= ko.observable(aRefDoc2Data.revenue);
	self.iReferralDoctor2email_report= ko.observable(aRefDoc2Data.email_report);
	self.sReferralDoctor2report= ko.observable(aRefDoc2Data.report);
	self.iReferralDoctor2discountType= ko.observable(aRefDoc2Data.discount_type);
	self.iReferralDoctor2FeeType= ko.observable(aRefDoc2Data.fee_type);
	self.iReferralDoctor2DefaultDiscount= ko.observable(aRefDoc2Data.default_discount);
	self.iReferralDoctor2ClinicID= ko.observable(aRefDoc2Data.clinic_id);	
}

function BookedBy(aBookedByData) {
	var self = this;
	self.iBookedByID= ko.observable(aBookedByData.iStaffID); 
	self.iBookedByTypeID= ko.observable(aBookedByData.iStaffTypeID); 
	self.sBookedByType= ko.observable(aBookedByData.sStaffType); 
	self.sBookedByNo= ko.observable(aBookedByData.sStaffNo); 
	self.sBookedByName= ko.observable(aBookedByData.sStaffName); 
	self.sBookedByFname= ko.observable(aBookedByData.sStaffFName); 
	self.sBookedByMname= ko.observable(aBookedByData.sStaffMName); 
	self.sBookedByLname= ko.observable(aBookedByData.sStaffLName); 
	self.sBookedByAddress= ko.observable(aBookedByData.sStaffAddress); 
	self.sBookedByCity= ko.observable(aBookedByData.sStaffCity); 
	self.sBookedByState= ko.observable(aBookedByData.sStaffState); 
	self.sBookedByCountry= ko.observable(aBookedByData.sStaffCountry); 
	self.iBookedByPincode= ko.observable(aBookedByData.iStaffPincode); 
	self.iBookedByPhone= ko.observable(aBookedByData.iStaffPhone); 
	self.iBookedByMobile= ko.observable(aBookedByData.iStaffMobile); 
	self.iBookedByAltMobile= ko.observable(aBookedByData.iAltStaffMobile); 
	self.sBookedByEmail= ko.observable(aBookedByData.sStaffEmail); 
	self.sBookedByAltEmail= ko.observable(aBookedByData.sAltStaffEmail); 
	self.sBookedByGender= ko.observable(aBookedByData.sStaffGender); 
	self.dBookedByDob= ko.observable(aBookedByData.dStaffDob); 
	self.dBookedByJoinDate= ko.observable(aBookedByData.dJoinDate); 
	self.sBookedByQualification= ko.observable(aBookedByData.sQualification); 
	self.sBookedByDesignation= ko.observable(aBookedByData.sDesignation); 
	self.sBookedByPhoto= ko.observable(aBookedByData.sStaffPhoto); 
	self.sBookedByPhotoDesc= ko.observable(aBookedByData.sStaffPhotoDesc); 
	self.iBookedByClinicID= ko.observable(aBookedByData.iClinicID); 
	self.sBookedBySignature= ko.observable(aBookedByData.sStaffSignature);
	self.iBookedByIsWellnessProvider= ko.observable(aBookedByData.iIsWellnewssProvider); 
	self.iBookedByStatus= ko.observable(aBookedByData.iStaffStatus);	
}

ko.bindingHandlers.contactPopover = {
    init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var attribute = ko.utils.unwrapObservable(valueAccessor());
        var templateContent = attribute.content;
        var popOverTemplate = "<div class='popOverClass' id='" + attribute.patientID + "-popover'>" + $(templateContent).html() + "</div>";
        $(element).popover({
            placement: 'right',
            content: popOverTemplate,
            html: true,
            title: 'Contact Details',
            trigger: 'focus'
        });
        // $(element).attr('patientID', "popover" + attribute.patientID + "_click");
        $(element).click(function () {
            $(".popOverClass").popover("hide");
            $(this).popover('toggle');
            var thePopover = document.getElementById(attribute.patientID + "-popover");
            childBindingContext = bindingContext.createChildContext(viewModel);
            ko.applyBindingsToDescendants(childBindingContext, thePopover);
        })
    }
}