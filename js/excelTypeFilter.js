var aSelect2Filter = [];
var aFilterData = [];
//! Get Proper data into template..
function getTemplateContentForTask (excelTypeFilter) {
    if(excelTypeFilter['bSortingAllowed'] == true){
        var sSorting = alphabeticSorting(excelTypeFilter['sColumnName']);
    }
    if (excelTypeFilter['iFilterType'] == 1) {
        var sTextFilter = sortingByText(excelTypeFilter['sColumnName']);
    }else if(excelTypeFilter['iFilterType'] == 2){
        var sTextFilter = sortBySelect2(excelTypeFilter['sColumnName'], excelTypeFilter['sFilterAjaxURL']);
    }else if(excelTypeFilter['iFilterType'] == 3){
        var sTextFilter = sortingByNumber(excelTypeFilter['sColumnName'], excelTypeFilter['sFilterAjaxURL']);
    }
    if(excelTypeFilter['bSelectClearAndAll'] == true){
        var sCheckBox = sortByCheckBoxClearAndAll(excelTypeFilter['sColumnName']);
        var sCheckBoxOptions = sortByCheckBoxOptions(excelTypeFilter['aQuickSelectOptions'], excelTypeFilter['sColumnName']);
    }

    return [
        sSorting,
        sTextFilter,
        sCheckBox,
        sCheckBoxOptions
    ].join("");
}

// For creating the html of sort by alphabet..
function alphabeticSorting(sColumnName){
    return '<div class="row">'+
                '<div class="col-md-12">'+
                    '<strong><span class="classSorting classSortingByAlphabet" data-column-name="'+sColumnName+'" data-sort-type="ASC">A to Z</span></strong>'+
                '</div>'+
            '</div>'+
            '<div class="row">'+
                '<div class="col-md-12">'+
                    '<strong><span class="classSorting classSortingByAlphabet" data-column-name="'+sColumnName+'" data-sort-type="DESC">Z to A</span></strong>'+
                '</div>'+
            '</div>';
}

// Created the html for the filter by text..
function sortingByText(sColumnName){
    return '<div class="row">'+
                '<div class="col-md-12">'+
                    '<input type="text" name="idTextFilter" id="idTextFilter" class="form-control classSorting classTextFilter" data-column-name="'+sColumnName+'">'+
                '</div>'+
            '</div>';
}
// Created the html for the filter by Number..
function sortingByNumber(sColumnName){
    return '<div class="row">'+
                '<div class="col-md-12">'+
                    '<input type="number" name="idNumberFilter" id="idNumberFilter" class="form-control classSorting classTextFilter" data-column-name="'+sColumnName+'">'+
                '</div>'+
            '</div>';
}

// For creating the html for clear and select all for options...
function sortByCheckBoxClearAndAll(sColumnName){
    return  '<div class="row">'+
                '<div class="col-md-6">'+
                    '<a class="classSorting classSelectAllAndClear" data-column-name="'+sColumnName+'" data-select-type=1>Select All</a>'+
                '</div>'+
                '<div class="col-md-6">'+
                    '<a class="classSorting classSelectAllAndClear" data-column-name="'+sColumnName+'" data-select-type=2>Clear All</a>'+
                '</div>'+
            '</div>';
}

// Creates the list of options for filter...
function sortByCheckBoxOptions(aQuickSelectOptions, sColumnName){
    var sOptions = "";
    for(var iTemp = 0; iTemp < aQuickSelectOptions.length; iTemp++){
        if (sColumnName == aQuickSelectOptions[iTemp]['sColumnName']) {
            sOptions += '<div class="row">'+
                            '<div class="col-md-12">'+
                                '<label class="classSorting"><input type="checkbox" value='+aQuickSelectOptions[iTemp]['id']+' class="classSelectAllCheckbox" data-column-name="'+sColumnName+'">'+aQuickSelectOptions[iTemp]['text']+'</label>'+
                            '</div>'+
                        '</div>';
        }
    }
    return sOptions;
}

//! Function of template to show other info of leads..
function getTemplate() {
    return [
        '<div class="popover" role="tooltip">',
            '<div class="arrow"></div>',
            '<h3 class="popover-title">Other Information</h3>',
            '<div class="popover-content">',
            '</div>',
        '</div>'
    ].join("");
}

// Function for the rendering select2
function sortBySelect2(sColumnName, sFilterAjaxURL){
    return '<div class="row">'+
                '<div class="col-md-12">'+
                    '<select name="idSelect2Filter" id="idSelect2Filter_'+sColumnName+'" class="form-control classSelect2Filter" data-column-name="'+sColumnName+'" data-ajax-url="'+sFilterAjaxURL+'" style="width:200px;"></select>'+
                '</div>'+
            '</div>';
}

// For selecting and unselecting all the checkboxes of excel type filter..
$(document).on("click", ".classSelectAllAndClear", function(){
    var iSelectType = $(this).attr("data-select-type");
    if (iSelectType == 1) {
        $(".classSelectAllCheckbox:visible").prop("checked", true).trigger("change");
    }else if(iSelectType == 2){
        $(".classSelectAllCheckbox:visible").prop("checked", false).trigger("change");
    }
});

// For sorting by alphabet..
$(document).on("click", ".classSortingByAlphabet", function(){
    var sSortType = $(this).attr("data-sort-type");
    var sColumnName = $(this).attr("data-column-name");
    var sTypedText = "";
    var aCheckBoxValues = [];
    
    $(".classTextFilter:visible").each(function(){
        if($(this).attr("data-column-name") == sColumnName){
            sTypedText = $(this).val();
            return false;
        }
    });

    $(".classSelectAllCheckbox:visible").each(function(){
        if($(this).attr("data-column-name") == sColumnName && $(this).is(":checked")){
            aCheckBoxValues.push($(this).val());
        }
    });

    aFilterData = aFilterData.filter(function( obj ) {
        return obj.sColumnName !== sColumnName;
    });

    aFilterData.push({
        'sSortType': sSortType,
        'sTypedText': sTypedText,
        'aCheckBoxValues': aCheckBoxValues,
        'sColumnName': sColumnName
    });
    oTableUIDataTable.draw();
});

// For searching by name...
$(document).on("change keyup", ".classTextFilter:visible", function(){
    var sColumnName = $(this).attr("data-column-name");
    var sTypedText = $(this).val();
    var aCheckBoxValues = [];

    $(".classSelectAllCheckbox:visible").each(function(){
        if($(this).attr("data-column-name") == sColumnName && $(this).is(":checked")){
            aCheckBoxValues.push($(this).val());
        }
    });

    aFilterData = aFilterData.filter(function( obj ) {
        return obj.sColumnName !== sColumnName;
    });

    aFilterData.push({
        'sTypedText': sTypedText,
        'aCheckBoxValues': aCheckBoxValues,
        'sColumnName': sColumnName
    });
    oTableUIDataTable.draw();
});

// Filter by checkbox..
$(document).on("change", ".classSelectAllCheckbox:visible", function(){
    var sColumnName = $(this).attr("data-column-name");
    var sTypedText = "";
    var aCheckBoxValues = [];

    $(".classTextFilter:visible").each(function(){
        if($(this).attr("data-column-name") == sColumnName){
            sTypedText = $(this).val();
            return false;
        }
    });

    $(".classSelectAllCheckbox:visible").each(function(){
        if($(this).attr("data-column-name") == sColumnName && $(this).is(":checked")){
            aCheckBoxValues.push($(this).val());
        }
    });

    aFilterData = aFilterData.filter(function( obj ) {
        return obj.sColumnName !== sColumnName;
    });

    aFilterData.push({
        'sTypedText': sTypedText,
        'aCheckBoxValues': aCheckBoxValues,
        'sColumnName': sColumnName
    });
    oTableUIDataTable.draw();
});

// For closing the popover on click outside...
$(document).on('click', function (e) {
    //did not click a popover toggle, or icon in popover toggle, or popover
    if ($(e.target).data('toggle') !== 'popover'
        && $(e.target).parents('[data-toggle="popover"]').length === 0
        && $(e.target).parents('.popover.in').length === 0) { 
        $('[data-toggle="popover"]').popover('hide');
    }
});

// For setting the select2 on popover show...
$(document).on('shown.bs.popover', function(){
    var sColumnName = $(this).find(".classSelect2Filter").attr("data-column-name");
    var sFilterAjaxURL = $(this).find(".classSelect2Filter").attr("data-ajax-url");
    $("#idSelect2Filter_"+sColumnName).select2({
        width: '100%',
        allowClear: false,
        minimumInputLength:3,
        width: 'resolve',
        ajax: {
            url: sFilterAjaxURL,
            data: function (params) {
                return {
                    term: params.term,
                };
            },
            processResults: function (data, params) {
                var results=[];
                $.each(data,function(key,value){
                    results.push({
                    id: value.entity_id,
                    text: value.entity_name
                });
            });
            return { results: results };
            }
        }
    });
});

// Filter by select2 
$(document).on("change", ".classSelect2Filter:visible", function(){
    var sColumnName = $(this).attr("data-column-name");
    var iEntityID = $(this).val();
    var aCheckBoxValues = [];
    if (iEntityID != null && iEntityID != "") {
        aCheckBoxValues.push($(this).val());
    }

    $(".classSelectAllCheckbox:visible").each(function(){
        if($(this).attr("data-column-name") == sColumnName && $(this).is(":checked")){
            aCheckBoxValues.push($(this).val());
        }
    });

    aFilterData = aFilterData.filter(function( obj ) {
        return obj.sColumnName !== sColumnName;
    });

    aFilterData.push({
        'aCheckBoxValues': aCheckBoxValues,
        'sColumnName': sColumnName
    });
    oTableUIDataTable.draw();     
});