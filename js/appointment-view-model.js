function AppointmentManager(oConfig) {
	var self = this;
	this.oConfig = oConfig;

	this.bUpdate = ko.observable(oConfig && oConfig.bUpdate ? oConfig.bUpdate : false);
	this.iScheduleID = ko.observable(oConfig && oConfig.iScheduleID ? oConfig.iScheduleID : 0);
	this.sDateFormat = ko.observable(oConfig && oConfig.sDateFormat ? oConfig.sDateFormat : "DD-MM-YYYY");

	if(self.iScheduleID()){
		$.ajax({
			url: 'ajaxEhr.php?sFlag=GetOPDBillDetailsForScheduleID&type=json',
			async: false,
			data: {iScheduleID: self.iScheduleID()}
		})
		.done(function(aOPDBill) {
			if(aOPDBill){
				self.oConfig.iBillID = aOPDBill['bill_id'];
			}
		});
	}

	this.iBillID = ko.observable(this.oConfig && this.oConfig.iBillID ? this.oConfig.iBillID : 0);

	this.iClinicID = ko.observable(1);
	this.iCustomerMasterID = ko.observable(0);
	this.iScheduleTypeID = ko.observable(0);
	this.iPriorityID = ko.observable(0);
	this.iPatientID = ko.observable(0);
	this.bCanAddScheduleWithEnroll = ko.observable(true);
	this.aPatientRecommendations = ko.observableArray([]);

	this.iOutsourceDiagnosticID = ko.observable(0);

	// Related to Timing
	this.dDate = ko.observable(moment().format(self.sDateFormat()));
	this.tTime = ko.observable(getCurrentScheduleTime());
	this.aDuration = getScheduleDurations();
	this.iVisitDuration = ko.observable(oConfig.defaultDurationSlot ? oConfig.defaultDurationSlot: "");

	// Related to User
	this.iStaffID = oConfig.iStaffID ? oConfig.iStaffID : 0;
	this.iUserID  = oConfig.iUserID ? oConfig.iUserID : 0;
	
	this.bReferralMandatory  = ko.observable(oConfig.bReferralMandatory);

	this.aClinics = ko.observableArray([]);
	this.aCustomers = ko.observableArray([]);
	this.aODs = ko.observableArray([]);
	this.aScheduleMethods = ko.observableArray([]);
	this.aScheduleType = ko.observableArray([]);
	this.aSchedulePriorities = ko.observableArray([]);
	this.aServiceTypes = ko.observableArray([]);
	this.aSuperServiceTypes = ko.observableArray([]);
	this.aSampleTypes = ko.observableArray([]);
	this.aOtherServiceCategories = ko.observableArray([]);
	//this.groups = ko.observableArray([]);

	this.iVisitMethod = ko.observable(0);
	this.oPatient = ko.observable();
	this.oReferralFirst = ko.observable();
	this.oReferralSecond = ko.observable();
	this.oOrderingPhysician = ko.observable();

	this.iChannelTypeID = ko.observable(0);
	this.oChannelType = ko.observable(0);
	this.iChannelID = ko.observable(0);
	this.oChannel = ko.observable();
	this.aAvailableRooms = ko.observableArray();
	this.aAvailableHCPPlans = ko.observableArray();

	//! Define variable for showing patient info icon...
	this.bShowPatientInfoIcon = ko.observable(false);

	this.aBillItemTypes = {
		1: "Service",
		2: "Health Checkup Plan",
		3: "Other Service",
		4: "Miscellaneous Item"
	};

	ko.computed(function() {
		$.ajax({url: 'apiv2/schedules/schedule-methods'})
			.done(function(aScheduleMethods) {
				self.aScheduleMethods(aScheduleMethods);
			});
	});

	ko.computed(function() {
	   $.ajax({
			url: 'apiv2/available-rooms',
			data: {
				iClinicID: self.iClinicID(),
				dScheduleDate: self.dDate(),
				tScheduleTime: self.tTime(),
				iScheduleDuration: self.iVisitDuration(),
				iIgnoreScheduleID: self.iScheduleID()
			},
		})
		.done(function(aRooms) {
			var aScheduleRooms = [];

			$.each(aRooms, function(index, aCurrentRooms) {
				aScheduleRooms.push(new Room({
					iRoomID: aCurrentRooms.iRoomID,
					sRoomNo: aCurrentRooms.sRoomNo,
					sRoomName: aCurrentRooms.sRoomName
				}))
			});

			self.aAvailableRooms(aScheduleRooms);
		});
	});

	ko.computed(function() {
	   $.ajax({
			url: 'apiv2/health-checkup-plans',
			data: {iOutsourceDiagnosticID: self.iOutsourceDiagnosticID()},
		})
		.done(function(aODs) {
			var aAvailableHCPPlans = [];

			$.each(aODs, function(index, aCurrentHCP) {
				aAvailableHCPPlans.push(new ScheduleItemHCP({
					iHCPID: aCurrentHCP['hcp_id'],
					sHCPName: aCurrentHCP['name']
				}));
			});

			self.aAvailableHCPPlans(aAvailableHCPPlans);
		});
	});

	// Check if Patient can be related to enrollement date
	ko.computed(function() {
		$.ajax({
			url: 'apiv2/patients/'+self.iPatientID()+"/can-add-schedule-with-enroll",
			data: {dScheduleDate: self.dDate(),iStaffID: self.iStaffID}
		})
		.done(function(iCanAddScheduleWithEnroll) {
			self.bCanAddScheduleWithEnroll(iCanAddScheduleWithEnroll == 1);
		});
	});

	this.oSchedule = ko.observableArray();
	this.oBill = ko.observableArray();
	this.bEnableBilling = ko.observable(oConfig ? !!oConfig.bEnableBilling : true );
	this.bEnableHCP = ko.observable(oConfig ? !!oConfig.bEnableHCP : true );
	this.bEnableRoom = ko.observable(oConfig ? !!oConfig.bEnableRoom : true );
	this.iNGSServiceTypeID = ko.observable(oConfig.iNGSServiceTypeID);
	
	this.iPatientID.subscribe(function(iPatientID) {
		if(iPatientID){
			$.ajax({url: 'apiv2/patient/'+iPatientID})
			.done(function(oPatient) {
				self.oPatient(oPatient);
			});
			// For new schedule fetch loyalty %
			if(!self.iScheduleID()){
				$.ajax({url: 'apiv2/patients/'+iPatientID+"/loyalty-discount"})
				.done(function(iPercentage) {
					self.oBill.iLoyaltyPercentage(iPercentage);
				});
			}
			if(iPatientID){
				$.ajax({url: 'apiv2/patients/'+iPatientID+"/pending-recommendations"})
				.done(function(aRecommendations) {
					self.aPatientRecommendations(aRecommendations.map(function(aRecommendation) {
						return new PatientRecommendation({
							sServiceName: aRecommendation['service_name'],
							iRecommendationStatus: aRecommendation['recommendation_status'],
							dScheduleDate: aRecommendation['schedule_date']
						});
					}));
				});
			} else {
				self.aPatientRecommendations([]);
			}
			
			// Check Available Services
			self.oSchedule.removeUnavailableServices(self);
			// Set rate of Services
			$.each(self.oSchedule.oScheduleItems(), function(index, oScheduleItem) {
				oScheduleItem.setRate(self);
				oScheduleItem.setAvailableServices(self);
				oScheduleItem.setAvailableServicesTypes(self)
			});
			// Set Rate of HCP Services
			$.each(self.oSchedule.oScheduleHCPItems(), function(index, oScheduleItem) {
				oScheduleItem.setRate(self);
			});

			self.loadBillItemsFromScheduleItems();
		} else {
			self.oPatient(null);
			// Resetting all items
			self.oSchedule.resetScheduleItems();
		}
	});

	this.iClinicID.subscribe(function(iClinicID) {
		// Check Available Services
		// Change Available Service
		self.oSchedule.removeUnavailableServices(self);

		// Change Service Rate && Available Doctor and team for service
		$.each(self.oSchedule.oScheduleItems(), function(index, oScheduleItem) {
			oScheduleItem.setRate(self);
			oScheduleItem.setAvailableServices(self);
			oScheduleItem.setAvailableServicesTypes(self)
			oScheduleItem.setAvailableDoctorTeams(self);
		});
		// Change Available Doctor and team for HCP
		$.each(self.oSchedule.oScheduleHCPItems(), function(index, oScheduleItem) {
			oScheduleItem.setAvailableDoctorTeams(self);
		});

		self.loadBillItemsFromScheduleItems();
	});

	this.dDate.subscribe(function() {
		// Set Service Rate & Available Doctor And Team for service 
		$.each(self.oSchedule.oScheduleItems(), function(index, oScheduleItem) {
			oScheduleItem.setRate(self);
			oScheduleItem.setAvailableDoctorTeams(self);
		});
		// Set Available Doctor And Team for HCP
		$.each(self.oSchedule.oScheduleHCPItems(), function(index, oScheduleItem) {
			oScheduleItem.setAvailableDoctorTeams(self);
		});
		// Change Bill date
		self.oBill.sBillDate(self.dDate());
	});

	this.tTime.subscribe(function() {
		// Set Service Rate & Available Doctor And Team for service 
		$.each(self.oSchedule.oScheduleItems(), function(index, oScheduleItem) {
			oScheduleItem.setAvailableDoctorTeams(self);
		});
		// Set Available Doctor And Team for HCP
		$.each(self.oSchedule.oScheduleHCPItems(), function(index, oScheduleItem) {
			oScheduleItem.setAvailableDoctorTeams(self);
		});
	});

	this.iVisitDuration.subscribe(function() {
		// Set Service Rate & Available Doctor And Team for service 
		$.each(self.oSchedule.oScheduleItems(), function(index, oScheduleItem) {
			oScheduleItem.setAvailableDoctorTeams(self);
		});
	});

	this.iChannelID.subscribe(function(iChannelID) {
		if(iChannelID){
			$.ajax({url: 'apiv2/channels/'+iChannelID})
			.done(function(oChannel) {
				self.oChannel(oChannel);
			});
		} else {
			self.oChannel(null);
		}

		// Set rate of Services
		$.each(self.oSchedule.oScheduleItems(), function(index, oScheduleItem) {
			oScheduleItem.setRate(self);
		});
		// Set Rate of HCP Services
		$.each(self.oSchedule.oScheduleHCPItems(), function(index, oScheduleItem) {
			oScheduleItem.setRate(self);
		});
		// Set Rate of Other Services
		$.each(self.oSchedule.oScheduleOtherServiceItems(), function(index, oScheduleItem) {
			oScheduleItem.setOtherServiceRate(self);
		});
	});

	this.iChannelTypeID.subscribe(function(iChannelTypeID) {
		if(iChannelTypeID){
			$.ajax({url: 'apiv2/channel-types/'+iChannelTypeID})
			.done(function(oChannelType) {
				self.oChannelType(oChannelType);
			});
		} else {
			self.oChannelType(null);
		}
	});

	this.iOutsourceDiagnosticID.subscribe(function(iODID){
		if(iODID){
			$.ajax({url: 'apiv2/outsource-diagnostics/'+iODID})
			.done(function(oOD) {
				self.bEnableBilling(oOD.bEnableBilling);
				self.bEnableHCP(oOD.bHCPEnabled);
			});
		} else {
			self.bEnableBilling(self.oConfig ? !!self.oConfig.bEnableBilling : true );
			self.bEnableHCP(self.oConfig ? !!self.oConfig.bEnableHCP : true );
		}
	});

	this.bEnableBilling.subscribe(function() {
		// Add One Blank Other Service Raw
		self.oSchedule.oScheduleOtherServiceItems([new ScheduleOtherServiceItem()]);
		// Add One Blank Other Service Raw
		self.oSchedule.oScheduleMiscItems([new ScheduleMiscItem()]);

		self.loadBillItemsFromScheduleItems();
	});

	this.bEnableHCP.subscribe(function(bEnableHCP) {
		if(!bEnableHCP){
			self.oSchedule.oScheduleHCPItems([new ScheduleItemHCP()]);

			self.loadBillItemsFromScheduleItems();
		}
	});

	this.loadSchedule = function(iScheduleID) {
		this.oSchedule = new Schedule(self,iScheduleID);
	}

	this.loadBill = function(iBillID) {
		this.oBill = new Bill(self,iBillID);
	}

	this.loadBillItemsFromScheduleItems = function() {
		var aBillItems = [];

		$.each(['oScheduleItems','oScheduleHCPItems','oScheduleOtherServiceItems','oScheduleMiscItems'], function(index, sItemType) {
			// Loop through each item in oSchedule and Add it to oBill
			if(self.oSchedule[sItemType]){
				$.each(self.oSchedule[sItemType](), function(index, oScheduleItem) {
					var oBillItem = oScheduleItem.getBillItemObject();

					if(oBillItem){
						aBillItems.push(oBillItem);
					}
				});
			}
		});

		if(self.oBill.aBillItems){
			self.oBill.aBillItems(aBillItems);
		}
	}
	
	//! For check date is in range..
    function checkDateInRange(dateFrom,dateTo,dateCheck){
		var d1 = dateFrom.split("/");
		var d2 = dateTo.split("/");
		var c = dateCheck.split("/");

		var from = new Date(d1[2], parseInt(d1[1])-1, d1[0]);  // -1 because months are from 0 to 11
		var to   = new Date(d2[2], parseInt(d2[1])-1, d2[0]);
		var check = new Date(c[2], parseInt(c[1])-1, c[0]);
		if(check > from && check <= to){
			return true;
		}else{
			return false;					
		}
    }

	// Related to saving of Schedule
	this.submitScheduleData = function(bSaveScheuleWithSample) {
		// Validate Data
		var bValidated = self.validateScheduleData();

		if(!bValidated) {
			return false;
		}
		// Updating Bill
		self.loadBillItemsFromScheduleItems();
		// Run Pre hooks
		// Save Data
		if(self.oSchedule.iScheduleID()){

		} else {
			// for checking at least one service for pathology..
			var aServices = self.oSchedule.getAttachedServicesToSchedule();
			var bIsPAthoService =true;
			/*var bIsPAthoService =false;
			for (var iii = 0; iii < aServices.length; iii++) {
				if(aServices[iii]['oServiceType']['sServiceTypeName']=="Pathology"){
					bIsPAthoService = true;
				}
			}*/
			var iCustomerMasterID = self.iCustomerMasterID();
			var iScheduleType = $('#idScheduleType').val();
			if(!iCustomerMasterID && iScheduleType!=2){
				$.growl.error({message:"Please Select Customer."});
				return false;
			}
			var iCustomerBranch = $("#idCustomerBranch").val();
			if (iCustomerBranch == "") {
				$.growl.error({message:"Please Select Collection Centre."});
				return false;
			}

			var dSampleCollectionDate = $('#idSampleCollectionDate').val();
			var tSampleCollectionTime = $('#idSampleCollectionTime').val();
			var sDNAConcentration = $('#idDNAConcentration').val();
			var sDissolvedIn = $('#idDissolvedIn').val();
			var iIsSampleInfectious = $('input[name=idIsSampleInfectious]:checked').val();
			var sSampleInfectiousComment = $('#idSampleInfectiousComment').val();
			var dSampleReceivedDate = $('#idSampleReceivedDate').val();
			var tSampleReceivedTime = $('#idSampleReceivedTime').val();

			if(dSampleCollectionDate=="" || dSampleCollectionDate==undefined){
				$.growl.error({message:"Collection Date cannot be empty."});
				return false;
			}
			if($('.classServiceType').val()== 7 && (tSampleCollectionTime=="" || tSampleCollectionTime==undefined)){
				$.growl.error({message:"Collection Time cannot be empty."});
				return false;
			}
			if(dSampleReceivedDate=="" || dSampleReceivedDate==undefined){
				$.growl.error({message:"Received Date cannot be empty."});
				return false;
			}
			if(tSampleReceivedTime=="" || tSampleReceivedTime==undefined){
				$.growl.error({message:"Received Time cannot be empty."});
				return false;
			}

			
			var d = new Date();			
			var dDateFrom =  d.getDate() + "/" + (d.getMonth()-2) + "/" + d.getFullYear();
			var dDateTo = d.getDate() + "/" + (d.getMonth()+1) + "/" + d.getFullYear();

			//! For check collection date..
			var dDateCheck = dSampleCollectionDate;
			var bRes1 = checkDateInRange(dDateFrom,dDateTo,dDateCheck);
			if (bRes1==false) {
				$.growl.error({message: "Collection Cant be less than 3 months."});
				return false;
			}

			//! For check received date..
			var dReceivedDateCheck = dSampleReceivedDate;
			var bRes2 = checkDateInRange(dDateFrom,dDateTo,dReceivedDateCheck);
			if (bRes2==false) {
				$.growl.error({message: "Received Cant be less than 3 months."});
				return false;
			}

			//! For check scan date..
			var dScanDateCheck =  $('#idSampleScanDate').val();
			if (dScanDateCheck!="") {
				var bRes3 = checkDateInRange(dDateFrom,dDateTo,dScanDateCheck);
				if (bRes3==false) {
				$.growl.error({message: "Scan Cant be less than 3 months."});
				return false;
			}
			}


			//! Validation for NGS service type id..
			let iSampleID = $(".classWorkorderSample").val();
			let iSampleExtractedDNA_1 = '12';
			let iSampleExtractedDNA_2 = '13';
			let iSampleDNAAF = '18';
			let iSampleDNACV = '19';
			let iSampleDNA = '21';
			let aSampleID = [iSampleExtractedDNA_1, iSampleExtractedDNA_2, iSampleDNAAF, iSampleDNACV, iSampleDNA];
			if(parseInt($('.classServiceType').val()) == parseInt(self.iNGSServiceTypeID()) && self.iNGSServiceTypeID() > 0 && aSampleID.indexOf(iSampleID) != -1){
				if(sDNAConcentration=="" || sDNAConcentration==undefined){
					$.growl.error({message:"Concentration cannot be empty."});
					return false;					
				}

				if(sDissolvedIn=="" || sDissolvedIn==undefined){
					$.growl.error({message:"Dissolved In cannot be empty."});
					return false;					
				}

				if(iIsSampleInfectious=="1"){
					if(sSampleInfectiousComment=="" || sSampleInfectiousComment==undefined){
						$.growl.error({message:"Sample infection comment cannot be empty."});
						return false;						
					}
				}
			}

			//! for check sample collection time format..
			var sRegexp =  /^([01]?[0-9]|2[0-3]):[0-5][0-9]$/;
    		var bIsTimeCorrect = (tSampleCollectionTime.search(sRegexp) >= 0) ? true : false;

    		if($('.classServiceType').val()== 7 && bIsTimeCorrect==false){
				$.growl.error({message:"Collection Time not in proper format."});
				return false;
			}

			//! for check sample collection time format..
			var sRegexp =  /^([01]?[0-9]|2[0-3]):[0-5][0-9]$/;
    		var bIsTimeCorrect = (tSampleReceivedTime.search(sRegexp) >= 0) ? true : false;

    		if(bIsTimeCorrect==false){
				$.growl.error({message:"Received Time not in proper format."});
				return false;
			}

			//! For check previous sample id is not empty..
			if($('#idRepeatSample').is(':checked')){
				var sPreviousSampleID = $('#idPreviousSampleIDNew').val();
				if (sPreviousSampleID=='' || sPreviousSampleID==undefined) {
					$.growl.error({message:"Previous Sample ID cant be empty."});
					return false;
				}
			}

			// For DNA extension retrival and discard....
			let iSelectedServiceTypeID = $(".classServiceType").val();
        	let sPrevBarcode = $("#idPreviousSampleID").val();
        	if((iSelectedServiceTypeID == 9 || iSelectedServiceTypeID == 21 || iSelectedServiceTypeID == 23) && sPrevBarcode == ""){
        		$.growl.error({message:"Please enter previous sample id."});
        		return false;
        	}

			var bIsSampleCollection = $('#idIsSampleCollectionEnabled').val();

			//! for run sample collection block
			if (bIsSampleCollection==true && bIsPAthoService==true && bSaveScheuleWithSample==false) {

			}
			else{
				self.createSchedule();
			}
		}
	}

	this.validateScheduleData = function() {
		var iPatientID = self.iPatientID(),
			iClinicID = self.iClinicID(),
			iCustomerMasterID = self.iCustomerMasterID(),
			oScheduleItems = self.oSchedule.oScheduleItems(),
			oScheduleHCPItems = self.oSchedule.oScheduleHCPItems(),
			oScheduleMiscItems = self.oSchedule.oScheduleMiscItems();
		var bValidationFailed = false;

		if(!iPatientID){
			$.growl.error({message:"Please Select Patient for Schedule."});
			return false;
		}
		if(!iClinicID){
			$.growl.error({message: "Please Select Clinic for Schedule."});
			return false;
		}
		

		// Validating that minmun 1 service should be there in application
		var aServices = self.oSchedule.getAttachedServicesToSchedule();

		if(aServices.length == 0){
			$.growl.error({message: "Minimum one service should be added in the schedule"});
			return false;
		}


		// Doctor should be selected for all Schedule services and rooms as well
		/*$.each(oScheduleItems, function(index, oScheduleItem) {
			if(oScheduleItem.iServiceID()){
				if(!oScheduleItem.oDoctorTeam() || !oScheduleItem.oDoctorTeam().iEntityID){
					$.growl.error({message: "Please Select Doctor/Network Partner/Team for "+oScheduleItem.oService().sServiceName});
					bValidationFailed = true;
					return false;		
				}

				if(self.bEnableRoom()){
					if(!oScheduleItem.oRoom() || !oScheduleItem.oRoom().iRoomID){
						$.growl.error({message: "Please Select Room for "+oScheduleItem.oService().sServiceName});
						bValidationFailed = true;
						return false;		
					}
				}
			}
		});*/

		// Doctor should be selected for all Schedule services and rooms as well
		$.each(oScheduleHCPItems, function(index, oScheduleHCPItem) {
			if(oScheduleHCPItem.iHCPID()){
				$.each(oScheduleHCPItem.oScheduleItems(), function(index, oScheduleItem) {
					if(oScheduleItem.iServiceID()){
						if(!oScheduleItem.oDoctorTeam() || !oScheduleItem.oDoctorTeam().iEntityID){
							$.growl.error({message: "Please Select Doctor/Network Partner/Team for "+oScheduleItem.oService().sServiceName});
							bValidationFailed = true;
							return false;		
						}

						if(self.bEnableRoom()){
							if(!oScheduleItem.oRoom() || !oScheduleItem.oRoom().iRoomID){
								$.growl.error({message: "Please Select Room for "+oScheduleItem.oService().sServiceName});
								bValidationFailed = true;
								return false;		
							}
						}
					}
				});
			}
		});

		// If Misc Item name is added then rate must be added
		$.each(oScheduleMiscItems, function(index, oScheduleMiscItem) {
			var sMiscItemName = oScheduleMiscItem.sMiscItemName();
			if(sMiscItemName && (oScheduleMiscItem.iRate() === "" || oScheduleMiscItem.iRate() === undefined)){
				$.growl.error({message: "Add Rate for Miscellaneous Item "+sMiscItemName});
				bValidationFailed = true;
				return false;
			}
		});

		if(parseFloat(self.oBill.iPaidAmount()) > parseFloat(self.oBill.iNetAmount())){
			$.growl.error({message: "Paid amount should never be more then net amount."});
			bValidationFailed = true;
			return false;
		}

		return !bValidationFailed;
	}

	this.buildRequestData = function () {
		var oSchedule = self.oSchedule,
			oBill = self.oBill,
			aMisItems = oSchedule.oScheduleMiscItems().filter(function(oScheduleMiscItem) {
				return oScheduleMiscItem.sMiscItemName() && oScheduleMiscItem.iRate();
			}),
			aPayments = oBill.aPayments().filter(function(aPayment) {
				return aPayment.iPaymentMode() && aPayment.iPaymentAmount();
			}),
			aOtherServices = oSchedule.oScheduleOtherServiceItems().filter(function(oScheduleOtherServiceItem) {
				return oScheduleOtherServiceItem.iID();
			}),
			oScheduleItems = oSchedule.oScheduleItems().filter(function(oScheduleItem) {
				return oScheduleItem.iServiceID() && oScheduleItem.oDoctorTeam() && oScheduleItem.oDoctorTeam().iEntityID;
			}),
			oScheduleHCPItems = oSchedule.oScheduleHCPItems().filter(function(oScheduleHCPItem) {
				// Making Sure that doctors are selected for all
				return oScheduleHCPItem.iHCPID() && oScheduleHCPItem.oScheduleItems().filter(function(oScheduleItem) {
					return oScheduleItem.oDoctorTeam() && oScheduleItem.oDoctorTeam().iEntityID;
				}).length == oScheduleHCPItem.oScheduleItems().length;
			}),
			aSuperServiceType = [],aServiceType = [],aService = [],aServiceID = [],aStaffID = [],
			aOPDServiceID = [],aIsServiceHCP = [],aOPDServiceName = [],
			aOPDServiceInfo = [],aRateType = [],aServiceRate = [], aAttachedGenes = [], aAttachedGeneFiles = [];

		//! For check test is combi or not...
		var iServiceTypeOptAttribute = $('option:selected', '.classServiceType').attr('data-iServiceTypeCategory');
		if (iServiceTypeOptAttribute!=100) {

			$.each(oScheduleItems, function(index, oScheduleItem) {
				var oDoctorTeam = oScheduleItem.oDoctorTeam(),
					iServiceID = oScheduleItem.iServiceID(),
					sServiceName = oScheduleItem.oService().sServiceName;

				aSuperServiceType.push(oScheduleItem.iSuperServiceTypeID());
				aServiceType.push(oScheduleItem.iServiceTypeID());
				aService.push(sServiceName);
				aServiceID.push(iServiceID);
				aStaffID.push(oDoctorTeam.iEntityID+","+oDoctorTeam.iEntityTypeID);

				aOPDServiceID.push(iServiceID);
				aIsServiceHCP.push(0);
				aOPDServiceName.push(sServiceName);
				aOPDServiceInfo.push("");
				aRateType.push(1);
				aServiceRate.push(oScheduleItem.iRate());
			});
		}

		/*$.each(oScheduleItems, function(index, oScheduleItem) {
			var oDoctorTeam = oScheduleItem.oDoctorTeam(),
				iServiceID = oScheduleItem.iServiceID(),
				sServiceName = oScheduleItem.oService().sServiceName;

			aSuperServiceType.push(oScheduleItem.iSuperServiceTypeID());
			aServiceType.push(oScheduleItem.iServiceTypeID());
			aService.push(sServiceName);
			aServiceID.push(iServiceID);
			aStaffID.push(oDoctorTeam.iEntityID+","+oDoctorTeam.iEntityTypeID);
			var aAttachedGenesTmp = oScheduleItem.aAttachedGenes();

			var aGeneData = [];
			for(var iii=0; iii < aAttachedGenesTmp.length; iii++) {
				aGeneData.push(aAttachedGenesTmp[iii].gene_id+"|"+aAttachedGenesTmp[iii].gene_code);
			}

			aAttachedGenes.push(aGeneData.join("||"));


			var aAttachedGeneDocsTmp = oScheduleItem.aGeneFiles();

			var aGeneFileData = [];
			for(var iii=0; iii < aAttachedGeneDocsTmp.length; iii++) {
				aGeneFileData.push(aAttachedGeneDocsTmp[iii].sDocumentName+"|"+aAttachedGeneDocsTmp[iii].sDocumentContentType+"|"+aAttachedGeneDocsTmp[iii].iDocumentSize+"|"+aAttachedGeneDocsTmp[iii].sDocumentBase64);
			}

			aAttachedGeneFiles.push(aGeneFileData.join("||"));

			aOPDServiceID.push(iServiceID);
			aIsServiceHCP.push(0);
			aOPDServiceName.push(sServiceName);
			aOPDServiceInfo.push("");
			aRateType.push(1);
			aServiceRate.push(oScheduleItem.iRate());
		});*/


		$.each(oScheduleHCPItems, function(index, oScheduleHCPItem) {
			var iHCPID = oScheduleHCPItem.iHCPID();
		
			aOPDServiceID.push(iHCPID);
			aIsServiceHCP.push(1);
			aOPDServiceName.push(oScheduleHCPItem.sHCPName);
			aOPDServiceInfo.push("");
			aRateType.push(1);
			aServiceRate.push(oScheduleHCPItem.iRate());

			$.each(oScheduleHCPItem.oScheduleItems(), function(index,oScheduleItem) {
				var oDoctorTeam = oScheduleItem.oDoctorTeam();
				aServiceType.push("HCP---"+iHCPID+"---"+oScheduleItem.iServiceTypeID());
				aService.push(oScheduleItem.oService().sServiceName);
				aServiceID.push(oScheduleItem.iServiceID());
				aStaffID.push(oDoctorTeam.iEntityID+","+oDoctorTeam.iEntityTypeID);
				var aAttachedGenesTmp = oScheduleItem.aAttachedGenes();

				var aGeneData = [];
				for(var iii=0; iii < aAttachedGenesTmp.length; iii++) {
					aGeneData = aAttachedGenesTmp[iii].gene_id+"|"+aAttachedGenesTmp[iii].gene_code
				}

				aAttachedGenes = aGeneData.join("||");
			});
		});		


		//! for sample collection...
		//! get Sample barcode
		var aSampleBarcode = [];
		$('.classInputSampleBarcode').each(function(){
			var sBarcode = $(this).val();
			aSampleBarcode.push(sBarcode);
		})

		//! get Sample collected at
		var aSampleCollectedAt = [];
		$('.classCollectedAt').each(function(){
			var sCollectedAt = $(this).val();
			aSampleCollectedAt.push(sCollectedAt);
		})

		//! get Sample collected at types
		var aSampleCollectedAtTypes = [];
		$('.classHiddenCollectedAtTypeID').each(function(){
			var iCollectedAtTypsId = $(this).val();
			aSampleCollectedAtTypes.push(iCollectedAtTypsId);
		})

		//! get Sample collected by
		var aSampleCollectedBy = [];
		$('.classCollectedBy').each(function(){
			var iCollectedBy = $(this).val();
			aSampleCollectedBy.push(iCollectedBy);
		})

		//! get Sample collection date
		var aSampleCollectionDate = [];
		$('.classCollectionDate').each(function(){
			var dCollectionDate = $(this).val();
			aSampleCollectionDate.push(dCollectionDate);
		})

		//! get Sample collection time...
		var aSampleCollectionTime = [];
		$('.classCollectionTime').each(function(){
			var tCollectionTime = $(this).val();
			aSampleCollectionTime.push(tCollectionTime);
		})

		//! get Sample patho service id...
		var aPathoServiceID = [];
		$('.classPathoServiceID').each(function(){
			var iiPathoServiceID = $(this).val();
			aPathoServiceID.push(iiPathoServiceID);
		})

		//! get Sample id...
		var aPathoSampleID = [];
		$('.classPathoSampleID').each(function(){
			var iiPathoSampleID = $(this).val();
			aPathoSampleID.push(iiPathoSampleID);
		})

		// for checking at least one service for pathology..
		var aServices = self.oSchedule.getAttachedServicesToSchedule();
		var bCollectSample =true;

		//! for gets save sample latter checked value...
		var bSaveSampleLater =$('#idSaveSampleLatter').is(":checked");

		if (bSaveSampleLater==false) {
			for (var iii = 0; iii < aServices.length; iii++) {
				//if(aServices[iii]['oServiceType']['sServiceTypeName']=="Pathology"){
					//bCollectSample = true;
				//}
			}
		}

		//! For gets fresh / repeat sample value.. 
		//! For sets 0 For fresh 1 for repeat..
		var bIsReapeatSample = 0;

		var sRepeatSample  = $('#idRepeatSample:checked').val();

		if (sRepeatSample==1) {
			bIsReapeatSample = 1;
		}

		//! For sets 0 For fresh 1 for repeat..
		var bIsValidationSample = 0;
		var sValidationSample  = $('#idValidationSample').is(':checked');

		if (sValidationSample==true) {
			bIsValidationSample = 1;
		}

		//! For sets 0 For fresh 1 for repeat..
		var bIsFOCSample = 0;
		var sFOCSample  = $('#idFOCSample').is(':checked');

		if (sFOCSample==true) {
			bIsFOCSample = 1;
		}

		var aData = {
			ajax: 1,
			patientID: self.iPatientID(),
			userID: self.iUserID,
			loggedStaffID: self.iStaffID,
			schClinicID: self.iClinicID(),
			iOutsourceDiagnosticId: self.iOutsourceDiagnosticID(),
			iTaxRate: oBill.iTaxRate(),
			scheduleNo: oSchedule.sScheduleNo(),
			scheduleDuration: self.iVisitDuration(),
			date: self.dDate(),
			hr: moment(self.tTime(),"HH:mm").format("HH"),
			min: moment(self.tTime(),"HH:mm").format("mm"),
			'schedule_method_id': self.iVisitMethod(),
			referralID1:self.oReferralFirst() ? self.oReferralFirst().iReferralID : 0,
			referralPriority1:1,
			referralID2:self.oReferralSecond() ? self.oReferralSecond().iReferralID : 0,
			referralPriority2:2,
			OPID: self.oOrderingPhysician() ? self.oOrderingPhysician().iID : 0,
			channelTypeID: self.iChannelTypeID(),
			channelID: self.iChannelID(),
			notes: oSchedule.sNote(),
			billNo: oBill.sBillNo(),
			billDate: oBill.sBillDate(),
    		billTo: self.oPatient().sPatientName,
    		
    		invProduct: [],
    		invProductId: [],
    		invRate: [],
    		invQuantity: [],
    		invTotal: [],
    		invSubTotal: 0,
    		'miscellaneous_items': aMisItems.map(function(aMisItem){aMisItem.sMiscItemName()}),
    		'miscellaneous_item_rate': aMisItems.map(function(aMisItem){aMisItem.iRate()}),

    		grandTotal:oBill.iGrandTotal(),
		    discount:oBill.iDiscountTotal(),
		    loyaltyDiscount:oBill.iLoyaltyDiscountTotal(),
		    loyaltyDiscountRate:oBill.iLoyaltyPercentage(),
		    tax:oBill.iTaxAmount(),
		    netAmount:oBill.iNetAmount(),
		    pendingAmount: oBill.iPendingAmount(),
		    paymentMode: aPayments.map(function(aPayment) {
		    	return aPayment.iPaymentMode();
		    }),
		    paymentNotes: aPayments.map(function(aPayment) {
		    	return aPayment.sPaymentNote();
		    }),
		    paymentPaid: aPayments.map(function(aPayment) {
		    	return aPayment.iPaymentAmount();
		    }),
		    otherItemCategory: aOtherServices.map(function(aOtherService) {
		    	return aOtherService.iCategoryID();
		    }),
		    otherService: aOtherServices.map(function(aOtherService) {
		    	return aOtherService.sName();
		    }),
		    otherServiceID: aOtherServices.map(function(aOtherService) {
		    	return aOtherService.iID();
		    }),
		    otherServiceName: aOtherServices.map(function(aOtherService) {
		    	return aOtherService.sName();
		    }),
		    otherItemRate: aOtherServices.map(function(aOtherService) {
		    	return aOtherService.iRate();
		    }),
		    superServiceType: aSuperServiceType,
		    serviceType: aServiceType,
		    service: aService,
		    serviceID: aServiceID,
		    staffID: aStaffID,
		    aAttachedGenes: aAttachedGenes,
		    aAttachedGeneFiles: aAttachedGeneFiles,

		    OPDServiceID: aOPDServiceID,
			isServiceHCP: aIsServiceHCP,
			OPDServiceName: aOPDServiceName,
			OPDServiceInfo: aOPDServiceInfo,
			rateType: aRateType,
			serviceRate: aServiceRate,

			//! For customer / branches / priority..
			iScheduleTypeID : self.iScheduleTypeID,
			iCustomerMasterID: self.iCustomerMasterID,
			iILCID: $('#idILC').val(),
			iCustomerID : $('#idCustomerID').val(),
			iCustomerBranch : $('#idCustomerBranch').val(),
			iCustomerDoctor : $('#idCustomerDoctors').val(),
			iPriorityID: self.iPriorityID,
			iCustomerStatus : $('#idCustomerStatus').val(),
			iActiveProjects : $('#idActiveProjects').val(),
			


			//! For sample collection data...
			sampleBarcode: aSampleBarcode,
			sSampleRemarks: $('#idSampleRemarks').val(),
			collectedAt: aSampleCollectedAt,
			collectedAtTypes: aSampleCollectedAtTypes,
			collectedBy: aSampleCollectedBy,
			//collectionDate: aSampleCollectionDate,
			//collectionTime: aSampleCollectionTime,
			collectionDate: aSampleCollectionDate,
			collectionTime: $('#idSampleCollectionTime').val(),
			bCollectSample:bCollectSample,

			//! For gets sample extra details..
			sIsRepeatSample: bIsReapeatSample,
			sPreviousSampleID: $('#idPreviousSampleIDNew').val(),
			dSampleCollectionDate: $('#idSampleCollectionDate').val(),
			dSampleCollectionTime: $('#idSampleCollectionTime').val(),
			dSampleReceivedDate: $('#idSampleReceivedDate').val(),
			tSampleReceivedTime: $('#idSampleReceivedTime').val(),
			sVolumeCollected: $('#idVolumeCollected').val(),
			sVolumeCollectedInit: $('#idVolumeCollectedInit').val(),
			sNoOfTube: $('#idNoOfTube').val(),
			dSampleScanDate: $('#idSampleScanDate').val(),
			dSampleScanTime: $('#idSampleScanTime').val(),
			sSampleCollectionComment: $('#idSampleCollectionComment').val(),
			sAgeOfManifestation: $('#idAgeOfManifestation').val(),
			sAgeOfManifestationUnit: $('#idAgeOfManifestationUnit').val(),
			sDNAConcentration : $('#idDNAConcentration').val(),
			sDissolvedIn : $('#idDissolvedIn').val(),
			iIsSampleInfectious : $('input[name=idIsSampleInfectious]:checked').val(),
			sSampleInfectiousComment : $('#idSampleInfectiousComment').val(),
			bIsValidationSample : bIsValidationSample,
			bIsFOCSample : bIsFOCSample
		};
		return aData;
	}

	this.createSchedule = function() {
		self.oSchedule.bScheduleBeingSaved(true);
		
		//! Pre Validations
		if(self.oBill.oStackedPayment().iPaymentAmount()>0) {
			$.growl.error({message: "There looks like a payment which is not added to the bill. Please add it or remove the amount."});
			self.oSchedule.bScheduleBeingSaved(false);
			return false;
		}

		if(self.bReferralMandatory() && (typeof self.oReferralFirst()=="undefined" || self.oReferralFirst.iReferralID=="" || self.oReferralFirst.iReferralID=="0")) {
			$.growl.error({message: "Selecting Referral is mandatory."});
			self.oSchedule.bScheduleBeingSaved(false);
			return false;
		}

		var aRequestData = self.buildRequestData(); 
		
		aRequestData.sampleID = [];
		aRequestData.pathServiceID = [];
		aRequestData.sampleLabel = [];

		var aServices = self.oSchedule.getAttachedServicesToSchedule();
		var aServiceIDs = [];
		$.each(aServices, function(index, oService){
			//if(oService.oServiceType.iServiceTypeID()==2) {
				aServiceIDs.push(oService.iServiceID());
			//}
		});


		$.ajax({
			url: "ajaxEhr.php?sFlag=GetSampleRequired",
			data: {aServiceIDs:aServiceIDs},
			async: true,
			success: function(aSamplesRequired){


				$.each(aServiceIDs, function(index, iServiceID){
					for(var iii=0; iii<aSamplesRequired[iServiceID].length; iii++) {
						
						if(aServiceIDs.length==3 && index ==0){
							
						}
						else{
							var aSample = aSamplesRequired[iServiceID][iii];
							aRequestData.sampleID.push(aSample.sample_id);
							aRequestData.pathServiceID.push(iServiceID);
							aRequestData.sampleLabel.push(aSample.sample_label.replace(/'/g, "\\'"));							
						}
					}
				});

				$.ajax({
					url: 'addScheduleEmulator.php',
					type: 'POST',
					data: aRequestData,
				})
				.done(function(iScheduleID) {
					if(iScheduleID){

						//! Function for check schedule has TRF...
						var iIsTRF = checkScheduleHasTRF(iScheduleID);

						if (iIsTRF==true) {
							window.location.href="addTRFDetails.php?iScheduleID="+iScheduleID;
						}
						else{
							window.location.href="addTRFDetails.php?iScheduleID="+iScheduleID;
							//window.location.href="viewSchedule.php?iScheduleID="+iScheduleID;
						}
					} else {
						$.growl.error({message: "There was error adding Schedule"});
					}
					self.oSchedule.bScheduleBeingSaved(false);
				});
			},
			complete: function() {
			}
		});

	}

	this.loadSchedule(self.iScheduleID());
	this.loadBill(self.iBillID());

	//for net amount by discount %
	this.iDiscountPercentage = ko.observable();
	this.iDiscountPercentage(0); // % initialise to value 0
	this.bDisableDiscount = ko.observable(); // to disable field discount
	this.bDisableDiscount(null); 	
	var fTotalByPercentage = 0 ;
	this.getNetAmoutByPercentage = function(data) {
		if(data.iDiscountPercentage() < 0 || data.iDiscountPercentage() > 100 ){
			alert("Please! select valid range 0-100");
			this.iDiscountPercentage(0);
		}else{
			var fGrandTotal = $('#idGrandTotal').val();
			this.bDisableDiscount("readonly"); 
			fTotalByPercentage = ((parseFloat(data.oBill.iGrandTotal()) * parseInt(data.iDiscountPercentage())) / 100 );			
			fTotalByPercentage = fTotalByPercentage.toFixed(2);			
			$('#idDiscount').val(fTotalByPercentage);
			data.oBill.iDiscountTotal = ko.observable(fTotalByPercentage);
			fTotalByPercentage = (fGrandTotal - fTotalByPercentage);			
			$('#idNetAmount').val(fTotalByPercentage.toFixed(2));
			data.oBill.iNetAmount = ko.observable(fTotalByPercentage);		
		}
		if(data.iDiscountPercentage() == 0){
			this.bDisableDiscount(null); 			
		}
	}

	function sampleCategories(label, aSampleType) {
	    this.label = ko.observable(label);
	    this.aSampleType = ko.observableArray(aSampleType);
	}

	function SampleType(label, property) {
	    this.label = ko.observable(label);
	    this.someOtherProperty = ko.observable(property);
	}

	this.aSampleCategories = ko.observableArray([]);

	//! For gets sample types with category...
	$.ajax({
		url: "ajaxEhr.php?sFlag=groupSampleTypesByCategory&type=json",
		async:false,
		data: {
		},
		success: function (aGroupSample){
			self.aSampleCategories(aGroupSample);
			aGroupSample = aGroupSample;
		}
	});

	this.loadingCompleted = ko.observable(true);
}


function Clinic(oClinic){
	this.iClinicID = oClinic.iClinicID ? oClinic.iClinicID : 0;
	this.sClinicName = oClinic.sClinicName ? oClinic.sClinicName : "";
}

//! For customer
function Customer(oCustomer){
	this.iCustomerMasterID = oCustomer.iCustomerMasterID ? oCustomer.iCustomerMasterID : 0;
	this.sCustomerName = oCustomer.sCustomerName ? oCustomer.sCustomerName : "";
}

//! For schedule types...
function ScheduleTypes(oScheduleTypes){
	this.iScheduleTypeID = oScheduleTypes.iScheduleTypeID ? oScheduleTypes.iScheduleTypeID : 0;
	this.sScheduleTypeName = oScheduleTypes.sScheduleTypeName ? oScheduleTypes.sScheduleTypeName : "";
}

//! For schedule priorities...
function SchedulePriorities(oSchedulePriorities){
	this.iPriorityID = oSchedulePriorities.iPriorityID ? oSchedulePriorities.iPriorityID : 0;
	this.sPriorityName = oSchedulePriorities.sPriorityName ? oSchedulePriorities.sPriorityName : "";
}

function OutsourceDiagnostic(oOD) {
	this.iODID = oOD.iODID ? oOD.iODID : 0;
	this.sODName = oOD.sODName ? oOD.sODName : "";
}

function Schedule(oVM, iScheduleID) {
	var oVM = oVM;
	var self = this;

	this.iScheduleID = ko.observable(iScheduleID);
	this.sScheduleNo = ko.observable();
	this.oScheduleItems = ko.observableArray();
	this.oScheduleHCPItems = ko.observableArray();
	this.oScheduleOtherServiceItems = ko.observableArray();
	this.oScheduleMiscItems = ko.observableArray();
	this.sNote = ko.observable("");

	this.oGeneSelectionScheduleItem = ko.observable();

	this.bScheduleBeingSaved = ko.observable(false);

	this.resetScheduleItems = function() {
		// Add One Blank Service into Schedule
		self.oScheduleItems([new ScheduleItem(oVM)]);
		// Add One Blank HCP Plan
		self.oScheduleHCPItems([new ScheduleItemHCP()]);
		// Add One Blank Other Service Raw
		self.oScheduleOtherServiceItems([new ScheduleOtherServiceItem()]);
		// Add One Blank Other Service Raw
		self.oScheduleMiscItems([new ScheduleMiscItem()]);

		oVM.loadBillItemsFromScheduleItems();
	}

	this.removeScheduleServiceItem = function (oScheduleItem) {
		var bFetchOtherServices = oScheduleItem.iServiceID() > 0,
			oScheduleItems = self.oScheduleItems();

		self.oScheduleItems.remove(oScheduleItem);

		if(bFetchOtherServices){
			self.scheduleServicesChanged();
		}

		if(oScheduleItems.length == 0){
			self.oScheduleItems([new ScheduleItem(oVM)]);
		}

		oVM.loadBillItemsFromScheduleItems();
	}

	// Loading Schedule Data
	this.iScheduleID.subscribe(function(iScheduleID) {
		self.loadSchedule(iScheduleID());
	}, this);

	// whenever Service Type changes
	this.serviceItemServiceTypeChanged = function (oScheduleItem){
		// Reset Service ID
		oScheduleItem.iServiceID(0);
		// Set Available Services for that schedule service item
		oScheduleItem.setAvailableServices(oVM);
	}

	// whenever Super Service Type changes
	this.serviceItemSuperServiceTypeChanged = function (oScheduleItem){
		// Reset Service ID
		oScheduleItem.iServiceTypeID(0);
		// Set Available Services for that schedule service types item
		oScheduleItem.setAvailableServicesTypes(oVM);
	}

	// whenever Sample Type changes
	this.serviceItemSampleTypeChanged = function (oScheduleItem){
		// Reset Service ID
		oScheduleItem.iServiceTypeID(0);
		// Set Available Services for that schedule service types item
		oScheduleItem.setAvailableServicesTypesBySample(oVM);
	}

	// whenever Sample Type changes
	this.sampleTypeSamplecategoryChanged = function (oScheduleItem){
		// Reset Service ID
		oScheduleItem.iServiceTypeID(0);
		// Set Available Services for that schedule service types item
		oScheduleItem.setAvailableSampleTypesBySampleCategory(oVM);
	}

	// Whenever service id changes
	this.serviceItemServiceIDChanged = function(oScheduleItem, index) {
		// Check that same service should not exist before
		var aServices = self.getAttachedServicesToSchedule();
		var	iSameServiceCount = 0;
		for(var iii=0; iii<aServices.length; iii++) {
			if(iii != index() && aServices[iii].iServiceID() == oScheduleItem.iServiceID()) {
				iSameServiceCount  = 1;
			}
		}
		if(iSameServiceCount){
			$.growl.error({message: "Same service already exist in the schedule."});
			self.removeScheduleServiceItem(oScheduleItem);
		} else {
			// Set Service Rate
			oScheduleItem.setRate(oVM);
			
			// Set Available Doctor Team
			oScheduleItem.setAvailableDoctorTeams(oVM);
		}

		//! For check test is combi or not...
		var iServiceTypeOptAttribute = $('option:selected', '.classServiceType').attr('data-iServiceTypeCategory');

		if (iServiceTypeOptAttribute==100) {
			// if(oScheudleItem.bIsHCPService()) {
				var iHCPID = $('.classServiceID').val();

				self.oScheduleHCPItems()[0].iHCPID(iHCPID);
				self.scheduleHCPIDChanged(self.oScheduleHCPItems()[0]);

			// }
			// else {

			// }
		}

	}

	this.addNewServiceItem = function() {
		self.oScheduleItems.push(new ScheduleItem(oVM));
	}

	this.removeServiceItem = function(oScheduleItem) {
		self.removeScheduleServiceItem(oScheduleItem);
	}

	// Whenever HCP Services is loaded
	this.hcpServicesLoaded = function (oScheduleHCPItem) {
		var bHasDuplicate = oScheduleHCPItem.checkDuplicateServices(oVM);
		if(!bHasDuplicate){
 			// oScheduleHCPItem.setAvailableDoctorTeams(oVM);
		} else {
			self.oScheduleHCPItems.splice(0,1);
			self.oScheduleHCPItems.push(new ScheduleItemHCP());
		}

		self.scheduleServicesChanged();
	}

	this.scheduleHCPIDChanged = function(oScheduleHCPItem) {
		oScheduleHCPItem.setRate(oVM);
	}

	this.otherServiceItemCategoryChanged = function(oScheduleOtherService) {
		oScheduleOtherService.setOtherServicesForCategory(oVM);
	}

	this.otherServiceItemIDChanged = function(oScheduleOtherService) {
		var bHasDuplicate = oScheduleOtherService.checkDuplicateOtherServices(oVM);
		if(bHasDuplicate){
			$.growl.error({message: "Selected Other Service is already exist in schedule."});
			self.oScheduleOtherServiceItems.remove(oScheduleOtherService);
		} else {
			oScheduleOtherService.setOtherServiceRate(oVM);
		}
	}

	this.addNewOtherServiceItem = function() {
		self.oScheduleOtherServiceItems.push(new ScheduleOtherServiceItem());
	}

	this.removeOtherServiceItem = function(oScheduleOtherServiceItem) {
		self.oScheduleOtherServiceItems.remove(oScheduleOtherServiceItem);
		oVM.loadBillItemsFromScheduleItems();
	}

	this.scheduleServicesChanged = function() {
		$.each(self.oScheduleOtherServiceItems(), function(index, oScheduleOtherServiceItem) {
			oScheduleOtherServiceItem.setOtherServicesForCategory(oVM);
		});
	}

	this.addNewScheduleMiscItem = function() {
		self.oScheduleMiscItems.push(new ScheduleMiscItem());
	}

	this.removeScheduleMiscItem = function(oScheduleMiscItem) {
		self.oScheduleMiscItems.remove(oScheduleMiscItem);
		oVM.loadBillItemsFromScheduleItems();
	}

	// Return the services attached to the schedule
	this.getAttachedServicesToSchedule = function() {
		var result = self.getAttachedScheduleServicesToSchedule();

		$.each(self.oScheduleHCPItems(), function(index, oScheduleHCPItem) {
			$.each(oScheduleHCPItem.oScheduleItems(), function(index, oScheduleItem) {
				var oService = oScheduleItem.oService(),
					iServiceID = typeof oService.iServiceID == "function" ? oService.iServiceID() : oService.iServiceID;

				if(iServiceID){
					result.push(oService);
				}
			});
		});

		return result;
	}

	this.getAttachedScheduleServicesToSchedule = function() {
		var aResult = [];

		$.each(self.oScheduleItems(), function(index, oScheduleItem) {
			var oService = oScheduleItem.oService();
			
			if(oService.iServiceID()){
				aResult.push(oService);
			}
		});

		return aResult;
	}

	this.removeUnavailableServices = function () {
		var aScheduleServiceItems = self.oScheduleItems();

		$.each(aScheduleServiceItems, function(index, aScheduleServiceItem) {
			aScheduleServiceItem.removeServiceIfNotAvailableForPatient(oVM);
		});
	}

	this.loadSchedule = function(iScheduleID) {
		if(iScheduleID){
			$.ajax({url: 'apiv2/schedules/'+iScheduleID})
				.done(function(oSchedule) {
					var oScheduleHCPItems = [],
						oScheduleItems = [],
						oScheduleHCPItem = new ScheduleItemHCP();

					self.sScheduleNo(oSchedule.sScheduleNo);
					oVM.iPatientID(oSchedule.oPatient.iPatientID);
					oVM.dDate(oSchedule.dScheduleDate);
					oVM.tTime(oSchedule.tScheduleTime.substring(0,5));
					oVM.iVisitDuration(oSchedule.iScheduleDuration);
					oVM.iVisitMethod(oSchedule.iScheduleMethodID);
					oVM.iClinicID(oSchedule.oClinic.iClinicID);
					self.sNote(oSchedule.sScheduleNotes);

					if(oSchedule.oFirstReferral){
						oVM.oReferralFirst(new Referral({
							iReferralID: oSchedule.oFirstReferral.iReferralID,
							sReferralName: oSchedule.oFirstReferral.sReferralName
						}));
					}
					if(oSchedule.oSecondReferral){
						oVM.oReferralSecond(new Referral({
							iReferralID: oSchedule.oSecondReferral.iReferralID,
							sReferralName: oSchedule.oSecondReferral.sReferralName
						}));
					}
					if(oSchedule.oOutSourceDiagnostics){
						oVM.iOutsourceDiagnosticID(oSchedule.oOutSourceDiagnostics.iODID);
					}
					if(oSchedule.oOrderingPhysician){
						oVM.oOrderingPhysician(new OrderingPhysician({
							iID: oSchedule.oOrderingPhysician.iID,
							sName: oSchedule.oOrderingPhysician.sName
						}));
					}
					if(oSchedule.oChannel){
						if(oSchedule.oChannel.oChannelType){
							oVM.iChannelTypeID(oSchedule.oChannel.oChannelType.iChannelTypeID);
						}

						oVM.iChannelID(oSchedule.oChannel.iChannelID);
					}

					$.each(oSchedule.aAppointments, function(index, oAppointment) {
						var oScheduleItem = new ScheduleItem(oVM);
						oScheduleItem.bIsExisting(true);

						oScheduleItem.iServiceTypeID(oAppointment.oService.oServiceType.iServiceTypeID);
						oScheduleItem.iServiceID(oAppointment.oService.iServiceID);

						oScheduleItem.setAvailableServices(oVM, oAppointment.oService.iServiceID);
						oScheduleItem.setAvailableServicesTypes(oVM, oAppointment.oService.iServiceTypID);

						//if(index == 1) {
							oScheduleItem.setRate(oVM,1);
						//}

						var oSelectedDocTeam = new DoctorTeam();

						if(oAppointment.oAppointmentStaff){
							oSelectedDocTeam = new DoctorTeam({
								iEntityID:oAppointment.oAppointmentStaff.iStaffID,
								iEntityTypeID:oAppointment.oAppointmentStaff.bIsTeam ? 2 : 1,
								sName:oAppointment.oAppointmentStaff.sStaffName,
							});
						}
						oScheduleItem.setAvailableDoctorTeams(oVM, oSelectedDocTeam);

						if(oAppointment.oRoom){
							oScheduleItem.oRoom(new Room({
								iRoomID: oAppointment.oRoom.iRoomID,
								sRoomNo: oAppointment.oRoom.sRoomNo,
								sRoomName: oAppointment.oRoom.sRoomName
							}));
						}

						if(!oAppointment.bIsHCP){
							oScheduleItems.push(oScheduleItem);
						} else {
							if(!oScheduleHCPItem.iHCPID()){
								oScheduleHCPItem.iHCPID(oAppointment.oHCP.iID);
							}

							oScheduleHCPItems.push(oScheduleItem);
						}
					});

					oScheduleHCPItem.oScheduleItems(oScheduleHCPItems)

					self.oScheduleHCPItems([oScheduleHCPItem]);

					if(oScheduleItems.length == 0){
						self.oScheduleItems([new ScheduleItem(oVM)]);
					} else {
						self.oScheduleItems(oScheduleItems);
					}
				});

		} else {
			oVM.iBillID(0);

			// Get Lastest Visit No
			$.ajax({url: 'apiv2/schedules/visit-no'})
				.done(function(sScheduleNo) {
					self.sScheduleNo(sScheduleNo);
				});

			self.resetScheduleItems();
		}
	}

	this.loadSchedule(self.iScheduleID());
}

function ScheduleItem(oVM) {
	var self = this;
	var oVM = oVM;

	this.bIsExisting = ko.observable(false);

	this.iServiceID = ko.observable();
	this.iServiceTypeID = ko.observable();
	this.iSuperServiceTypeID = ko.observable();
	this.iSampleTypeID = ko.observable();
	this.iDoctorTeamID = ko.observable();
	this.iDoctorTeamTypeID = ko.observable();
	this.iRate = ko.observable();
	this.sItemInfo = ko.observable();

	this.oService = ko.observable(new Service());
	this.oServiceType = ko.observable(new ServiceType());
	this.oSuperServiceType = ko.observable(new SuperServiceType());
	this.oSampleType = ko.observable(new SampleTypes());
	this.oDoctorTeam = ko.observable(new DoctorTeam());
	this.oRoom = ko.observable(new Room());

	this.aAvailableServices = ko.observableArray();
	this.aAvailableServiceTypes = ko.observableArray();
	this.aAvailableDoctorTeams = ko.observableArray();
	this.aAvailableSampleTypes = ko.observableArray();

	this.bGeneSelection = ko.observable(/*oVM.oConfig.bAllowGeneSelection*/);
	this.bAllowGeneChange = ko.observable(false);
	this.aAttachedGenes = ko.observableArray();
	this.bGeneAttachmentFileSelected = ko.pureComputed(function() {
		return this.aGeneFiles().length > 0
	},this);

	this.aGeneFiles = ko.observableArray();
	this.loadGeneFile = function(file) {
		getFileAsBase64(file).then(
		  function(data) {
		  	var sDocumentName=file.name;
		  	var sDocumentContentType=file.type;
		  	var iDocumentSize=file.size;
		  	self.aGeneFiles.push(new GeneDocument(sDocumentName, sDocumentContentType, iDocumentSize, data));
		  }
		);
	}
	self.removeAttachedGene = function(gene) {
        self.aAttachedGenes.remove(gene);
    }

	this.loadMappedGenes = function() {
		if(self.iServiceID()){
			$.ajax({url: 'apiv2/services/'+self.iServiceID()+'/mapped-genes'})
				.done(function(aMappedGenes) {
					for(var iii=0; iii < aMappedGenes.length; iii++) {
						self.aAttachedGenes.push(aMappedGenes[iii]);
					}
				});
		} else {
			self.aAttachedGenes([]);
		}
	}

	this.setSelectedDoctorTeam = function(iEntityID, iEntityTypeID) {
		$.each(self.aAvailableDoctorTeams(), function(){
			if(this.iEntityTypeID == iEntityTypeID && this.iEntityID == iEntityID) {
				self.oDoctorTeam(this);
			}
		}) 
	}

	this.iServiceID.subscribe(function(iServiceID) {
		if(iServiceID){
			$.ajax({url: 'apiv2/services/'+iServiceID})
				.done(function(oService) {
					self.oService(new Service({
						iServiceID: oService.iServiceID,
						sServiceName: oService.sServiceName,
						oServiceType: new ServiceType({
							iServiceTypeID:oService.oServiceType.iServiceTypeID,
							sServiceTypeName: oService.oServiceType.sServiceTypeName
						}),
					}));

					if(self.bGeneSelection()) {
						self.loadMappedGenes();
					}

				self.setAvailableDoctorTeams(oVM);
				});
		} else {
			self.oService(new Service());
		}
	}, this);

	this.iServiceTypeID.subscribe(function(iServiceTypeID) {
		self.oServiceType(new ServiceType({iServiceTypeID:iServiceTypeID}));
		//self.oSuperServiceType(new SuperServiceType({iSuperServiceTypeID:iSuperServiceTypeID}));
	}, this);

	this.setRate = function(oVM,iForBill=0) {
		var iServiceID = self.iServiceID();

		self.iRate(0);

		if(iServiceID&&iForBill==0){
			$.ajax({
				url: "ajaxEhr.php?sFlag=GetServiceRateDetailsForPatient&type=json",
				data: {
					iServiceID:iServiceID,
					iPatientID:oVM.iPatientID(),
					iChannelID:oVM.iChannelID(),
					iClinicID:oVM.iClinicID(),
					dScheduleDate: oVM.dDate(),
					iCustomerMasterID:oVM.iCustomerMasterID
				},
				success: function (aRate){
					var iRate = aRate && aRate['rate'] ? parseInt(aRate['rate']) : 0;
					self.iRate(iRate);
					
					oVM.loadBillItemsFromScheduleItems();
				}
			});
		} else {
			oVM.loadBillItemsFromScheduleItems();
		}

		//! For gets service bill rates....
		if(iServiceID&&iForBill==1){
			iScheduleID = oVM.iScheduleID();
			$.ajax({
				url: "ajaxEhr.php?sFlag=GetOPDBillDetailsForScheduleID&type=json",
				data: {
					iScheduleID:iScheduleID,
				},
				success: function (aBill){
					$.each(aBill['bill_service_items'],function(index,oItem){
						if(oItem.item_id == iServiceID){
							self.iRate(oItem.applied_rate);
							oVM.loadBillItemsFromScheduleItems();
						}
					});
				}
			});
		}
	}

	this.setAvailableDoctorTeams = function(oVM, oSelectedDocTeam) {
		var iServiceID = self.iServiceID();
		var iSuperServiceTypeID = 0;
		
		$.ajax({
			url: "ajaxEhr.php?sFlag=fGetServiceDetailsForServiceID",
			type: 'POST',
			async:false,
			data: {
				iServiceID:iServiceID,
			},
			success: function (data){
				iSuperServiceTypeID = data && data['super_service_type_id'] ? parseInt(data['super_service_type_id']) : 0;
			}
		});

		//! For set default doctor entity type id acording to department..
		//var iDefaultEntityID = self.iSuperServiceTypeID();
		var iDefaultEntityID = (iSuperServiceTypeID) ? iSuperServiceTypeID : 1;
		var iDefaultEntityTypeID = 2;
		var sDefaultEntityName = '';
		if(iServiceID){
			$.ajax({
	        	url: "ajaxEhr.php?sFlag=GetStaffListForService&type=json",
	        	data: {
	        		iServiceTypeID:self.iServiceTypeID(),
	        		iServiceID:iServiceID,
	        		iStaffID:oVM.iStaffID,
	        		iClinicID:oVM.iClinicID(),
	        		tScheduleTime:oVM.tTime(),
	        		dScheduleDate:oVM.dDate(),
	        		iSchDuration:oVM.iVisitDuration()
	        	},
	        	success: function (aData){
	        		var aDoctorTeams = [];
	        		$.each(aData, function(sType, aCurrentData) {
	        			$.each(aCurrentData, function(index, aDTData) {
		        			if(sType == "teams"){
		        				var iEntityID = aDTData['team_id'],
		        					iEntityTypeID = 2,
		        					sName = aDTData['team_name'];
		        			} else {
		        				var iEntityID = aDTData['staff_id'],
		        					iEntityTypeID = 1,
		        					sName = aDTData['staff_name'];
		        			}
							aDoctorTeams.push(new DoctorTeam({
								iEntityID: iEntityID,
								iEntityTypeID: iEntityTypeID,
								sName: sName
							}));
	        			});
	        		});
					self.aAvailableDoctorTeams(aDoctorTeams);
					if(typeof oSelectedDocTeam != "undefined" && oSelectedDocTeam.iEntityID > 0) {
						self.setSelectedDoctorTeam(oSelectedDocTeam.iEntityID, oSelectedDocTeam.iEntityTypeID);
					}

					self.setSelectedDoctorTeam(iDefaultEntityID,iDefaultEntityTypeID);
	        	}
	        });
		} else {
			self.aAvailableDoctorTeams([]);
		}
	}

	this.setAvailableServices = function(oVM, iServiceID) {
		var iServiceTypeID = self.iServiceTypeID();
		var iSampleTypeID = self.iSampleTypeID();
		var iServiceTypeCategory = $('option:selected', '.classServiceType').attr('data-iServiceTypeCategory');

		//! Only for customer users..
		if(oVM.oConfig.iUserTypeID == 36){
			$.ajax({
				url: 'ajaxEhr.php?sFlag=getSuperServiceType&iServiceTypeID='+iServiceTypeID,
				async: false,
		    	success: function (iSuperServiceTypeID){
		    		self.iSuperServiceTypeID(iSuperServiceTypeID);					
		    	}
			});
		}

		if(iServiceTypeID){
			$.ajax({
		    	url: "ajaxEhr.php?sFlag=GetAvailableServicesForPatient&type=json",
		    	data: {
		    		iPatientID:oVM.iPatientID(),
		    		iServiceTypeID:iServiceTypeID,
		    		iClinicID:oVM.iClinicID(),
		    		iSampleTypeID:iSampleTypeID,
		    		iServiceTypeCategory:iServiceTypeCategory,
		    	},
		    	success: function (aServices){
		    		self.aAvailableServices(aServices.map(function(aService) {
		    			var sInternalCode = (aService['internal_code']!=null) ? aService['internal_code'] : 'NA';

		    			return new Service({
	    			 		iServiceID: aService['service_id'],
	    			 		bIsCombi: aService['bIsCombi'],
							sServiceName: aService['service_name']/*+" ("+sInternalCode+")"*/
	    			 	})
		    		}));

		    		if(typeof iServiceID != "undefined") {
		    			self.iServiceID(iServiceID);
		    		}

		    		oVM.oSchedule.scheduleServicesChanged();
		    	}
		    });
		} else {
			self.aAvailableServices([]);
    		oVM.oSchedule.scheduleServicesChanged();
		}
	}

	this.setAvailableServicesTypes = function(oVM, iServiceTypeID) {
		var iSuperServiceTypeID = self.iSuperServiceTypeID();

		if(iSuperServiceTypeID){

			$.ajax({
		    	url: "apiv2/service-types-by-super-service-type/"+iSuperServiceTypeID,
		    	async: false,
		    	success: function (aServiceTypes){
		    		self.aAvailableServiceTypes(aServiceTypes.map(function(aServiceType) {
		    			return new ServiceType({
	    			 		iServiceTypeID: aServiceType.iServiceTypeID,
		        			sServiceTypeName: aServiceType.sServiceTypeName,
		        			iServiceTypeCategory: aServiceType.iServiceTypeCategory
	    			 	})
		    		}));

		    		if(typeof iServiceTypeID != "undefined") {
		    			self.iServiceTypeID(iServiceTypeID);
		    		}
		    	}
		    });
		} else {
			self.aAvailableServiceTypes([]);

		}
	}


	this.setAvailableServicesTypesBySample = function(oVM, iServiceTypeID) {
		this.iSampleTypeID = ko.observable($('.classWorkorderSample').val());

		var iSampleTypeID = self.iSampleTypeID();

		if(iSampleTypeID){

			$.ajax({
		    	url: "apiv2/service-types-by-sample-type/"+iSampleTypeID,
		    	success: function (aServiceTypes){
		    		self.aAvailableServiceTypes(aServiceTypes.map(function(aServiceType) {
		    			return new ServiceType({
	    			 		iServiceTypeID: aServiceType.iServiceTypeID,
		        			sServiceTypeName: aServiceType.sServiceTypeName,
		        			iServiceTypeCategory: aServiceType.iServiceTypeCategory
	    			 	})
		    		}));

		    		if(typeof iServiceTypeID != "undefined") {
		    			self.iServiceTypeID(iServiceTypeID);
		    		}
		    	}
		    });
		} else {
			self.aAvailableServiceTypes([]);

		}
	}

	//this.aSampleTypes = ko.observableArray([]);

	this.setAvailableSampleTypesBySampleCategory = function(oVM, iServiceTypeID) {
		this.iSampleCategoryID = ko.observable($('.classWorkorderSampleCategory').val());

		var iSampleCategoryID = self.iSampleCategoryID();

		if(iSampleCategoryID){

			$.ajax({
		    	url: "apiv2/sample-types-by-sample-category/"+iSampleCategoryID,
		    	success: function (aSampleTypes){
		    		self.aAvailableSampleTypes(aSampleTypes.map(function(aSampleType) {
		    			return new SampleTypes({
	    			 		iSampleTypeID: aSampleType.iSampleTypeID,
		        			sSampleTypeName: aSampleType.sSampleTypeName,
		        			iSampleCategoryID: aSampleType.iSampleCategoryID
	    			 	})
		    		}));
		    	}
		    });
		} else {
			self.aAvailableSampleTypes([]);

		}
	}

	this.removeServiceIfNotAvailableForPatient = function(oVM) {
		var iServiceID = self.iServiceID(),
			iServiceTypeID = self.iServiceTypeID();

		// If Study is not yet selecteds
		if(iServiceTypeID && iServiceID){
			$.ajax({
		    	url: "ajaxEhr.php?sFlag=GetAvailableServicesForPatient&type=json",
		    	data: {
		    		iPatientID:oVM.iPatientID(),
		    		iServiceTypeID:iServiceTypeID,
		    		iClinicID:oVM.iClinicID(),
		    	},
		    	success: function (aServices){
		    		var bAvailable = false;
		    		$.each(aServices, function(index,aService) {
		    			if(aService['service_id'] == iServiceID){
		    				bAvailable = true;
		    			}
		    		});

		    		if(!bAvailable){
			    		// Remove Self if not availble
			    		$.growl.error({message: self.oService().sServiceName+" Service is not available for patient."});
			    		oVM.oSchedule.removeScheduleServiceItem(self);
		    		}
		    	}
		    });
		}
	}

	this.getBillItemObject = function() {
		// Return null if Service ID is not set
		if(self.iServiceID()){
			return new BillItem({
				iItemTypeID: 1,
				iItemID: self.iServiceID(),
				sItemName: self.oService() ? self.oService().sServiceName : "",
				iRate: self.iRate()
			});
		}

		return null;
	}
}

function ScheduleItemHCP(oHcp) {
	var self = this;

	this.iHCPID = ko.observable(oHcp ? oHcp.iHCPID : 0);
	this.sHCPName = oHcp ? oHcp.sHCPName : "";
	this.oScheduleItems = ko.observable();
	this.iRate = ko.observable();
	this.sItemInfo = ko.observable();

	this.iHCPID.subscribe(function(iHCPID) {
		if(iHCPID){
			$.ajax({url: 'apiv2/health-checkup-plans/'+iHCPID+'/services'})
				.done(function(aServices) {
					var aAvailableServices = [];

		    		$.each(aServices, function(index, oService) {
		    			var oScheduleItem = new ScheduleItem();
		    			oScheduleItem.iServiceID(oService.iServiceID);
		    			oScheduleItem.iServiceTypeID(oService.oServiceType.iServiceTypeID);

		    			aAvailableServices.push(oScheduleItem);
		    		});
		    		self.oScheduleItems(aAvailableServices);
		    		self.setAvailableDoctorTeams(oAppManager);
				});

			$.ajax({url: 'apiv2/health-checkup-plans/'+iHCPID})
				.done(function(aHCP) {
		    		self.sHCPName = aHCP['name'];
				});
		} else {
			self.oScheduleItems([]);
		}
	});

	this.setRate = function(oVM) {
		self.iRate(0);

		if(self.iHCPID()){
			$.ajax({
				url: "ajaxEhr.php?sFlag=GetServiceRateDetailsForPatient&type=json",
				data: {
					iServiceID:self.iHCPID(),
					iPatientID:oVM.iPatientID(),
					iServiceFlag:2,
					iChannelID:oVM.iChannelID()
				},
				success: function (data){
					var iRate = data && data['rate'] ? parseInt(data['rate']) : 0;
					self.iRate(iRate);

					oVM.loadBillItemsFromScheduleItems();
				}
			});
		} else {
			oVM.loadBillItemsFromScheduleItems();
		}
	}

	// Undo HCP if Duplicate services
	this.checkDuplicateServices = function(oVM) {
		var aScheduleHCPServiceItems = self.oScheduleItems(),
			aServices = oVM.oSchedule.getAttachedScheduleServicesToSchedule(),
			aHCPServicIDs = aScheduleHCPServiceItems.map(function(aScheduleHCPServiceItem) {
				return aScheduleHCPServiceItem.iServiceID();
			}),
			aServiceIDs = aServices.map(function(aService) {
				return aService.iServiceID();
			});

		// HCP Can be added only if no services of HCP already exist in schedule
		var aCommonServices = aHCPServicIDs.filter(function(iServiceID) {
			return aServiceIDs.indexOf(iServiceID) != -1;
		});

		if(aCommonServices.length){
			// Getting Common Services to display message
			aHCPServices = aScheduleHCPServiceItems.filter(function(aScheduleHCPServiceItem) {
				return aServiceIDs.indexOf(aScheduleHCPServiceItem.iServiceID()) != -1;
			}).map(function(aScheduleHCPServiceItem) {
				return aScheduleHCPServiceItem.oService().sServiceName;
			});

			var sServicesLabel = aHCPServices.length == 1 ? "Service":"Services";

			$.growl.error({ message: "Health Checkup Plan can't be added as "+aHCPServices.join(",")+" "+sServicesLabel+" already exist in schedule."});

			self.iHCPID(0);

			return true;
		}

		return false;
	}

	this.setAvailableDoctorTeams = function(oVM) {
		// Set Available Doctor Team
		$.each(self.oScheduleItems(), function(index, oScheduleItem) {
			$.ajax({
	        	url: "ajaxEhr.php?sFlag=GetDoctorForHCPService&type=json",
	        	data: {
	        		iServiceID:oScheduleItem.iServiceID(),
	        		iStaffID:oVM.iStaffID,
	        		iClinicID:oVM.iClinicID(),
	        		tScheduleTime:oVM.tTime(),
	        		dScheduleDate:oVM.dDate()
	        	},
	        	success: function (aData){
	        		var aDoctorTeams = [];
	        		$.each(aData, function(sType, aCurrentData) {
	        			$.each(aCurrentData, function(index, aDTData) {
		        			if(sType == "teams"){
		        				var iEntityID = aDTData['team_id'],
		        					iEntityTypeID = 2,
		        					sName = aDTData['team_name'];
		        			} else {
		        				var iEntityID = aDTData['staff_id'],
		        					iEntityTypeID = 1,
		        					sName = aDTData['staff_name'];
		        			}
							aDoctorTeams.push(new DoctorTeam({
								iEntityID: iEntityID,
								iEntityTypeID: iEntityTypeID,
								sName: sName
							}));
	        			});
	        		});

					oScheduleItem.aAvailableDoctorTeams(aDoctorTeams);
					oScheduleItem.setSelectedDoctorTeam(iDefaultEntityID=1,iDefaultEntityTypeID=2);
	        	}
			});
		});
	}

	this.getBillItemObject = function() {
		// Return null if HCP is not set
		if(self.iHCPID()){
			return new BillItem({
				iItemTypeID: 2,
				iItemID: self.iHCPID(),
				sItemName: self.sHCPName,
				iRate: self.iRate()
			});
		}

		return null;			
	}
}

function ScheduleOtherServiceItem() {
	var self = this;
	this.iCategoryID = ko.observable();
	this.iID = ko.observable();
	this.sName = ko.observable();
	this.iRate = ko.observable(0);


	this.iID.subscribe(function(iID) {
		if(iID){
			$.ajax({
				url: 'apiv2/other-services/'+iID,
			})
			.done(function(aOtherService) {
				self.sName(aOtherService['name']);
			});
		} else {
			self.sName("");
		}
	});

	this.aAvailableOtherServices = ko.observableArray();

	this.setOtherServicesForCategory = function(oVM) {
		var aServices = oVM.oSchedule.getAttachedServicesToSchedule(),
			aServiceID = aServices.map(function(oService) {
				return oService.iServiceID();
			});

		$.ajax({
        	url: "ajaxEhr.php?sFlag=GetAllOtherServicesForCategory&type=json",
        	data: {
        		iCategoryID:self.iCategoryID(),
        		aServices:aServiceID
        	},
        	success: function (aData){
        		self.aAvailableOtherServices(aData.map(function(oOtherService) {
        			return new OtherService({
        				iID: oOtherService.id,
						sName: oOtherService.name,
						oOtherServiceCategory: new OtherServiceCategory({
							iID: oOtherService.category_id
						})
        			});
        		}));
        	}
        });
	}

	this.setOtherServiceRate = function(oVM) {
		$.ajax({
	    	url: "ajaxEhr.php?sFlag=GetOtherServiceRateForID&type=json",
	    	data: {
	    		iOtherServiceID:self.iID(),
	    		iChannelID:oVM.iChannelID()
	    	},
	    	asyn: false,
	    	success: function (iRate){
				self.iRate(iRate);
				oVM.loadBillItemsFromScheduleItems();
	    	}
	    });
	}

	this.getBillItemObject = function() {
		// Return null if Other Service is not selected
		if(self.iID()){
			return new BillItem({
				iItemTypeID: 3,
				iItemID: self.iID(),
				sItemName: self.sName(),
				iRate: self.iRate()
			});
		}

		return null;
	}

	this.checkDuplicateOtherServices = function(oVM) {
		var bExist = false,
			aOtherServices = oVM.oSchedule.oScheduleOtherServiceItems();

		var aDuplicateOtherServices = aOtherServices.filter(function(aOtherService) {
			return aOtherService !== self && aOtherService.iID() == self.iID();
		});

		return aDuplicateOtherServices.length;
	}
}

function ScheduleMiscItem() {
	var self = this;

	this.sMiscItemName = ko.observable();
	this.sBillItemInfo = ko.observable();
	this.iRate = ko.observable();

	this.getBillItemObject = function() {
		if(self.sMiscItemName() && self.iRate()){
			return new BillItem({
				iItemTypeID: 4,
				iItemID: 0,
				sItemName: self.sMiscItemName(),
				iRate: self.iRate()
			});
		}

		return null;
	}
}

function Service(oService) {
	this.iServiceID = ko.observable(oService ? oService.iServiceID : 0);
	this.sServiceName = oService ? oService.sServiceName : "";
	this.oServiceType = oService ? oService.oServiceType : new ServiceType();
	this.oSuperServiceType = oService ? oService.oSuperServiceType : new SuperServiceType();
	this.oSampleType = oService ? oService.oSampleType : new SampleTypes();
	this.bIsCombi = ko.observable(oService ? oService.bIsCombi : 0);

	this.iServiceID.subscribe(function(iServiceID) {
		if(iServiceID){
			$.ajax({url: 'apiv2/services/'+iServiceID})
			.done(function(oService) {
				self.sServiceName = oService.sServiceName;
				self.oServiceType = new ServiceType({iServiceTypeID:oService.iServiceTypeID});
			});
		} else {
			self.sServiceName = "";
			self.oServiceType = new ServiceType();
		}
	}, this);
}

function ServiceType(oServiceType) {
	var self = this;
	this.iServiceTypeID = ko.observable(oServiceType ? oServiceType.iServiceTypeID : 0);
	this.sServiceTypeName = oServiceType ? oServiceType.sServiceTypeName : "";
	this.iServiceTypeCategory = oServiceType ? oServiceType.iServiceTypeCategory : 0;

	this.iServiceTypeID.subscribe(function(iServiceTypeID) {
		if(iServiceTypeID){
			$.ajax({url: 'apiv2/service-types/'+iServiceTypeID})
			.done(function(oServiceType) {
				self.sServiceTypeName = oServiceType.sServiceTypeName;
				self.iServiceTypeCategory = oServiceType.iServiceTypeCategory;
			});
		} else {
			self.sServiceTypeName = "";
			self.iServiceTypeCategory = 0;
		}
	}, this);
}

function SuperServiceType(oSuperServiceType) {
	var self = this;
	this.iSuperServiceTypeID = ko.observable(oSuperServiceType ? oSuperServiceType.iSuperServiceTypeID : 0);
	this.sSuperServiceTypeName = oSuperServiceType ? oSuperServiceType.sSuperServiceTypeName : "";

	this.iSuperServiceTypeID.subscribe(function(iSuperServiceTypeID) {
		if(iSuperServiceTypeID){
			$.ajax({url: 'ajaxEhr.php?sFlag=GetAllSuperServiceTypesForSChedule'+iSuperServiceTypeID})
			.done(function(oSuperServiceType) {
				self.sSuperServiceTypeName = oSuperServiceType.sSuperServiceTypeName;
				self.iSuperServiceTypeID = oSuperServiceType.iSuperServiceTypeID;
			});
		} else {
			self.sSuperServiceTypeName = "";
		}
	}, this);
}

function SampleTypes(oSampleType) {
	var self = this;
	this.iSampleTypeID = ko.observable(oSampleType ? oSampleType.iSampleTypeID : 0);
	this.sSampleTypeName = oSampleType ? oSampleType.sSampleTypeName : "";

	this.iSampleTypeID.subscribe(function(iSampleTypeID) {
		if(iSampleTypeID){
			$.ajax({url: 'ajaxEhr.php?sFlag=getAllSampleTypes'})
			.done(function(oSampleType) {
				self.sSampleTypeName = oSampleType.sample_name;
				self.iSampleTypeID = oSampleType.sample_id;
			});
		} else {
			self.sSampleTypeName = "";
		}
	}, this);
}

function OtherServiceCategory(oOtherServiceCategory) {
	this.iID = ko.observable(oOtherServiceCategory ? oOtherServiceCategory.iID : null);
	this.sCategoryName = oOtherServiceCategory ? oOtherServiceCategory.sCategoryName : null;
	this.iStatus = oOtherServiceCategory ? oOtherServiceCategory.iStatus : null;
}

function OtherService(oOtherService) {
	this.iID   = ko.observable(oOtherService ? oOtherService.iID : null);
	this.sName = oOtherService ? oOtherService.sName : null;

	this.oOtherServiceCategory = oOtherService ? oOtherService.oOtherServiceCategory : null;
}

function DoctorTeam(oDoctorTeam) {
	this.iEntityID = oDoctorTeam ? oDoctorTeam.iEntityID : 0;

	// 1 Staff (Doctor, Network Diagnostic, Network Doctors)
	// 2 Team
	this.iEntityTypeID = oDoctorTeam ? oDoctorTeam.iEntityTypeID : 0;
	this.sName = oDoctorTeam ? oDoctorTeam.sName : 0;
}

function Room(oRoom) {
	this.iRoomID = oRoom ? oRoom.iRoomID : 0;
	this.sRoomNo = oRoom ? oRoom.sRoomNo : "";
	this.sRoomName = oRoom ? oRoom.sRoomName : "";
}

function PatientRecommendation(oConfig) {
	this.sServiceName = oConfig ? oConfig.sServiceName : "";
	this.iRecommendationStatus = oConfig ? oConfig.iRecommendationStatus : 0;
	this.dScheduleDate = oConfig ? oConfig.dScheduleDate : "NA";
}

function Referral(oReferral) {
	this.iReferralID = oReferral.iReferralID ? oReferral.iReferralID : 0;
	this.sReferralName = oReferral.sReferralName ? oReferral.sReferralName : "";
}

function OrderingPhysician(oOrderingPhysician) {
	this.iID = oOrderingPhysician.iID ? oOrderingPhysician.iID : 0;
	this.sName = oOrderingPhysician.sName ? oOrderingPhysician.sName : "";
}

function ChannelType(oChannelType) {
	this.iChannelTypeID = oChannelType.iChannelTypeID ? oChannelType.iChannelTypeID : 0;
	this.sChannelTypeName = oChannelType.sChannelTypeName ? oChannelType.sChannelTypeName : "";
}

function Channel(oChannel) {
	this.iChannelID = oChannel.iChannelID ? oChannel.iChannelID : 0;
	this.sChannelName = oChannel.sChannelName ? oChannel.sChannelName : "";
}

function Bill(oVM, iBillID) {
	var self = this;
	var oVM = oVM;

	this.iBillID = ko.observable();
	this.sBillNo = ko.observable();
	this.sBillDate = ko.observable();

	this.aBillItems = ko.observableArray([]);

	this.aBillServiceItems = ko.observableArray();
	this.aBillOtherServiceItems = ko.observableArray();
	this.aBillInventoryItems = ko.observableArray();
	this.aBillMiscItems = ko.observableArray();
	this.aPayments = ko.observableArray();
	this.aPaymentMethods = ko.observableArray();
	this.oStackedPayment = ko.observable(new BillPayment());
	this.iTaxRate = ko.observable(0);

	// Loading Schedule Data
	this.iBillID.subscribe(function(iBillID) {
		if(iBillID){
			$.ajax({
				url: 'ajaxEhr.php?sFlag=GetOPDBillDetailsForScheduleID&type=json',
				data: {iScheduleID: oVM.iScheduleID()}
			})
			.done(function(aOPDBill) {

				if(aOPDBill){

					var oScheduleOtherServiceItems = [],
						oScheduleMiscItems = [];

					self.sBillNo(aOPDBill.bill_no);
					self.sBillDate(aOPDBill.bill_date);
					self.iDiscountTotal(aOPDBill.discount);
					self.iLoyaltyPercentage(aOPDBill.loyalty_discount_rate);

					// Setting Other Services
					$.each(aOPDBill.bill_other_service_items, function(index, aBillItem) {
						var oScheduleOtherService = new ScheduleOtherServiceItem();

						oScheduleOtherService.iCategoryID(aBillItem['category_id']);
						oScheduleOtherService.iID(aBillItem['id']);
						oScheduleOtherService.sName(aBillItem['item_name']);
						oScheduleOtherService.iRate(aBillItem['applied_rate']);

						oScheduleOtherServiceItems.push(oScheduleOtherService);
					});

					// Setting Misc Items
					$.each(aOPDBill.bill_miscellaneous_items, function(index, aBillItem) {
						var oScheduleMiscItem = new ScheduleMiscItem();

						oScheduleMiscItem.sMiscItemName(aBillItem['item_name']);
						oScheduleMiscItem.sBillItemInfo(aBillItem['bill_item_information']);
						oScheduleMiscItem.iRate(aBillItem['applied_rate']);

						oScheduleMiscItems.push(oScheduleMiscItem);
					});

					if(!oScheduleOtherServiceItems.length){
						oScheduleOtherServiceItems.push(new ScheduleOtherServiceItem);
					}

					if(!oScheduleMiscItems.length){
						oScheduleMiscItems.push(new ScheduleMiscItem);
					}

					//! for sests the payment bills details...
					$.each(aOPDBill.bill_payments,function(key,item){
						if(item.payment_date!=undefined && item.payment_date!=""){
							self.aPayments.push(
								new BillPayment({
									"dPaymentDate":item.payment_date,
									"iPaymentMode":item.payment_mode,
									"sPaymentNote":item.payment_notes,
									"iPaymentAmount":item.paid_amount					
								})
							);
						}
					});
					oVM.oSchedule.oScheduleOtherServiceItems(oScheduleOtherServiceItems);
					oVM.oSchedule.oScheduleMiscItems(oScheduleMiscItems);
				}	
				
				
			});
		} else {
			// Get Lastest Bill No
			$.ajax({url: 'ajaxEhr.php?sFlag=GetAutoOPDBillNo&type=json'})
				.done(function(sBillNo) {
					self.sBillNo(sBillNo);
				});
		}
	}, this);

	self.iBillID(iBillID);

	ko.computed(function() {
	   $.ajax({url: 'ajaxEhr.php?sFlag=GetAllPaymentModes&type=json'})
		.done(function(aData) {
			var aPaymentMethods = [];

			$.each(aData, function(index, aPaymentMethod) {
				aPaymentMethods.push(new PaymentMethod({
					iPaymentMode: aPaymentMethod['payment_mode_id'],
					sPaymentMode: aPaymentMethod['payment_mode']
				}));
			});

			self.aPaymentMethods(aPaymentMethods);
		});
	});

	ko.computed(function() {
	   $.ajax({url: 'apiv2/taxes/date/'+oVM.dDate()})
		.done(function(iTaxRate) {
			self.iTaxRate( parseInt(iTaxRate) );
		});
	});

	this.addStackedPayment = function () {
		var oStackedPayment = self.oStackedPayment(),
			iPendingAmount = self.iPendingAmount(),
			iPaymentAmount = oStackedPayment.iPaymentAmount();

		if(oStackedPayment.iPaymentMode() && iPaymentAmount){
			// Dont allow to pay more then pending
			if(parseFloat(iPaymentAmount) > parseFloat(iPendingAmount)){
				$.growl.error({ message: "Are you sure want to pay more than pending amount?" });
			} else {
				self.aPayments.push(self.oStackedPayment());
				self.oStackedPayment(new BillPayment);
			}
		} else {
			$.growl.error({ message: "Please add payment detail." });
		}
	}

	this.removePayment = function (oBillPaymentItem) {
		self.aPayments.remove(oBillPaymentItem);
	}

	this.iServiceTotal = ko.computed(function() {
		return 0;
	});
	this.iOtherServiceTotal = ko.computed(function() {
		return 0;
	});
	this.iInventoryServiceTotal = ko.computed(function() {
		return 0;
	});
	this.iMiscTotal = ko.computed(function() {
		return 0;
	});

	this.iTaxAmount = ko.pureComputed(function() {
		if(this.iTaxRate()){
			return (
				(this.iGrandTotal() - this.iDiscountTotal() - this.iLoyaltyDiscountTotal())*this.iTaxRate()
			)/100;
		} else {
			return 0;
		}
	},this);

	this.iGrandTotal = ko.computed(function() {
		var iGrandTotal = 0;

		$.each(self.aBillItems(), function(index, aBillItem) {
			iGrandTotal += parseInt(aBillItem.iRate());
		});

		return iGrandTotal.toFixed(2);
	});

	this.iDiscountTotal = ko.observable(0);
	this.iLoyaltyPercentage = ko.observable(0);
	this.iLoyaltyDiscountTotal = ko.pureComputed(function() {
		var iLoyaltyDiscountTotal = 0,
			iLoyaltyPercentage = this.iLoyaltyPercentage();

		if(iLoyaltyPercentage){
			$.each(self.aBillItems(), function(index, aBillItem) {
				// Only apply on service && HCP 
				if(aBillItem.iItemTypeID() == 1 || aBillItem.iItemTypeID() == 2){
					iLoyaltyDiscountTotal += (parseInt(aBillItem.iRate()) * iLoyaltyPercentage)/100;
				}
			});
		}

		return iLoyaltyDiscountTotal.toFixed(2);
	}, this);

	this.iNetAmount = ko.computed(function() {
		return (this.iGrandTotal() - this.iDiscountTotal() - this.iLoyaltyDiscountTotal() + this.iTaxAmount()).toFixed(2);
	}, this);


	this.iPaidAmount = ko.computed(function() {
		var aPayments = self.aPayments(),
			iPaidAmount = 0;

		$.each(aPayments, function(index, oPayment) {
			iPaidAmount += parseInt(oPayment.iPaymentAmount());
		});

		return iPaidAmount.toFixed(2);
	}, this);

	this.iPendingAmount = ko.computed(function() {
		return (this.iNetAmount() - this.iPaidAmount()).toFixed(2);
	}, this);

	this.bIsBillPaid = ko.computed(function() {
		return this.iPendingAmount() == 0;
	}, this);

}

function BillItem(oBillItem) {
	// 1 then Service
	// 2 then HCP
	// 3 then Other Service
	// 4 then Misc Items
	this.iItemTypeID = ko.observable(oBillItem ? oBillItem.iItemTypeID : 0);
	this.iItemID = ko.observable(oBillItem ? oBillItem.iItemID : 0);
	this.sItemName = ko.observable(oBillItem ? oBillItem.sItemName : "");
	this.iRate = ko.observable(oBillItem ? oBillItem.iRate : 0);

	this.sItemType = ko.pureComputed(function() {
		var aItemTypes = {
			1: "Service",
			2: "Health Checkup Plan",
			3: "Other Service",
			4: "Miscellaneous Item"
		};

		return aItemTypes[this.iItemTypeID()];
	}, this);
}

function BillPayment(oBillPayment) {
	this.dPaymentDate = ko.observable(oBillPayment ? oBillPayment.dPaymentDate : moment().format("DD-MM-YYYY"));
	this.iPaymentMode = ko.observable(oBillPayment ? oBillPayment.iPaymentMode : "");
	this.sPaymentNote = ko.observable(oBillPayment ? oBillPayment.sPaymentNote : "");
	this.iPaymentAmount = ko.observable(oBillPayment ? oBillPayment.iPaymentAmount :0);
}

function PaymentMethod(oPaymentMethod) {
	this.iPaymentMode = oPaymentMethod ? oPaymentMethod.iPaymentMode : 0;
	this.sPaymentMode = oPaymentMethod ? oPaymentMethod.sPaymentMode : "";
}

function getCurrentScheduleTime(){
	var hour = moment().format("HH"),
		minute = moment().format("mm");

	/*if(minute < 20){
		minute = 20;
	} else if(minute < 40){
		minute = 40;
	} else {
		minute = "00";
		hour++;
	}*/

	return hour+":"+minute;
}

function getScheduleDurations(){

	var iMinimumDuration = 10;
	var iMaximumDuration = 120;
	var iTickForDuration = 10;

	var iii = iMinimumDuration;
	var aDuration = [];

	while(iii<=iMaximumDuration) {
		aDuration.push(iii);
	    iii += iTickForDuration;
	}

	return aDuration;
}

//! for check schedule has TRF or not
function checkScheduleHasTRF(iScheduleID) {
	var iIsTRF =false;
	$.ajax({
		type: 'GET',
		url: "ajaxEhr.php?sFlag=checkScheduleHasTRF&iScheduleID="+iScheduleID,
    	async: false,
		success: function(data) {
			if($.trim(data) != false) {
				bResult = $.parseJSON(data);
				iIsTRF = bResult;
			}
		}
	});
	return iIsTRF;
}

ko.bindingHandlers.select2 = {
    init: function(el, valueAccessor, allBindingsAccessor, viewModel) {
      ko.utils.domNodeDisposal.addDisposeCallback(el, function() {
        $(el).select2('destroy');
      });

      var allBindings = allBindingsAccessor(),
          select2 = ko.utils.unwrapObservable(allBindings.select2);

      $(el).select2(select2);
    },
    update: function (el, valueAccessor, allBindingsAccessor, viewModel) {
        var allBindings = allBindingsAccessor();

        if ("value" in allBindings) {
            if ((allBindings.select2.multiple || el.multiple) && allBindings.value().constructor != Array) {                
                $(el).val(allBindings.value().split(',')).trigger('change');
            }
            else {
                //$(el).val(allBindings.value()).trigger('change');
            }
        } else if ("selectedOptions" in allBindings) {
            var converted = [];
            var textAccessor = function(value) { return value; };
            if ("optionsText" in allBindings) {
                textAccessor = function(value) {
                    var valueAccessor = function (item) { return item; }
                    if ("optionsValue" in allBindings) {
                        valueAccessor = function (item) { return item[allBindings.optionsValue]; }
                    }
                    var items = $.grep(allBindings.options(), function (e) { return valueAccessor(e) == value});
                    if (items.length == 0 || items.length > 1) {
                        return "UNKNOWN";
                    }
                    return items[0][allBindings.optionsText];
                }
            }
            $.each(allBindings.selectedOptions(), function (key, value) {
                converted.push({id: value, text: textAccessor(value)});
            });
            $(el).select2("data", converted);
        }
        $(el).trigger("change");
    }
};

ko.bindingHandlers.customerTypeahead = {
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
	    var allBindings = allBindingsAccessor();
    	var enableTypeahead = allBindings.enableTypeahead;

        if(enableTypeahead){
        	initCustomerTypeahead(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext);
        } else {
        	$(element).typeahead('destroy');
        }
    }
};


ko.bindingHandlers.testUniversalTypeahead = {
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
	    var allBindings = allBindingsAccessor();
    	var enableTypeahead = allBindings.enableTypeahead;

        if(enableTypeahead){
        	initUniversalTestTypeahead(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext);
        } else {
        	$(element).typeahead('destroy');
        }
    }
};

var initCustomerTypeahead = function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
    var $element = $(element);
    var allBindings = allBindingsAccessor();
    var typeaheadArr = ko.utils.unwrapObservable(valueAccessor());
    var sExternalPatientLabel = allBindings.externalPatientLabel;
    var enableTypeahead = allBindings.enableTypeahead;

    var timeout;
    var currentAjaxRequest = null;

    $element
    	.attr("autocomplete", "off")
        .typeahead({
            hint: true,
            highlight: true,
            minLength: 3,
        }, {
            source: function (query, process, async) {

            	if (timeout) {
	                clearTimeout(timeout);
	            }

	            timeout = setTimeout(function() {
	            	currentAjaxRequest = $.ajax({
	            		url: "ajaxCustomerSearch.php?sFlag=searchCustomerByCustomerName",
				    	async: true,
				    	data:  {'term': query},
				    	beforeSend: function() {
				    		if(currentAjaxRequest != null) {
					            currentAjaxRequest.abort();
					        }
				    	},
						success: function(data) {
							var searchResults = [];

		                    /*$.map(data, function(patient){
		                        var group;
		                        group = {
		                            type: patient.iPatientID,
		                            name: patient.sPatientName,
		                            sMobile: patient.sMobile,
		                            sPatientNo: patient.sPatientNo,
		                            toString: function () {
		                                return JSON.stringify(this);
		                            },
		                            toLowerCase: function () {
		                                return this.name.toLowerCase();
		                            },
		                            indexOf: function (string) {
		                                return String.prototype.indexOf.apply(this.name, arguments);
		                            },
		                            replace: function (string) {
		                                return String.prototype.replace.apply(this.name, arguments);
		                            }
		                        };

		                        searchResults.push(group);
		                    });*/

		                    return async(data);
						},
						complete: function(){
							currentAjaxRequest = null;
						}
	            	});

	            }, 800);
            },
            display: 'name',
            templates: {
                suggestion: function (data) {
                	var sTypeLabel = "";

                	if(data.type=="customer") {
                    	return '<div><strong>' + data.customerData.customer_name + '</strong> <small class="text-muted">('+data.customerData.customer_id+')</small><br/><div class=""><label class="label label-warning">Customer</label></div></div>';
                	}
                	else if(data.type=="location") {
                    	return '<div><strong>' + data.customerData.customer_name + '</strong> <small class="text-muted">('+data.customerData.customer_id+')</small><br/><strong>' + data.customerData.aMatchingBranch.branch_name + '</strong> <small class="text-muted">,'+data.customerData.aMatchingBranch.branch_city+'</small><br/><div class=""><label class="label label-warning">Customer</label> <label class="label label-info">Location</label></div></div>';
                	}
                	else if(data.type=="op") {
                    	return '<div><strong>' + data.opData.sOPName + '</strong> <small class="text-muted">('+data.opData.sOrderingProviderNo+')</small><br/><div class=""><label class="label label-warning">Customer</label> <label class="label label-info">OP</label></div></div>';
                	}
                	else {
                    	return '<div><strong>' + data.directData.sOPName + '</strong> <small class="text-muted">('+data.directData.sOrderingProviderNo+')</small><br/><div class=""><label class="label label-danger">Direct</label> <label class="label label-info">OP</label></div></div>';
                	}
                }
            }
        })
		.on('typeahead:selected', function (obj, datum) {
			var value = valueAccessor();
			if(datum.type=="customer") {
            	value.iCustomerMasterID(datum.customerData.customer_master_id);
            	$('#idCustomer').change();
			}
            if(datum.type=="location") {
            	value.iCustomerMasterID(datum.customerData.customer_master_id);
            	$('#idCustomer').change();
            	$('#idCustomerBranch').val(datum.customerData.aMatchingBranch.customer_branch_master_id).change();
            }
        }).on('typeahead:autocompleted', function (obj, datum) {
        	var value = valueAccessor();
            //value.setPatient(datum.iPatientID);
        });
}

ko.bindingHandlers.testUniversalTypeahead = {
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
	    var allBindings = allBindingsAccessor();
    	var enableTypeahead = allBindings.enableTypeahead;

        if(enableTypeahead){
        	initUniversalTestTypeahead(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext);
        } else {
        	$(element).typeahead('destroy');
        }
    }
};

var initUniversalTestTypeahead = function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
    var $element = $(element);
    var allBindings = allBindingsAccessor();
    var typeaheadArr = ko.utils.unwrapObservable(valueAccessor());
    var sExternalPatientLabel = allBindings.externalPatientLabel;
    var enableTypeahead = allBindings.enableTypeahead;

    var timeout;
    var currentAjaxRequest = null;

    $element
    	.attr("autocomplete", "off")
        .typeahead({
            hint: true,
            highlight: true,
            minLength: 3,
        }, {
            source: function (query, process, async) {

            	if (timeout) {
	                clearTimeout(timeout);
	            }

	            timeout = setTimeout(function() {
	            	currentAjaxRequest = $.ajax({
	            		url: "ajaxCustomerSearch.php?sFlag=searchTests",
				    	async: true,
				    	data:  {'term': query},
				    	beforeSend: function() {
				    		if(currentAjaxRequest != null) {
					            currentAjaxRequest.abort();
					        }
				    	},
						success: function(data) {
							var searchResults = [];

		                    return async(data);
						},
						complete: function(){
							currentAjaxRequest = null;
						}
	            	});

	            }, 800);
            },
            display: 'name',
            templates: {
                suggestion: function (data) {
                	var sTypeLabel = "";
                	return '<div><strong>' + data.service_name + '</strong> <small class="text-muted">('+data.service_type+')</small><br/><div class=""><label class="label label-danger">'+data.internal_code+'</label> <label class="label label-info">'+data.super_service_type+'</label></div></div>';
                }
            }
        })
		.on('typeahead:selected', function (obj, datum) {
			var oLastScheduleItem = viewModel.oSchedule.oScheduleItems()[viewModel.oSchedule.oScheduleItems().length -1];
			var bNewEntry = !(oLastScheduleItem.iServiceID() == 0 || oLastScheduleItem.iServiceID() == "" || oLastScheduleItem.iServiceID() == null || typeof oLastScheduleItem.iServiceID() == "undefined");

			if(bNewEntry) {
				// Add New
				var oScheduleItem = new ScheduleItem(viewModel);
			}
			else {
				// Use this
				var oScheduleItem = oLastScheduleItem;
			}

			console.log(oScheduleItem);
			console.log(bNewEntry);
			oScheduleItem.iSuperServiceTypeID(datum.super_service_type_id);
			oScheduleItem.iServiceTypeID(datum.service_type_id);
			oScheduleItem.iServiceID(datum.service_id);

			oScheduleItem.setAvailableServicesTypes(viewModel, datum.service_type_id);
			oScheduleItem.setAvailableServices(viewModel, datum.service_id);

			if(bNewEntry) {
				viewModel.oSchedule.oScheduleItems.push(oScheduleItem);
			}

			//if(index == 1) {
				oScheduleItem.setRate(viewModel,1);
        }).on('typeahead:autocompleted', function (obj, datum) {
        	var value = valueAccessor();
            //value.setPatient(datum.iPatientID);
        });
}

ko.bindingHandlers.attachGeneSearch = {
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
	    var allBindings = allBindingsAccessor();
    	var enableTypeahead = allBindings.enableTypeahead;

        if(enableTypeahead){
        	initGeneSearch(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext);
        } else {
        	$(element).typeahead('destroy');
        }
    }
};

var initGeneSearch = function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
    var $element = $(element);
    var allBindings = allBindingsAccessor();
    var typeaheadArr = ko.utils.unwrapObservable(valueAccessor());
    var sExternalPatientLabel = allBindings.externalPatientLabel;
    var enableTypeahead = allBindings.enableTypeahead;

    var timeout;
    var currentAjaxRequest = null;

    $element
    	.attr("autocomplete", "off")
        .typeahead({
            hint: true,
            highlight: true,
            minLength: 3,
        }, {
            source: function (query, process, async) {

            	if (timeout) {
	                clearTimeout(timeout);
	            }

	            timeout = setTimeout(function() {
	            	currentAjaxRequest = $.ajax({
	            		url: "apiv2/search-genes",
				    	async: true,
				    	data:  {'sKeyword': query},
				    	beforeSend: function() {
				    		if(currentAjaxRequest != null) {
					            currentAjaxRequest.abort();
					        }
				    	},
						success: function(data) {
							var searchResults = [];

		                    return async(data);
						},
						complete: function(){
							currentAjaxRequest = null;
						}
	            	});

	            }, 800);
            },
            display: 'name',
            templates: {
                suggestion: function (data) {
                	var sTypeLabel = "";
                	return '<div><strong>' + data.gene_code + '</strong> <label class="label label-success">'+data.gene_omim+'</label> <br/><div class=""><small class="text-muted">('+data.gene_description+')</small></div></div>';
                }
            }
        })
		.on('typeahead:selected', function (obj, datum) {
			viewModel.oSchedule.oGeneSelectionScheduleItem().aAttachedGenes.push(datum);
        }).on('typeahead:autocompleted', function (obj, datum) {
        	var value = valueAccessor();
            //value.setPatient(datum.iPatientID);
        });
}

function getFileAsBase64(file) {
	console.log(file.files)
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
  });
}

function GeneDocument(sDocumentName, sDocumentContentType, iDocumentSize, sDocumentBase64) {
	this.sDocumentName = sDocumentName;
	this.sDocumentContentType = sDocumentContentType;
	this.iDocumentSize = iDocumentSize;
	this.sDocumentBase64 = sDocumentBase64;

	this.sHumanReadableDocumentSize = ko.pureComputed(function(){
		return formatBytes(this.iDocumentSize);
	}, this)
}

function formatBytes(a,b){if(0==a)return"0 Bytes";var c=1024,d=b||2,e=["Bytes","KB","MB","GB","TB","PB","EB","ZB","YB"],f=Math.floor(Math.log(a)/Math.log(c));return parseFloat((a/Math.pow(c,f)).toFixed(d))+" "+e[f]}