function startLoading(ele, sizeW, sizeH, transparency, text) {
    ele.waitMe({
        effect : 'roundBounce',
        text : '',
        bg : "rgba(255,255,255,"+transparency+")",
        color : "#000",
        sizeW : sizeW,
        sizeH : sizeH,
        text: text,
        color: "#000"
    });

}

function stopLoading(ele) {
    ele.waitMe("hide");
}