//! Function to get samples for culture and harvest..
function getAllSamplesForCytologyCultureTest()
{
    var sSampleTableRow = '';
    var aCultureDoneIDs = [];
    $.ajax({
        url: "ajaxCytologyLabManagement.php?sFlag=getSamplesForCytologyTest",
        data: {iScheduleID:iScheduleID, iServiceID:iServiceID, iPatientID:iPatientID},
        async: true,
        type: 'POST',
        datatype: 'json',
        success: function(data)
        {   
            if($.trim(data) != false)
            {
                var iTemp = 1;
                var sBarcode = '';
                $.each(data, function(key, value)
                {

                	if (value['sample_status'] == 1)
                	{
                		sBarcode = value['collection_sample_barcode'];
                	}else if (value['sample_status'] == 2)
                	{
                		sBarcode = value['accession_sample_barcode'];
                	}else if (value['sample_status'] == -1)
                	{
                		sBarcode = value['accession_sample_barcode'];
                	}else{
                		sBarcode = "N/A";	
                	}

                    sSampleTableRow += "<tr>";
                       sSampleTableRow += '<td>'+iTemp+'</td>';
                       sSampleTableRow += '<td>'+value["sample_label"]+'</td>';
                       sSampleTableRow += '<td>'+sBarcode+'</td>';
                       sSampleTableRow += '<td>'+value['tat_first']+'</td>';
                       sSampleTableRow += '<td>'+value['tat_final']+'</td>';
                       sSampleTableRow += '<td><span class="btn btn-primary btn-small classAddCultureInformation" data-sample-id = '+value['sample_id']+' data-sample-request-id = '+value['sample_request_id']+' data-sample-process-master-id = '+value['aSampleProcessMasterID']['cytology_sample_process_master_id']+' id="idBtnAddCultureInfo" data-toggle="modal" data-target="#idModalAddCultureInformation">Add</span></td>';
                       if (!$.isEmptyObject(value['aCultureInfo'])) {
                           sSampleTableRow += '<td><span class="btn btn-primary btn-small classAddHarvestInformation" data-sample-id = '+value['sample_id']+' data-sample-request-id = '+value['sample_request_id']+' data-sample-process-master-id = '+value['aSampleProcessMasterID']['cytology_sample_process_master_id']+' data-culture-date = '+value['aCultureInfo'][0]['dCultureDate']+' id="idBtnAddHarvestInfo" data-toggle="modal" data-target="#idModalAddHarvestInformation">Add</span></td>';
                       }else{
                            sSampleTableRow += '<td><span class="btn btn-primary btn-small classAddHarvestInformation" disabled>Add</span></td>';
                       }
                       sSampleTableRow += '<td><span class="btn btn-primary btn-small classAddDisposalInformation" data-sample-id = '+value['sample_id']+' data-sample-request-id = '+value['sample_request_id']+' data-sample-process-master-id = '+value['aSampleProcessMasterID']['cytology_sample_process_master_id']+' id="idBtnAddSampleDisposal" data-toggle="modal" data-target="#idModalAddSampleDisposal">Add</span></td>';
                       sSampleTableRow +='<td><input type="checkbox" name="idSampleCultureHarvest[]" id="idSampleCultureHarvest_'+value['aSampleProcessMasterID']['cytology_sample_process_master_id']+'" class="classSelectSample" data-sample-process-master-id = '+value['aSampleProcessMasterID']['cytology_sample_process_master_id']+'></td>';
                    sSampleTableRow += "<tr>";

                    if (value['aSampleProcessMasterID']['is_culture_and_harvest_done'] == 1)
                    {
                        aCultureDoneIDs.push(value['aSampleProcessMasterID']['cytology_sample_process_master_id']);
                    }
                    
                    iTemp ++;
                });
                $(".classSamplesForCultureAndHarvest").html(sSampleTableRow);

                //! To mark check whos culture and harvest is done..
                $.each(aCultureDoneIDs, function(key, value)
                {
                    $("#idSampleCultureHarvest_"+value).prop("checked", true);
                });
            }
        }
    })
}

//! Function to get samples for slide preparation..
function getAllSamplesForCytologyTestSlidePreparation()
{
    var sSampleTableRow = '';
    $.ajax({
        url: "ajaxCytologyLabManagement.php?sFlag=getSamplesForCytologyTest",
        data: {iScheduleID:iScheduleID, iServiceID:iServiceID, iPatientID:iPatientID},
        async: true,
        type: 'POST',
        datatype: 'json',
        success: function(data)
        {   
            if($.trim(data) != false)
            {   
                var iTemp = 1;
                var sBarcode = '';
                $.each(data, function(key, value)
                {

                    if (value['sample_status'] == 1)
                    {
                        sBarcode = value['collection_sample_barcode'];
                    }else if (value['sample_status'] == 2)
                    {
                        sBarcode = value['accession_sample_barcode'];
                    }else if (value['sample_status'] == -1)
                    {
                        sBarcode = value['accession_sample_barcode'];
                    }else{
                        sBarcode = "N/A";   
                    }

                    sSampleTableRow += "<tr>";
                       sSampleTableRow += '<td>'+iTemp+'</td>';
                       sSampleTableRow += '<td>'+value["sample_label"]+'</td>';
                       sSampleTableRow += '<td>'+sBarcode+'</td>';
                       sSampleTableRow += '<td>'+value['tat_first']+'</td>';
                       sSampleTableRow += '<td>'+value['tat_final']+'</td>';

                       if (value['aSampleProcessMasterID']['bCultureHarvestDone'] == true)
                       {
                           sSampleTableRow += '<td><span class="btn btn-primary btn-small classAddBloodSilde" data-sample-id = '+value['sample_id']+' data-sample-request-id = '+value['sample_request_id']+' data-sample-process-master-id = '+value['aSampleProcessMasterID']['cytology_sample_process_master_id']+' id="idBtnAddBloodSlideInfo" data-toggle="modal" data-target="#idModalAddBloodSlideInformation" data-sample-name="'+value["sample_label"]+'">Add</span></td>';
                           sSampleTableRow += '<td><span class="btn btn-primary btn-small classAddPreNatalSlideInformation" data-sample-id = '+value['sample_id']+' data-sample-request-id = '+value['sample_request_id']+' data-sample-process-master-id = '+value['aSampleProcessMasterID']['cytology_sample_process_master_id']+' id="idBtnAddPreNatalInfo" data-toggle="modal" data-target="#idModalAddPreNatalSlideInformation" data-sample-name="'+value["sample_label"]+'">Add</span></td>';
                           sSampleTableRow += '<td><span class="btn btn-primary btn-small classAddPelletStorage" data-sample-id = '+value['sample_id']+' data-sample-request-id = '+value['sample_request_id']+' data-sample-process-master-id = '+value['aSampleProcessMasterID']['cytology_sample_process_master_id']+' id="idBtnAddBufferChecking" data-toggle="modal" data-target="#idModalAddPelletStorage" data-sample-name="'+value["sample_label"]+'">Add</span></td>';
                       }else{
                            sSampleTableRow += '<td><span class="btn btn-primary btn-small" disabled>Add</span></td>';
                            sSampleTableRow += '<td><span class="btn btn-primary btn-small" disabled>Add</span></td>';
                            sSampleTableRow += '<td><span class="btn btn-primary btn-small" disabled>Add</span></td>';
                       }

                    sSampleTableRow += "<tr>";
                    
                    iTemp ++;
                });
                $(".classSamplesForSlidePreparation").html(sSampleTableRow);
            }
        }
    })
}        

//! Function to get samples list for image capturing..
function getSamplesForCytologyImageCapturingTest(){
    var sSampleTableRow = '';
    $.ajax({
        url: "ajaxCytologyLabManagement.php?sFlag=getSamplesForCytologyTest",
        data: {iScheduleID:iScheduleID, iServiceID:iServiceID, iPatientID:iPatientID},
        async: true,
        type: 'POST',
        datatype: 'json',
        success: function(data)
        {
            if($.trim(data) != false)
            {
                var iTemp = 1;
                var sBarcode = '';
                $.each(data, function(key, value)
                {

                    if (value['sample_status'] == 1)
                    {
                        sBarcode = value['collection_sample_barcode'];
                    }else if (value['sample_status'] == 2)
                    {
                        sBarcode = value['accession_sample_barcode'];
                    }else if (value['sample_status'] == -1)
                    {
                        sBarcode = value['accession_sample_barcode'];
                    }else{
                        sBarcode = "N/A";
                    }

                    if (value['aSampleProcessMasterID']['bSlidePreparationDone'] == true)
                    {
                        if (value['aSampleProcessMasterID']['is_image_capturing_done_done']==1) {
                            var dImageConfirmationDate = value['aSampleProcessMasterID']['dImageConfirmationDate'];
                            var tImageConfirmationTime = value['aSampleProcessMasterID']['tImageConfirmationTime'];
                            var sConfirmImageCaptureDone = 'Confirmed on '+dImageConfirmationDate+' '+tImageConfirmationTime;
                        }
                        else{
                            var sConfirmImageCaptureDone = '<span class="btn btn-primary btn-small classBtnConfirmImageCapture" id="idBtnConfirmImageCapture_'+key+'" data-sample-id = '+value['sample_id']+' data-sample-request-id = '+value['sample_request_id']+' data-sample-process-master-id = '+value['aSampleProcessMasterID']['cytology_sample_process_master_id']+'>Confirm</span>';
                        }
                    }else{
                        var sConfirmImageCaptureDone = '<span class="btn btn-primary btn-small" disabled>Confirm</span>';
                    }       

                    sSampleTableRow += "<tr>";
                       sSampleTableRow += '<td>'+iTemp+'</td>';
                       sSampleTableRow += '<td>'+value["sample_label"]+'</td>';
                       sSampleTableRow += '<td>'+sBarcode+'</td>';
                       sSampleTableRow += '<td>'+value['tat_first']+'</td>';
                       sSampleTableRow += '<td>'+value['tat_final']+'</td>';
                       sSampleTableRow += '<td>'+sConfirmImageCaptureDone+'</td>';
                    sSampleTableRow += "<tr>";

                    iTemp ++;
                });
                $(".classTableBodyImageCapturing").html(sSampleTableRow);
            }
        }
    })
}

//! Function to get samples list for karyotyping stage..
function getSamplesForCytologyKaryotypingTest(){
    var sSampleTableRow = '';
    $.ajax({
        url: "ajaxCytologyLabManagement.php?sFlag=getSamplesForCytologyTest",
        data: {iScheduleID:iScheduleID, iServiceID:iServiceID, iPatientID:iPatientID},
        async: true,
        type: 'POST',
        datatype: 'json',
        success: function(data)
        {
            if($.trim(data) != false)
            {
                var iTemp = 1;
                var sBarcode = '';
                $.each(data, function(key, value)
                {

                    if (value['sample_status'] == 1)
                    {
                        sBarcode = value['collection_sample_barcode'];
                    }else if (value['sample_status'] == 2)
                    {
                        sBarcode = value['accession_sample_barcode'];
                    }else if (value['sample_status'] == -1)
                    {
                        sBarcode = value['accession_sample_barcode'];
                    }else{
                        sBarcode = "N/A";
                    }

                    if (value['aSampleProcessMasterID']['bImageCapturingDone'] == true)
                    {
                        if (value['aSampleProcessMasterID']['is_karyostorage_stage_done']==1) {
                            var dKaryotypingConfirmationDate = value['aSampleProcessMasterID']['dKaryotypingConfirmationDate'];
                            var tKaryotypingConfirmationTime = value['aSampleProcessMasterID']['tKaryotypingConfirmationTime'];
                            var sConfirmKaryotypinngDone = 'Confirmed on '+dKaryotypingConfirmationDate+' '+tKaryotypingConfirmationTime;
                        }
                        else{
                            var sConfirmKaryotypinngDone = '<span class="btn btn-primary btn-small classBtnConfirmKaryotyping" id="idBtnConfirmKaryotyping_'+key+'" data-sample-id = '+value['sample_id']+' data-sample-request-id = '+value['sample_request_id']+' data-sample-process-master-id = '+value['aSampleProcessMasterID']['cytology_sample_process_master_id']+'>Confirm</span>';
                        }
                    }else{
                        var sConfirmKaryotypinngDone = '<span class="btn btn-primary btn-small" disabled>Confirm</span>';
                    }   

                    sSampleTableRow += "<tr>";
                       sSampleTableRow += '<td>'+iTemp+'</td>';
                       sSampleTableRow += '<td>'+value["sample_label"]+'</td>';
                       sSampleTableRow += '<td>'+sBarcode+'</td>';
                       sSampleTableRow += '<td>'+value['tat_first']+'</td>';
                       sSampleTableRow += '<td>'+value['tat_final']+'</td>';
                       sSampleTableRow += '<td>'+sConfirmKaryotypinngDone+'</td>';
                    sSampleTableRow += "<tr>";

                    iTemp ++;
                });
                $(".classTableBodyKaryotyping").html(sSampleTableRow);
            }
        }
    })
}

//! Function to get samples list for finalization stage..
function getSamplesForCytologyFinalizationTest(){
    var sSampleTableRow = '';
    $.ajax({
        url: "ajaxCytologyLabManagement.php?sFlag=getSamplesForCytologyTest",
        data: {iScheduleID:iScheduleID, iServiceID:iServiceID, iPatientID:iPatientID},
        async: true,
        type: 'POST',
        datatype: 'json',
        success: function(data)
        {
            if($.trim(data) != false)
            {
                var iTemp = 1;
                var sBarcode = '';
                $.each(data, function(key, value)
                {

                    if (value['sample_status'] == 1)
                    {
                        sBarcode = value['collection_sample_barcode'];
                    }else if (value['sample_status'] == 2)
                    {
                        sBarcode = value['accession_sample_barcode'];
                    }else if (value['sample_status'] == -1)
                    {
                        sBarcode = value['accession_sample_barcode'];
                    }else{
                        sBarcode = "N/A";
                    }

                    if (value['aSampleProcessMasterID']['bKaryotypingStageDone'] == true)
                    {
                        if (value['aSampleProcessMasterID']['is_finalization_done']==1) {
                            var dFinalizationDate = value['aSampleProcessMasterID']['dFinalizationDate'];
                            var tFinalizationTime = value['aSampleProcessMasterID']['tFinalizationTime'];
                            var sConfirmFinalizationBtn = 'Confirmed on '+dFinalizationDate+' '+tFinalizationTime;
                            var sAddFinalizationBtn = '<span class="btn btn-info btn-small classBtnAddFinalizationInformation" id="idBtnAddFinalizationInformation_'+key+'" data-sample-id = '+value['sample_id']+' data-sample-request-id = '+value['sample_request_id']+' data-sample-process-master-id = '+value['aSampleProcessMasterID']['cytology_sample_process_master_id']+' data-finalize="1" data-toggle="modal" data-target="#idModalAddFinalizeInformation">View</span>';
                        }
                        else{
                            var sAddFinalizationBtn = '<span class="btn btn-primary btn-small classBtnAddFinalizationInformation" id="idBtnAddFinalizationInformation_'+key+'" data-sample-id = '+value['sample_id']+' data-sample-request-id = '+value['sample_request_id']+' data-sample-process-master-id = '+value['aSampleProcessMasterID']['cytology_sample_process_master_id']+' data-finalize="0" data-toggle="modal" data-target="#idModalAddFinalizeInformation">Add</span>';

                            var sConfirmFinalizationBtn = '<span class="btn btn-primary btn-small classBtnConfirmFinalization" id="idBtnConfirmFinalization_'+key+'" data-sample-id = '+value['sample_id']+' data-sample-request-id = '+value['sample_request_id']+' data-sample-process-master-id = '+value['aSampleProcessMasterID']['cytology_sample_process_master_id']+'>Confirm</span>';
                        }
                    }else{
                        var sAddFinalizationBtn = '<span class="btn btn-primary btn-small" disabled>Add</span>';

                        var sConfirmFinalizationBtn = '<span class="btn btn-primary btn-small" disabled>Confirm</span>';
                    }

                    sSampleTableRow += "<tr>";
                       sSampleTableRow += '<td>'+iTemp+'</td>';
                       sSampleTableRow += '<td>'+value["sample_label"]+'</td>';
                       sSampleTableRow += '<td>'+sBarcode+'</td>';
                       sSampleTableRow += '<td>'+value['tat_first']+'</td>';
                       sSampleTableRow += '<td>'+value['tat_final']+'</td>';
                       sSampleTableRow += '<td>'+sAddFinalizationBtn+' '+sConfirmFinalizationBtn+'</td>';
                    sSampleTableRow += "<tr>";

                    iTemp ++;
                });
                $(".classTableBodyCytologyFinalization").html(sSampleTableRow);
            }
        }
    })
}

//! TO get the samples for sample rejection helper..
function cytologyLabKTSampleRejection(iFlag)
{
    var sSampleRow = '';
    $.ajax({
        type: "POST",
        url: "ajaxCytologyLabManagement.php?sFlag=getSampleListOfCytologyKT&iIsKaryotypingOrFish="+iFlag,
        datatype: "JSON",
        data:{iPatientID:iPatientID,iServiceID:iServiceID,iScheduleID:iScheduleID},
        success: function(data) { 
            if(data != false)
            {
                var iSrNo = 1;
                $.each(data, function(key, value)
                {
                    if (value['sample_status'] == 1)
                    {
                        var sBarcode = value['collection_sample_barcode'];
                    }else if (value['sample_status'] == 2)
                    {
                        var sBarcode = value['accession_sample_barcode'];
                    }else if (value['sample_status'] == -1)
                    {
                        var sBarcode = value['accession_sample_barcode'];
                    }else{
                        var sBarcode = "N/A";  
                    }

                    if (value['sample_status'] == -1 || value['sample_status'] == -2)
                    {
                        sSampleRow += '<tr style="background:#ff7c7c;">';
                        var sReject = '<a class="btn btn-danger btn-small" disabled>Rejected</a>';
                    }else{
                        sSampleRow += '<tr>';
                        var sReject = '<a data-toggle="modal" data-target="#idModalSampleRejection" class="btn btn-danger btn-small classSampleRejection" data-sample-id = "'+value['sample_id']+'" data-sample-request-id = "'+value['sample_request_id']+'" data-sample-collection-instance-id = "'+value['sample_collection_instance_id']+'">Reject</a>';
                    }

                        sSampleRow += '<td>'+iSrNo+'</td>';
                        sSampleRow += '<td>'+sBarcode+'</td>';
                        sSampleRow += '<td>'+value['sample_label']+'</td>';
                        sSampleRow += '<td>'+sReject+'</td>';
                    sSampleRow += '</tr>';

                    iSrNo ++;
                });
                $("#idSampleRejection").html(sSampleRow);
            }else{
                $("#idSampleRejection").html("<span style='color:red;'>No samples found.</span>");
            }
        }            
    });
}