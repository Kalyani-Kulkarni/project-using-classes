<?php   

include_once "classes/class.SessionManager.php";
include_once "classes/class.DBConnManager.php";
include_once "config/config.database.php";
include_once "config/config.medixcel.php";
include_once "classes/class.User.php";

$oSessionManager = new SessionManager;

$oSessionManager->fLogOut();

header('location:login.php');

?>