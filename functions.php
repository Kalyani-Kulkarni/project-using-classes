<?php
include_once "classes/class.DBConnManager.php";
include_once "config/config.database.php";
include_once "config/config.medixcel.php";
include_once "config/congig.messageConstants.php";

//function to redirect user to specified url
function fRedirectWithAlert($url, $msgCodes)
{
	$sMsg = implode("::x::",$msgCodes);
	$sCode = urlsafe_b64encode($sMsg);
	if(strstr($url,'?')!==FALSE){
		$fURL =  $url."&alerts={$sCode}";
	}
	else {
		$fURL =  $url."?alerts={$sCode}";
	}
	header("Location: {$fURL}");
}

//function returns the base64_encode of the @a $string that can be passed in URL
function urlsafe_b64encode($string)
{
  $data = base64_encode($string);
  $data = str_replace(array('+','/','='),array('-','_','.'),$data);
  return $data;
}

//function to add customer list price
function addCustomerListPrice($iCustomerType, $iYear, $iServiceType, $iPrice, $iCurrencyID, $iUserID)
{
	$dAddedOn = date('Y-m-d H:i:s');
	$DBMan = new DBConnManager();
	$conn = $DBMan->getConnInstance();
	$sTable = "mxcel_customer_list_price";
	$sQuery = "Insert into `{$sTable}` (`id`, `customer_type_id`, `year`, `service_id`, `price`, `added_on`, `added_by`, `is_hcp`, `currency_id`, `deleted`) values (null, '{$iCustomerType}', '{$iYear}', '{$iServiceType}', '{$iPrice}', '{$dAddedOn}', '{$iUserID}', 0, '{$iCurrencyID}', 0)";
	$sResult = $conn->query($sQuery);
	return $sResult;
}

function getAllCombiBillingRecords($iStart = 0, $iLength = 20, $aFilters= array(), $bCountOnly = false)
{
	$aData = [];
	$DBMan = new DBConnManager();
	$conn = $DBMan->getConnInstance();

	$sPackageTable = 'cph_hcp';
	$sPackageServiceMappingTable = 'cph_hcp_services';
	$sServicesTable = 'mxcel_services';
	$sSampleTable = 'cph_pathology_test_sample_master';
	$sSampleServiceMappingTable = 'cph_pathology_test_sample_mapping';
	$sMasterTable = 'mxcel_combi_billing_master';

	//! Filter by sample type..
    if(isset($aFilters['iSampleTypeID']) && $aFilters['iSampleTypeID'] != ''){
        $sWhereClause .= " AND e.`sample_id` = '{$aFilters['iSampleTypeID']}'";
    }

    //! Filter by package type..
    if(isset($aFilters['iPackageID']) && $aFilters['iPackageID'] != ''){
        $sWhereClause .= " AND a.`hcp_id` = '{$aFilters['iPackageID']}'";
    }

    //! Filter by service id..
    if(isset($aFilters['iServiceID']) && $aFilters['iServiceID'] != ''){
        $sWhereClause .= " AND c.`service_id` = '{$aFilters['iServiceID']}'";
    }

	if($iLength>0)
	{
		$sWhereClauseEnd .= " LIMIT {$iStart}, {$iLength}";
	}

	if($bCountOnly == true){
        //! This will only return count reducing the network transfer between mysql and web server
        $sQuery = "SELECT count(*) FROM `{$sPackageTable}` a 
				LEFT JOIN `{$sPackageServiceMappingTable}` b
				ON a.hcp_id = b.hcp_id AND b.status=1
				LEFT JOIN `{$sServicesTable}` c
				ON b.service_id = c.service_id AND c.status=1
				LEFT JOIN `{$sSampleServiceMappingTable}` d
				ON d.service_id = c.service_id AND d.status=1
				LEFT JOIN `{$sSampleTable}` e
				ON e.sample_id = d.sample_id AND e.status =1
				LEFT JOIN `{$sMasterTable}` f
				ON b.service_id = f.service_id AND f.deleted=0 AND b.hcp_id = f.hcp_id
				WHERE a.status = 1 {$sWhereClause}
				ORDER BY b.hcp_id, b.service_id
				{$sWhereClauseEnd}";
        

        $rResult = $conn->query($sQuery);
        if($rResult) {
            $aRow = $rResult->fetch_array();
            return $aRow[0];
        }else {
            return $conn->error;
        }
    }

	$sQuery = "SELECT e.sample_label, a.name, c.service_name, e.sample_id, a.hcp_id, c.service_id, f.test_completed_successfully_percentage, f.test_completed_successfully_amount, f.test_rejected_percentage, f.test_rejected_amount, f.test_cancelled_percentage, f.test_cancelled_amount FROM `{$sPackageTable}` a 
				LEFT JOIN `{$sPackageServiceMappingTable}` b
				ON a.hcp_id = b.hcp_id AND b.status=1
				LEFT JOIN `{$sServicesTable}` c
				ON b.service_id = c.service_id AND c.status=1
				LEFT JOIN `{$sSampleServiceMappingTable}` d
				ON d.service_id = c.service_id AND d.status=1
				LEFT JOIN `{$sSampleTable}` e
				ON e.sample_id = d.sample_id AND e.status =1
				LEFT JOIN `{$sMasterTable}` f
				ON b.service_id = f.service_id AND f.deleted=0 AND b.hcp_id = f.hcp_id
				WHERE a.status = 1 {$sWhereClause}
				ORDER BY b.hcp_id, b.service_id
				{$sWhereClauseEnd}";

	$sResult = $conn->query($sQuery);

	if($sResult!=FALSE)
	{
		while ($aRows = $sResult->fetch_assoc()) {
			$aData[] = $aRows;
		}
	}
	return $aData;
}

function invalidateCombiBill($iServiceId, $iPackageId)
{
	$DBMan = new DBConnManager();
	$conn = $DBMan->getConnInstance();
	$sTable = 'mxcel_combi_billing_master';
	$sQuery = "UPDATE `{$sTable}` SET `deleted`= 1 WHERE `service_id`='{$iServiceId}' and `hcp_id`='{$iPackageId}' and deleted = 0";
	$sResult = $conn->query($sQuery);
	return $sResult;
}

function addCombiBillingMaster($iSampleID, $iPackageId, $iServiceId, $fTestCompleteAmount, $fTestRejectAmount, $fTestCancelAmount, $fTestCompletePercent, $fTestRejectPercent, $fTestCancelPercent, $iUserID)
{
	$DBMan = new DBConnManager();
	$conn = $DBMan->getConnInstance();
	$sMasterTable = 'mxcel_combi_billing_master';
	$dAddedOn = date('Y-m-d H-i-s');

	$sQuery1 = "INSERT INTO `{$sMasterTable}` (`auto_id`, `sample_id`, `hcp_id`, `service_id`, `test_completed_successfully_percentage`, `test_completed_successfully_amount`, `test_rejected_percentage`, `test_rejected_amount`, `test_cancelled_percentage`, `test_cancelled_amount`, `added_on`, `added_by`, `deleted`) VALUES (null, '{$iSampleID}', '{$iPackageId}', '{$iServiceId}', '{$fTestCompletePercent}', '{$fTestCompleteAmount}', '{$fTestRejectPercent}', '{$fTestRejectAmount}', '{$fTestCancelPercent}', '{$fTestCancelAmount}', '{$dAddedOn}', '{$iUserID}', 0)";

		$sResult1 = $conn->query($sQuery1);
		
		if($sResult1 > 0)
		{
			$iMasterCombiBillingId = $conn->insert_id;
		}

	return $iMasterCombiBillingId;
}

function getAllServiceSamples($aFilters = array())
{
	$aData = array();
    $DBMan = new DBConnManager();
    $conn =  $DBMan->getConnInstance();
    $sQueryWHERE = "";
    $sGroupBy = "";
    
    // Searching samples by name..
    if(isset($aFilters['sSampleName'])) {
        $sQueryWHERE = " WHERE `sample_label` LIKE '%{$aFilters['sSampleName']}%' AND `status` = 1";
    }

    // For avoiding the duplicate entry...
    if (isset($aFilters['iIsGroupBy']) && $aFilters['iIsGroupBy'] != 0) {
        $sGroupBy = " GROUP BY `sample_label` ";
    }

    $sTable = 'cph_pathology_test_sample_master';
    $sSQuery = "SELECT * FROM `{$sTable}` {$sQueryWHERE} {$sGroupBy}";

    if($conn != false){

        $sSQueryR = $conn->query($sSQuery);

        if($sSQueryR != false){
            
            while($aRow = $sSQueryR->fetch_assoc()){
            	$aData[] = $aRow;
            }
        }
    }
    
    return $aData;
}

function getAllPackageNames($aFilters = array())
{
	$aData = array();
    $DBMan = new DBConnManager();
    $conn =  $DBMan->getConnInstance();
    $sQueryWHERE = "";
    $sGroupBy = "";
    
    // Searching package by name..
    if(isset($aFilters['sPackageName'])) {
        $sQueryWHERE = " WHERE `name` LIKE '%{$aFilters['sPackageName']}%' AND `status` = 1";
    }

    // For avoiding the duplicate entry...
    if (isset($aFilters['iIsGroupBy']) && $aFilters['iIsGroupBy'] != 0) {
        $sGroupBy = " GROUP BY `name` ";
    }

    $sTable = 'cph_hcp';
    $sSQuery = "SELECT * FROM `{$sTable}` {$sQueryWHERE} {$sGroupBy}";

    if($conn != false){

        $sSQueryR = $conn->query($sSQuery);

        if($sSQueryR != false){
            
            while($aRow = $sSQueryR->fetch_assoc()){
            	$aData[] = $aRow;
            }
        }
    }
    
    return $aData;
}

function getAllServiceNames($aFilters = array())
{
	$aData = array();
    $DBMan = new DBConnManager();
    $conn =  $DBMan->getConnInstance();
    $sQueryWHERE = "";
    $sGroupBy = "";
    
    // Searching service by name..
    if(isset($aFilters['sTestName'])) {
        $sQueryWHERE = " WHERE a.service_name LIKE '%{$aFilters['sTestName']}%' AND a.status = 1";
    }

    // For avoiding the duplicate entry...
    if (isset($aFilters['iIsGroupBy']) && $aFilters['iIsGroupBy'] != 0) {
        $sGroupBy = " GROUP BY a.service_name ";
    }

    $sTable = 'mxcel_services';
    $sMappingTable = 'cph_pathology_test_sample_mapping';
    $sSampleTable = 'cph_pathology_test_sample_master';
    $sQuery = "SELECT a.service_id, a.service_name, c.sample_label FROM `{$sTable}` a
    			left join `{$sMappingTable}` b on a.service_id = b.service_id
    			left join `{$sSampleTable}` c on b.sample_id = c.sample_id 
    			{$sQueryWHERE} {$sGroupBy}";

    if($conn != false){

        $sQueryR = $conn->query($sQuery);

        if($sQueryR != false){
            
            while($aRow = $sQueryR->fetch_assoc()){
            	$aData[] = $aRow;
            }
        }
    }    
    return $aData;
}

function getAllCombiBillingMaster($aFilter)
{
	$DBMan = new DBConnManager();
    $conn =  $DBMan->getConnInstance();

    $aCombiBillingMasterData = array();
    
    $sQueryWHERE = "";
    if(isset($aFilter['iServiceId']) && $aFilter['iServiceId']) {
        $sQueryWHERE .= " AND `service_id` = '{$aFilter['iServiceId']}'";
    }

    $sTable = 'mxcel_combi_billing_master';

    $sSQuery = "SELECT * FROM `{$sTable}` WHERE `deleted` = 0 {$sQueryWHERE}";

    if($conn != false){
        $sSQueryR = $conn->query($sSQuery);
        if($sSQueryR != false){
            while($aRow = $sSQueryR->fetch_assoc()){
                $aCombiBillingMasterData[] = $aRow;
            }
        }
    }
    return $aCombiBillingMasterData;
}