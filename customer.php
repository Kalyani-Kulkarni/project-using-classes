<?php

include_once "sessionCheck.php";

$sPageTopTitle = "Customer";
$iID = $oSessionManager->iUserID;
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
 	<?php include_once 'medixcelHeaderB3.php'; ?>
 	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="js/moment.js"></script>
</head>
<body class="stickyMedixcelFooter flat-blue">
<?php include_once 'medixcelNavbarB3.php'; ?>

	<div class="container-fluid classContainerBody">
        <div class="row mr-3">
            <div class="col-lg-12">
                <a href="#idModalAddCustomerPrice" class="btn btn-dark pull-right text-white" data-toggle="modal" name="idAddCustomerPrice" id="idAddCustomerPrice">Add Customer List Price</a>
            </div>
        </div>
        <br>
	</div>

    <!-- Modal to Add Customer List Price -->
    <div class="modal fade" id="idModalAddCustomerPrice" role="dialog" style="margin-left: 20%;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"><b>Add Customer List Price</b></h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <form class="classFormSubmitPriceList" id="idCustomerListPriceForm">
                    <div class="modal-body">
                        <div class="row-fluid">
                            <div class="col-lg-4 col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label for="idCustomerType">Customer Type :</label>
                                    <select name="idCustomerType" id="idCustomerType" class="form-control">
                                        <option value>Select Any</option>
                                        <option value="1">Lab in Lab</option>
                                        <option value="2">Collection Center</option>
                                        <option value="3">Direct Partner</option>
                                        <option value="4">ILC</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-lg-4 col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label for="idYear">Year :</label>
                                    <select name="idYear" id="idYear" class="form-control">
                                    </select>
                                </div>
                            </div>

                            <div class="col-lg-4 col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label for="idServiceID">Service Type :</label>
                                    <select name="idServiceID" id="idServiceID" class="form-control">
                                        <option value>Select Any</option>
                                        <option value="1">Heamoglobin</option>
                                        <option value="2">Ortho</option>
                                        <option value="3">Cardiology</option>
                                        <option value="4">Physiotherapy</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-lg-4 col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label for="idPrice">Price :</label>
                                    <input type="text" class="form-control" id="idPrice" name="idPrice">
                                </div>
                            </div>

                            <div class="col-lg-4 col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label for="idCurrencyID">Currency Type :</label>
                                    <select name="idCurrencyID" id="idCurrencyID" class="form-control">
                                        <option value>Select Any</option>
                                        <option value="1">Rupees</option>
                                        <option value="2">Dollar</option>
                                    </select>
                                </div>
                                 <input type="hidden" name="idUserId" id="idUserId" value="<?php echo $iID ?>">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                       <input type='submit' name='idButton' id='idButton' value='Add' class='btn btn-dark text-white' />
                        <button type="button" class="btn btn-dark text-white btn-md" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


	<!-- Modal for register new user -->
    <div class="modal fade" id="idAddModal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Register User</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <form method="post" id="idAddUserForm" novalidate>
                        <div class="form-group">
                              <label for="usernm">User Name :</label>
                              <input type="text" class="form-control" id="idAddName" name="idAddName" placeholder="Enter name">
                        </div>


                        <div class="form-group">
                              <label for="email">Email :</label>
                              <input type="email" class="form-control" id="idAddEmail" name="idAddEmail" placeholder="Enter email">
                        </div>

                        <div class="form-group">
                              <label for="userid">User ID :</label>
                              <input type="text" class="form-control" id="idAddUsername" name="idAddUsername" placeholder="Enter id">
                        </div>

                        <div class="form-group">
                              <label for="password">Password :</label>
                              <input type="password" class="form-control" id="idAddPassword" name="idAddPassword" placeholder="Enter Password">
                        </div>

                        <div class="form-group">
                            <label for="repassword">Retype Password :</label>
                            <input type="password" class="form-control" id="idAddRePassword" name="idAddRePassword" placeholder="Re-enter Password">
                        </div>
                    </form>
                    <input type=hidden name="idAddUserId" id="idAddUserId">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-dark text-white" name="idRegsiter" id="idRegister" data-dismiss="modal">Register</button>
                    <button type="button" class="btn btn-dark text-white" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

<?php //include_once 'medixcelFooterB3.php'; ?>

<script type="text/javascript">

	$(document).ready(function(){
        generateYear();
    });

    function generateYear(){
        var oObject = new moment();
        var year=oObject.format('YYYY');
        for(let iii=1; iii<=30; iii++){
            var dYear = year++;
            $('#idYear').append($("<option></option>").val(dYear).text(dYear));
        }
    }

    //Script to register new user
    function register()
    {
        var sName=$('#idAddName').val();
        var sEmail=$('#idAddEmail').val();
        var sUsername=$('#idAddUsername').val();
        var sPassword=$('#idAddPassword').val();
        $.ajax({
            url:"ajaxFile.php?sFlag=AddUser",
            method:"post",
            data: {
                sName:sName,
                sEmail:sEmail,
                sUsername:sUsername,
                sPassword:sPassword
            },
            success:function(data){
                $('#idAddModal').modal('hide');
            },
        });
    }

    //Script to clear modal content
    $("#idAddModal").on('hidden.bs.modal', function () {
        $(this).find("input").val('');
    });

    //Script to check whether password matches the retype password on the registration form
    $(document).ready(function(){
        $('#idRegister').on("click", function(event){
            var sPassword=$('#idAddPassword').val();
            var sRePassword=$('#idAddRePassword').val();
            if(sPassword==sRePassword)
            {
                register();
            }
            else
            {
                alert("Password and Retype password doesn't match");
            }
        });
    });

    //Script to add customer list price form by submit event
    $(function() {
        $('#idCustomerListPriceForm').submit(function(e) {
            e.preventDefault();

            data = new FormData($('#idCustomerListPriceForm')[0]);

            $.ajax({
                type: 'POST',
                url: 'ajaxFile.php?sFlag=AddCustomerListPrice',
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                success: function(data){
                    if(data!=false)
                    {
                        $('#idModalAddCustomerPrice').modal('hide');
                        alert("success");
                    }
                }
            });
        });
    });

</script>
</body>
</html>