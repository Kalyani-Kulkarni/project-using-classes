<?php 

include_once "config/config.database.php";
include_once "config/config.medixcel.php";
include_once "classes/class.User.php";
include_once "classes/class.SessionManager.php";
include_once "classes/class.DBConnManager.php";
include_once "functions.php";

$oSessionManager = new SessionManager();
$oSessionManager->fRequireAuth();

$iUserID=$_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_iUserID'];
$sUsername=$_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_sUsername'];
$sName=$_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_sName'];

$sUrl="http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
$_SESSION['_'.MEDIXCEL_UNIQUE_KEY.'_url'] = $sUrl;
 ?>