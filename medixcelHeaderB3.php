<title><?php echo(isset($sPageTopTitle) ? $sPageTopTitle : ORG_NAME) ?></title>
<link rel='shortcut icon' href='images/favicon.ico' type='image/x-icon' >

<meta charset="utf-8">
<link rel="stylesheet" href="assets/bootstrap/dist/css/bootstrap.min.css">
<link rel="stylesheet" href="assets/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css">
<link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="assets/select2/dist/css/select2.min.css" />
<link rel="stylesheet" href="css/app-boot3.css">
<link rel="stylesheet" href="css/theme.css">
<link rel="stylesheet" href="css/slider.css">
<link rel="stylesheet" href="css/timeline.css">
<link rel="stylesheet" type="text/css" href="css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="assets/bootstrap-daterangepicker/daterangepicker.css" />

<script src="assets/jquery/dist/jquery.min.js"></script>
<script src="assets/bootstrap/dist/js/bootstrap.min.js"></script>
<script src='assets/moment/min/moment.min.js'></script>
<script src="assets/knockout/dist/knockout.debug.js"></script>
<script src="assets/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="assets/select2/dist/js/select2.full.min.js"></script>
<script src="assets/growl/javascripts/jquery.growl.js"></script>
<script type="text/javascript" language="javascript" src="js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="assets/bootstrap-daterangepicker/daterangepicker.js"></script>
