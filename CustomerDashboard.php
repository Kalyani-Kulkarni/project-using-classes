<?php

include_once "sessionCheck.php";

$sPageTopTitle = "Customer Dashboard";
$iID = $oSessionManager->iUserID;
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
 	<?php include_once 'medixcelHeaderB3.php'; ?>
 	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="js/moment.js"></script>
</head>
<body class="stickyMedixcelFooter flat-blue">
<?php include_once 'medixcelNavbarB3.php'; ?>

	<!-- Modal for register new user -->
    <div class="modal fade" id="idAddModal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Register User</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <form method="post" id="idAddUserForm" novalidate>
                        <div class="form-group">
                              <label for="usernm">User Name :</label>
                              <input type="text" class="form-control" id="idAddName" name="idAddName" placeholder="Enter name">
                        </div>


                        <div class="form-group">
                              <label for="email">Email :</label>
                              <input type="email" class="form-control" id="idAddEmail" name="idAddEmail" placeholder="Enter email">
                        </div>

                        <div class="form-group">
                              <label for="userid">User ID :</label>
                              <input type="text" class="form-control" id="idAddUsername" name="idAddUsername" placeholder="Enter id">
                        </div>

                        <div class="form-group">
                              <label for="password">Password :</label>
                              <input type="password" class="form-control" id="idAddPassword" name="idAddPassword" placeholder="Enter Password">
                        </div>

                        <div class="form-group">
                            <label for="repassword">Retype Password :</label>
                            <input type="password" class="form-control" id="idAddRePassword" name="idAddRePassword" placeholder="Re-enter Password">
                        </div>
                    </form>
                    <input type=hidden name="idAddUserId" id="idAddUserId">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-dark text-white" name="idRegsiter" id="idRegister" data-dismiss="modal">Register</button>
                    <button type="button" class="btn btn-dark text-white" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

<?php //include_once 'medixcelFooterB3.php'; ?>

<script type="text/javascript">

    //Script to register new user
    function register()
    {
        var sName=$('#idAddName').val();
        var sEmail=$('#idAddEmail').val();
        var sUsername=$('#idAddUsername').val();
        var sPassword=$('#idAddPassword').val();
        $.ajax({
            url:"ajaxFile.php?sFlag=AddUser",
            method:"post",
            data: {
                sName:sName,
                sEmail:sEmail,
                sUsername:sUsername,
                sPassword:sPassword
            },
            success:function(data){
                $('#idAddModal').modal('hide');
            },
        });
    }

    //Script to clear modal content
    $("#idAddModal").on('hidden.bs.modal', function () {
        $(this).find("input").val('');
    });

    //Script to check whether password matches the retype password on the registration form
    $(document).ready(function(){
        $('#idRegister').on("click", function(event){
            var sPassword=$('#idAddPassword').val();
            var sRePassword=$('#idAddRePassword').val();
            if(sPassword==sRePassword)
            {
                register();
            }
            else
            {
                alert("Password and Retype password doesn't match");
            }
        });
    });

</script>
</body>
</html>