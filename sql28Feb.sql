SELECT e.sample_label, a.name, c.service_name, f.test_completed_successfully_percentage, f.test_completed_successfully_amount, f.test_rejected_percentage, f.test_rejected_amount, f.test_cancelled_percentage, f.test_cancelled_amount
FROM cph_hcp a

LEFT JOIN cph_hcp_services b
ON a.hcp_id = b.hcp_id AND b.status=1

LEFT JOIN mxcel_services c
ON b.service_id = c.service_id AND c.status=1

LEFT JOIN cph_pathology_test_sample_mapping d
ON d.service_id = c.service_id AND d.status=1

LEFT JOIN cph_pathology_test_sample_master e
ON e.sample_id = d.sample_id AND e.status =1

LEFT JOIN mxcel_combi_billing_master f
ON b.service_id = f.service_id AND deleted=0

WHERE a.status = 1

ORDER BY b.hcp_id, b.service_id